package com.tuclinicavirtual.mvo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;


public class Preferences {

    private static SharedPreferences mSharedPref;
    private static String TAG_PREFERENCE="MyPreferences_animalitos";
    private static String TAG_TOKEN_USER="Token_User";
    private static String TAG_PLAYER_ID_TOKEN="player_id_token";
    private static String TAG_ID_USER="ID_USER";
    private static String TAG_ID_PATIENTID="ID_PATIENTID";
    private static String TAG_CLAVE_LOGIN="CLAVE_LOGIN";
    private static String TAG_USUARIO_LOGIN="USUARIO_LOGIN";
    private static String TAG_USUARIO_FOTO="USUARIO_FOTO";
    private static String TAG_CHECK_RECORDAR="CHECK_RECORDAR";
    private static String TAG_NOMBRE="NOMBRE_USUARIO";
    private static String TAG_PAISUSUARIO="PAIS_USUARIO";
    private static String TAG_UTC="USUARIO_UTC";
    private static String TAG_ID_SESION="ID_SESION";
    private static String TAG_TOKEN_OPENTOK="TOKEN_OPENTOK";
    private static String TAG_EMAIL="EMAIL";
    private static String TAG_UTC_PACIENTE_CREAR_CONS="TAG_UTC_PACIENTE_CREAR_CONS";


    public static String getTagPlayerIdToken() {
        return mSharedPref.getString(TAG_PLAYER_ID_TOKEN, "");
    }

    public static void setTagPlayerIdToken(String tagPlayerIdToken) {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_PLAYER_ID_TOKEN, tagPlayerIdToken);
        editor.commit();
    }

    public static String getEmailUser()    {
        return mSharedPref.getString(TAG_EMAIL, ""); //0 is the default value
    }

    public static void setEmailUser(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_EMAIL, value);
        editor.commit();
    }

    public static String getUtcPacienteCrearCons()    {
        return mSharedPref.getString(TAG_UTC_PACIENTE_CREAR_CONS, ""); //0 is the default value
    }

    public static void setTagUtcPacienteCrearCons(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_UTC_PACIENTE_CREAR_CONS, value);
        editor.commit();
    }


    public static String getId_sesion()    {
        return mSharedPref.getString(TAG_ID_SESION, ""); //0 is the default value
    }

    public static void setId_sesion(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_ID_SESION, value);
        editor.commit();
    }

    public static String getToken_opentok()    {
        return mSharedPref.getString(TAG_TOKEN_OPENTOK, ""); //0 is the default value
    }

    public static void setToken_opentok(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_TOKEN_OPENTOK, value);
        editor.commit();
    }

    public static String getUTC_usuario()    {
        return mSharedPref.getString(TAG_UTC, ""); //0 is the default value
    }

    public static void setUTC_usuario(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_UTC, value);
        editor.commit();
    }

    public static String getNombre()    {
        return mSharedPref.getString(TAG_NOMBRE, ""); //0 is the default value
    }

    public static void setNombre(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_NOMBRE, value);
        editor.commit();
    }

    public static String getPais()    {
        return mSharedPref.getString(TAG_PAISUSUARIO, ""); //0 is the default value
    }

    public static void setPais(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_PAISUSUARIO, value);
        editor.commit();
    }



    public static void setFotoUsuario(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_USUARIO_FOTO, value);
        editor.commit();
    }

    public static String getFotoUsuario()    {
        return mSharedPref.getString(TAG_USUARIO_FOTO, ""); //0 is the default value
    }


    public static void init(Context context)
    {
        if(mSharedPref == null){
            mSharedPref = context.getSharedPreferences(TAG_PREFERENCE, Activity.MODE_PRIVATE);
        }
    }

    public static void setTokenUser(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_TOKEN_USER, value);
        editor.commit();
    }

    public static String getTokenUser()    {
        return mSharedPref.getString(TAG_TOKEN_USER, ""); //0 is the default value
    }


    public static void setIDUser(int value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putInt(TAG_ID_USER, value);
        editor.commit();
    }

    public static int getIDPatientid()    {
        return mSharedPref.getInt(TAG_ID_PATIENTID, 0); //0 is the default value
    }

    public static void setIDPatientid(int value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putInt(TAG_ID_PATIENTID, value);
        editor.commit();
    }

    public static int getIDUser()    {
        return mSharedPref.getInt(TAG_ID_USER, 0); //0 is the default value
    }


    public static void setUusuarioLogin(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_USUARIO_LOGIN, value);
        editor.commit();
    }

    public static String getUsuarioLogin()    {
        return mSharedPref.getString(TAG_USUARIO_LOGIN, ""); //0 is the default value
    }

    public static void setClaveLogin(String value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(TAG_CLAVE_LOGIN, value);
        editor.commit();
    }

    public static String getClaveLogin()    {
        return mSharedPref.getString(TAG_CLAVE_LOGIN, ""); //0 is the default value
    }

    public static void setCheckRecordar(boolean value){
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putBoolean(TAG_CHECK_RECORDAR, value);
        editor.apply();
    }

    public static boolean getCheckRecordar()    {
        return mSharedPref.getBoolean(TAG_CHECK_RECORDAR,false); //0 is the default value
    }

    public static void cleanPreferences()
    {
        mSharedPref.edit().clear();
    }

}

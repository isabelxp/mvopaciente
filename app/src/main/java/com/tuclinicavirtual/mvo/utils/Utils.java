package com.tuclinicavirtual.mvo.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasEscritas;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasProgramada;
import com.tuclinicavirtual.mvo.menu.consultas.interfaces.ConsultationInterface;
import com.tuclinicavirtual.mvo.menu.consultas.models.ScoreRequest;
import com.tuclinicavirtual.mvo.menu.consultas.models.ScoreResponse;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens.SolicitarConsultaFragment;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;
import com.tuclinicavirtual.mvo.views.activitis.LoginActivity;
import com.tuclinicavirtual.mvo.views.activitis.SplashActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Utils {

    public static void setRate(final Context context) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dialogo_rating, null);
        final RatingBar ratingBar=(RatingBar) view.findViewById(R.id.ratingBar);

        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                ConsultationInterface rest;
                ScoreRequest request= new ScoreRequest(Preferences.getUTC_usuario(),AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationid(),Preferences.getIDUser(),ratingBar.getNumStars());

                rest = RetrofitClient.getClient(Consts.BASE_URL).create(ConsultationInterface.class);

                rest.setScore(request,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ScoreResponse>() {
                    @Override
                    public void onResponse(Call<ScoreResponse> call, Response<ScoreResponse> response) {
                        if(response.isSuccessful()) {
                            if (response.body() != null) {

                                Log.i("setRate", "--->getScore " + response.body().getMessage());
                                Log.i("setRate", "--->getScore " + response.body().getStatus());
                            }
                        }else{

                            Log.i("setRate", "--->Error en getPrescripcion " + response.code());
                            Log.i("setRate", "--->Error en getPrescripcion " + response.errorBody());
                            Log.i("setRate", "--->Error en getPrescripcion" + response.message());
                            Log.i("setRate", "--->Error en getPrescripcion " + call.request().toString());
                        }  }
                    @Override
                    public void onFailure(Call<ScoreResponse> call, Throwable t) {
                        Log.i("setRate","--->failed :( "+t.getMessage());
                    }
                });

                context.startActivity(new Intent(context,HomeActivity.class));
                dialog.cancel();
            }
        });


        builder.setView(view);
        builder.show();

    }
}

package com.tuclinicavirtual.mvo;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.tuclinicavirtual.mvo.firebase.MyFirebaseInstanceIDService;
import com.tuclinicavirtual.mvo.utils.Preferences;

import io.fabric.sdk.android.Fabric;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import static io.fabric.sdk.android.services.concurrency.AsyncTask.init;


public class MyApplication extends Application {

    private final String TAG = "APP";
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Log.i(TAG,"--->init() application");
        context = this;
        Preferences.init(this);
        //FirebaseMessaging.getInstance();


       // Log.e("FirebaseInstanceId", "1"+ FirebaseInstanceId.getInstance().getToken());

    }

    public static Context getContext() {
        return context;
    }



//    private class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
//        // This fires when a notification is opened by tapping on it.
//        @Override
//        public void notificationOpened(OSNotificationOpenResult result) {
//
//            OSNotificationAction.ActionType actionType = result.action.type;
//            JSONObject data = result.notification.payload.additionalData;
//            String launchUrl = result.notification.payload.launchURL; // update docs launchUrl
//            Log.e("OneSignalExample", "NotificationID received: ");
//            String customKey;
//            String openURL = null;
//            Preferences.setId_sesion(""+data);
//           // Object activityToLaunch = MainActivity.class;
//
//            if (data != null) {
//                customKey = data.optString("customkey", null);
//                openURL = data.optString("openURL", null);
//                Preferences.setId_sesion(customKey);
//                Preferences.setToken_opentok(""+data);
//                if (customKey != null)
//                    Log.e("OneSignalExample", "customkey set with value: " + customKey);
//
//                if (openURL != null)
//                    Log.e("OneSignalExample", "openURL to webview with URL value: " + openURL);
//            }
//
//            if (actionType == OSNotificationAction.ActionType.ActionTaken) {
//                Log.e("OneSignalExample", "Button pressed with id: " + result.action.actionID);
//
//                if (result.action.actionID.equals("id1")) {
//                    Log.e("OneSignalExample", "button id called: " + result.action.actionID);
//                    //activityToLaunch = GreenActivity.class;
//                } else
//                    Log.e("OneSignalExample", "button id called: " + result.action.actionID);
//            }
//            EventBus.getDefault().post("noti");
//        }
//    }

}

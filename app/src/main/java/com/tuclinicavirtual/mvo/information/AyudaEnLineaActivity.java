package com.tuclinicavirtual.mvo.information;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.views.HelpOnlineAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AyudaEnLineaActivity extends AppCompatActivity {
    private LinearLayoutManager layoutManager;
    @BindView(R.id.rv_onlinehelp)
    RecyclerView rv_onlinehelp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ayuda_en_linea);
        ButterKnife.bind(this);

        List<String> title = new ArrayList<>();


        title.add(getResources().getString(R.string.online_help));
        title.add(getResources().getString(R.string.solic));
        title.add(getResources().getString(R.string.see_details));
        title.add(getResources().getString(R.string.regis));
        title.add(getResources().getString(R.string.report));
        title.add(getResources().getString(R.string.prepare));
        title.add(getResources().getString(R.string.regi));
        title.add(getResources().getString(R.string.connect));
        title.add(getResources().getString(R.string.consult));

        List<String> description = new ArrayList<>();


        description.add(getResources().getString(R.string.solic_descrip));
        description.add(getResources().getString(R.string.see_details_descrip));
        description.add(getResources().getString(R.string.regis_descrip));
        description.add(getResources().getString(R.string.report_descrip));
        description.add(getResources().getString(R.string.prepare_descrip));
        description.add(getResources().getString(R.string.regi_descrip));
        description.add(getResources().getString(R.string.connect_descrip));
        description.add(getResources().getString(R.string.consult_descrip));

        List<Boolean> show = new ArrayList<>();
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);

        this.layoutManager = new LinearLayoutManager(this);
        this.rv_onlinehelp.setLayoutManager(this.layoutManager);
        HelpOnlineAdapter helpOnlineAdapter = new HelpOnlineAdapter(title, description, show);
        this.rv_onlinehelp.setAdapter(helpOnlineAdapter);



    }
}

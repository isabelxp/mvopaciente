package com.tuclinicavirtual.mvo.information;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.views.HelpOnlineAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class TermsOfUseFragment extends Fragment {


    public TermsOfUseFragment() {
        // Required empty public constructor
    }

    private LinearLayoutManager layoutManager;
    @BindView(R.id.rv_onlinehelp)
    RecyclerView rv_onlinehelp;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_terms_of_use, container, false);
        ButterKnife.bind(this, v);
        List<String> title = new ArrayList<>();
        title.add(getResources().getString(R.string.conditions));
        title.add(getResources().getString(R.string.register));
        title.add(getResources().getString(R.string.platform));
        title.add(getResources().getString(R.string.lower_age));
        title.add(getResources().getString(R.string.datos));
        title.add(getResources().getString(R.string.propety));
        title.add(getResources().getString(R.string.pay));
        title.add(getResources().getString(R.string.services));
        title.add(getResources().getString(R.string.act));



        List<String> description = new ArrayList<>();
        description.add(getResources().getString(R.string.conditions_descrip));
        description.add(getResources().getString(R.string.regiter_descrip));
        description.add(getResources().getString(R.string.platform_descrip));
        description.add(getResources().getString(R.string.lower_age_descrip));
        description.add(getResources().getString(R.string.datos_descrip));
        description.add(getResources().getString(R.string.propety_descrip));
        description.add(getResources().getString(R.string.pay_descrip));
        description.add(getResources().getString(R.string.services_descrip));
        description.add(getResources().getString(R.string.act_descrip));


        List<Boolean> show = new ArrayList<>();
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);
        show.add(false);

        this.layoutManager = new LinearLayoutManager(getContext());
        this.rv_onlinehelp.setLayoutManager(this.layoutManager);
        HelpOnlineAdapter helpOnlineAdapter = new HelpOnlineAdapter(title, description, show);
        this.rv_onlinehelp.setAdapter(helpOnlineAdapter);




        return v;
    }

}

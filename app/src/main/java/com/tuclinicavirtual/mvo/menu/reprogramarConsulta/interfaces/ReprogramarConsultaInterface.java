package com.tuclinicavirtual.mvo.menu.reprogramarConsulta.interfaces;

import com.tuclinicavirtual.mvo.menu.antecedentes.models.AntecedenteRequest;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.RespAntecedente;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos.ReprogramarRequest;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos.ReprogramarResponses;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.PUT;

public interface ReprogramarConsultaInterface {

    @PUT("consultations")
    Call<ReprogramarResponses> putConsultas(@Body ReprogramarRequest guardar, @Header("Authorization") String valor);
}

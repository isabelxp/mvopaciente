package com.tuclinicavirtual.mvo.menu.examenes.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasProgramada;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.ConsultasPragramadaFragment;
import com.tuclinicavirtual.mvo.menu.consultas.models.Consultation;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesPresentador;
import com.tuclinicavirtual.mvo.menu.examenes.adapters.ExamenesAdapter;
import com.tuclinicavirtual.mvo.menu.examenes.interfaces.deleteExam;
import com.tuclinicavirtual.mvo.menu.examenes.models.Exam;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamModels;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequest;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequestDelete;
import com.tuclinicavirtual.mvo.menu.examenes.models.ModeloExamenes;
import com.tuclinicavirtual.mvo.menu.examenes.mvp.ExamContrato;
import com.tuclinicavirtual.mvo.menu.examenes.mvp.ExamPresentador;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens.SolicitarConsultaFragment;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;
import com.tuclinicavirtual.mvo.views.activitis.fragmens.RegistroFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ru.whalemare.sheetmenu.SheetMenu;

import static android.app.Activity.RESULT_OK;

public class MisExamenesFragment extends Fragment implements deleteExam,ExamContrato.View,FamiliaresContrato.View{


    public MisExamenesFragment() {
    }
    public static String BASE64 = "data:image/png;base64,";
    private  final static int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    final int GALLERY_REQUEST = 22131;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.search2)
    EditText search;
    @BindView(R.id.spinner_antecedente)
    Spinner spinner_antecedente;

    List<String> listPaciente=new ArrayList<>();
    List<Integer> idPacientes=new ArrayList<>();
    List<Integer> idUser=new ArrayList<>();

    ExamenesAdapter adaptador;
    ArrayList<ModeloExamenes> listExamenes=new ArrayList<>();
    ExamPresentador presentador;
    FamiliaresPresentador presentadorFamiliares;
    private Uri uri;
    String exam;
    int id;
    int IDPatientid;
    int IDUsuarioid;
    List<ExamModels> exameneslist=new ArrayList<>();
    ExamRequest examRequest;
    String nameArchivo="";
    List<Exam> listExam=new ArrayList<>();
    ProgressDialog progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_mis_examenes, container, false);
        ButterKnife.bind(this, v);
        initUI();

        presentadorFamiliares.getFamiliares(Preferences.getIDUser());
        HomeActivity.setDetalleConsulta(false);
        presentador.getExam(Preferences.getIDPatientid());
        IDPatientid=Preferences.getIDPatientid();
        IDUsuarioid=Preferences.getIDUser();
        return v;
    }

    public void initUISpinner(final List<String> familiar)
    {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, familiar);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner_antecedente.setAdapter(dataAdapter);

        if(familiar!=null)
        {
            for(int i=0;i<familiar.size();i++)
            {
                if(familiar.get(i).contains("Titular"))
                {
                    spinner_antecedente.setSelection(i);
                }
            }

        }

        spinner_antecedente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                //progress.show();
                IDPatientid=idPacientes.get(pos);
                IDUsuarioid=idUser.get(pos);
                Log.d("exam","----->"+idUser.get(pos));
                presentador.getExam(idPacientes.get(pos));
               // adaptador.notifyDataSetChanged();
            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    public void dialogExam() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();


        View v = inflater.inflate(R.layout.item_dialog_exam, null);
        final EditText textExam=(EditText) v.findViewById(R.id.editText2);
        builder.setView(v);

        builder.setView(v)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if(textExam.length()>5)
                        {
                            nameArchivo=textExam.getText().toString();
                                    getRecurso();
                        }else
                        {
                            textExam.setError("Ingresa un nombre para el exámen");
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.create();

        AlertDialog dialog = builder.create();
        dialog.show();

    }




    @OnClick(R.id.floatingActionButton)
    public void floatingActionButton() {
        SheetMenu.with(getActivity())
                .setTitle(R.string.text_examenes)
                .setMenu(R.menu.menu_sheet)
                .setAutoCancel(true)
                .setClick(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if(item.getItemId()==R.id.id_eco)
                        {
                            id=4;
                            exam="Eco";
                            Log.d("MenuSheet","id_eco");
                        }
                        if(item.getItemId()==R.id.id_examenes_laboratorio)
                        {
                            id=1;
                            exam="Exámenes de laboratorio";
                            Log.d("MenuSheet","id_examenes_gramgrama");
                        }
                        if(item.getItemId()==R.id.id_examenes_gramgrama)
                        {
                            id=5;
                            exam="Grammagrama";
                            Log.d("MenuSheet","id_examenes_gramgrama");
                        }
                        if(item.getItemId()==R.id.id_informe_medico)
                        {
                            id=2;
                            exam="Informe médico";
                            Log.d("MenuSheet","id_informe_medico");
                        }
                        if(item.getItemId()==R.id.id_placa_radiologica)
                        {
                            id=3;
                            exam="Placa radiológica";
                            Log.d("MenuSheet","id_examenes_gramgrama");
                        }
                        if(item.getItemId()==R.id.id_otros_exam)
                        {
                            id=6;
                            exam="Otros exámenes";
                            Log.d("MenuSheet","id_otros_exam");
                        }
                        dialogExam();
                        return false;
                    }
                }).show();
    }


   public void getRecurso()
   {


       if(Build.VERSION.SDK_INT >= 23) {
           if (ContextCompat.checkSelfPermission(getActivity(),
                   android.Manifest.permission.READ_EXTERNAL_STORAGE)
                   != PackageManager.PERMISSION_GRANTED) {

               if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                       android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                   // Show an expanation to the user *asynchronously* -- don't block
                   // this thread waiting for the user's response! After the user
                   // sees the explanation, try again to request the permission.
                   Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
               } else {
                   // No explanation needed, we can request the permission.
                   requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                           MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
               }

           } else {
               Intent photoIntent = new Intent(Intent.ACTION_PICK);
               photoIntent.setType("image/*");
               startActivityForResult(photoIntent, GALLERY_REQUEST);
           }

           //ANDROID SDK <= 22
       } else {
           //add Photo from gallery
           Intent photoIntent = new Intent(Intent.ACTION_PICK);
           photoIntent.setType("image/*");
           startActivityForResult(photoIntent, GALLERY_REQUEST);
       }
   }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE : {
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
                }
                else {
                  //  Toast.makeText(getActivity(), "Denegado"), Toast.LENGTH_SHORT).show();
                }
            }
            /* other permissions */
        }
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK)
        {

            if( requestCode == GALLERY_REQUEST) {

                String filePath = null;
                Uri _uri = data.getData();

                Log.d("selectedImageUri",""+ _uri);
                if (_uri != null && "content".equals(_uri.getScheme())) {
                    Cursor cursor = getActivity().getContentResolver().query(_uri, new String[] { android.provider.MediaStore.Images.ImageColumns.DATA }, null, null, null);
                    cursor.moveToFirst();
                    filePath = cursor.getString(0);
                    cursor.close();
                    Log.d("selectedImageUri","33"+ filePath );
                } else {
                    filePath = _uri.getPath();
                    Log.d("selectedImageUri","66"+ filePath );
                }
               if(filePath!=null)
               {

                   File file = new File(filePath);
                   try {
                       FileInputStream fileInputStream = new FileInputStream(file);
                       Log.d("selectedImageUri",""+ fileInputStream);
                   } catch (FileNotFoundException e) {
                       e.printStackTrace();
                   }
                   //nameArchivo=file.getName();
                   RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
                   progress.show();
                   presentador.uploadExam(Preferences.getIDUser(),Preferences.getIDPatientid(),fbody,file.getName());
               }else
               {
                   Toast.makeText(getActivity(),"Error en archivo seleccionado!", Toast.LENGTH_SHORT).show();
               }
            }

        }
    }


    public void initUI()
    {
        progress = new ProgressDialog(getActivity());
        progress.setTitle(getActivity().getString(R.string.registro_cargando));
        progress.setCancelable(false);
        presentador=new ExamPresentador(this);
        presentadorFamiliares=new FamiliaresPresentador(this);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(reci);
        recycler.setHasFixedSize(true);
        addTextListener(listExam);
    }

    public void addTextListener(final List<Exam> dato){

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();

                final List<Exam> datoAuxi = new ArrayList<>();

                for (int i = 0; i < dato.size(); i++) {

                    final String text = dato.get(i).getName().toLowerCase();
                    if (text.contains(query)) {
                        datoAuxi.add(dato.get(i));
                    }
                }

                adaptador= new ExamenesAdapter(datoAuxi,getActivity(),MisExamenesFragment.this);
                recycler.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void getExamenes(List<Exam> dato) {

        listExam.clear();
        listExam=dato;
        adaptador= new ExamenesAdapter(dato,getActivity(),MisExamenesFragment.this);
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
        addTextListener(listExam);
    }

    @Override
    public void getExamRespFalse() {
        listExam.clear();
        adaptador= new ExamenesAdapter(listExam,getActivity(),MisExamenesFragment.this);
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void getDeleteResp(String dato) {

    }

    @Override
    public void setExam(String dato) {
        presentador.getExam(IDPatientid);

    }

    @Override
    public void setUpload(String dato) {

        progress.dismiss();
        exameneslist.clear();
        exameneslist.add(new ExamModels(Preferences.getUTC_usuario(),nameArchivo,dato.toString(),id,id));
        examRequest=new ExamRequest(IDPatientid,IDUsuarioid,exameneslist);
        presentador.setExam(examRequest);
    }


    @Override
    public void deleteExam() {
        presentador.getExam(IDPatientid);
    }

    @Override
    public void getListFamiliares(List<Patient> familiar) {

        for (int i=0;i<familiar.size();i++)
        {

            listPaciente.add(familiar.get(i).getName()+" "+familiar.get(i).getLastname() +" - "+familiar.get(i).getRelationshipName());
            idPacientes.add(familiar.get(i).getPatientid());
            idUser.add(familiar.get(i).getUserid());

        }

        initUISpinner(listPaciente);

    }

    @Override
    public void setFamiliainfo(String rest) {

    }

    @Override
    public void setDeleteFamiliar(String rest) {

    }
}

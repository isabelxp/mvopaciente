package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScoreRequest {
    @SerializedName("utc")
    @Expose
    private String utc;
    @SerializedName("consultationid")
    @Expose
    private Integer consultationid;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("scoreid")
    @Expose
    private Integer scoreid;

    public String getUtc() {
        return utc;
    }

    public ScoreRequest(String utc, Integer consultationid, Integer userid, Integer scoreid) {
        this.utc = utc;
        this.consultationid = consultationid;
        this.userid = userid;
        this.scoreid = scoreid;
    }

    public void setUtc(String utc) {
        this.utc = utc;
    }

    public Integer getConsultationid() {
        return consultationid;
    }

    public void setConsultationid(Integer consultationid) {
        this.consultationid = consultationid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getScoreid() {
        return scoreid;
    }

    public void setScoreid(Integer scoreid) {
        this.scoreid = scoreid;
    }


}

package com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter.ListDoctoresAdapter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter.ListEspecialidadAdapter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultaPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultasContrato;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MedicoFragment extends Fragment implements SolicitarConsultasContrato.View{


    public MedicoFragment() { }

    SolicitarConsultaPresentador presentador;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    ListDoctoresAdapter adaptador;
    ProgressDialog progress;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_medico, container, false);
        ButterKnife.bind(this, v);
        initUI();
        return v;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String num) {

        Log.d("pasarSiguiente","Medico"+num);

        if(num.equals("1"))
        {
            //progress.show();
            presentador.getDoctorList(SolicitarConsultaFragment.getNumEspecialidad());
            Log.d("Medico","----->"+SolicitarConsultaFragment.getNumEspecialidad());
        }

    };

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }



    @Override
    public void getListEspecialidad(List<Especialidad> especialistas) { }

    @Override
    public void getListDoctor(List<Doctor> doctor) {
        progress.cancel();
        adaptador= new ListDoctoresAdapter(doctor,getContext());
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

    @Override
    public void getHorarioDoctor(List<Availabilty> horarioDoctor) {

    }

    @Override
    public void getSolicitarConsulta(DataSolicitudConsulta code) {

    }

    @Override
    public void getPaymentsResp(DataPayment dataPayment) {

    }

    @Override
    public void getRespDoctorFail(String text) {

    }

    @Override
    public void getShoulderDoctor(List<Schedule> hours) {

    }

    @Override
    public void getError(String dato) {

    }

    public void initUI()
    {

        progress = new ProgressDialog(getActivity());
        presentador=new SolicitarConsultaPresentador(this);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(reci);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler.setHasFixedSize(true);
    }

}

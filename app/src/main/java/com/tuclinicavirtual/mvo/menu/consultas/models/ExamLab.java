
package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExamLab {

    @SerializedName("consultationid")
    @Expose
    private Integer consultationid;
    @SerializedName("examid")
    @Expose
    private Integer examid;
    @SerializedName("exam")
    @Expose
    private String exam;
    @SerializedName("exam_category")
    @Expose
    private String examCategory;
    @SerializedName("exam_type")
    @Expose
    private String examType;

    @Override
    public String toString() {
        return "ExamLab{" +
                "consultationid=" + consultationid +
                ", examid=" + examid +
                ", exam='" + exam + '\'' +
                ", examCategory='" + examCategory + '\'' +
                ", examType='" + examType + '\'' +
                '}';
    }

    public Integer getConsultationid() {
        return consultationid;
    }

    public void setConsultationid(Integer consultationid) {
        this.consultationid = consultationid;
    }

    public Integer getExamid() {
        return examid;
    }

    public void setExamid(Integer examid) {
        this.examid = examid;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getExamCategory() {
        return examCategory;
    }

    public void setExamCategory(String examCategory) {
        this.examCategory = examCategory;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

}

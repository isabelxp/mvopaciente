package com.tuclinicavirtual.mvo.menu.notificaciones.interfaces;

import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPacientes;
import com.tuclinicavirtual.mvo.menu.notificaciones.models.NotificacionesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface NotificacionesInterfaces {
    @GET("notification/'UTC -03:00'/{id}")
    Call<NotificacionesResponse> getNotificaciones(@Path("id") int id, @Header("Authorization") String valor);
}

package com.tuclinicavirtual.mvo.menu.examenes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExamModels {
    @SerializedName("utc")
    @Expose
    private String utc;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("URL")
    @Expose
    private String URL;
    @SerializedName("typeid")
    @Expose
    private Integer typeid;
    @SerializedName("typename")
    @Expose
    private Integer typename;

    public ExamModels(String utc, String name, String URL, Integer typeid, Integer typename) {
        this.utc = utc;
        this.name = name;
        this.URL = URL;
        this.typeid = typeid;
        this.typename = typename;
    }

    @Override
    public String toString() {
        return "ExamModels{" +
                "utc='" + utc + '\'' +
                ", name='" + name + '\'' +
                ", URL='" + URL + '\'' +
                ", typeid=" + typeid +
                ", typename=" + typename +
                '}';
    }

    public String getUtc() {
        return utc;
    }

    public void setUtc(String utc) {
        this.utc = utc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String uRL) {
        this.URL = uRL;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Integer getTypename() {
        return typename;
    }

    public void setTypename(Integer typename) {
        this.typename = typename;
    }
}

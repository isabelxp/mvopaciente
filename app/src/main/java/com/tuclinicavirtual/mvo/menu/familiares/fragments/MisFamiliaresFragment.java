package com.tuclinicavirtual.mvo.menu.familiares.fragments;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.antecedentes.MisAntecedentesFragment;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionList;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesPresentador;
import com.tuclinicavirtual.mvo.menu.examenes.fragments.MisExamenesFragment;
import com.tuclinicavirtual.mvo.menu.familiares.adapters.FamiliaresAdapter;
import com.tuclinicavirtual.mvo.menu.familiares.models.DeleteRequestFamilia;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliasModelo;
import com.tuclinicavirtual.mvo.menu.familiares.models.Family;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresPresentador;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MisFamiliaresFragment extends Fragment implements FamiliaresContrato.View{


    public MisFamiliaresFragment() {}

    FamiliaresPresentador presenter;

    @BindView(R.id.recycler)
    RecyclerView recycler;
    FamiliaresAdapter adaptador;
    List<Patient> listFamiliares=new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_mis_familiares, container, false);
        ButterKnife.bind(this, v);

        initUI();
        Log.d("getIdUser","----->"+Preferences.getIDUser());

        presenter.getFamiliares(Preferences.getIDUser());
        HomeActivity.setDetalleConsulta(false);

        return v;
    }
    public void initUI()
    {
        presenter=new FamiliaresPresentador(this);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(reci);
        recycler.setHasFixedSize(true);
    }





    @Override
    public void getListFamiliares(List<Patient> datos) {
        Log.d("getListFamiliares","--->"+datos.toString());
        listFamiliares.clear();
        for (int i=0;i<datos.size();i++)
        {
            if(!datos.get(i).getRelationshipName().equals("Titular"))
            {
                listFamiliares.add(datos.get(i));
            }
        }
        adaptador= new FamiliaresAdapter(listFamiliares,getActivity());
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
        if (datos.size()==1)
        {
            dialog("No tienes familiares agregados");
        }
    }

    @Override
    public void setFamiliainfo(String rest) {

    }

    @Override
    public void setDeleteFamiliar(String rest) {
        Log.d("setDeleteFamiliar","--->"+rest);
        presenter.getFamiliares(Preferences.getIDUser());
    }

    public void dialog(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(resp);
        //builder.setCancelable(false);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}

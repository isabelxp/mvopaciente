package com.tuclinicavirtual.mvo.menu.antecedentes.mvp;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuclinicavirtual.mvo.menu.antecedentes.interfaces.AntecedenteInterface;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.AntecedenteRequest;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.RespAntecedente;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.ResponseAntecedentes;
import com.tuclinicavirtual.mvo.menu.examenes.interfaces.ExamInterfaces;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequest;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRespuestaPost;
import com.tuclinicavirtual.mvo.menu.familiares.interfaces.FamiliaresInterfaces;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaRequest;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaResponse;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.models.RegistroResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AntecedentePresentador implements AntecedenteContrato.Presenter{

    AntecedenteInterface rest;

    public AntecedenteContrato.View view;

    private String TAG;

    public AntecedentePresentador(AntecedenteContrato.View view) {
        this.view = view;
        TAG = this.getClass().getSimpleName();
    }


    @Override
    public void getAntecedente(int id) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(AntecedenteInterface.class);

        rest.getAntecedente(id,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ResponseAntecedentes>() {
            @Override
            public void onResponse(Call<ResponseAntecedentes> call, Response<ResponseAntecedentes> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i(TAG, "--->Antecedente: " + call.request().url());
                        Log.i(TAG, "--->Antecedente: " + response.body().getMessage());
                        Log.i(TAG, "--->Antecedente: " + response.body().getStatus());
                   //     Log.i(TAG, "--->Antecedente: " + response.body().getData().getAntecedent().get(0).toString());
                        view.getListAntecedente(response.body().getData().getAntecedent());
                    }
                }else{

                    Gson gson = new GsonBuilder().create();
                    ResponseAntecedentes pojo = new ResponseAntecedentes();
                    try {
                        pojo = gson.fromJson(response.errorBody().string(), ResponseAntecedentes.class);
                        view.setAntecedenteResp(pojo.getMessage());
                        Log.i(TAG, "--->Error Antecedente: 99 " + pojo.getMessage());
                    } catch (IOException e) { }

                    Log.i(TAG, "--->Error Antecedente: " + call.request().url());
                    Log.i(TAG, "--->Error Antecedente: " + response.code());
                    Log.i(TAG, "--->Error Antecedente: " + response.message());
                    } }
            @Override
            public void onFailure(Call<ResponseAntecedentes> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }


    public void setPutAntecedente(AntecedenteRequest guardar) {

        Log.i(TAG, "--->set antecedente: "+guardar.toString());

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(AntecedenteInterface.class);
        Log.i(TAG, "--->set antecedente: ");

        rest.setPutAntecedente(guardar,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<RespAntecedente>() {
            @Override
            public void onResponse(Call<RespAntecedente> call, Response<RespAntecedente> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setAntecedenteResp(response.body().getStatus());
                        Log.i(TAG, "--->set antecedente: " + response.body().getMessage());
                        Log.i(TAG, "--->set antecedente: " + response.code());
                        Log.i(TAG, "--->set antecedente: " + response.body().getStatus());
                        Log.i(TAG, "--->set antecedente: " + response.body());
                    }
                }else{

                    Gson gson = new GsonBuilder().create();
                    ResponseAntecedentes pojo = new ResponseAntecedentes();
                    try {
                        pojo = gson.fromJson(response.errorBody().string(), ResponseAntecedentes.class);
                        view.setAntecedenteResp(pojo.getMessage());
                        Log.i(TAG, "--->Error Antecedente: 99 " + pojo.getMessage());
                    } catch (IOException e) { }

                    Log.i(TAG, "--->set Error en antecedente: " + call.request().url());
                    Log.i(TAG, "--->set Error en antecedente: " + response.code());
                    Log.i(TAG, "--->set Error en antecedente: " + response.message());
                    Log.i(TAG, "--->set Error en antecedente: " + response.headers());
                }  }
            @Override
            public void onFailure(Call<RespAntecedente> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

}

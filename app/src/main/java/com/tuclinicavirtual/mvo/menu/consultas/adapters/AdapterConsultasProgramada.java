package com.tuclinicavirtual.mvo.menu.consultas.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.antecedentes.MisAntecedentesFragment;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.ConsultasPragramadaFragment;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.DetalleConsultaFragment;
import com.tuclinicavirtual.mvo.menu.consultas.interfaces.DetalleConsultaInterfaz;
import com.tuclinicavirtual.mvo.menu.consultas.models.Consultation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterConsultasProgramada extends RecyclerView.Adapter<AdapterConsultasProgramada.ViewHolder>{

    public static List<Consultation> listConsultaAdapter=new ArrayList<>();
    public static int numeroPosicion22 =0;
    Context context;
    DetalleConsultaInterfaz interfaz;

    public static Consultation detalleConsultaProgramada;

    public static Consultation getDetalleConsultaProgramada() {
        return detalleConsultaProgramada;
    }

    public static void setDetalleConsultaProgramada(Consultation detalleConsultaProgramada) {
        AdapterConsultasProgramada.detalleConsultaProgramada = detalleConsultaProgramada;
    }

    public static int getNumeroPosicion22() {
        return numeroPosicion22;
    }

    public static void setNumeroPosicion22(int numeroPosicion22) {
        AdapterConsultasProgramada.numeroPosicion22 = numeroPosicion22;
    }

    public AdapterConsultasProgramada(List<Consultation> listConsulta, Context context,ConsultasPragramadaFragment fragment) {
        this.listConsultaAdapter=listConsulta;
        this.context=context;
        interfaz=(DetalleConsultaInterfaz)fragment;
    }

    @Override
    public AdapterConsultasProgramada.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_consultas_escritas, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdapterConsultasProgramada.ViewHolder holder, final int position) {


            Log.d("ConsultasProgramada",""+listConsultaAdapter.get(position).getIsRated());
            holder.especialidad.setText(listConsultaAdapter.get(position).getSpecialityname());
            holder.nombre_medico.setText(listConsultaAdapter.get(position).getDoctorName());

            holder.text_fecha.setText(parseDateToddMMyyyy(listConsultaAdapter.get(position).getConsultationDate()));
            holder.bindData(listConsultaAdapter.get(position),context);
            holder.text_hora.setText(hourFormatter(listConsultaAdapter.get(position).getConsultationHour()));
            holder.text_status.setText(listConsultaAdapter.get(position).getStatusname());

            holder.img_consulta.setBackgroundResource(R.drawable.circule);

            if (listConsultaAdapter.get(position).getTypeid()==2)
            {

                holder.img_consulta.setImageResource(R.drawable.ic_chat_bubble_black_24dp);

            }
            if (listConsultaAdapter.get(position).getTypeid()==3)
            {

                holder.img_consulta.setImageResource(R.drawable.ic_local_phone_black_24dp);
            }
            if (listConsultaAdapter.get(position).getTypeid()==4)
            {
                holder.img_consulta.setImageResource(R.drawable.ic_videocam_white_24dp);
            }
            if (listConsultaAdapter.get(position).getTypeid()==1)
            {
                holder.img_consulta.setImageResource(R.drawable.ic_create_black_24dp);
            }

        if(listConsultaAdapter.get(position).getStatusid()==7)
        {
            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
        }


//        if(listConsultaAdapter.get(position).getStatusid()==9)
//        {
//            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
//            holder.layout_text_status.setBackgroundResource(R.drawable.status_finalizada_con_diagnostico);
//        }
//        if(listConsultaAdapter.get(position).getStatusid()==3)
//        {
//            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
//            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
//        }
        if(listConsultaAdapter.get(position).getStatusid()==5)
        {
            // incompleta
            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
        }
        if(listConsultaAdapter.get(position).getStatusid()==1)
        {
            // incompleta
            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
        }
//        if(listConsultaAdapter.get(position).getStatusid()==8)
//        {
//            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
//            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
//        }
//        if(listConsultaAdapter.get(position).getStatusid()==6)
//        {
//            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
//            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
//        }
//        if(listConsultaAdapter.get(position).getStatusid()==4)
//        {
//            // medico coonectado
//            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
//            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
//        }
//        if(listConsultaAdapter.get(position).getStatusid()==10)
//        {
//            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
//            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_reprogramar);
//        }
//        if(listConsultaAdapter.get(position).getStatusid()==11)
//        {
//            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
//            holder.layout_text_status.setBackgroundResource(R.drawable.status_reprogramada);
//        }
//        if(listConsultaAdapter.get(position).getStatusid()==12)
//        {
//            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
//            holder.layout_text_status.setBackgroundResource(R.drawable.status_reprogramada);
//        }
        if(listConsultaAdapter.get(position).getStatusid()==6)
        {
            holder.text_status.setTextColor(Color.parseColor("#000000"));
            holder.layout_text_status.setBackgroundResource(R.color.white);
        }
        holder.layoutConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumeroPosicion22(position);
                //initFragments(context);
                setDetalleConsultaProgramada(listConsultaAdapter.get(position));
                interfaz.cambio();
                Log.d("getStatusid","--->"+listConsultaAdapter.get(position).getStatusid());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listConsultaAdapter.size();
    }


    public static String hourFormatter(String startTime){
        //String startTime = "2013-02-27 21:06:30";
        String formatedDate =null;

        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
        Date date = null;
        try {
            date = dateFormatter.parse(startTime);


            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
            String displayValue = timeFormatter.format(date);
            return displayValue;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formatedDate;
    }


    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    private static String deleteChar(String text, int charPos) {
        return new StringBuilder(text).deleteCharAt(charPos).toString();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        @BindView(R.id.layout_consulta)
        LinearLayout layoutConsulta;

        @BindView(R.id.nombre_medico)
        TextView nombre_medico;

        @BindView(R.id.especialidad)
        TextView especialidad;

        @BindView(R.id.text_fecha)
        TextView text_fecha;

        @BindView(R.id.text_hora)
        TextView text_hora;

        @BindView(R.id.img_consulta)
        ImageView img_consulta;

        @BindView(R.id.text_status)
        TextView text_status;

        @BindView(R.id.layout_text_status)
        LinearLayout layout_text_status;

        Context context;
        public ViewHolder(View v) {
            super(v);
            view = v;
            ButterKnife.bind(this, v);
        }

        public void bindData(Consultation c,Context context) {
         this.context=context;
        }



    }



}

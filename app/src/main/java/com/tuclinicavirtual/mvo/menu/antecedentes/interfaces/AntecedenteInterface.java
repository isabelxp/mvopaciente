package com.tuclinicavirtual.mvo.menu.antecedentes.interfaces;

import com.tuclinicavirtual.mvo.menu.antecedentes.models.AntecedenteRequest;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.RespAntecedente;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.ResponseAntecedentes;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPacientes;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.GuardarUsuarioRequest;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.GuardarUsuarioResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AntecedenteInterface {

//    @Headers({"Content-Type:application/json",
//              "Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjc3LCJpYXQiOjE1MjU0NDY2NzgsImV4cCI6MTUyNjY1NjI3OCwic3RhdHVzIjpmYWxzZX0.VrQKF7WzMiWkDedtMydhZO94j52o0q3-XTUcH3eSsaI"})
    @PUT("patients/antecedents")
    Call<RespAntecedente> setPutAntecedente(@Body AntecedenteRequest guardar,@Header("Authorization") String valor);

    //@Headers("Authorization:bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI4MCIsImlhdCI6MTUyMzU4MjUzOCwiZXhwIjoxNTI0NzkyMTM4LCJzdGF0dXMiOmZhbHNlfQ.Mmo4W44mBmxWMLXL_xONiYQhvlBW6Z5jzwYcpahfVxc")
    @GET("patients/antecedents/{id}")
    Call<ResponseAntecedentes> getAntecedente(@Path("id") int id,@Header("Authorization") String valor);

}

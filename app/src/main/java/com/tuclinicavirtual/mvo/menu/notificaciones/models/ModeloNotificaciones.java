package com.tuclinicavirtual.mvo.menu.notificaciones.models;

public class ModeloNotificaciones {
String asunto;
String estado;
String confirmacion;
String fecha;
String hora;

    public ModeloNotificaciones(String asunto, String estado, String confirmacion, String fecha, String hora) {
        this.asunto = asunto;
        this.estado = estado;
        this.confirmacion = confirmacion;
        this.fecha = fecha;
        this.hora = hora;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getConfirmacion() {
        return confirmacion;
    }

    public void setConfirmacion(String confirmacion) {
        this.confirmacion = confirmacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}

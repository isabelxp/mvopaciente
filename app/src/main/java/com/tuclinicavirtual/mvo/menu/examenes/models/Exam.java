package com.tuclinicavirtual.mvo.menu.examenes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Exam {

    @SerializedName("examid")
    @Expose
    private Integer examid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("URL")
    @Expose
    private String uRL;
    @SerializedName("typeid")
    @Expose
    private Integer typeid;
    @SerializedName("typename")
    @Expose
    private String typename;
    @SerializedName("patientid")
    @Expose
    private Integer patientid;

    public Exam(Integer examid, String name, String uRL, Integer typeid, String typename, Integer patientid) {
        this.examid = examid;
        this.name = name;
        this.uRL = uRL;
        this.typeid = typeid;
        this.typename = typename;
        this.patientid = patientid;
    }

    public Integer getExamid() {
        return examid;
    }

    public void setExamid(Integer examid) {
        this.examid = examid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getURL() {
        return uRL;
    }

    public void setURL(String uRL) {
        this.uRL = uRL;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }
}

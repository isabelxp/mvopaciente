package com.tuclinicavirtual.mvo.menu.datosPersonales.modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuardarUsuarioRequest {

    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("patienid")
    @Expose
    private Integer patienid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("identification_type")
    @Expose
    private String identificationType;
    @SerializedName("identification_number")
    @Expose
    private String identificationNumber;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("occupationid")
    @Expose
    private Integer occupationid;
    @SerializedName("countryid")
    @Expose
    private Integer countryid;
    @SerializedName("res_countryid")
    @Expose
    private Integer resCountryid;
    @SerializedName("timezoneid")
    @Expose
    private Integer timezoneid;
    @SerializedName("relationshipid")
    @Expose
    private Integer relationshipid;

    public GuardarUsuarioRequest(Integer userid, Integer patienid, String name, String lastname, String sex, String identificationType, String identificationNumber, String birthdate, Integer occupationid, Integer countryid, Integer resCountryid, Integer timezoneid, Integer relationshipid) {
        this.userid = userid;
        this.patienid = patienid;
        this.name = name;
        this.lastname = lastname;
        this.sex = sex;
        this.identificationType = identificationType;
        this.identificationNumber = identificationNumber;
        this.birthdate = birthdate;
        this.occupationid = occupationid;
        this.countryid = countryid;
        this.resCountryid = resCountryid;
        this.timezoneid = timezoneid;
        this.relationshipid = relationshipid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getPatienid() {
        return patienid;
    }

    public void setPatienid(Integer patienid) {
        this.patienid = patienid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Integer getOccupationid() {
        return occupationid;
    }

    public void setOccupationid(Integer occupationid) {
        this.occupationid = occupationid;
    }

    public Integer getCountryid() {
        return countryid;
    }

    public void setCountryid(Integer countryid) {
        this.countryid = countryid;
    }

    public Integer getResCountryid() {
        return resCountryid;
    }

    public void setResCountryid(Integer resCountryid) {
        this.resCountryid = resCountryid;
    }

    public Integer getTimezoneid() {
        return timezoneid;
    }

    public void setTimezoneid(Integer timezoneid) {
        this.timezoneid = timezoneid;
    }

    public Integer getRelationshipid() {
        return relationshipid;
    }

    public void setRelationshipid(Integer relationshipid) {
        this.relationshipid = relationshipid;
    }

}

package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Diagnostic {
    @SerializedName("diagnosticid")
    @Expose
    private Integer diagnosticid;
    @SerializedName("consultationid")
    @Expose
    private Integer consultationid;
    @SerializedName("diseaseid")
    @Expose
    private Integer diseaseid;
    @SerializedName("diseasename")
    @Expose
    private String diseasename;
    @SerializedName("diseasecode")
    @Expose
    private String diseasecode;
    @SerializedName("diseasetypeid")
    @Expose
    private Integer diseasetypeid;
    @SerializedName("diseasetype")
    @Expose
    private String diseasetype;
    @SerializedName("diseasetypecode")
    @Expose
    private String diseasetypecode;

    @Override
    public String toString() {
        return "Diagnostic{" +
                "diagnosticid=" + diagnosticid +
                ", consultationid=" + consultationid +
                ", diseaseid=" + diseaseid +
                ", diseasename='" + diseasename + '\'' +
                ", diseasecode='" + diseasecode + '\'' +
                ", diseasetypeid=" + diseasetypeid +
                ", diseasetype='" + diseasetype + '\'' +
                ", diseasetypecode='" + diseasetypecode + '\'' +
                '}';
    }

    public Integer getDiagnosticid() {
        return diagnosticid;
    }

    public void setDiagnosticid(Integer diagnosticid) {
        this.diagnosticid = diagnosticid;
    }

    public Integer getConsultationid() {
        return consultationid;
    }

    public void setConsultationid(Integer consultationid) {
        this.consultationid = consultationid;
    }

    public Integer getDiseaseid() {
        return diseaseid;
    }

    public void setDiseaseid(Integer diseaseid) {
        this.diseaseid = diseaseid;
    }

    public String getDiseasename() {
        return diseasename;
    }

    public void setDiseasename(String diseasename) {
        this.diseasename = diseasename;
    }

    public String getDiseasecode() {
        return diseasecode;
    }

    public void setDiseasecode(String diseasecode) {
        this.diseasecode = diseasecode;
    }

    public Integer getDiseasetypeid() {
        return diseasetypeid;
    }

    public void setDiseasetypeid(Integer diseasetypeid) {
        this.diseasetypeid = diseasetypeid;
    }

    public String getDiseasetype() {
        return diseasetype;
    }

    public void setDiseasetype(String diseasetype) {
        this.diseasetype = diseasetype;
    }

    public String getDiseasetypecode() {
        return diseasetypecode;
    }

    public void setDiseasetypecode(String diseasetypecode) {
        this.diseasetypecode = diseasetypecode;
    }

}

package com.tuclinicavirtual.mvo.menu.antecedentes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataAntecedente {

    @SerializedName("antecedent")
    @Expose
    private List<Antecedentes2> antecedent = null;

    public List<Antecedentes2> getAntecedent() {
        return antecedent;
    }

    public void setAntecedent(List<Antecedentes2> antecedent) {
        this.antecedent = antecedent;
    }

}

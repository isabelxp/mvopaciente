package com.tuclinicavirtual.mvo.menu.datosPersonales.interfaces;

import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPacientes;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.GuardarUsuarioResponse;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionResponse;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.GuardarUsuarioRequest;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.PasswordRequest;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.PasswordResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RestdatosPersonalesInterfaces {

    //@Headers("Authorization:bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3NyIsImlhdCI6MTUyNTA0MTY4MywiZXhwIjoxNTI2MjUxMjgzLCJzdGF0dXMiOmZhbHNlfQ.L45ntmmmVSvCakOQmuFFMaUk1KWSOOMFeR-JM_vagbo")
    @GET("patients/listvalues/2/country")
    Call<InformacionResponse> getPaises(@Header("Authorization") String valor);

    //@Headers("Authorization:bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3NyIsImlhdCI6MTUyNTA0MTY4MywiZXhwIjoxNTI2MjUxMjgzLCJzdGF0dXMiOmZhbHNlfQ.L45ntmmmVSvCakOQmuFFMaUk1KWSOOMFeR-JM_vagbo")
    @GET("patients/listvalues/2/occupation")
    Call<InformacionResponse> getOcupacion(@Header("Authorization") String valor);

    //@Headers("Authorization:bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3NyIsImlhdCI6MTUyNTA0MTY4MywiZXhwIjoxNTI2MjUxMjgzLCJzdGF0dXMiOmZhbHNlfQ.L45ntmmmVSvCakOQmuFFMaUk1KWSOOMFeR-JM_vagbo")
    @GET("patients/listvalues/2/relationship")
    Call<InformacionResponse> getRelacionFamiliar(@Header("Authorization") String valor);

    @Headers("Content-Type:application/json")
    @PUT("patients")
    Call<GuardarUsuarioResponse> setGuardarUsuario(@Body GuardarUsuarioRequest guardar,@Header("Authorization") String valor);

    @Headers("Content-Type:application/json")
    @GET("patients/{id}")
    Call<DataPacientes> getPaciente(@Path("id") int id, @Header("Authorization") String valor);

    @GET("patients/listvalues/2/timezone")
    Call<InformacionResponse> getZonaHoraria(@Header("Authorization") String valor);

    @Headers("Content-Type:application/json")
    @POST("users/password")
    Call<PasswordResponse> setNewPassword(@Body PasswordRequest password,@Header("Authorization") String valor);


}

package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataItemPrescripcion {

    @SerializedName("prescriptions")
    @Expose
    private List<PrescripcionItem> prescriptions = null;

    public List<PrescripcionItem> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<PrescripcionItem> prescriptions) {
        this.prescriptions = prescriptions;
    }
}

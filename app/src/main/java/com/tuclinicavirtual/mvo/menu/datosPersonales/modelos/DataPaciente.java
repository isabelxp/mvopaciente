package com.tuclinicavirtual.mvo.menu.datosPersonales.modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataPaciente {

        @SerializedName("patients")
        @Expose
        private List<Patient> patients = null;

        public List<Patient> getPatients() {
            return patients;
        }

        public void setPatients(List<Patient> patients) {
            this.patients = patients;
        }

}

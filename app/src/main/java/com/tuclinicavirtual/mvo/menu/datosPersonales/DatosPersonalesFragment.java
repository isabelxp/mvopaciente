package com.tuclinicavirtual.mvo.menu.datosPersonales;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.MisConsultasFragment;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPaciente;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionList;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesContrato;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens.SolicitarConsultaFragment;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DatosPersonalesFragment extends Fragment implements DatosPersonalesContrato.View {


    public DatosPersonalesFragment() { }

    DatosPersonalesPresentador presenter;

    List<InformacionList> listOcupacion = new ArrayList<InformacionList>();
    List<InformacionList> listPais = new ArrayList<InformacionList>();
    List<InformacionList> listZonaHorario = new ArrayList<InformacionList>();
    List<InformacionList> ZonaHorario = new ArrayList<InformacionList>();




    List<String> listPaisString = new ArrayList<String>();
    List<String> listOcupString = new ArrayList<String>();
    List<String> listZonaHorarioString = new ArrayList<String>();
    //id de los paises y ocupaciones

    List<Integer> listIdPais= new ArrayList<Integer>();
    List<Integer> listIdPaisreci= new ArrayList<Integer>();
    List<Integer> listIdOcupacion= new ArrayList<Integer>();
    List<Integer> listIdZonaHorario= new ArrayList<Integer>();
    String dateBirthday;

    @BindView(R.id.spinner_tipo_ident)
    Spinner spinner_tipo_ident;

    @BindView(R.id.spinner_genero)
    Spinner spinner_genero;

    @BindView(R.id.spinner_pais)
    Spinner spinner_pais;

    @BindView(R.id.spinner_pais_reci)
    Spinner spinner_pais_reci;

    @BindView(R.id.spinner_ocupacion)
    Spinner spinner_ocupacion;

    @BindView(R.id.spinner_tipo_nacionalidad)
    Spinner spinner_tipo_nacionalidad;

    @BindView(R.id.spinner_zona_horario)
    Spinner spinner_zona_horario;

    @BindView(R.id.edit_numero_ident)
    EditText edit_numero_ident;

    @BindView(R.id.edit_nombre)
    EditText edit_nombre;

    @BindView(R.id.edit_apellido)
    EditText edit_apellido;

    @BindView(R.id.edit_fecha)
    EditText edit_fecha;

    @BindView(R.id.edit_zona_horario)
    EditText edit_zona_horario;



    int num_ocupacion=1000;
    int num_pais=1000;
    int num_pais_reci=1000;
    int num_zona_horario=1000;

    int num_ocupacionAuxi=1000;
    int num_paisAuxi=1000;
    int num_pais_reciAuxi=1000;
    int num_zona_horarioAuxilia=0;
    boolean zoneHoraria=true;

    int resultado=0;

    String numero_identificacion;
    ProgressDialog progress;

    int years;
    int month;
    int day;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_datos_personales, container, false);
        ButterKnife.bind(this, v);

        iniUI();
        presenter.getPaciente(Preferences.getIDPatientid());
        HomeActivity.setDetalleConsulta(false);
        return v;
    }

    @OnClick(R.id.botton_guardar)
    public void guardarDatos() {
        progress.show();
        if(validarDatos())
        {
            numero_identificacion="V-"+edit_numero_ident.getText().toString();
            presenter.setGuardarUsuario(
                    Preferences.getIDUser(),
                    Preferences.getIDPatientid(),
                    edit_nombre.getText().toString(),
                    edit_apellido.getText().toString(),
                    spinner_genero.getSelectedItem().toString(),
                    spinner_tipo_ident.getSelectedItem().toString(),
                    numero_identificacion,
                    dateBirthday,
                    listIdOcupacion.get(num_ocupacion),
                    listIdPais.get(num_pais),
                    listIdPaisreci.get(num_pais_reci),
                    listIdZonaHorario.get(num_zona_horario),
                    1);
            Preferences.setNombre(edit_nombre.getText().toString()+" "+edit_apellido.getText().toString());
            Preferences.setPais(listPaisString.get(num_pais_reci));
        }

    }

    @OnClick(R.id.edit_fecha)
    public void seleccionarFecha() {
        if(edit_fecha.getText().toString().equals("Seleccione"))
        {
            setTime("");
        }
        else
        {
            setTime(dateBirthday);
        }

//        Time date = new Time();
//        date.setToNow();
//
//        final int fechaActual=date.year;
//        DatePickerDialog.OnDateSetListener dpd = new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear,
//                                  int dayOfMonth) {
//                int month = monthOfYear +1;
//                edit_fecha.setText(year+"/"+month+"/"+ dayOfMonth);
////                resultado=fechaActual-year;
////                if(resultado>=18)
////                {
////                    edit_fecha.setText(year+"/"+month+"/"+ dayOfMonth);
////                }else
////                {
////                    dialog("Debes ser mayor de 18 años!",false);
////                }
//
//            }
//        };
//
//        DatePickerDialog d = new DatePickerDialog(getActivity(), dpd, date.year ,date.month, date.monthDay);
//        d.show();
    }




    private void setTime(String birthday) {

        if (getActivity() == null)
            return;
        Time today = new Time();
        today.setToNow();

        if(birthday.equals("")){
            years = today.year;
            month = today.month;
            day = today.monthDay;
        }
        if (birthday != null || !birthday.equals("")) {
            String date;
            date = birthday;
            String parts[] = date.split("/");

            if (!parts[0].isEmpty() && !parts[1].isEmpty()
                    && !parts[2].isEmpty()) {
                years = Integer.parseInt(parts[0]);
                month = Integer.parseInt(parts[2]) - 1;
                day = Integer.parseInt(parts[1]);
            } else {
                years = today.year;
                month = today.month;
                day = today.monthDay;
            }
        } else {
            years = today.year;
            month = today.month;
            day = today.monthDay;
        }


        DatePickerDialog dialog = new DatePickerDialog(getActivity(), android.R.style.Theme_Holo_Dialog, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                Calendar calendar = Calendar.getInstance();
                calendar.clear();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);
                Date date = calendar.getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
                String newDate = df.format(date);
                dateBirthday=newDate;
                edit_fecha.setText(parseDateToddMMyyyy(newDate));

            }
        }, years, month, day);
        dialog.getDatePicker().setMaxDate(new Date().getTime());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
    }

    private boolean mayorEdad(String birthday)
    {
        Time today = new Time();
        today.setToNow();
        int years = 0;
        int month;
        int day;

        if (birthday != null && !birthday.isEmpty()) {
            String date;
            date = birthday;
            String parts[] = date.split("/");

            if (!parts[0].isEmpty() && !parts[1].isEmpty()
                    && !parts[2].isEmpty()) {
                years = Integer.parseInt(parts[0]);
                month = Integer.parseInt(parts[2]) - 1;
                day = Integer.parseInt(parts[1]);
            }
            resultado=today.year-years;
            if(resultado>=18)
            {
                return true;
            }
        }
        return false;
    }

    public void iniUI()
    {
        progress = new ProgressDialog(getActivity());
        progress.setTitle(getActivity().getString(R.string.registro_cargando));
        progress.setCancelable(false);
        presenter = new DatosPersonalesPresentador(this);

        addGenero();
        addTipo();
        addTipoNacionalidad();
       // addZonaHoraria();

    }

    public void dialog(String resp,final boolean respboolean)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage(resp);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(respboolean)
                {
                    initFragment();
                }
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void initFragment()
    {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, new MisConsultasFragment());
        ft.addToBackStack(null);
        ft.commit();
    }

    public boolean validarDatos()
    {
        if(edit_numero_ident.getText().toString().isEmpty())
        {
            progress.dismiss();
            edit_numero_ident.setError("Ingrese número de identidad");
            dialog("Ingrese número de identidad",false);
            return false;
        }
        if(edit_nombre.getText().toString().isEmpty())
        {
            progress.dismiss();
            edit_nombre.setError("Ingrese nombre");
            dialog("Ingrese nombre",false);
            return false;
        }
        if(edit_apellido.getText().toString().isEmpty())
        {
            progress.dismiss();
            edit_apellido.setError("Ingrese apellido");
            dialog("Ingrese apellido",false);
            return false;
        }
        if(edit_fecha.getText().toString().equals("Seleccione"))
        {

            progress.dismiss();
            edit_fecha.setError("Selecciona fecha de nacimiento");
            dialog("Selecciona fecha de nacimiento",false);
            return false;
        }
        if(!mayorEdad(edit_fecha.getText().toString()))
        {

            progress.dismiss();
            dialog("Debes ser mayor de 18 años!",false);
            return false;
        }
        edit_fecha.setError(null);
        if(spinner_genero.getSelectedItem().toString().equals("Seleccione"))
        {
            progress.dismiss();
            TextView errorText = (TextView)spinner_genero.getSelectedView();
            errorText.setError("Seleccione género");
            errorText.setTextColor(Color.RED);
            //errorText.setText("Seleccione género");
            dialog("Seleccione género",false);
            return false;
        }
        if(num_pais==0)
        {
            progress.dismiss();
            TextView errorText = (TextView)spinner_pais.getSelectedView();
            errorText.setError("Seleccione un país");
            errorText.setTextColor(Color.RED);
            //errorText.setText("Seleccione un país");
            dialog("Seleccione un país",false);
            return false;
        }
        if(num_pais_reci==0)
        {
            progress.dismiss();
            TextView errorText = (TextView)spinner_pais_reci.getSelectedView();
            errorText.setError("Seleccione un país donde reside");
            errorText.setTextColor(Color.RED);
            //errorText.setText("Seleccione un país donde reside");
            dialog("Seleccione un país donde reside actualmente",false);
            return false;
        }
        if(num_zona_horario==0)
        {
            progress.dismiss();
            TextView errorText = (TextView)spinner_zona_horario.getSelectedView();
            errorText.setError("Seleccione zona horaria");
            errorText.setTextColor(Color.RED);
            //errorText.setText("Seleccione un país donde reside");
            dialog("Seleccione una zona horaria",false);
            return false;
        }
        if(num_ocupacion==0)
        {
            progress.dismiss();
            TextView errorText = (TextView)spinner_ocupacion.getSelectedView();
            errorText.setError("Seleccione una ocupación");
            errorText.setTextColor(Color.RED);
            dialog("Seleccione una ocupación",false);
            return false;
        }
        if (!mayorEdad(edit_fecha.getText().toString()))
        {
            progress.dismiss();
            dialog("Debes ser mayor de 18 años!",false);
            return false;
        }

        return true;
    }

    public void addGenero() {

        List<String> list = new ArrayList<String>();
        list.add("Seleccione");
        list.add("Femenino");
        list.add("Masculino");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_genero.setAdapter(dataAdapter);

    }
    public void addTipoNacionalidad() {

        List<String> list = new ArrayList<String>();
        list.add("V - ");
       // list.add("E-");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tipo_nacionalidad.setAdapter(dataAdapter);
    }

    public void addTipo() {

        List<String> list = new ArrayList<String>();

        list.add("Documento de Identidad");
        list.add("Partida de Nacimiento");
        list.add("Pasaporte");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tipo_ident.setAdapter(dataAdapter);

    }

    public void addOcupacion() {
        num_ocupacion=num_ocupacion;
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listOcupString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_ocupacion.setAdapter(dataAdapter);
        //spinner_ocupacion.setSelection(110);
        if (num_ocupacionAuxi!=1000)
        {
            buscarOcupaciones(num_ocupacionAuxi);
        }

        spinner_ocupacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                num_ocupacion = pos;
                Log.d("Adapter","spinner"+pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void addPais() {
        num_pais=num_paisAuxi;
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listPaisString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pais.setAdapter(dataAdapter);
        //spinner_pais.setSelection(195);

        if(num_paisAuxi!=1000)
        {
            Log.d("num_paisAuxi","1"+num_paisAuxi);
            buscarPaisOrigen(num_paisAuxi);
        }

        spinner_pais.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                num_pais=pos;
                Log.d("Adapter","spinner Pais"+pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void addPaisReci() {
        num_pais_reci=num_pais_reci;
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listPaisString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pais_reci.setAdapter(dataAdapter);
       // spinner_pais_reci.setSelection(195);

        buscarPaisReci(num_pais_reciAuxi);

        spinner_pais_reci.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                num_pais_reci=pos;
                //buscarZone();
                buscarZone(listIdPaisreci.get(num_pais_reci));
                Log.d("Adapter","spinner Pais"+listIdPaisreci.get(num_pais_reci));
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    public void addZonaHoraria() {

        //num_zona_horario=num_zona_horario;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listZonaHorarioString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_zona_horario.setAdapter(dataAdapter);

        buscarZonaHoraria(num_zona_horarioAuxilia);

        spinner_zona_horario.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                num_zona_horario=pos;
                Log.d("Adapter","spinner Pais"+pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }



    @Override
    public void setDataPaises(List<InformacionList>  dato) {
        listPais = dato;
        listPaisString.add("Seleccione");
        listIdPais.add(0);
        listIdPaisreci.add(0);
        for (int i=1;i<listPais.size();i++)
        {
            listPaisString.add(listPais.get(i).getValuename());
            listIdPais.add(listPais.get(i).getValueid());
            listIdPaisreci.add(listPais.get(i).getValueid());
        }
        addPais();
        addPaisReci();
        buscarPaisOrigen(num_paisAuxi);
        Log.d("--->","Paises"+dato.toString());
    }

    @Override
    public void setDataOcupaciones(List<InformacionList>  dato) {
        listOcupacion = dato;
        listOcupString.add("Seleccione");
        listIdOcupacion.add(0);
        for (int i=1;i<listOcupacion.size();i++)
        {
            listOcupString.add(listOcupacion.get(i).getValuename());
            listIdOcupacion.add(listOcupacion.get(i).getValueid());
        }
        addOcupacion();
        Log.d("--->","Ocupaciones"+dato.toString());
    }

    @Override
    public void setDataParentesco(List<InformacionList> dato) { }

    @Override
    public void setZonaHoraria(List<InformacionList> dato) {
        ZonaHorario.clear();
        listZonaHorario.clear();
        listZonaHorarioString.clear();
        listIdZonaHorario.clear();

        listZonaHorario=dato;
        ZonaHorario=dato;

        if(zoneHoraria)
        {
            buscarZone(num_pais_reciAuxi);
            zoneHoraria=false;
        }

    }

    public void buscarZone(int num)
    {
        listZonaHorarioString.clear();
        listIdZonaHorario.clear();

            listIdZonaHorario.add(0);
            listZonaHorarioString.add("Seleccione");

            for (int i=1;i<ZonaHorario.size();i++)
            {
                Log.d("--->AQUI","holamundo"+ZonaHorario.size());
                if(ZonaHorario.get(i).getParentid()==num)
                {
                    edit_zona_horario.setText(listZonaHorario.get(i).getValuename());
                    listZonaHorarioString.add(listZonaHorario.get(i).getValuename());
                    listIdZonaHorario.add(listZonaHorario.get(i).getValueid());
                    Log.d("--->AQUI","holamundo"+listZonaHorario.get(i).getValuename());
                    Log.d("--->AQUI","holamundo"+listZonaHorario.get(i).getValuename());
                }
            }


        addZonaHoraria();
    }

    @Override
    public void setRespDatosUsuario(String sms) {
        Log.d("--->","DatosUsuario"+sms);
        if (sms.equals("ok"))
        {
            dialog("Sus Datos personales han sido actualizados",true);
        }
        else
        {
            dialog(sms,false);
            Preferences.setNombre("");
            Preferences.setPais("");
        }
        progress.cancel();
    }

    @Override
    public void getDatosPacientes(Patient a) {

        Log.d("getDatosPacientes","--->1");


     if(a!=null) {
         if (a.getIdentificationNumber().length() > 3) {
             String numeroIde = a.getIdentificationNumber();
             String result = numeroIde.substring(2);
             edit_numero_ident.setText(result.toString());
         }
         if (a.getTimezoneid() != null) {
             num_zona_horarioAuxilia= a.getTimezoneid();
         }
         if (a.getCountryid() != null) {
             Log.d("num_paisAuxi","get Datos "+a.getCountryid());
             num_paisAuxi = a.getCountryid();
             presenter.getPaises();
             //num_pais_reciAuxi = a.getResCountryid();
         }else
         {
             presenter.getPaises();
         }
         if (a.getResCountryid() != null) {
             num_pais_reciAuxi = a.getResCountryid();
             buscarZone(a.getResCountryid());
         }
         if (a.getOccupationid() != null) {
             num_ocupacionAuxi = a.getOccupationid();
             presenter.getOcupaciones();
         }else
         {
             presenter.getOcupaciones();
         }
         if (a.getLastname() != null) {
             edit_apellido.setText(a.getLastname());
         }
         if (a.getTimezoneid() != null) {
             Log.d("num_zona_horarioAuxilia","-->"+ a.getTimezoneid());
             num_zona_horarioAuxilia= a.getTimezoneid();
             presenter.getZonaHoraria();
         }else
         {
             presenter.getZonaHoraria();
         }
         if (a.getName() != null) {
             edit_nombre.setText(a.getName());
             Preferences.setNombre(a.getName()+" "+a.getLastname());

         }
         if (a.getResCountryName() != null) {
             Preferences.setPais(a.getResCountryName());
         }
         if (a.getSex() != "") {
             buscarSex(a.getSex());
         }
         if (a.getIdentificationType() != null) {
             buscarDoc(a.getIdentificationType());
         }
         if (a.getBirthdate() != null) {
             Log.d("getBirthdate","getBirthdate --->"+a.getBirthdate());
             if(a.getBirthdate().equals("1970/1/01") || a.getBirthdate().equals(""))
             {
              edit_fecha.setText("Seleccione");
             }
             else
             {
                 dateBirthday=a.getBirthdate();
                 edit_fecha.setText(parseDateToddMMyyyy(a.getBirthdate()));
             }
         }

     }

    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    private  String deleteChar(String text, int charPos) {
        return new StringBuilder(text).deleteCharAt(charPos).toString();
    }
    @Override
    public void setRespPasswordNew(String resp, String Token) {

    }

    public void buscarPaisOrigen(int numPais)
    {
        Log.d("num_paisAuxi","--->buscarPaisOrigen");
        for (int i=1;i<listIdPais.size();i++)
        {
            if(numPais==listIdPais.get(i))
            {
                spinner_pais.setSelection(i);
                break;
            }
        }
    }
    public void buscarSex(String sex)
    {
       if(sex.equals("Femenino"))
       {
           spinner_genero.setSelection(1);
       }else
       {
           spinner_genero.setSelection(2);
       }
    }

    public void buscarDoc(String doc)
    {

        if(doc.equals("Documento de Identidad"))
        {
            spinner_tipo_ident.setSelection(0);
        }
        if(doc.equals("Partida de Nacimiento"))
        {
            spinner_tipo_ident.setSelection(1);
        }
        if(doc.equals("Pasaporte"))
        {
            spinner_tipo_ident.setSelection(2);
        }
    }

    public void buscarPaisReci(int numPais)
    {

        for (int i=1;i<listIdPaisreci.size();i++)
        {
            if(numPais==listIdPaisreci.get(i))
            {
                spinner_pais_reci.setSelection(i);
                break;
            }
        }

    }

    public void buscarZonaHoraria(int numZona)
    {
        Log.d("num_zona_horarioAuxilia","add-->"+numZona);

        for (int i=1;i<listIdZonaHorario.size();i++)
        {
            if(numZona==listIdZonaHorario.get(i))
            {
                Log.d("num_zona_horarioAuxilia","-i->"+listIdZonaHorario.get(i));
                spinner_zona_horario.setSelection(i);
                break;
            }
        }

    }

    public void buscarOcupaciones(int numOcu)
    {

        for (int i=1;i<listIdOcupacion.size();i++)
        {
            if(numOcu==listIdOcupacion.get(i))
            {
                spinner_ocupacion.setSelection(i);
                break;
            }
        }

    }

}

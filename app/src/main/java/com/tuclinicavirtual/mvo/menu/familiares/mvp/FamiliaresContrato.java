package com.tuclinicavirtual.mvo.menu.familiares.mvp;

import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionList;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.familiares.models.DeleteRequestFamilia;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaRequest;

import java.util.List;

public class FamiliaresContrato {

    public interface View{
        void getListFamiliares(List<Patient> familiar);
        void setFamiliainfo(String rest);
        void setDeleteFamiliar(String rest);
    }

    public interface Presenter{
        void getFamiliares(int id);
        void setFamiliar(FamiliaRequest familiar);
        void deleteFamiliar(DeleteRequestFamilia familiar);
    }

}

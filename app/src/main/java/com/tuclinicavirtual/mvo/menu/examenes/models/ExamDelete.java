package com.tuclinicavirtual.mvo.menu.examenes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExamDelete {

    @SerializedName("examid")
    @Expose
    private Integer examid;

    public ExamDelete(Integer examid) {
        this.examid = examid;
    }

    public Integer getExamid() {
        return examid;
    }

    public void setExamid(Integer examid) {
        this.examid = examid;
    }
}

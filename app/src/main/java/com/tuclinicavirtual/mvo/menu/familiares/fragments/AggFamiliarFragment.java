package com.tuclinicavirtual.mvo.menu.familiares.fragments;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionList;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesContrato;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesPresentador;
import com.tuclinicavirtual.mvo.menu.familiares.adapters.FamiliaresAdapter;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaRequest;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliasModelo;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresPresentador;
import com.tuclinicavirtual.mvo.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.whalemare.sheetmenu.SheetMenu;

/**
 * A simple {@link Fragment} subclass.
 */
public class AggFamiliarFragment extends Fragment implements DatosPersonalesContrato.View, FamiliaresContrato.View {


    public AggFamiliarFragment() {
        // Required empty public constructor
    }
    FamiliaresPresentador presenterFamilia;

    @BindView(R.id.spinner_tipo_ident)
    Spinner spinner_tipo_ident;

    @BindView(R.id.spinner_genero)
    Spinner spinner_genero;

    @BindView(R.id.spinner_pais)
    Spinner spinner_pais;

    @BindView(R.id.spinner_pais_reci)
    Spinner spinner_pais_reci;

    @BindView(R.id.spinner_ocupacion)
    Spinner spinner_ocupacion;

    @BindView(R.id.spinner_parentesco)
    Spinner spinner_parentesco;

    @BindView(R.id.edit_numero_ident)
    EditText edit_numero_ident;

    @BindView(R.id.edit_nombre)
    EditText edit_nombre;

    @BindView(R.id.edit_apellido)
    EditText edit_apellido;

    @BindView(R.id.edit_fecha)
    EditText edit_fecha;

    String numero_identificacion;

    List<InformacionList> listOcupacion = new ArrayList<InformacionList>();
    List<InformacionList> listPais = new ArrayList<InformacionList>();
    List<InformacionList> listParentesco = new ArrayList<InformacionList>();

    List<String> listPaisString = new ArrayList<String>();
    List<String> listOcupString = new ArrayList<String>();
    List<String> listParentescoString = new ArrayList<String>();


    ProgressDialog progress;
    //id de los paises y ocupaciones

    List<Integer> listIdPais= new ArrayList<Integer>();
    List<Integer> listIdPaisreci= new ArrayList<Integer>();
    List<Integer> listIdOcupacion= new ArrayList<Integer>();
    List<Integer> listIdParentesco= new ArrayList<Integer>();

    private int num_ocupacion=0;
    private int num_pais=0;
    private int num_pais_reci=0;
    private int num_parentesco=0;

    DatosPersonalesPresentador presenter;
    List<FamiliasModelo> listPaciente=new ArrayList<FamiliasModelo>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_agg_familiar, container, false);
        ButterKnife.bind(this, v);
        initUI();
        presenter.getOcupaciones();
        presenter.getPaises();
        presenter.getRalicionFamiliar();
        //listPaciente.add(new FamiliasModelo("Fina 2 2", "Lopez", "Masculino", "Documento de Identidad","V-24444445", "1991/05/14", 105, 189, 38, 12, 3));

       // presenterFamilia.setFamiliar(new FamiliaRequest(Preferences.getIDUser(),listPaciente));

        return v;
    }

    @OnClick(R.id.edit_fecha)
    public void seleccionarFecha() {

        DatePickerDialog.OnDateSetListener dpd = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                int month = monthOfYear +1;
                edit_fecha.setText(year+"/"+month+"/"+ dayOfMonth);
            }
        };
        Time date = new Time();
        date.setToNow();
        DatePickerDialog d = new DatePickerDialog(getActivity(), dpd, date.year ,date.month, date.monthDay);
        d.show();
    }

    @OnClick(R.id.botton_guardar)
    public void botton_guardar() {
        progress.show();
        if(validarDatos()) {
            numero_identificacion="V-"+edit_numero_ident.getText().toString();
            listPaciente.add(new FamiliasModelo(edit_nombre.getText().toString(), edit_apellido.getText().toString(), spinner_genero.getSelectedItem().toString(), spinner_tipo_ident.getSelectedItem().toString(), numero_identificacion, edit_fecha.getText().toString(), listIdOcupacion.get(num_ocupacion), listIdPais.get(num_pais), listIdPaisreci.get(num_pais_reci), 12, listIdParentesco.get(num_parentesco)));
            presenterFamilia.setFamiliar(new FamiliaRequest(Preferences.getIDUser(), listPaciente));
        }
    }

    public boolean validarDatos()
    {
        if(edit_nombre.getText().toString().isEmpty())
        {
            progress.dismiss();
            edit_nombre.setError("");
            return false;
        }
        if(edit_apellido.getText().toString().isEmpty())
        {
            progress.dismiss();
            edit_apellido.setError("");
            return false;
        }
        if(edit_numero_ident.getText().toString().isEmpty())
        {
            progress.dismiss();
            edit_numero_ident.setError("");
            return false;
        }
        if(edit_fecha.getText().toString().isEmpty())
        {
            progress.dismiss();
            edit_fecha.setError("");
            return false;
        }

        return true;
    }

    public void initUI()
    {
        progress = new ProgressDialog(getActivity());
        progress.setTitle(getActivity().getString(R.string.registro_cargando));
        presenterFamilia=new FamiliaresPresentador(this);
        presenter = new DatosPersonalesPresentador(this);
        addTipo();
        addGenero();
    }

    public void addTipo() {

        List<String> list = new ArrayList<String>();

        list.add("Documento de Identidad");
        list.add("Passaporte");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tipo_ident.setAdapter(dataAdapter);

    }

    public void addOcupacion() {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listOcupString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_ocupacion.setAdapter(dataAdapter);

        spinner_ocupacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                num_ocupacion = pos;
                Log.d("Adapter","spinner"+pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void addPais() {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listPaisString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pais.setAdapter(dataAdapter);

        spinner_pais.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                num_pais=pos;
                Log.d("Adapter","spinner Pais"+pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }



    public void addPaisReci() {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listPaisString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pais_reci.setAdapter(dataAdapter);

        spinner_pais_reci.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                num_pais_reci=pos;

                Log.d("Adapter","spinner Pais"+pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    public void addParentesco() {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listParentescoString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_parentesco.setAdapter(dataAdapter);

        spinner_parentesco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                num_parentesco=pos;
                Log.d("Adapter","spinner Pais"+pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }
    public void addGenero() {

        List<String> list = new ArrayList<String>();
        list.add("Femenino");
        list.add("Masculino");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_genero.setAdapter(dataAdapter);

    }
    @Override
    public void setDataPaises(List<InformacionList> dato) {
        listPais = dato;
        for (int i=0;i<listPais.size();i++)
        {
            listPaisString.add(listPais.get(i).getValuename());
            listIdPais.add(listPais.get(i).getValueid());
            listIdPaisreci.add(listPais.get(i).getValueid());
        }
        addPais();
        addPaisReci();
    }

    @Override
    public void setDataOcupaciones(List<InformacionList> dato) {
        listOcupacion = dato;
        for (int i=0;i<listOcupacion.size();i++)
        {
            listOcupString.add(listOcupacion.get(i).getValuename());
            listIdOcupacion.add(listOcupacion.get(i).getValueid());
        }
        addOcupacion();
        Log.d("--->","Ocupaciones"+dato.toString());
    }

    @Override
    public void setDataParentesco(List<InformacionList> dato) {
        listParentesco = dato;

        for (int i=0;i<listParentesco.size();i++)
        {

            listParentescoString.add(listParentesco.get(i).getValuename());
            listIdParentesco.add(listParentesco.get(i).getValueid());

        }
        addParentesco();

    }

    @Override
    public void setZonaHoraria(List<InformacionList> dato) {

    }

    @Override
    public void setRespDatosUsuario(String sms) {

    }

    @Override
    public void getDatosPacientes(Patient a) {

    }

    @Override
    public void setRespPasswordNew(String resp, String Token) {

    }

    @Override
    public void getListFamiliares(List<Patient> familiar) {

    }

    @Override
    public void setFamiliainfo(String rest) {
    Log.d("setFamilia","-->"+rest);

            FragmentTransaction ft =  getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.contenedor, new MisFamiliaresFragment());
            ft.addToBackStack(null);
            ft.commit();
            Toast.makeText(getActivity(),rest, Toast.LENGTH_SHORT).show();

    progress.cancel();
    }

    @Override
    public void setDeleteFamiliar(String rest) {

    }
}

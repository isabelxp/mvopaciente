package com.tuclinicavirtual.mvo.menu.consultas.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasEscritas;
import com.tuclinicavirtual.mvo.menu.consultas.models.Consultation;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostic;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostico;
import com.tuclinicavirtual.mvo.menu.consultas.models.EstatusResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ExamLab;
import com.tuclinicavirtual.mvo.menu.consultas.models.Prescripcion;
import com.tuclinicavirtual.mvo.menu.consultas.models.PrescripcionItem;
import com.tuclinicavirtual.mvo.menu.consultas.mvp.ConsultasContrato;
import com.tuclinicavirtual.mvo.menu.consultas.mvp.ConsultasPresentador;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresPresentador;
import com.tuclinicavirtual.mvo.utils.Preferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConsultasEscritaFragment extends Fragment implements ConsultasContrato.View,FamiliaresContrato.View{


    public ConsultasEscritaFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.spinner)
    Spinner spinner;
    AdapterConsultasEscritas adaptador;
    List<Consultation> listConsultaEscritas =new ArrayList<>();
    ConsultasPresentador presentador;
    FamiliaresPresentador presentadorFamiliares;
    boolean init=false;
    List<String> listPaciente=new ArrayList<>();
    List<Integer> idPacientes=new ArrayList<>();
    List<Integer> idUser=new ArrayList<>();
    List<String> utcPaciente=new ArrayList<>();

    public int idPacienteMisCOnsultas;
    public String stringPacienteMisCOnsultas;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_consultas_programadas, container, false);
        ButterKnife.bind(this, v);
        presentadorFamiliares=new FamiliaresPresentador(this);
        presentadorFamiliares.getFamiliares(Preferences.getIDUser());
        initUI();


        return v;
    }

    public void initUI()
    {
        presentador=new ConsultasPresentador(this);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(reci);
        recycler.setHasFixedSize(true);
        addTextListener(listConsultaEscritas);

    }
    public void initUISpinner(final List<String> familiar)
    {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, familiar);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        if(familiar!=null)
        {
            for(int i=0;i<familiar.size();i++)
            {
                if(familiar.get(i).contains("Titular"))
                {
                    spinner.setSelection(i);
                }
            }

        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                idPacienteMisCOnsultas=idPacientes.get(pos);
                stringPacienteMisCOnsultas=utcPaciente.get(pos);
                EventBus.getDefault().post("MisConsultas");

            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public void addTextListener(final List<Consultation> dato2){
        Log.d("addTextListener",""+dato2.size());
        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();

                final List<Consultation> datoAuxi2 = new ArrayList<>();

                for (int i = 0; i < dato2.size(); i++) {

                    final String text = dato2.get(i).getDoctorName().toLowerCase();
                    if (text.contains(query)) {
                        datoAuxi2.add(dato2.get(i));
                    }
                }
                Log.d("addTextListener",""+dato2.size());
                Log.d("addTextListener",""+datoAuxi2.size());

                adaptador= new AdapterConsultasEscritas(datoAuxi2,getActivity());
                recycler.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
            }
        });
    }
    @Override
    public void getDiagnosticoList(List<Diagnostico> dato) {

    }

    @Override
    public void getPrescripcionList(List<Prescripcion> dato) {

    }

    @Override
    public void getPrescripcionItemList(List<PrescripcionItem> dato) {



    }



    @Override
    public void getExamLab(List<ExamLab> dato) {

    }

    @Override
    public void getConsultation(List<Consultation> dato) {
            listConsultaEscritas.clear();
            listConsultaEscritas=dato;
            adaptador= new AdapterConsultasEscritas(listConsultaEscritas,getActivity());
            recycler.setAdapter(adaptador);
            adaptador.notifyDataSetChanged();
            addTextListener(dato);
            init=true;
    }

    @Override
    public void getReporte(String dato) {

    }

    @Override
    public void getScore(String sms) {

    }

    @Override
    public void getDiagnosticoItem(List<Diagnostic> dato) {
        Log.d("getDiagnosticoItem","-->"+dato.toString());
    }

    @Override
    public void getStatusToken(EstatusResponse status) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String num) {

//        if(num.equals("200"))
//        {
//            presentador.getConsultation(Preferences.getIDPatientid(),1);
//        }

        if(num.equals("MisConsultas"))
        {
            listConsultaEscritas.clear();
            adaptador= new AdapterConsultasEscritas(listConsultaEscritas,getActivity());
            recycler.setAdapter(adaptador);
            adaptador.notifyDataSetChanged();
            presentador.getConsultation(idPacienteMisCOnsultas,1,stringPacienteMisCOnsultas);
        }


    };

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void getListFamiliares(List<Patient> familiar) {
        listPaciente.clear();
        idPacientes.clear();
        idUser.clear();
        utcPaciente.clear();
        for (int i=0;i<familiar.size();i++)
        {

            listPaciente.add(familiar.get(i).getName()+" "+familiar.get(i).getLastname()+" - "+familiar.get(i).getRelationshipName());
            idPacientes.add(familiar.get(i).getPatientid());
            idUser.add(familiar.get(i).getPatientid());
            utcPaciente.add(familiar.get(i).getTimezoneName());
        }

        initUISpinner(listPaciente);
    }

    @Override
    public void setFamiliainfo(String rest) {

    }

    @Override
    public void setDeleteFamiliar(String rest) {

    }
}

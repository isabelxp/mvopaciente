package com.tuclinicavirtual.mvo.menu.datosPersonales.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.ConsultasEscritaFragment;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.ConsultasPragramadaFragment;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.MisConsultasFragment;
import com.tuclinicavirtual.mvo.menu.datosPersonales.DatosPersonalesFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MisDatosPersonalesFragment extends Fragment {


    public MisDatosPersonalesFragment() {
        // Required empty public constructor
    }

    SectionsPagerAdapter mSectionsPagerAdapter;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.container)
    ViewPager mViewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_mis_datos_personales, container, false);
        ButterKnife.bind(this, v);
        initUI();
        return v;
    }




    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new DatosPersonalesFragment();
                case 1:
                    return new MiCuentaFragment();
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Mi Perfil";
                case 1:
                    return "Mi Cuenta";
            }
            return null;
        }
    }

    public void initUI()
    {

        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mViewPager.setCurrentItem(2,true);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabsFromPagerAdapter(mSectionsPagerAdapter);

    }
}

package com.tuclinicavirtual.mvo.menu.solicitarConsulta.interfaces;

import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPacientes;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DoctoHorariosRequest;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.RequestPaymens;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponseDoctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponseHorarioDoctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponsePayments;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponsesEspecialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.SheduleResponse;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.SolicitarConsultaRequest;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.SolicitarConsultaResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SolicitarConsultaInterfaces {

    @GET("doctor/specialities")
    Call<ResponsesEspecialidad> getEspecialidad(@Header("Authorization") String valor);

    @GET("doctors/{id}")
    Call<ResponseDoctor> getDoctores(@Path("id") int id,@Header("Authorization") String valor);

    @GET("doctor/schedule/{utc}")
    Call<SheduleResponse> getShoulderDoctor(@Path("utc") String utc, @Header("Authorization") String valor);

    @Headers("Content-Type:application/json")
    @POST("doctors/availabilities")
    Call<ResponseHorarioDoctor> setDoctHorarios(@Body DoctoHorariosRequest request, @Header("Authorization") String valor);

    @Headers("Content-Type:application/json")
    @POST("consultations")
    Call<SolicitarConsultaResponse> setConsultaPaciente(@Body SolicitarConsultaRequest request, @Header("Authorization") String valor);

    @Headers("Content-Type:application/json")
    @POST("patients/consultations/payments")
    Call<ResponsePayments> setPaymentsUser(@Body RequestPaymens request, @Header("Authorization") String valor);


}

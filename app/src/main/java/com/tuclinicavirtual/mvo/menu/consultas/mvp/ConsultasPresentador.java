package com.tuclinicavirtual.mvo.menu.consultas.mvp;

import android.util.Log;

import com.tuclinicavirtual.mvo.menu.consultas.interfaces.ConsultationInterface;
import com.tuclinicavirtual.mvo.menu.consultas.models.ConsultationResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.DiagnocItemResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.DiagnosticoResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.EstatusResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ExamLabResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ModeloResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.PrescripcionItemResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.PrescripcionResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ReporteRequest;
import com.tuclinicavirtual.mvo.menu.consultas.models.ScoreRequest;
import com.tuclinicavirtual.mvo.menu.consultas.models.ScoreResponse;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.utils.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConsultasPresentador {

    ConsultationInterface rest;
    public  ConsultasContrato.View view;
    private String TAG;

    public ConsultasPresentador(ConsultasContrato.View view) {
        this.view = view;
        TAG = this.getClass().getSimpleName();
    }
    public void getConsultation(int id,int tipo,String utc) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ConsultationInterface.class);
        Log.i("getUTC_usuario", "--->Consultation: " + Preferences.getUTC_usuario());
        rest.getConsultation(id,utc,tipo,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ConsultationResponse>() {
            @Override
            public void onResponse(Call<ConsultationResponse> call, Response<ConsultationResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getConsultation(response.body().getData().getConsultations());
                        Log.i(TAG, "--->Consultation: " + response.body().getMessage());
                        Log.i(TAG, "--->Consultation: " + response.body().getStatus());
                        Log.i(TAG, "--->Consultation: " + call.request().url());

                    }
                }else{
                    Log.i("getUTC_usuario", "--->Error en Consultation: " + Preferences.getUTC_usuario());
                    Log.i(TAG, "--->Error en Consultation: " + call.request().url());
                    Log.i(TAG, "--->Error en Consultation: " + response.code());
                    Log.i(TAG, "--->Error en Consultation: " + response.errorBody());
                    Log.i(TAG, "--->Error en Consultation: " + response.message());
                    Log.i(TAG, "--->Error en Consultation: " + call.request().toString());

                }  }
            @Override
            public void onFailure(Call<ConsultationResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void setReporte(ReporteRequest request) {
        Log.i(TAG, "--->setReporte: " + request.toString());
        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ConsultationInterface.class);

        rest.setReporte(request,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ModeloResponse>() {
            @Override
            public void onResponse(Call<ModeloResponse> call, Response<ModeloResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getReporte(response.body().getMessage());
                        Log.i(TAG, "--->setReporte: " + response.body().getMessage());
                        Log.i(TAG, "--->setReporte: " + response.body().getStatus());
                    }
                }else{
                    view.getReporte("no no");
                    Log.i(TAG, "--->Error en setReporte: " + call.request().url());
                    Log.i(TAG, "--->Error en setReporte: " + response.code());
                    Log.i(TAG, "--->Error en setReporte: " + response.errorBody());
                    Log.i(TAG, "--->Error en setReporte: " + response.message());
                    Log.i(TAG, "--->Error en setReporte: " + call.request().toString());

                }  }
            @Override
            public void onFailure(Call<ModeloResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void getDiagnostico(int id) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ConsultationInterface.class);

        rest.getDiagnostico(id,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<DiagnosticoResponse>() {

            @Override
            public void onResponse(Call<DiagnosticoResponse> call, Response<DiagnosticoResponse> response) {
                Log.i(TAG, "-->Error en getDiagnostico: " + call.request().url());
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getDiagnosticoList(response.body().getData().getDiagnostics());
                        Log.i(TAG, "--->getDiagnostico: " + response.body().getMessage());
                        Log.i(TAG, "--->getDiagnostico: " + response.body().getStatus());
                        Log.i(TAG, "--->getDiagnostico: " + response.body().getData().toString());

                    }
                }else{
                    Log.i(TAG, "--->Error en getDiagnostico: " + call.request().url());
                    Log.i(TAG, "--->Error en getDiagnostico: " + response.code());
                    Log.i(TAG, "--->Error en getDiagnostico: " + response.errorBody());
                    Log.i(TAG, "--->Error en getDiagnostico: " + response.message());
                    Log.i(TAG, "--->Error en getDiagnostico: " + call.request().toString());

                }  }
            @Override
            public void onFailure(Call<DiagnosticoResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void getDiagnosticoItem(int id) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ConsultationInterface.class);

        rest.getDiagnosticoItem(id,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<DiagnocItemResponse>() {
            @Override
            public void onResponse(Call<DiagnocItemResponse> call, Response<DiagnocItemResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getDiagnosticoItem(response.body().getData().getDiagnostics());
                        Log.i(TAG, "--->getDiagnostico: " + response.body().getMessage());
                        Log.i(TAG, "--->getDiagnostico: " + response.body().getStatus());
                    }
                }else{
                    Log.i(TAG, "--->Error en getDiagnosticoItem: " + call.request().url());
                    Log.i(TAG, "--->Error en getDiagnosticoItem: " + response.code());
                    Log.i(TAG, "--->Error en getDiagnosticoItem: " + response.errorBody());
                    Log.i(TAG, "--->Error en getDiagnosticoItem: " + response.message());
                    Log.i(TAG, "--->Error en getDiagnosticoItem: " + call.request().toString());

                }  }
            @Override
            public void onFailure(Call<DiagnocItemResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void getTokenOpentok(String utc, int idConsulta) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ConsultationInterface.class);

        rest.getTokenOpentok(idConsulta,utc,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<EstatusResponse>() {
            @Override
            public void onResponse(Call<EstatusResponse> call, Response<EstatusResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getStatusToken(response.body());
                        Log.i(TAG, "--->getTokenOpentok " + response.body().getMessage());
                        Log.i(TAG, "--->getTokenOpentok " + response.body().getStatus());
                        Log.i(TAG, "--->getTokenOpentok " + response.body().toString());
                        Log.i(TAG, "--->getTokenOpentok " + response.body().getSession());

                    }
                }else{
                    Log.i(TAG, "--->Error en getTokenOpentok " + call.request().url());
                    Log.i(TAG, "--->Error en getTokenOpentok " + response.code());
                    Log.i(TAG, "--->Error en getTokenOpentok " + response.errorBody());
                    Log.i(TAG, "--->Error en getTokenOpentok " + response.message());
                    Log.i(TAG, "--->Error en getTokenOpentok " + call.request().toString());

                }  }
            @Override
            public void onFailure(Call<EstatusResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void getPrescripcion(int id) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ConsultationInterface.class);

        rest.getPrescripcion(id,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<PrescripcionResponse>() {
            @Override
            public void onResponse(Call<PrescripcionResponse> call, Response<PrescripcionResponse> response) {
                Log.i(TAG, "--->" + call.request().url());

                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getPrescripcionList(response.body().getData().getPrescriptions());
                        Log.i(TAG, "--->getPrescripcion " + response.body().getMessage());
                        Log.i(TAG, "--->getPrescripcion " + response.body().getStatus());
                    }
                }else{
                    Log.i(TAG, "--->Error en getPrescripcion " + call.request().url());
                    Log.i(TAG, "--->Error en getPrescripcion " + response.code());
                    Log.i(TAG, "--->Error en getPrescripcion " + response.errorBody());
                    Log.i(TAG, "--->Error en getPrescripcion " + response.message());
                    Log.i(TAG, "--->Error en getPrescripcion " + call.request().toString());

                }  }
            @Override
            public void onFailure(Call<PrescripcionResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void getPrescripcionItem(int id) {
        Log.i(TAG, "--->getPrescripcionITEM ");

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ConsultationInterface.class);

        rest.getPrescripcionItem(id,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<PrescripcionItemResponse>() {
            @Override
            public void onResponse(Call<PrescripcionItemResponse> call, Response<PrescripcionItemResponse> response) {
                Log.i(TAG, "--->" + call.request().url());

                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getPrescripcionItemList(response.body().getData().getPrescriptions());
                        Log.i(TAG, "--->getPrescripcionitem " + response.body().getMessage());
                        Log.i(TAG, "--->getPrescripcionitem " + response.body().getStatus());
                    }
                }else{
                    Log.i(TAG, "--->Error en getPrescripcionitem" + call.request().url());
                    Log.i(TAG, "--->Error en getPrescripcionitem " + response.code());
                    Log.i(TAG, "--->Error en getPrescripcionitem " + response.errorBody());
                    Log.i(TAG, "--->Error en getPrescripcionitem" + response.message());
                    Log.i(TAG, "--->Error en getPrescripcionItem " + call.request().toString());

                }  }
            @Override
            public void onFailure(Call<PrescripcionItemResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }
    public void getExamLab(int id) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ConsultationInterface.class);

        rest.getExamLab(id,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ExamLabResponse>() {
            @Override
            public void onResponse(Call<ExamLabResponse> call, Response<ExamLabResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getExamLab(response.body().getData().getExamLab());
                        Log.i(TAG, "--->getExamLab " + response.body().getMessage());
                        Log.i(TAG, "--->getExamLab" + response.body().getStatus());
                        Log.i(TAG, "--->getExamLab" + response.body().getData().getExamLab());
                    }
                }else{
                    Log.i(TAG, "--->Error getExamLab " + call.request().url());
                    Log.i(TAG, "--->Error getExamLab " + response.code());
                    Log.i(TAG, "--->Error getExamLab " + response.errorBody());
                    Log.i(TAG, "--->Error getExamLab" + response.message());
                    Log.i(TAG, "--->Error getExamLab " + call.request().toString());

                }  }
            @Override
            public void onFailure(Call<ExamLabResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }
    public void setRate(ScoreRequest request) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ConsultationInterface.class);

        rest.setScore(request,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ScoreResponse>() {
            @Override
            public void onResponse(Call<ScoreResponse> call, Response<ScoreResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getScore(response.body().getStatus());
                        Log.i(TAG, "--->getScore " + response.body().getMessage());
                        Log.i(TAG, "--->getScore " + response.body().getStatus());
                    }
                }else{
//                    Log.i(TAG, "--->Error en getPrescripcion " + call.request().url());
//                    Log.i(TAG, "--->Error en getPrescripcion " + response.code());
//                    Log.i(TAG, "--->Error en getPrescripcion " + response.errorBody());
//                    Log.i(TAG, "--->Error en getPrescripcion" + response.message());
//                    Log.i(TAG, "--->Error en getPrescripcion " + call.request().toString());
                }  }
            @Override
            public void onFailure(Call<ScoreResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }


}

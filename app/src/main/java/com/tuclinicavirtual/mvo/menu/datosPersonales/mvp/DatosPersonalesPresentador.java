package com.tuclinicavirtual.mvo.menu.datosPersonales.mvp;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuclinicavirtual.mvo.menu.datosPersonales.interfaces.RestdatosPersonalesInterfaces;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPacientes;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.GuardarUsuarioRequest;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.GuardarUsuarioResponse;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionResponse;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.PasswordRequest;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.PasswordResponse;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.models.Data;
import com.tuclinicavirtual.mvo.views.activitis.models.RegistroRequest;
import com.tuclinicavirtual.mvo.views.activitis.models.RegistroResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DatosPersonalesPresentador implements DatosPersonalesContrato.Presenter {

    RestdatosPersonalesInterfaces rest;
    public DatosPersonalesContrato.View view;
    private String TAG;

    public DatosPersonalesPresentador(DatosPersonalesContrato.View view) {
        this.view = view;
        TAG = this.getClass().getSimpleName();
    }


    public void getPaises() {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(RestdatosPersonalesInterfaces.class);

        rest.getPaises("bearer "+Preferences.getTokenUser()).enqueue(new Callback<InformacionResponse>() {
            @Override
            public void onResponse(Call<InformacionResponse> call, Response<InformacionResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setDataPaises(response.body().getList());
                        Log.i(TAG, "--->Token setDataPaises: " + response.body().getList().toString());
                    }
                }else{
                    Log.i(TAG, "--->Error en setDataPaises: " + call.request().url());
                    Log.i(TAG, "--->Error en setDataPaises: " + response.code());
                }  }
            @Override
            public void onFailure(Call<InformacionResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void getOcupaciones() {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(RestdatosPersonalesInterfaces.class);

        rest.getOcupacion("bearer "+Preferences.getTokenUser()).enqueue(new Callback<InformacionResponse>() {
            @Override
            public void onResponse(Call<InformacionResponse> call, Response<InformacionResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setDataOcupaciones(response.body().getList());
                        Log.i(TAG, "--->Token setDataOcupaciones: " + response.body().getList().toString());
                    }
                }else{
                    Log.i(TAG, "--->Error en setDataOcupaciones: " + call.request().url());
                    Log.i(TAG, "--->Error en setDataOcupaciones: " + response.code());
                }  }
            @Override
            public void onFailure(Call<InformacionResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void setGuardarUsuario(int id,int idp,String name, String lastname,String sex, String type, String number, String cumple, int ocupacionid, int paisid,int paisresid,int timezoneid, int relationship) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(RestdatosPersonalesInterfaces.class);
        GuardarUsuarioRequest request = new GuardarUsuarioRequest(id,idp,name,lastname,sex,type,number,cumple,ocupacionid,paisid,paisresid,timezoneid,1);

        Gson gson2 = new GsonBuilder().serializeNulls().create();
        String json = gson2.toJson(request);

        Log.i(TAG, "---> setDataPaciente" +json);

        rest.setGuardarUsuario(request,"bearer "+Preferences.getTokenUser()).enqueue(new Callback<GuardarUsuarioResponse>() {
            @Override
            public void onResponse(Call<GuardarUsuarioResponse> call, Response<GuardarUsuarioResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setRespDatosUsuario(response.body().getMessage());
                        Log.i(TAG, "--->Nuevo setDataPaciente: " + response.body().getMessage());
                        Log.i(TAG, "--->Nuevo setDataPaciente: " + response.body().getStatus());
                        Log.i(TAG, "--->Nuevo setDataPaciente: " + call.request().url());
                    }
                }else{
                    Gson gson = new GsonBuilder().create();
                    GuardarUsuarioResponse pojo = new GuardarUsuarioResponse();
                    try {
                        pojo = gson.fromJson(response.errorBody().string(), GuardarUsuarioResponse.class);
                        view.setRespDatosUsuario(pojo.getMessage());
                    } catch (IOException e) { }

                    Log.i(TAG, "--->Error en consulta registro: " + response.code());

                    Log.i(TAG, "--->Error en setDataPaciente: " + call.request().url());
                    Log.i(TAG, "--->Error en setDataPaciente: " + response.code());
                    Log.i(TAG, "--->Error en setDataPaciente: " + response.toString());
                   // Log.i(TAG, "--->Error en setDataPaciente: " + response.body().getStatus());
                }  }
            @Override
            public void onFailure(Call<GuardarUsuarioResponse> call, Throwable t) {
                view.setRespDatosUsuario("Error de conexion con el Servidor. Intente mas tarde");
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }


    public void getPaciente(int patientid) {

        rest =  RetrofitClient.getClient(Consts.BASE_URL)
                .create(RestdatosPersonalesInterfaces.class);

        rest.getPaciente(patientid,"bearer "+Preferences.getTokenUser()).enqueue(new Callback<DataPacientes>() {
            @Override
            public void onResponse(Call<DataPacientes> call, Response<DataPacientes> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        if(response.body().getData().getPatients()!=null)
                        {
                            view.getDatosPacientes(response.body().getData().getPatients().get(0));
                        }
                        Log.i(TAG, "--->Paciente: " + response.body().getMessage());
                        Log.i(TAG, "--->Paciente: " + response.body().getStatus());
                        Log.i(TAG, "--->Paciente: " + response.body().getData().getPatients().get(0));
                        Log.i(TAG, "--->Paciente: " + call.request().url());
                    }
                }else{
                    Log.i(TAG, "--->Error Paciente: " + call.request().url());
                    Log.i(TAG, "--->Error Paciente: " + response.code());
                }  }
            @Override
            public void onFailure(Call<DataPacientes> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }



    public void getRalicionFamiliar() {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(RestdatosPersonalesInterfaces.class);

        rest.getRelacionFamiliar("bearer "+Preferences.getTokenUser()).enqueue(new Callback<InformacionResponse>() {
            @Override
            public void onResponse(Call<InformacionResponse> call, Response<InformacionResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setDataParentesco(response.body().getList());
                        Log.i(TAG, "--->RalicionFamiliar: " + response.body().getList().toString());
                        Log.i(TAG, "--->RalicionFamiliar: " + response.body().getMessage());
                    }
                }else{
                    Log.i(TAG, "--->Error RalicionFamiliar: " + call.request().url());
                    Log.i(TAG, "--->Error RalicionFamiliar: " + response.code());
                }  }
            @Override
            public void onFailure(Call<InformacionResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void getZonaHoraria() {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(RestdatosPersonalesInterfaces.class);
        Log.i(TAG, "--->ZonaHoraria: ");
        rest.getZonaHoraria("bearer "+Preferences.getTokenUser()).enqueue(new Callback<InformacionResponse>() {
            @Override
            public void onResponse(Call<InformacionResponse> call, Response<InformacionResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setZonaHoraria(response.body().getList());
                        Log.i(TAG, "--->ZonaHoraria: " + response.body().getList().toString());
                        Log.i(TAG, "--->ZonaHoraria: " + response.body().getMessage());
                    }
                }else{
                    Log.i(TAG, "--->Error ZonaHoraria: " + call.request().url());
                    Log.i(TAG, "--->Error ZonaHoraria: " + response.code());
                }  }
            @Override
            public void onFailure(Call<InformacionResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void setNewPassword(PasswordRequest password) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(RestdatosPersonalesInterfaces.class);
        Log.i(TAG, "--->setNewPassword"+password.toString());

        rest.setNewPassword(password,"bearer "+Preferences.getTokenUser()).enqueue(new Callback<PasswordResponse>() {
            @Override
            public void onResponse(Call<PasswordResponse> call, Response<PasswordResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setRespPasswordNew(response.body().getStatus(),response.body().getMessage());
                        Log.i(TAG, "--->setNewPassword");
                        Log.i(TAG, "--->setNewPassword" + response.body().getMessage());
                    }
                }else{
                    view.setRespPasswordNew("","fallo");
                    Log.i(TAG, "--->setNewPassword" + call.request().url());
                    Log.i(TAG, "--->setNewPassword" + response.code());
                }  }
            @Override
            public void onFailure(Call<PasswordResponse> call, Throwable t) {
                view.setRespPasswordNew("","mal fallo");
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }


}

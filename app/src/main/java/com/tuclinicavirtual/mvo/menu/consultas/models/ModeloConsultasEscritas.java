package com.tuclinicavirtual.mvo.menu.consultas.models;

public class ModeloConsultasEscritas {

  String nombre;
  String hora;
  String fecha;
  String especialidad;
  String estado;


    public ModeloConsultasEscritas(String nombre, String hora, String fecha, String especialidad, String estado) {
        this.nombre = nombre;
        this.hora = hora;
        this.fecha = fecha;
        this.especialidad = especialidad;
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}

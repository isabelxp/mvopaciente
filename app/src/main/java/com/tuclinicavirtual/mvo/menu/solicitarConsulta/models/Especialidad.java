package com.tuclinicavirtual.mvo.menu.solicitarConsulta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Especialidad {

    @SerializedName("specialitiesid")
    @Expose
    private Integer specialitiesid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("acronym")
    @Expose
    private String acronym;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("duration_consultation")
    @Expose
    private Integer durationConsultation;
    @SerializedName("duration_diagnostic:")
    @Expose
    private Integer durationDiagnostic;
    @SerializedName("consultation_cost:")
    @Expose
    private Integer consultationCost;
    @SerializedName("visible_web")
    @Expose
    private String visibleWeb;
    @SerializedName("status")
    @Expose
    private String status;

    @Override
    public String toString() {
        return "Especialidad{" +
                "specialitiesid=" + specialitiesid +
                ", name='" + name + '\'' +
                ", acronym='" + acronym + '\'' +
                ", description='" + description + '\'' +
                ", durationConsultation=" + durationConsultation +
                ", durationDiagnostic=" + durationDiagnostic +
                ", consultationCost=" + consultationCost +
                ", visibleWeb='" + visibleWeb + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public Integer getSpecialitiesid() {
        return specialitiesid;
    }

    public void setSpecialitiesid(Integer specialitiesid) {
        this.specialitiesid = specialitiesid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDurationConsultation() {
        return durationConsultation;
    }

    public void setDurationConsultation(Integer durationConsultation) {
        this.durationConsultation = durationConsultation;
    }

    public Integer getDurationDiagnostic() {
        return durationDiagnostic;
    }

    public void setDurationDiagnostic(Integer durationDiagnostic) {
        this.durationDiagnostic = durationDiagnostic;
    }

    public Integer getConsultationCost() {
        return consultationCost;
    }

    public void setConsultationCost(Integer consultationCost) {
        this.consultationCost = consultationCost;
    }

    public String getVisibleWeb() {
        return visibleWeb;
    }

    public void setVisibleWeb(String visibleWeb) {
        this.visibleWeb = visibleWeb;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

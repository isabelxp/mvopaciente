package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataPrescripcion {
    @SerializedName("prescriptions")
    @Expose
    private List<Prescripcion> prescriptions = null;

    public List<Prescripcion> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<Prescripcion> prescriptions) {
        this.prescriptions = prescriptions;
    }

}

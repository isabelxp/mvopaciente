package com.tuclinicavirtual.mvo.menu.antecedentes.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.antecedents;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.Antecedentes2;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.interfaces.GuardarAntecedentesInter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AntecedentesAdapter extends RecyclerView.Adapter<AntecedentesAdapter.ViewHolder> {

    List<Antecedentes2> listAntecedente=new ArrayList<>();

    public static List<antecedents> listAuxiliar=new ArrayList<>();
    public static List<antecedents> listAuxiliarConsultas=new ArrayList<>();
    public static String peso;
    public static String altura;
    public static boolean errorEdit=false;
    public static boolean errorEditStatura=false;

    public static String stringAntecedentePeso;


    public AntecedentesAdapter(List<Antecedentes2> listAntecedente) {
        this.listAntecedente=listAntecedente;
        listAuxiliar.clear();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_antecedente, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


         holder.bindData(listAntecedente.get(position));
         if(listAntecedente.get(position).getName().equals("Peso (kgs)"))
         {

             Log.d("AntecedentesAdapter","true : "+listAntecedente.get(position).getName());
         }
        if(listAntecedente.get(position).getName().equals("Estatura (cms)"))
         {
            Log.d("AntecedentesAdapter","true : "+listAntecedente.get(position).getName());
         }


         holder.edit_antecedente.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                listAuxiliar.get(holder.getAdapterPosition()).setObservation(holder.edit_antecedente.getText().toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.id_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.id_switch.isChecked())
                {
                    listAuxiliar.get(position).setAnswer(1);
                }else
                {
                    listAuxiliar.get(position).setAnswer(0);
                }
            }
        });



    }

    @Override
    public int getItemCount() {

        return listAntecedente.size();
    }





    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.edit_antecedente)
        EditText edit_antecedente;
        @BindView(R.id.text_antecedente)
        TextView text_antecedente;
        @BindView(R.id.id_switch)
        Switch id_switch;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }

        public void bindData(final Antecedentes2 antecedente) {
            String observacion;

            if (antecedente.getObservation()!=null)
            {
                observacion=antecedente.getObservation().toString();
            }else
            {
                observacion="";
            }

            listAuxiliar.add(new antecedents(antecedente.getRecordid(), antecedente.getAntecedentid(), antecedente.getAnswer(),  observacion));

            if(antecedente.getName().equals("Peso (kgs)") || antecedente.getName().equals("Estatura (cms)"))
            {
                text_antecedente.setText(antecedente.getName()+" * ");
            }else
            {
                Log.d("bindData",""+antecedente.getName());
                text_antecedente.setText(antecedente.getName());
            }

            if(antecedente.getObservation()!=null)
            {
               edit_antecedente.setText(""+antecedente.getObservation());
            }else
            {
                edit_antecedente.setText("");
            }

            if(antecedente.getName().equals("Peso (kgs)"))
            {
                if(antecedente.getObservation()==null)
                {
                    edit_antecedente.setError("Indicar peso");

                }
                if(antecedente.getObservation()!=null)
                {
                if(antecedente.getObservation().length()>3)
                {
                    String sCadena = ""+antecedente.getObservation();
                    String sSubCadena = sCadena.substring(0,3);
                    Log.d("bindData",""+sSubCadena);
                    edit_antecedente.setText(sSubCadena);
                }
                }
                edit_antecedente.setInputType(InputType.TYPE_CLASS_NUMBER);
                edit_antecedente.setFilters(new InputFilter[] {new InputFilter.LengthFilter(3)});
            }
            if(antecedente.getName().equals("Estatura (cms)"))
            {
                if(antecedente.getObservation()==null)
                {
                    edit_antecedente.setError("Indicar estatura");
                }

                edit_antecedente.setInputType(InputType.TYPE_CLASS_NUMBER);
                edit_antecedente.setFilters(new InputFilter[] {new InputFilter.LengthFilter(3)});

                if(antecedente.getObservation()!=null)
                {
                    if(antecedente.getObservation().length()>3)
                    {
                        String sCadena = ""+antecedente.getObservation();
                        String sSubCadena = sCadena.substring(0,3);
                        edit_antecedente.setText(sSubCadena);
                    }

                }
            }
            if(antecedente.getName().equals("Tiene hijos"))
            {
                    edit_antecedente.setError(null);
            }
            if(antecedente.getName().equals("Fuma"))
            {
                edit_antecedente.setInputType(InputType.TYPE_CLASS_TEXT);
                edit_antecedente.setFilters(new InputFilter[] {new InputFilter.LengthFilter(60)});
            }
            if(antecedente.getName().equals("Hipertens@"))
            {
                edit_antecedente.setInputType(InputType.TYPE_CLASS_TEXT);
                edit_antecedente.setFilters(new InputFilter[] {new InputFilter.LengthFilter(60)});
            }

            if(antecedente.getAnswer()==1)
            {
                id_switch.setChecked(true);
                id_switch.isChecked();
            }
            else
                id_switch.setChecked(false);
        }

    }




}

package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReporteRequest {
    @SerializedName("consultationid")
    @Expose
    private Integer consultationid;
    @SerializedName("utc")
    @Expose
    private String utc;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("report_typeid")
    @Expose
    private Integer reportTypeid;
    @SerializedName("observation")
    @Expose
    private String observation;

    public ReporteRequest(Integer consultationid, String utc, Integer userid, Integer reportTypeid, String observation) {
        this.consultationid = consultationid;
        this.utc = utc;
        this.userid = userid;
        this.reportTypeid = reportTypeid;
        this.observation = observation;
    }

    @Override
    public String toString() {
        return "ReporteRequest{" +
                "consultationid=" + consultationid +
                ", utc='" + utc + '\'' +
                ", userid=" + userid +
                ", reportTypeid=" + reportTypeid +
                ", observation='" + observation + '\'' +
                '}';
    }

    public Integer getConsultationid() {
        return consultationid;
    }

    public void setConsultationid(Integer consultationid) {
        this.consultationid = consultationid;
    }

    public String getUtc() {
        return utc;
    }

    public void setUtc(String utc) {
        this.utc = utc;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getReportTypeid() {
        return reportTypeid;
    }

    public void setReportTypeid(Integer reportTypeid) {
        this.reportTypeid = reportTypeid;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
}

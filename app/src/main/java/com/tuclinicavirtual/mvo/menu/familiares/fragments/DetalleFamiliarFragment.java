package com.tuclinicavirtual.mvo.menu.familiares.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.antecedentes.MisAntecedentesFragment;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionList;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesContrato;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesPresentador;
import com.tuclinicavirtual.mvo.menu.examenes.fragments.MisExamenesFragment;
import com.tuclinicavirtual.mvo.menu.familiares.adapters.FamiliaresAdapter;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetalleFamiliarFragment extends Fragment implements DatosPersonalesContrato.View{


    public DetalleFamiliarFragment() {
    }

    @BindView(R.id.spinner_tipo_ident)
    Spinner spinner_tipo_ident;

    @BindView(R.id.spinner_genero)
    Spinner spinner_genero;

    @BindView(R.id.spinner_pais)
    Spinner spinner_pais;

    @BindView(R.id.spinner_pais_reci)
    Spinner spinner_pais_reci;

    @BindView(R.id.spinner_ocupacion)
    Spinner spinner_ocupacion;

    @BindView(R.id.spinner_parentesco)
    Spinner spinner_parentesco;

    @BindView(R.id.spinner_zona)
    Spinner spinner_zona;

    @BindView(R.id.edit_numero_ident)
    EditText edit_numero_ident;

    @BindView(R.id.edit_nombre)
    EditText edit_nombre;

    @BindView(R.id.edit_apellido)
    EditText edit_apellido;

    @BindView(R.id.edit_fecha)
    EditText edit_fecha;


    List<InformacionList> listOcupacion = new ArrayList<InformacionList>();
    List<InformacionList> listPais = new ArrayList<InformacionList>();
    List<InformacionList> listParentesco = new ArrayList<InformacionList>();

    List<String> listPaisString = new ArrayList<String>();
    List<String> listOcupString = new ArrayList<String>();
    List<String> listParentescoString = new ArrayList<String>();


    List<String> listPa = new ArrayList<String>();

    //id de los paises y ocupaciones

    List<Integer> listIdPais= new ArrayList<Integer>();
    List<Integer> listIdPaisreci= new ArrayList<Integer>();
    List<Integer> listIdOcupacion= new ArrayList<Integer>();
    List<Integer> listIdParentesco= new ArrayList<Integer>();

    public static int idPacienteFamiliar;
    public static int idUsuarioFamiliar;

    private int num_ocupacion=1000;
    private int num_pais=1000;
    private int num_pais_reci=1000;
    private int num_parentesco=1000;

    private int num_ocupacionAuxi=1000;
    private int num_paisAuxi=1000;
    private int num_pais_reciAuxi=1000;
    private int num_parentescoAuxi=1000;

    DatosPersonalesPresentador presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_detalle_familiar, container, false);
        Log.d("-->Detalle","Familiar"+ FamiliaresAdapter.getAuxi().toString());
        ButterKnife.bind(this, v);
        initUI();

        num_parentescoAuxi = FamiliaresAdapter.getAuxi().getPatientid();
        HomeActivity.setDetalleConsulta(false);
        idPacienteFamiliar=FamiliaresAdapter.getAuxi().getPatientid();

        return v;
    }
    public  String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    private static String deleteChar(String text, int charPos) {
        return new StringBuilder(text).deleteCharAt(charPos).toString();
    }

    public void initUI()
    {
        presenter = new DatosPersonalesPresentador(this);
        edit_numero_ident.setText(FamiliaresAdapter.getAuxi().getIdentificationNumber());
        edit_nombre.setText(FamiliaresAdapter.getAuxi().getName());
        edit_apellido.setText(FamiliaresAdapter.getAuxi().getLastname());
        edit_fecha.setText(parseDateToddMMyyyy(FamiliaresAdapter.getAuxi().getBirthdate()));
        spinner_zona.setPrompt(FamiliaresAdapter.getAuxi().getTimezoneName());
       // spinner_tipo_ident.setPrompt(FamiliaresAdapter.getAuxi().getIdentificationType());
        //spinner_parentesco.setPrompt(FamiliaresAdapter.getAuxi().getRelationshipName());
       // spinner_genero.setPrompt(FamiliaresAdapter.getAuxi().getSex());
        //spinner_pais.setPrompt(FamiliaresAdapter.getAuxi().getCountryName());
        spinner_pais_reci.setPrompt(FamiliaresAdapter.getAuxi().getResCountryName());
        spinner_ocupacion.setPrompt(FamiliaresAdapter.getAuxi().getOccupationName());


        addParentesco();
        addTipo();
        addGenero();
        addPais();
        addPaisReci();
        addOcupacion();
        addZona();

    }

    public void addGenero() {

        List<String> list = new ArrayList<String>();
        list.add(FamiliaresAdapter.getAuxi().getSex());


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_genero.setAdapter(dataAdapter);

    }
    @OnClick(R.id.button_exam)
    public void goExam() {
        initFragment(new ExamFamilyFragment());

    }
    @OnClick(R.id.button_antecedentes)
    public void goAntecedentes() {
        initFragment(new FamilyAntecedentsFragment());


    }
    public void initFragment(Fragment fragment)
    {
        FragmentTransaction ft =  getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }
    public void addZona() {

        List<String> list = new ArrayList<String>();
        list.add(FamiliaresAdapter.getAuxi().getTimezoneName());

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_zona.setAdapter(dataAdapter);

    }


    @Override
    public void setDataPaises(List<InformacionList> dato) {
        listPais = dato;
        for (int i=0;i<listPais.size();i++)
        {
            listPaisString.add(listPais.get(i).getValuename());
            listIdPais.add(listPais.get(i).getValueid());
            listIdPaisreci.add(listPais.get(i).getValueid());
        }
        addPais();
        addPaisReci();

        buscarPaisOrigen(FamiliaresAdapter.getAuxi().getCountryid());
        buscarPaisReci(FamiliaresAdapter.getAuxi().getResCountryid());
    }

    @Override
    public void setDataOcupaciones(List<InformacionList> dato) {
        listOcupacion = dato;
        for (int i=0;i<listOcupacion.size();i++)
        {
            listOcupString.add(listOcupacion.get(i).getValuename());
            listIdOcupacion.add(listOcupacion.get(i).getValueid());
        }
        addOcupacion();
        buscarDoc(FamiliaresAdapter.getAuxi().getIdentificationType());
        buscarOcupaciones(FamiliaresAdapter.getAuxi().getOccupationid());
        buscarSex(FamiliaresAdapter.getAuxi().getSex());

        Log.d("--->","Ocupaciones"+dato.toString());
    }

    @Override
    public void setDataParentesco(List<InformacionList> dato) {
        listParentesco = dato;

        for (int i=0;i<listParentesco.size();i++)
        {

            listPa.add(listParentesco.get(i).getValuename());
            listIdParentesco.add(listParentesco.get(i).getValueid());
            //Log.d("-------->","___"+listParentesco.get(i));
          //  Log.d("-------->","AQUI"+listParentescoString.get(i).toString());
        }
        addParentesco();
        buscarParentesco(FamiliaresAdapter.getAuxi().getRelationshipid());
    }

    @Override
    public void setZonaHoraria(List<InformacionList> dato) {

    }


    public void addTipo() {

        List<String> list = new ArrayList<String>();

        list.add(FamiliaresAdapter.getAuxi().getIdentificationType());

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tipo_ident.setAdapter(dataAdapter);

    }

    public void addOcupacion() {


        listOcupString.add(FamiliaresAdapter.getAuxi().getOccupationName());
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listOcupString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_ocupacion.setAdapter(dataAdapter);

        spinner_ocupacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void addPais() {


        listPaisString.add(FamiliaresAdapter.getAuxi().getCountryName());

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listPaisString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pais.setAdapter(dataAdapter);


        spinner_pais.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void addPaisReci() {

        List<String> list = new ArrayList<String>();
        list.add(FamiliaresAdapter.getAuxi().getResCountryName());

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pais_reci.setAdapter(dataAdapter);


        spinner_pais_reci.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    public void addParentesco() {

        listPa.add(FamiliaresAdapter.getAuxi().getRelationshipName());

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, listPa);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_parentesco.setAdapter(dataAdapter);

        spinner_parentesco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    public void buscarPaisOrigen(int numPais)
    {
        for (int i=0;i<listIdPais.size();i++)
        {
            if(numPais==listIdPais.get(i))
            {
                spinner_pais.setSelection(i);
                break;
            }
        }
    }
    public void buscarSex(String sex)
    {
        if(sex=="Femenino")
        {
            spinner_genero.setSelection(0);
        }else
        {
            spinner_genero.setSelection(1);
        }
    }

    public void buscarDoc(String doc)
    {
        if(doc=="Documento de Identidad")
        {
            spinner_tipo_ident.setSelection(1);
        }else
        {
            spinner_tipo_ident.setSelection(0);
        }
    }

    public void buscarPaisReci(int numPais)
    {

        for (int i=0;i<listIdPaisreci.size();i++)
        {
            if(numPais==listIdPaisreci.get(i))
            {
                spinner_pais_reci.setSelection(i);
                break;
            }
        }

    }
    public void buscarParentesco(int num)
    {

        for (int i=0;i<listIdParentesco.size();i++)
        {
            if(num==listIdParentesco.get(i))
            {
                spinner_parentesco.setSelection(i);
                break;
            }
        }

    }
    public void buscarOcupaciones(int numOcu)
    {

        for (int i=0;i<listIdOcupacion.size();i++)
        {
            if(numOcu==listIdOcupacion.get(i))
            {
                spinner_ocupacion.setSelection(i);
                break;
            }
        }

    }

    @Override
    public void setRespDatosUsuario(String sms) {

    }

    @Override
    public void getDatosPacientes(Patient a) {

    }

    @Override
    public void setRespPasswordNew(String resp, String Token) {

    }
}

package com.tuclinicavirtual.mvo.menu.familiares.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.familiares.fragments.DetalleFamiliarFragment;
import com.tuclinicavirtual.mvo.menu.familiares.fragments.MisFamiliaresFragment;
import com.tuclinicavirtual.mvo.menu.familiares.models.DeleteRequestFamilia;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaRequest;
import com.tuclinicavirtual.mvo.menu.familiares.models.Family;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresPresentador;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static org.greenrobot.eventbus.EventBus.TAG;

public class FamiliaresAdapter extends RecyclerView.Adapter<FamiliaresAdapter.ViewHolder> implements FamiliaresContrato.View{

    List<Patient> listFamilia;
    Context context;
    public static Patient auxi;

    public static Patient getAuxi() {
        return auxi;
    }

    public static void setAuxi(Patient auxi) {
        FamiliaresAdapter.auxi = auxi;
    }

    FamiliaresPresentador presenter;

    public FamiliaresAdapter(List<Patient> listFamilia, Context context) {
        this.listFamilia=listFamilia;
        this.context=context;

    }

    @Override
    public FamiliaresAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_familiares, parent, false);
        presenter=new FamiliaresPresentador(FamiliaresAdapter.this);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FamiliaresAdapter.ViewHolder holder,final int position) {
        if(listFamilia.get(position).getPatientid()!= Preferences.getIDPatientid())
        {
            holder.bindData(listFamilia.get(position),context);

            holder.img_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog("¿Deseas eliminar a "+ listFamilia.get(position).getName() +"? ",listFamilia.get(position).getTimezoneName(),listFamilia.get(position).getPatientid());
                }
            });
        }


        Log.i(TAG, "--->Token Familiares: id" + listFamilia.get(position).getPatientid());
        Log.i(TAG, "--->Token Familiares: id" + listFamilia.get(position).getTimezoneName());
    }

    @Override
    public int getItemCount() {

        return listFamilia.size();
    }


    @Override
    public void getListFamiliares(List<Patient> familiar) {

    }

    @Override
    public void setFamiliainfo(String rest) {

    }

    @Override
    public void setDeleteFamiliar(String rest) {
            Log.d("setDeleteFamiliar","--->"+rest);
            if(rest.equals("500"))
            {
                dialog("Su familiar tiene una consulta asociada, no se puede borrar.");
            }else
            {
                FragmentTransaction ft =  ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.contenedor, new MisFamiliaresFragment());
                ft.addToBackStack(null);
                ft.commit();

            }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_nombre_familiar)
        TextView text_nombre_familiar;
        @BindView(R.id.text_parentesco_familiar)
        TextView text_parentesco_familiar;
        Context context;
        @BindView(R.id.layout_detalle_familiar)
        LinearLayout layout_detalle_familiar;
        @BindView(R.id.img_delete)
        ImageView img_delete;
        @BindView(R.id.imageViewFoto2)
        CircleImageView imageViewFoto;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }

        public void bindData(final Patient c, Context contex) {
            context=contex;


            if (c.getName()=="")
            {
                text_nombre_familiar.setText("No disponible");
                text_parentesco_familiar.setText("No disponible");
            }else
            text_nombre_familiar.setText(c.getName());
            text_parentesco_familiar.setText(c.getRelationshipName());
            layout_detalle_familiar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAuxi(c);
                    FragmentTransaction ft =  ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.contenedor, new DetalleFamiliarFragment());
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });

            if(c.getSex().equals("Femenino"))
            {
                imageViewFoto.setImageDrawable(contex.getDrawable(R.mipmap.ic_icon_women));
            }
            if(c.getSex().equals("Masculino"))
            {
                imageViewFoto.setImageDrawable(contex.getDrawable(R.mipmap.ic_icon_man));
            }


        }

    }

    public void dialog(String resp, final String utc, final int idpaciente)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle(resp);

        builder.setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                List<Family> families=new ArrayList<>();
                families.add(new Family(utc,idpaciente));
                presenter.deleteFamiliar(new DeleteRequestFamilia(77,families));
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void dialog(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(resp);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }


}

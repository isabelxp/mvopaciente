package com.tuclinicavirtual.mvo.menu.familiares.mvp;

import android.util.Log;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuclinicavirtual.mvo.menu.datosPersonales.interfaces.RestdatosPersonalesInterfaces;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPacientes;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionResponse;
import com.tuclinicavirtual.mvo.menu.familiares.interfaces.FamiliaresInterfaces;
import com.tuclinicavirtual.mvo.menu.familiares.models.DeleteRequestFamilia;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaRequest;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaResponse;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.models.RegistroResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FamiliaresPresentador implements FamiliaresContrato.Presenter{

    FamiliaresInterfaces rest;
    public FamiliaresContrato.View view;
    private String TAG;

    public FamiliaresPresentador(FamiliaresContrato.View view) {
        this.view = view;
        TAG = this.getClass().getSimpleName();
    }

    public void setFamiliar(FamiliaRequest familiar)
    {
        rest = RetrofitClient.getClient(Consts.BASE_URL).create(FamiliaresInterfaces.class);

        Gson gson2 = new GsonBuilder().serializeNulls().create();
        String json = gson2.toJson(familiar);

        Log.i("---->", "Familia json" +json);

        rest.setFamiliar(familiar,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<FamiliaResponse>() {
            @Override
            public void onResponse(Call<FamiliaResponse> call, Response<FamiliaResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setFamiliainfo(response.body().getMessage());
                        Log.i(TAG, "--->setFamiliares: " + call.request().url());
                        Log.i(TAG, "--->setFamiliares: " + response.body().getMessage());
                        Log.i(TAG, "--->setFamiliares: " + response.body().getStatus());
                    }
                }else{
                    Log.i(TAG, "--->Error en Familiares: " + call.request().url());
                    Log.i(TAG, "--->Error en Familiares: " + response.code());
                    Log.i(TAG, "--->Error en Familiares: " + response.message());
                    Log.i(TAG, "--->Error en Familiares: " + response.body().toString());
                    //Log.i(TAG, "--->Error en setFamiliares: " + response.g);
                }  }
            @Override
            public void onFailure(Call<FamiliaResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }
    public void deleteFamiliar(DeleteRequestFamilia familiar)
    {
        rest = RetrofitClient.getClient(Consts.BASE_URL).create(FamiliaresInterfaces.class);

        Gson gson2 = new GsonBuilder().serializeNulls().create();
        String json = gson2.toJson(familiar);

        Log.i("---->", "Familia json" +json);

        rest.deleteFamiliar(familiar,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<FamiliaResponse>() {
            @Override
            public void onResponse(Call<FamiliaResponse> call, Response<FamiliaResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setDeleteFamiliar(response.body().getMessage());
                        Log.i(TAG, "--->setFamiliares: " + call.request().url());
                        Log.i(TAG, "--->setFamiliares: " + response.body().getMessage());
                        Log.i(TAG, "--->setFamiliares: " + response.body().getStatus());
                    }
                }else{
                    Gson gson = new GsonBuilder().create();
                    FamiliaResponse pojo = new FamiliaResponse();
                    try {
                        pojo = gson.fromJson(response.errorBody().string(), FamiliaResponse.class);
                        view.setDeleteFamiliar("500");
                        Log.i(TAG, "--->Error en Familiares: " + pojo.getMessage());

                    } catch (IOException e) { }

                    Log.i(TAG, "--->Error en Familiares: " + call.request().url());
                    Log.i(TAG, "--->Error en Familiares: " + response.code());
                    Log.i(TAG, "--->Error en Familiares: " + response.message());
                   // Log.i(TAG, "--->Error en Familiares: " + response.body().toString());
                    //Log.i(TAG, "--->Error en setFamiliares: " + response.g);
                }  }
            @Override
            public void onFailure(Call<FamiliaResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }


    public void getFamiliares(int id)
    {
        rest = RetrofitClient.getClient(Consts.BASE_URL).create(FamiliaresInterfaces.class);

        rest.getFamiliares(id,"bearer "+Preferences.getTokenUser()).enqueue(new Callback<DataPacientes>() {
            @Override
            public void onResponse(Call<DataPacientes> call, Response<DataPacientes> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getListFamiliares(response.body().getData().getPatients());
                        Log.i(TAG, "--->Token setFamiliares: " + response.body().getData().toString());

                        Log.i(TAG, "--->Token setFamiliares: " + response.body().getData().getPatients().get(0).toString());
                    }
                }else{


                    Log.i(TAG, "--->Error en setFamiliares: " + call.request().url());
                    Log.i(TAG, "--->Error en setFamiliares: " + response.code());
                }  }
            @Override
            public void onFailure(Call<DataPacientes> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

}

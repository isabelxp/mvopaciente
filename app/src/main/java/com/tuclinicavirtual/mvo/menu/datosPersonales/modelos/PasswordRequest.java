package com.tuclinicavirtual.mvo.menu.datosPersonales.modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PasswordRequest {
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("newpassword")
    @Expose
    private String newpassword;
    @SerializedName("roleid")
    @Expose
    private int roleid;

    @Override
    public String toString() {
        return "PasswordRequest{" +
                "username='" + username + '\'' +
                ", newpassword='" + newpassword + '\'' +
                '}';
    }

    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public PasswordRequest(String username, String newpassword, int roleid) {
        this.username = username;
        this.newpassword = newpassword;
        this.roleid = roleid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }
}

package com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReprogramarRequest {

    @SerializedName("utc")
    @Expose
    private String utc;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("consultationid")
    @Expose
    private Integer consultationid;
    @SerializedName("patientid")
    @Expose
    private Integer patientid;
    @SerializedName("doctorid")
    @Expose
    private Integer doctorid;
    @SerializedName("specialityid")
    @Expose
    private Integer specialityid;
    @SerializedName("consultation_typeid")
    @Expose
    private Integer consultationTypeid;
    @SerializedName("paymentid")
    @Expose
    private Integer paymentid;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("consultation_date")
    @Expose
    private String consultationDate;
    @SerializedName("consultation_hour")
    @Expose
    private String consultationHour;

    public ReprogramarRequest(String utc, Integer userid, Integer consultationid, Integer patientid, Integer doctorid, Integer specialityid, Integer consultationTypeid, Integer paymentid, String reason, String consultationDate, String consultationHour) {
        this.utc = utc;
        this.userid = userid;
        this.consultationid = consultationid;
        this.patientid = patientid;
        this.doctorid = doctorid;
        this.specialityid = specialityid;
        this.consultationTypeid = consultationTypeid;
        this.paymentid = paymentid;
        this.reason = reason;
        this.consultationDate = consultationDate;
        this.consultationHour = consultationHour;
    }

    public String getUtc() {
        return utc;
    }

    public void setUtc(String utc) {
        this.utc = utc;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getConsultationid() {
        return consultationid;
    }

    public void setConsultationid(Integer consultationid) {
        this.consultationid = consultationid;
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    public Integer getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Integer doctorid) {
        this.doctorid = doctorid;
    }

    public Integer getSpecialityid() {
        return specialityid;
    }

    public void setSpecialityid(Integer specialityid) {
        this.specialityid = specialityid;
    }

    public Integer getConsultationTypeid() {
        return consultationTypeid;
    }

    public void setConsultationTypeid(Integer consultationTypeid) {
        this.consultationTypeid = consultationTypeid;
    }

    public Integer getPaymentid() {
        return paymentid;
    }

    public void setPaymentid(Integer paymentid) {
        this.paymentid = paymentid;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getConsultationDate() {
        return consultationDate;
    }

    public void setConsultationDate(String consultationDate) {
        this.consultationDate = consultationDate;
    }

    public String getConsultationHour() {
        return consultationHour;
    }

    public void setConsultationHour(String consultationHour) {
        this.consultationHour = consultationHour;
    }
}

package com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.MisConsultasFragment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter.ListEspecialidadAdapter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultaPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultasContrato;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class EspecialidadFragment extends Fragment implements SolicitarConsultasContrato.View {


    public EspecialidadFragment() {

    }
    @BindView(R.id.recycler)
    RecyclerView recycler;
    ListEspecialidadAdapter adaptador;
    ArrayList<Especialidad> listEspecialidad=new ArrayList<>();
    SolicitarConsultaPresentador presentador;
    ProgressDialog progress;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_especialidad, container, false);
        ButterKnife.bind(this, v);

            initUI();
            //progress.show();
            presentador.getEspecialistas();

        return v;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String num) {

        Log.d("pasarSiguiente","Medico"+num);

        if(num.equals("0"))
        {
            presentador.getEspecialistas();
        }

    };

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onAttach(Context context) {
          super.onAttach(context);

    }



    public void initUI()
    {
        progress = new ProgressDialog(getActivity());
        progress.setTitle(getActivity().getString(R.string.registro_cargando));
        presentador=new SolicitarConsultaPresentador(this);
        listEspecialidad.clear();
        LinearLayoutManager reci = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(reci);
        recycler.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recycler.setHasFixedSize(true);

    }

    @Override
    public void getListEspecialidad(List<Especialidad> especialistas) {
        Log.d("Especialidad","---->"+especialistas.toString());
        progress.cancel();
        adaptador= new ListEspecialidadAdapter(especialistas,getActivity());
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

    @Override
    public void getListDoctor(List<Doctor> doctor) {

    }

    @Override
    public void getHorarioDoctor(List<Availabilty> horarioDoctor) {

    }

    @Override
    public void getSolicitarConsulta(DataSolicitudConsulta code) {

    }

    @Override
    public void getPaymentsResp(DataPayment dataPayment) {

    }

    @Override
    public void getRespDoctorFail(String text) {

    }

    @Override
    public void getShoulderDoctor(List<Schedule> hours) {

    }

    @Override
    public void getError(String dato) {

    }


}

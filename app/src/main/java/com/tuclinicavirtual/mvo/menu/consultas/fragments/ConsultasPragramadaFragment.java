package com.tuclinicavirtual.mvo.menu.consultas.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasProgramada;
import com.tuclinicavirtual.mvo.menu.consultas.interfaces.DetalleConsultaInterfaz;
import com.tuclinicavirtual.mvo.menu.consultas.models.Consultation;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostic;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostico;
import com.tuclinicavirtual.mvo.menu.consultas.models.EstatusResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ExamLab;
import com.tuclinicavirtual.mvo.menu.consultas.models.Prescripcion;
import com.tuclinicavirtual.mvo.menu.consultas.models.PrescripcionItem;
import com.tuclinicavirtual.mvo.menu.consultas.mvp.ConsultasContrato;
import com.tuclinicavirtual.mvo.menu.consultas.mvp.ConsultasPresentador;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresPresentador;
import com.tuclinicavirtual.mvo.utils.Preferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ConsultasPragramadaFragment extends Fragment implements DetalleConsultaInterfaz,ConsultasContrato.View,FamiliaresContrato.View {


    public ConsultasPragramadaFragment() {

    }
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.search2)
    EditText search;

    AdapterConsultasProgramada adaptador;
    List<Consultation> listConsulta=new ArrayList<>();
    ConsultasPresentador presentador;
    ProgressDialog progress;
    Context context;
    @BindView(R.id.spinner)
    Spinner spinner;
    List<String> listPaciente=new ArrayList<>();
    List<Integer> idPacientes=new ArrayList<>();
    List<Integer> idUser=new ArrayList<>();
    List<String> utcPaciente=new ArrayList<>();

    public int idPacienteMisCOnsultas;
    public String stringPacienteMisCOnsultas;

    FamiliaresPresentador presentadorFamiliares;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_consultas_escritas, container, false);
        ButterKnife.bind(this, v);
        presentadorFamiliares=new FamiliaresPresentador(this);
        presentadorFamiliares.getFamiliares(Preferences.getIDUser());
        initUI();

        return v;
    }
    public void initUI()
    {
        progress = new ProgressDialog(getActivity());
        progress.setTitle(getActivity().getString(R.string.registro_cargando));
        presentador=new ConsultasPresentador(this);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(reci);
        recycler.setHasFixedSize(true);
        addTextListener(listConsulta);
    }



    public void initUISpinner(final List<String> familiar)
    {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, familiar);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        if(familiar!=null)
        {
            for(int i=0;i<familiar.size();i++)
            {
                if(familiar.get(i).contains("Titular"))
                {
                    spinner.setSelection(i);
                }
            }

        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                idPacienteMisCOnsultas=idPacientes.get(pos);
                stringPacienteMisCOnsultas=utcPaciente.get(pos);
                EventBus.getDefault().post("MisConsultas");

            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addTextListener(final List<Consultation> dato){

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();

                final List<Consultation> datoAuxi = new ArrayList<>();

                for (int i = 0; i < dato.size(); i++) {

                    final String text = dato.get(i).getDoctorName().toLowerCase();
                    if (text.contains(query)) {
                        datoAuxi.add(dato.get(i));
                    }
                }

                adaptador= new AdapterConsultasProgramada(datoAuxi,getActivity(),ConsultasPragramadaFragment.this);
                recycler.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        //presentador.getConsultation(Preferences.getIDPatientid(),2);
    }

    @Override
    public void onPause() {
        super.onPause();
       // presentador.getConsultation(Preferences.getIDPatientid(),2);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }



    @Override
    public void getDiagnosticoList(List<Diagnostico> dato) {
       // Log.d("getDiagnosticoList",""+dato.get(0).getDiagnostic());
    }

    @Override
    public void getPrescripcionList(List<Prescripcion> dato) {
       // Log.d("getPrescripcionList",""+dato.get(0).getName());
    }

    @Override
    public void getPrescripcionItemList(List<PrescripcionItem> dato) {

    }


    @Override
    public void getExamLab(List<ExamLab> dato) {

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String num) {

        if(num.equals("noti"))
        {
            presentador.getConsultation(Preferences.getIDPatientid(),2,stringPacienteMisCOnsultas);
        }
        if(num.equals("200"))
        {
            presentador.getConsultation(Preferences.getIDPatientid(),2,stringPacienteMisCOnsultas);
        }
        if(num.equals("MisConsultas"))
        {
            listConsulta.clear();
            adaptador= new AdapterConsultasProgramada(listConsulta,getActivity(),ConsultasPragramadaFragment.this);
            recycler.setAdapter(adaptador);
            adaptador.notifyDataSetChanged();
            presentador.getConsultation(idPacienteMisCOnsultas,2,stringPacienteMisCOnsultas);
           // presentador.getConsultation(MisConsultasFragment.idPacienteMisCOnsultas,2);
        }

    };

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        //presentador.getConsultation(Preferences.getIDPatientid(),2);
    }


    @Override
    public void getConsultation(List<Consultation> dato) {

        listConsulta.clear();
        for (int i=0;i<dato.size();i++)
        {
            if(dato.get(i).getTypeid()!=1)
            {
                listConsulta.add(dato.get(i));
            }
        }
        adaptador= new AdapterConsultasProgramada(listConsulta,getActivity(),ConsultasPragramadaFragment.this);
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();

    }

    @Override
    public void getReporte(String dato) {

    }

    @Override
    public void getScore(String sms) {

    }

    @Override
    public void getDiagnosticoItem(List<Diagnostic> dato) {

    }

    @Override
    public void getStatusToken(EstatusResponse status) {

    }

    @Override
    public void cambio() {
        initFragments(new DetalleConsultaFragment());
    }

        public void initFragments(Fragment fragment)
        {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.contenedor, fragment);
            ft.addToBackStack(null);
            ft.commit();
        }

    @Override
    public void getListFamiliares(List<Patient> familiar) {
        listPaciente.clear();
        idPacientes.clear();
        idUser.clear();
        utcPaciente.clear();
        for (int i=0;i<familiar.size();i++)
        {

            listPaciente.add(familiar.get(i).getName()+" "+familiar.get(i).getLastname()+" - "+familiar.get(i).getRelationshipName());
            idPacientes.add(familiar.get(i).getPatientid());
            idUser.add(familiar.get(i).getPatientid());
            utcPaciente.add(familiar.get(i).getTimezoneName());
        }

        initUISpinner(listPaciente);
    }

    @Override
    public void setFamiliainfo(String rest) {

    }

    @Override
    public void setDeleteFamiliar(String rest) {

    }
}

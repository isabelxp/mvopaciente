package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Prescripcion {

    @SerializedName("prescriptionid")
    @Expose
    private Integer prescriptionid;
    @SerializedName("consultationid")
    @Expose
    private Integer consultationid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("presentation")
    @Expose
    private String presentation;
    @SerializedName("observation")
    @Expose
    private String observation;

    public Integer getPrescriptionid() {
        return prescriptionid;
    }

    public void setPrescriptionid(Integer prescriptionid) {
        this.prescriptionid = prescriptionid;
    }

    public Integer getConsultationid() {
        return consultationid;
    }

    public void setConsultationid(Integer consultationid) {
        this.consultationid = consultationid;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Prescripcion{" +
                "prescriptionid=" + prescriptionid +
                ", consultationid=" + consultationid +
                ", name='" + name + '\'' +
                ", presentation='" + presentation + '\'' +
                ", observation='" + observation + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

}

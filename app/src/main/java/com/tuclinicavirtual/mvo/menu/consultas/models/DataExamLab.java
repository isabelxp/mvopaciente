package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataExamLab {

    @SerializedName("examLab")
    @Expose
    private List<ExamLab> examLab = null;

    public List<ExamLab> getExamLab() {
        return examLab;
    }

    public void setExamLab(List<ExamLab> examLab) {
        this.examLab = examLab;
    }
}

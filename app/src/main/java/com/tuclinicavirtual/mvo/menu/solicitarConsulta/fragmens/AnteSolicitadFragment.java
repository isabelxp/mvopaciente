package com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.antecedentes.adapter.AntecedentesAdapter;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.antecedents;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.AntecedenteRequest;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.Antecedentes2;
import com.tuclinicavirtual.mvo.menu.antecedentes.mvp.AntecedenteContrato;
import com.tuclinicavirtual.mvo.menu.antecedentes.mvp.AntecedentePresentador;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.interfaces.GuardarAntecedentesInter;
import com.tuclinicavirtual.mvo.utils.Preferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnteSolicitadFragment extends Fragment implements FamiliaresContrato.View, AntecedenteContrato.View{


    public AnteSolicitadFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.spinner_antecedente)
    Spinner spinner_antecedente;


    List<String> listPaciente=new ArrayList<>();
    List<Integer> idPacientes=new ArrayList<>();
    List<String> utcPaciente=new ArrayList<>();
    List<Integer> idUser=new ArrayList<>();

    FamiliaresPresentador presenter2;


    @BindView(R.id.recycler)
    RecyclerView recycler;
    AntecedentesAdapter adaptador;
    List<Antecedentes2> listAntecedentes=new ArrayList<>();

    AntecedentePresentador presenter;
    AntecedenteRequest request;
    ProgressDialog progress;
    int idpacienteAuxi;

    public static ArrayList<antecedents> listAntecedenteGuardar=new ArrayList<>();


    public static int idPacienteConsulta;
    public static int idUserPaciente;
    public static String nombrePaciente;

    public static int getIdPacienteConsulta() {
        return idPacienteConsulta;
    }

    public static int getIdUserPaciente() {
        return idUserPaciente;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public static void setIdPacienteConsulta(int idPacienteConsulta) {
        AnteSolicitadFragment.idPacienteConsulta = idPacienteConsulta;
    }

    public static void setIdUserPaciente(int idUserPaciente) {
        AnteSolicitadFragment.idUserPaciente = idUserPaciente;
    }

    public static void setNombrePaciente(String nombrePaciente) {
        AnteSolicitadFragment.nombrePaciente = nombrePaciente;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_antecedente_solicitadk, container, false);
        ButterKnife.bind(this, v);


        initUI();
        setIdPacienteConsulta(Preferences.getIDPatientid());
        setIdUserPaciente(Preferences.getIDUser());
        Log.d("spinner","--0-->"+Preferences.getIDPatientid());
        //progress.show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(getActivity(), R.color.button_bg));
        }


        return v;

    }
    @Override
    public void getListAntecedente(List<Antecedentes2> antecedente) {
        progress.cancel();
        adaptador= new AntecedentesAdapter(antecedente);
        recycler.setAdapter(adaptador);
        recycler.setNestedScrollingEnabled(false);
        adaptador.notifyDataSetChanged();
        Log.d("getListAntecedente","-->"+antecedente);
    }

    @Override
    public void setAntecedenteResp(String resp) {
        progress.cancel();

        if(resp.equals("ok"))
        {
           // dialog("Antecedentes actualizados");
        }
        presenter.getAntecedente(getIdPacienteConsulta());
        Log.d("setAntecedente","-->"+resp);
    }

    public void initUI()
    {
        progress = new ProgressDialog(getActivity());
        progress.setTitle(getActivity().getString(R.string.registro_cargando));
        presenter=new AntecedentePresentador(this);

        LinearLayoutManager reci = new LinearLayoutManager(getContext());
        reci.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(reci);
        //recycler.setHasFixedSize(true);
    }

    @OnClick(R.id.id_guardar)
    public void botton_guardar() {
        progress.show();
        boolean valida=true;
        for (int i=0;i<AntecedentesAdapter.listAuxiliar.size();i++)
        {
            if(AntecedentesAdapter.listAuxiliar.get(i).getAntecedentid()==12)
            {
                if(AntecedentesAdapter.listAuxiliar.get(i).getObservation().equals(""))
                {
                    progress.cancel();
                    valida=false;
                    AntecedentesAdapter.errorEdit=true;
                  //  dialog("Usted debe indicar su altura!");

                    Log.d("AntecedentesAdapter","Esta sin nada 1");
                }
            }
            if(AntecedentesAdapter.listAuxiliar.get(i).getAntecedentid()==13)
            {
                if(AntecedentesAdapter.listAuxiliar.get(i).getObservation().equals(""))
                {
                    progress.cancel();
                    valida=false;
                    AntecedentesAdapter.errorEdit=true;
                    //dialog("Usted debe indicar su peso!");

                    Log.d("AntecedentesAdapter","Esta sin nada 2");
                }
            }

        }
        if(valida)
        {
            presenter.setPutAntecedente(new AntecedenteRequest(getIdUserPaciente(),getIdPacienteConsulta(),AntecedentesAdapter.listAuxiliar));
            SolicitarConsultaFragment.setPasarSiguiente(true);
        }

        Log.d("botton_guardar",""+AntecedentesAdapter.listAuxiliar.size());

    }

    public static boolean pesoAltura()
    {
        for (int i=0;i<AntecedentesAdapter.listAuxiliar.size();i++)
        {
            if(AntecedentesAdapter.listAuxiliar.get(i).getAntecedentid()==12)
            {
                if(AntecedentesAdapter.listAuxiliar.get(i).getObservation().equals(""))
                {

                    return false;
                }
            }
            if(AntecedentesAdapter.listAuxiliar.get(i).getAntecedentid()==13)
            {
                if(AntecedentesAdapter.listAuxiliar.get(i).getObservation().equals(""))
                {
                    return  false;
                }
            }

        }
        return true;
    }



    public void dialog(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(resp);
        builder.setCancelable(false);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                // presenter.getAntecedente(getIdPacienteConsulta());
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void initUI(final List<String> familiar)
    {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, familiar);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_antecedente.setAdapter(dataAdapter);
        if(familiar!=null)
        {
            for(int i=0;i<familiar.size();i++)
            {
                if(familiar.get(i).contains("Titular"))
                {
                    spinner_antecedente.setSelection(i);
                }
            }

        }
        spinner_antecedente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                setIdUserPaciente(idUser.get(pos));
                setIdPacienteConsulta(idPacientes.get(pos));
                presenter.getAntecedente(idPacientes.get(pos));
                setNombrePaciente(familiar.get(pos));
                Preferences.setTagUtcPacienteCrearCons(utcPaciente.get(pos));
                EventBus.getDefault().post("4");
                Log.d("spinner","--3-->"+idPacientes.get(pos));
                Log.d("spinner","--4-->"+getIdPacienteConsulta());
                //Log.d("Adapter","spinner"+SolicitarConsultaFragment.isPasarSiguiente());
            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String num) {
        Log.d("pasarSiguiente","Antecedente"+num);
        if(num.equals("3"))
        {
            presenter2=new FamiliaresPresentador(this);
            presenter2.getFamiliares(Preferences.getIDUser());
          //  SolicitarConsultaFragment.setPasarSiguiente(true);

            Log.d("pasarSiguiente","bollean"+SolicitarConsultaFragment.isPasarSiguiente());
        }if (num.equals("100"))
        {
            guardar();
        }

    };

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    @Override
    public void getListFamiliares(List<Patient> familiar) {
        listPaciente.clear();
        idPacientes.clear();
        utcPaciente.clear();
        idUser.clear();

        for (int i=0;i<familiar.size();i++)
        {
            listPaciente.add(familiar.get(i).getName()+" "+familiar.get(i).getLastname()+" - "+familiar.get(i).getRelationshipName());
            idPacientes.add(familiar.get(i).getPatientid());
            utcPaciente.add(familiar.get(i).getTimezoneName());
            idUser.add(familiar.get(i).getUserid());
        }

        initUI(listPaciente);

        presenter.getAntecedente(idPacientes.get(0));
//        if(pesoAltura())
//        {
//            SolicitarConsultaFragment.setPasarSiguiente(true);
//        }
    }

    @Override
    public void setFamiliainfo(String rest) { }

    @Override
    public void setDeleteFamiliar(String rest) {

    }





    public void guardar()
    {
        progress.show();
        boolean valida=true;
        for (int i=0;i<AntecedentesAdapter.listAuxiliar.size();i++)
        {
            if(AntecedentesAdapter.listAuxiliar.get(i).getAntecedentid()==12)
            {
                if(AntecedentesAdapter.listAuxiliar.get(i).getObservation().equals(""))
                {
                    progress.cancel();
                    valida=false;
                    dialog("Usted debe indicar su estatura!");
                    Log.d("AntecedentesAdapter","Esta sin nada 1");
                }
                if(AntecedentesAdapter.listAuxiliar.get(i).getObservation()!=null)
                {

                    if(Integer.parseInt(AntecedentesAdapter.listAuxiliar.get(i).getObservation())<40 || Integer.parseInt(AntecedentesAdapter.listAuxiliar.get(i).getObservation())>250)
                    {
                        progress.cancel();
                        valida=false;
                        dialog("Notificación: El valor de la estatura es incorrecto.Recuerde escribirlo en cm");
                    }
                        AntecedentesAdapter.listAuxiliar.get(i).setAnswer(1);
                }

            }
            if(AntecedentesAdapter.listAuxiliar.get(i).getAntecedentid()==13)
            {
                if(AntecedentesAdapter.listAuxiliar.get(i).getObservation().equals(""))
                {
                    progress.cancel();
                    valida=false;
                    dialog("Usted debe indicar su peso!");
                    Log.d("AntecedentesAdapter","Esta sin nada 2");
                }
                if(AntecedentesAdapter.listAuxiliar.get(i).getObservation()!=null)
                {
                    if(Integer.parseInt(AntecedentesAdapter.listAuxiliar.get(i).getObservation())<1 || Integer.parseInt(AntecedentesAdapter.listAuxiliar.get(i).getObservation())>500)
                    {
                        progress.cancel();
                        valida=false;
                        dialog("Notificación: El valor del peso es incorrecto.Recuerde escribirlo en Kgs");
                    }
                        AntecedentesAdapter.listAuxiliar.get(i).setAnswer(1);
                }
            }

        }
        if(valida)
        {
            EventBus.getDefault().post("4");
            presenter.setPutAntecedente(new AntecedenteRequest(getIdUserPaciente(),getIdPacienteConsulta(),AntecedentesAdapter.listAuxiliar));
            SolicitarConsultaFragment.setPasarSiguiente(true);
        }
    }

}

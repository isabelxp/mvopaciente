package com.tuclinicavirtual.mvo.menu.solicitarConsulta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Schedule {

    @SerializedName("scheduleid")
    @Expose
    private Integer scheduleid;
    @SerializedName("doctorid")
    @Expose
    private Integer doctorid;
    @SerializedName("doctor")
    @Expose
    private String doctor;
    @SerializedName("specialtyid")
    @Expose
    private Integer specialtyid;
    @SerializedName("specialty")
    @Expose
    private String specialty;
    @SerializedName("dayid")
    @Expose
    private Integer dayid;
    @SerializedName("dayname")
    @Expose
    private String dayname;
    @SerializedName("start_hour")
    @Expose
    private String startHour;
    @SerializedName("finish_hour")
    @Expose
    private String finishHour;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getScheduleid() {
        return scheduleid;
    }

    public void setScheduleid(Integer scheduleid) {
        this.scheduleid = scheduleid;
    }

    public Integer getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Integer doctorid) {
        this.doctorid = doctorid;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public Integer getSpecialtyid() {
        return specialtyid;
    }

    public void setSpecialtyid(Integer specialtyid) {
        this.specialtyid = specialtyid;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public Integer getDayid() {
        return dayid;
    }

    public void setDayid(Integer dayid) {
        this.dayid = dayid;
    }

    public String getDayname() {
        return dayname;
    }

    public void setDayname(String dayname) {
        this.dayname = dayname;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getFinishHour() {
        return finishHour;
    }

    public void setFinishHour(String finishHour) {
        this.finishHour = finishHour;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "scheduleid=" + scheduleid +
                ", doctorid=" + doctorid +
                ", doctor='" + doctor + '\'' +
                ", specialtyid=" + specialtyid +
                ", specialty='" + specialty + '\'' +
                ", dayid=" + dayid +
                ", dayname='" + dayname + '\'' +
                ", startHour='" + startHour + '\'' +
                ", finishHour='" + finishHour + '\'' +
                ", status=" + status +
                '}';
    }
}

package com.tuclinicavirtual.mvo.menu.consultas.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.DetalleConsEscritasFragment;
import com.tuclinicavirtual.mvo.menu.consultas.models.Consultation;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostic;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostico;
import com.tuclinicavirtual.mvo.menu.consultas.models.EstatusResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ExamLab;
import com.tuclinicavirtual.mvo.menu.consultas.models.Prescripcion;
import com.tuclinicavirtual.mvo.menu.consultas.models.PrescripcionItem;
import com.tuclinicavirtual.mvo.menu.consultas.mvp.ConsultasContrato;
import com.tuclinicavirtual.mvo.menu.consultas.mvp.ConsultasPresentador;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdapterConsultasEscritas extends RecyclerView.Adapter<AdapterConsultasEscritas.ViewHolder> implements ConsultasContrato.View{
    public static List<Consultation> listConsultaEscritas;
    Context context;
    ConsultasPresentador presentador;

   public static Consultation consultaEscrita;

    public static Consultation getConsultaEscrita() {
        return consultaEscrita;
    }

    public static void setConsultaEscrita(Consultation consultaEscrita) {
        AdapterConsultasEscritas.consultaEscrita = consultaEscrita;
    }

    public static int posicionEscrito;

    public static int getPosicionEscrito() {
        return posicionEscrito;
    }

    public static void setPosicionEscrito(int posicionEscrito) {
        AdapterConsultasEscritas.posicionEscrito = posicionEscrito;
    }

    public AdapterConsultasEscritas(List<Consultation> listConsulta, Context context) {
        this.listConsultaEscritas =listConsulta;
        this.context=context;
    }

    @Override
    public AdapterConsultasEscritas.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_consultas_escritas, parent, false);

        return new AdapterConsultasEscritas.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdapterConsultasEscritas.ViewHolder holder, final int position) {



            holder.especialidad.setText(listConsultaEscritas.get(position).getSpecialityname());
            holder.nombre_medico.setText(listConsultaEscritas.get(position).getDoctorName());
            holder.text_fecha.setText(parseDateToddMMyyyy(listConsultaEscritas.get(position).getConsultationDate()));
            holder.bindData(listConsultaEscritas.get(position),context);
            holder.text_status.setText(listConsultaEscritas.get(position).getStatusname());
                 holder.img_consulta.setBackgroundResource(R.drawable.circule);

        //Log.d("consultasEscritas",""+listConsultaEscritas.get(position).getid);
                holder.img_consulta.setImageResource(R.drawable.ic_create_black_24dp);

        holder.layoutConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPosicionEscrito(position);
                initFragments(context);
                setConsultaEscrita(listConsultaEscritas.get(position));
                Log.d("ConsultasProgramada",""+listConsultaEscritas.get(position).getConsultationid());
            }
        });

        // Medico no respondio (3)
        if(listConsultaEscritas.get(position).getStatusid()==3)
        {
            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
        }
        // Respondio
        if(listConsultaEscritas.get(position).getStatusid()==4)
        {
            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);

        }
        // Por responder (2)
        if(listConsultaEscritas.get(position).getStatusid()==2)
        {
            //holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
          //  holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
        }
        // Estatus incompleta (1)
        if(listConsultaEscritas.get(position).getStatusid()==1)
        {
            holder.text_status.setTextColor(Color.parseColor("#DDEEF4"));
            holder.layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
        }

    }

    public static String hourFormatter(String startTime){
        //String startTime = "2013-02-27 21:06:30";
        String formatedDate =null;

        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
        Date date = null;
        try {
            date = dateFormatter.parse(startTime);


            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
            String displayValue = timeFormatter.format(date);
            return displayValue;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formatedDate;
    }


    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    private static String deleteChar(String text, int charPos) {
        return new StringBuilder(text).deleteCharAt(charPos).toString();
    }

    @Override
    public int getItemCount() {
        return listConsultaEscritas.size();
    }

    @Override
    public void getDiagnosticoList(List<Diagnostico> dato) {

    }

    @Override
    public void getPrescripcionList(List<Prescripcion> dato) {

    }

    @Override
    public void getPrescripcionItemList(List<PrescripcionItem> dato) {

    }

    @Override
    public void getExamLab(List<ExamLab> dato) {

    }

    @Override
    public void getConsultation(List<Consultation> dato) {

    }

    @Override
    public void getReporte(String dato) {

    }

    @Override
    public void getScore(String sms) {

    }

    @Override
    public void getDiagnosticoItem(List<Diagnostic> dato) {

    }

    @Override
    public void getStatusToken(EstatusResponse status) {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        @BindView(R.id.layout_consulta)
        LinearLayout layoutConsulta;

        @BindView(R.id.nombre_medico)
        TextView nombre_medico;

        @BindView(R.id.especialidad)
        TextView especialidad;

        @BindView(R.id.text_fecha)
        TextView text_fecha;

        @BindView(R.id.img_consulta)
        ImageView img_consulta;

        @BindView(R.id.layout_text_status)
        LinearLayout layout_text_status;

        @BindView(R.id.text_status)
        TextView text_status;


        Context context;
        public ViewHolder(View v) {
            super(v);
            view = v;
            ButterKnife.bind(this, v);
        }

        public void bindData(Consultation c,Context context) {
            this.context=context;
        }
        @OnClick(R.id.layout_consulta)
        public void initConsultarDetalle(){


        }


    }
    public void initFragments(Context context)
    {
        FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, new DetalleConsEscritasFragment());
        ft.addToBackStack(null);
        ft.commit();
    }
}

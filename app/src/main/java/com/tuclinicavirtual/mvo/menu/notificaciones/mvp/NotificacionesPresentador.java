package com.tuclinicavirtual.mvo.menu.notificaciones.mvp;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuclinicavirtual.mvo.menu.familiares.interfaces.FamiliaresInterfaces;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaRequest;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaResponse;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.notificaciones.interfaces.NotificacionesInterfaces;
import com.tuclinicavirtual.mvo.menu.notificaciones.models.NotificacionesResponse;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.utils.Preferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificacionesPresentador implements NotificacionesContrato.Presenter {

    NotificacionesInterfaces rest;
    public NotificacionesContrato.View view;
    private String TAG;

    public NotificacionesPresentador(NotificacionesContrato.View view) {
        this.view = view;
        TAG = this.getClass().getSimpleName();
    }


    @Override
    public void getNotificaciones(int id) {
        rest = RetrofitClient.getClient(Consts.BASE_URL).create(NotificacionesInterfaces.class);

        rest.getNotificaciones(id,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<NotificacionesResponse>() {
            @Override
            public void onResponse(Call<NotificacionesResponse> call, Response<NotificacionesResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getListNotificaciones(response.body().getData().getNotification());
                        Log.i(TAG, "--->getNotificacion: " + call.request().url());
                        Log.i(TAG, "--->getNotificacion: " + response.body().getMessage());
                        Log.i(TAG, "--->getNotificacion: " + response.body().getStatus());
                    }
                }else{
                    Log.i(TAG, "--->Error en getNotificacion: " + call.request().url());
                    Log.i(TAG, "--->Error en getNotificacion: " + response.code());
                    Log.i(TAG, "--->Error en getNotificacion: " + response.message());
                    Log.i(TAG, "--->Error en getNotificacion: " + response.body().toString());

                }  }
            @Override
            public void onFailure(Call<NotificacionesResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }
}

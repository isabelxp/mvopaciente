package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Consultation {

    @SerializedName("consultationid")
    @Expose
    private Integer consultationid;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("patientid")
    @Expose
    private Integer patientid;
    @SerializedName("patient_name")
    @Expose
    private String patientName;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("identification_type")
    @Expose
    private String identificationType;
    @SerializedName("identification_number")
    @Expose
    private String identificationNumber;
    @SerializedName("doctorid")
    @Expose
    private Integer doctorid;
    @SerializedName("doctor_name")
    @Expose
    private String doctorName;
    @SerializedName("doctor_userid")
    @Expose
    private Integer doctorUserid;
    @SerializedName("specialityid")
    @Expose
    private Integer specialityid;
    @SerializedName("specialityname")
    @Expose
    private String specialityname;
    @SerializedName("categoryid")
    @Expose
    private Integer categoryid;
    @SerializedName("categoryname")
    @Expose
    private String categoryname;
    @SerializedName("typeid")
    @Expose
    private Integer typeid;
    @SerializedName("typename")
    @Expose
    private String typename;
    @SerializedName("paymentid")
    @Expose
    private Integer paymentid;
    @SerializedName("paymentnumber")
    @Expose
    private String paymentnumber;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("consultation_date")
    @Expose
    private String consultationDate;
    @SerializedName("consultation_hour")
    @Expose
    private String consultationHour;
    @SerializedName("real_start_date")
    @Expose
    private String realStartDate;
    @SerializedName("real_finish_date")
    @Expose
    private String realFinishDate;
    @SerializedName("statusid")
    @Expose
    private Integer statusid;
    @SerializedName("statusname")
    @Expose
    private String statusname;
    @SerializedName("doctor_answer")
    @Expose
    private Object doctorAnswer;
    @SerializedName("medical_report")
    @Expose
    private Object medicalReport;
    @SerializedName("general_diagnostic")
    @Expose
    private Object generalDiagnostic;
    @SerializedName("general_indication")
    @Expose
    private Object generalIndication;
    @SerializedName("general_lab_exam")
    @Expose
    private Object generalLabExam;
    @SerializedName("general_rx_exam")
    @Expose
    private Object generalRxExam;
    @SerializedName("other_exam")
    @Expose
    private Object otherExam;
    @SerializedName("is_rated")
    @Expose
    private Integer isRated;
    @SerializedName("session_link")
    @Expose
    private String sessionLink;
    @SerializedName("doctor_started")
    @Expose
    private Integer doctorStarted;
    @SerializedName("guest_email")
    @Expose
    private Object guestEmail;
    @SerializedName("guest_url")
    @Expose
    private Object guestUrl;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("date_updated")
    @Expose
    private String dateUpdated;

    @Override
    public String toString() {
        return "Consultation{" +
                "consultationid=" + consultationid +
                ", code='" + code + '\'' +
                ", userid=" + userid +
                ", patientid=" + patientid +
                ", patientName='" + patientName + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", sex='" + sex + '\'' +
                ", identificationType='" + identificationType + '\'' +
                ", identificationNumber='" + identificationNumber + '\'' +
                ", doctorid=" + doctorid +
                ", doctorName='" + doctorName + '\'' +
                ", doctorUserid=" + doctorUserid +
                ", specialityid=" + specialityid +
                ", specialityname='" + specialityname + '\'' +
                ", categoryid=" + categoryid +
                ", categoryname='" + categoryname + '\'' +
                ", typeid=" + typeid +
                ", typename='" + typename + '\'' +
                ", paymentid=" + paymentid +
                ", paymentnumber='" + paymentnumber + '\'' +
                ", reason='" + reason + '\'' +
                ", consultationDate='" + consultationDate + '\'' +
                ", consultationHour='" + consultationHour + '\'' +
                ", realStartDate=" + realStartDate +
                ", realFinishDate=" + realFinishDate +
                ", statusid=" + statusid +
                ", statusname='" + statusname + '\'' +
                ", doctorAnswer=" + doctorAnswer +
                ", medicalReport=" + medicalReport +
                ", generalDiagnostic=" + generalDiagnostic +
                ", generalIndication=" + generalIndication +
                ", generalLabExam=" + generalLabExam +
                ", generalRxExam=" + generalRxExam +
                ", otherExam=" + otherExam +
                ", isRated=" + isRated +
                ", sessionLink='" + sessionLink + '\'' +
                ", doctorStarted=" + doctorStarted +
                ", guestEmail=" + guestEmail +
                ", guestUrl=" + guestUrl +
                ", dateCreated='" + dateCreated + '\'' +
                ", dateUpdated='" + dateUpdated + '\'' +
                '}';
    }

    public Integer getConsultationid() {
        return consultationid;
    }

    public void setConsultationid(Integer consultationid) {
        this.consultationid = consultationid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Integer getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Integer doctorid) {
        this.doctorid = doctorid;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public Integer getDoctorUserid() {
        return doctorUserid;
    }

    public void setDoctorUserid(Integer doctorUserid) {
        this.doctorUserid = doctorUserid;
    }

    public Integer getSpecialityid() {
        return specialityid;
    }

    public void setSpecialityid(Integer specialityid) {
        this.specialityid = specialityid;
    }

    public String getSpecialityname() {
        return specialityname;
    }

    public void setSpecialityname(String specialityname) {
        this.specialityname = specialityname;
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public Integer getPaymentid() {
        return paymentid;
    }

    public void setPaymentid(Integer paymentid) {
        this.paymentid = paymentid;
    }

    public String getPaymentnumber() {
        return paymentnumber;
    }

    public void setPaymentnumber(String paymentnumber) {
        this.paymentnumber = paymentnumber;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getConsultationDate() {
        return consultationDate;
    }

    public void setConsultationDate(String consultationDate) {
        this.consultationDate = consultationDate;
    }

    public String getConsultationHour() {
        return consultationHour;
    }

    public void setConsultationHour(String consultationHour) {
        this.consultationHour = consultationHour;
    }

    public Object getRealStartDate() {
        return realStartDate;
    }

    public void setRealStartDate(String realStartDate) {
        this.realStartDate = realStartDate;
    }

    public String getRealFinishDate() {
        return realFinishDate;
    }

    public void setRealFinishDate(String realFinishDate) {
        this.realFinishDate = realFinishDate;
    }

    public Integer getStatusid() {
        return statusid;
    }

    public void setStatusid(Integer statusid) {
        this.statusid = statusid;
    }

    public String getStatusname() {
        return statusname;
    }

    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }

    public Object getDoctorAnswer() {
        return doctorAnswer;
    }

    public void setDoctorAnswer(Object doctorAnswer) {
        this.doctorAnswer = doctorAnswer;
    }

    public Object getMedicalReport() {
        return medicalReport;
    }

    public void setMedicalReport(Object medicalReport) {
        this.medicalReport = medicalReport;
    }

    public Object getGeneralDiagnostic() {
        return generalDiagnostic;
    }

    public void setGeneralDiagnostic(Object generalDiagnostic) {
        this.generalDiagnostic = generalDiagnostic;
    }

    public Object getGeneralIndication() {
        return generalIndication;
    }

    public void setGeneralIndication(Object generalIndication) {
        this.generalIndication = generalIndication;
    }

    public Object getGeneralLabExam() {
        return generalLabExam;
    }

    public void setGeneralLabExam(Object generalLabExam) {
        this.generalLabExam = generalLabExam;
    }

    public Object getGeneralRxExam() {
        return generalRxExam;
    }

    public void setGeneralRxExam(Object generalRxExam) {
        this.generalRxExam = generalRxExam;
    }

    public Object getOtherExam() {
        return otherExam;
    }

    public void setOtherExam(Object otherExam) {
        this.otherExam = otherExam;
    }

    public Integer getIsRated() {
        return isRated;
    }

    public void setIsRated(Integer isRated) {
        this.isRated = isRated;
    }

    public String getSessionLink() {
        return sessionLink;
    }

    public void setSessionLink(String sessionLink) {
        this.sessionLink = sessionLink;
    }

    public Integer getDoctorStarted() {
        return doctorStarted;
    }

    public void setDoctorStarted(Integer doctorStarted) {
        this.doctorStarted = doctorStarted;
    }

    public Object getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(Object guestEmail) {
        this.guestEmail = guestEmail;
    }

    public Object getGuestUrl() {
        return guestUrl;
    }

    public void setGuestUrl(Object guestUrl) {
        this.guestUrl = guestUrl;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

}


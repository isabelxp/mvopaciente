package com.tuclinicavirtual.mvo.menu.notificaciones.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification {
    @SerializedName("notificationid")
    @Expose
    private Integer notificationid;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("notification_typeid:")
    @Expose
    private Integer notificationTypeid;
    @SerializedName("notification_type")
    @Expose
    private String notificationType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("is_read")
    @Expose
    private Integer isRead;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getNotificationid() {
        return notificationid;
    }

    public void setNotificationid(Integer notificationid) {
        this.notificationid = notificationid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getNotificationTypeid() {
        return notificationTypeid;
    }

    public void setNotificationTypeid(Integer notificationTypeid) {
        this.notificationTypeid = notificationTypeid;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}

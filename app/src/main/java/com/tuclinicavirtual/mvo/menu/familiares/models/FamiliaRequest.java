package com.tuclinicavirtual.mvo.menu.familiares.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;

import java.util.List;

public class FamiliaRequest {

    @Expose
    private Integer userid;
    @SerializedName("patients")
    @Expose
    private List<FamiliasModelo> patients = null;

    public FamiliaRequest(Integer userid, List<FamiliasModelo> patients) {
        this.userid = userid;
        this.patients = patients;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public List<FamiliasModelo> getPatients() {
        return patients;
    }

    public void setPatients(List<FamiliasModelo> patients) {
        this.patients = patients;
    }


}

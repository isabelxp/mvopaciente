package com.tuclinicavirtual.mvo.menu.consultas.mvp;

import com.tuclinicavirtual.mvo.menu.consultas.models.Consultation;
import com.tuclinicavirtual.mvo.menu.consultas.models.DataDiagnostico;
import com.tuclinicavirtual.mvo.menu.consultas.models.DataPrescripcion;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostic;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostico;
import com.tuclinicavirtual.mvo.menu.consultas.models.EstatusResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ExamLab;
import com.tuclinicavirtual.mvo.menu.consultas.models.Prescripcion;
import com.tuclinicavirtual.mvo.menu.consultas.models.PrescripcionItem;
import com.tuclinicavirtual.mvo.menu.consultas.models.ReporteRequest;
import com.tuclinicavirtual.mvo.menu.consultas.models.ScoreRequest;

import java.util.List;

public class ConsultasContrato {
    public interface View{
        void getDiagnosticoList(List<Diagnostico> dato);
        void getPrescripcionList(List<Prescripcion> dato);
        void getPrescripcionItemList(List<PrescripcionItem> dato);
        void getExamLab(List<ExamLab> dato);
        void getConsultation(List<Consultation> dato);
        void getReporte(String dato);
        void getScore(String sms);
        void getDiagnosticoItem(List<Diagnostic> dato);
        void getStatusToken(EstatusResponse status);

    }

    public interface Presenter{
        void getExamLab(List<ExamLab> dato);
        void getDiagnostico(int id);
        void getConsultation(int patientid);
        void setReporte(ReporteRequest request);
        void getPrescripcion(int id);
        void setRate(ScoreRequest request);
        void getDiagnosticoItem(int dato);
        void getTokenOpentok(String utc,int idconsulta);
        void getPrescripcionItem(int id);
    }
}

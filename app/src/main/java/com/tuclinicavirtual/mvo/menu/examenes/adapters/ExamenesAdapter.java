package com.tuclinicavirtual.mvo.menu.examenes.adapters;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.himangi.imagepreview.ImagePreviewActivity;
import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.models.ModeloConsultasEscritas;
import com.tuclinicavirtual.mvo.menu.examenes.fragments.DetalleExamFragment;
import com.tuclinicavirtual.mvo.menu.examenes.fragments.MisExamenesFragment;
import com.tuclinicavirtual.mvo.menu.examenes.interfaces.deleteExam;
import com.tuclinicavirtual.mvo.menu.examenes.models.Exam;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamDelete;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequestDelete;
import com.tuclinicavirtual.mvo.menu.examenes.models.ModeloExamenes;
import com.tuclinicavirtual.mvo.menu.examenes.mvp.ExamContrato;
import com.tuclinicavirtual.mvo.menu.examenes.mvp.ExamPresentador;
import com.tuclinicavirtual.mvo.menu.familiares.fragments.DetalleFamiliarFragment;
import com.tuclinicavirtual.mvo.menu.familiares.fragments.MisFamiliaresFragment;
import com.tuclinicavirtual.mvo.menu.familiares.models.DeleteRequestFamilia;
import com.tuclinicavirtual.mvo.menu.familiares.models.Family;
import com.tuclinicavirtual.mvo.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExamenesAdapter extends RecyclerView.Adapter<ExamenesAdapter.ViewHolder> implements ExamContrato.View{
    List<Exam> listExamenes;
    Context context;
    ExamPresentador presentador;
    deleteExam delete;
    public ExamenesAdapter(List<Exam> listExamenes, Context context, android.support.v4.app.Fragment fragment) {
        this.listExamenes=listExamenes;
        this.context=context;
        delete=(deleteExam) fragment;
    }

    @Override
    public ExamenesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_examenes, parent, false);
        presentador=new ExamPresentador(ExamenesAdapter.this);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ExamenesAdapter.ViewHolder holder, final int position) {
        holder.bindData(listExamenes.get(position),context);

        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog(listExamenes.get(position).getName(),listExamenes.get(position).getExamid());
            }
        });


        holder.layout_exam_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // holder.progress.show();
                ArrayList<String> imageList = new ArrayList<>();
                imageList.add(listExamenes.get(position).getURL());

                Intent intent = new Intent(context,
                        ImagePreviewActivity.class);
                intent.putExtra(ImagePreviewActivity.IMAGE_LIST,
                        imageList);
                intent.putExtra(ImagePreviewActivity.CURRENT_ITEM, 0);
                context.startActivity(intent);
               //holder.progress.cancel();
            }
        });


    }

    @Override
    public int getItemCount() {
        if (listExamenes!=null)
        {
            return listExamenes.size();
        }else
        return 0;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_exam)
        TextView text_exam;
        @BindView(R.id.exam_descr)
        TextView exam_descr;
        @BindView(R.id.img_delete)
        ImageView img_delete;
        @BindView(R.id.layout_exam_detail)
        LinearLayout layout_exam_detail;
        ProgressDialog progress;
        Context context;
        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }

        public void bindData(Exam c,Context context) {
            this.context=context;
            text_exam.setText(c.getName());
            exam_descr.setText(c.getURL());
            progress = new ProgressDialog(context);
            progress.setTitle(context.getString(R.string.registro_cargando));
            progress.setCancelable(false);
        }
    }

    @Override
    public void getExamenes(List<Exam> dato) {
        Log.d("-->","getExam"+dato.toString());

    }

    @Override
    public void getExamRespFalse() {

    }

    @Override
    public void getDeleteResp(String dato) {

          delete.deleteExam();
//        FragmentTransaction ft =  ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
//        ft.replace(R.id.contenedor, new MisExamenesFragment());
//        ft.addToBackStack(null);
//        ft.commit();
        Log.d("-->","getDeleteResp"+dato.toString());
    }

    @Override
    public void setExam(String dato) {

    }

    @Override
    public void setUpload(String dato) {

    }

    public void dialog(String resp, final int idexam)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("¿Deseas eliminar "+resp+"?");

        builder.setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                List<ExamDelete> exam=new ArrayList<>();
                exam.add(new ExamDelete(idexam));
                presentador.deleteExam(new ExamRequestDelete(Preferences.getIDUser(),exam));
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


}

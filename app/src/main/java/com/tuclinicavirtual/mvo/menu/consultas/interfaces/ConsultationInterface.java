package com.tuclinicavirtual.mvo.menu.consultas.interfaces;

import com.tuclinicavirtual.mvo.menu.consultas.models.ConsultationResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.DiagnocItemResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.DiagnosticoResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.EstatusResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ExamLabResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ModeloResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.PrescripcionItemResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.PrescripcionResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ReporteRequest;
import com.tuclinicavirtual.mvo.menu.consultas.models.ScoreRequest;
import com.tuclinicavirtual.mvo.menu.consultas.models.ScoreResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ConsultationInterface {

    @GET("consultations/patientid={id}/{utc}/{idtpo}")
    Call<ConsultationResponse> getConsultation(@Path("id") int id,@Path("utc") String utc,@Path("idtpo") int idtpo, @Header("Authorization") String valor);

    @POST("consultations/reports")
    Call<ModeloResponse> setReporte(@Body ReporteRequest request, @Header("Authorization") String valor);

    @GET("consultations/diagnostics/{id}")
    Call<DiagnosticoResponse> getDiagnostico(@Path("id") int id, @Header("Authorization") String valor);

    @GET("consultations/diagnostics/item/UTC -03:00/{id}")
    Call<DiagnocItemResponse> getDiagnosticoItem(@Path("id") int id, @Header("Authorization") String valor);

    @GET("consultation/prescriptions/UTC -03:00/{id}")
    Call<PrescripcionResponse> getPrescripcion(@Path("id") int id, @Header("Authorization") String valor);

    @GET("consultations/prescriptions/item/UTC -03:00/{id}")
    Call<PrescripcionItemResponse> getPrescripcionItem(@Path("id") int id, @Header("Authorization") String valor);

    @GET("consultations/exam/UTC -03:00/{id}/null")
    Call<ExamLabResponse> getExamLab(@Path("id") int id, @Header("Authorization") String valor);

    @POST("consultation/scores")
    Call<ScoreResponse> setScore(@Body ScoreRequest request, @Header("Authorization") String valor);

    @GET("consultation/estatus/token/{utc}/{id}")
    Call<EstatusResponse> getTokenOpentok(@Path("id") int id, @Path("utc") String utc, @Header("Authorization") String valor);

}

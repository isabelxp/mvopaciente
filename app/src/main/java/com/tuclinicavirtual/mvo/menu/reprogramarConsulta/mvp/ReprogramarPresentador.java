package com.tuclinicavirtual.mvo.menu.reprogramarConsulta.mvp;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuclinicavirtual.mvo.menu.antecedentes.interfaces.AntecedenteInterface;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.AntecedenteRequest;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.RespAntecedente;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.ResponseAntecedentes;
import com.tuclinicavirtual.mvo.menu.antecedentes.mvp.AntecedenteContrato;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.interfaces.ReprogramarConsultaInterface;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos.ReprogramarRequest;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos.ReprogramarResponses;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.utils.Preferences;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReprogramarPresentador implements ReprogramarContrato.Presenter{

    ReprogramarConsultaInterface rest;

    public ReprogramarContrato.View view;

    private String TAG;

    public ReprogramarPresentador(ReprogramarContrato.View view) {
        this.view = view;
        TAG = this.getClass().getSimpleName();
    }

    public void setPutReprogramar(ReprogramarRequest guardar) {

        Log.i(TAG, "--->setPutReprogramar "+guardar.toString());

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ReprogramarConsultaInterface.class);
        Log.i(TAG, "--->setPutReprogramar ");

        rest.putConsultas(guardar,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ReprogramarResponses>() {
            @Override
            public void onResponse(Call<ReprogramarResponses> call, Response<ReprogramarResponses> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getRespuestaPutReprogramr(response.body());
                        Log.i(TAG, "--->setPutReprogramar " + response.body().getMessage());

                    }
                }else{

                    Gson gson = new GsonBuilder().create();
                    ResponseAntecedentes pojo = new ResponseAntecedentes();
                    try {
                        pojo = gson.fromJson(response.errorBody().string(), ResponseAntecedentes.class);
                        //view.setAntecedenteResp(pojo.getMessage());
                        Log.i(TAG, "--->Error setPutReprogramar" + pojo.getMessage());
                    } catch (IOException e) { }


                }  }
            @Override
            public void onFailure(Call<ReprogramarResponses> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }
}

package com.tuclinicavirtual.mvo.menu.reprogramarConsulta.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.adapter.reprogramarConsultaAdapter;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos.ReprogramarRequest;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos.ReprogramarResponses;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.mvp.ReprogramarContrato;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.mvp.ReprogramarPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter.ListDoctoresAdapter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultaPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultasContrato;
import com.tuclinicavirtual.mvo.utils.Preferences;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReprogramarConsultaActivity extends AppCompatActivity implements SolicitarConsultasContrato.View, ReprogramarContrato.View{
    @Override
    public void getShoulderDoctor(List<Schedule> hours) {

    }

    @Override
    public void getError(String dato) {

    }

    SolicitarConsultaPresentador presentador;
    ReprogramarPresentador presentadorReprogramar;

    @BindView(R.id.recycler)
    RecyclerView recycler;
    reprogramarConsultaAdapter adaptador;
    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reprogramar_consulta);
        ButterKnife.bind(this);

        initUI();

        presentadorReprogramar.setPutReprogramar(new ReprogramarRequest("UTC -03:00", Preferences.getIDUser(), 38, Preferences.getIDPatientid(), 1, 12, 3, 5, "Mega Dolor", "2018/07/10", "09:00:00"));
        presentador.getDoctorList(4);
    }
    public void initUI()
    {
        progress = new ProgressDialog(this);
        presentador=new SolicitarConsultaPresentador(this);
        presentadorReprogramar = new ReprogramarPresentador(this);
        LinearLayoutManager reci = new LinearLayoutManager(this);
        recycler.setLayoutManager(reci);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setHasFixedSize(true);
        progress.show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    @Override
    public void getListEspecialidad(List<Especialidad> especialistas) {

    }

    @Override
    public void getListDoctor(List<Doctor> doctor) {
        progress.cancel();
        adaptador= new reprogramarConsultaAdapter(doctor,this);
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

    @Override
    public void getHorarioDoctor(List<Availabilty> horarioDoctor) {

    }

    @Override
    public void getSolicitarConsulta(DataSolicitudConsulta code) {

    }

    @Override
    public void getPaymentsResp(DataPayment dataPayment) {

    }

    @Override
    public void getRespDoctorFail(String text) {

    }

    @Override
    public void getRespuestaPutReprogramr(ReprogramarResponses responses) {
        Log.d("--->setPutReprogramar","respuestafin:"+responses.getData().getConsultations());
    }
}

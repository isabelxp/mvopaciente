package com.tuclinicavirtual.mvo.menu.datosPersonales.mvp;

import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPaciente;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionList;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionResponse;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.PasswordRequest;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.views.activitis.models.Data;

import java.util.List;

public class DatosPersonalesContrato {

    public interface View{
        void setDataPaises(List<InformacionList>  dato);
        void setDataOcupaciones(List<InformacionList>  dato);
        void setDataParentesco(List<InformacionList>  dato);
        void setZonaHoraria(List<InformacionList>  dato);
        void setRespDatosUsuario(String sms);
        void getDatosPacientes(Patient a);
        void setRespPasswordNew(String resp, String Token);
    }


    public interface Presenter{
        void getZonaHoraria();
        void getOcupaciones();
        void getPaises();
        void getRalicionFamiliar();
        void getPaciente(int patientid);
        void setNewPassword(PasswordRequest request);
        void setGuardarUsuario(int id,int idp, String name, String lastname,String sex, String type, String number, String cumple, int ocupacionid, int paisid,int paisresid,int timezoneid, int relationship);
    }
}

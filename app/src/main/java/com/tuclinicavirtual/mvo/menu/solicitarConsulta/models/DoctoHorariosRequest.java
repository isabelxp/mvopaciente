package com.tuclinicavirtual.mvo.menu.solicitarConsulta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctoHorariosRequest {

    @SerializedName("utc")
    @Expose
    private String utc;
    @SerializedName("doctorid")
    @Expose
    private Integer doctorid;
    @SerializedName("specialityid")
    @Expose
    private String specialityid;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("starthour")
    @Expose
    private String starthour;

    public DoctoHorariosRequest(String utc, Integer doctorid, String specialityid, String date, String starthour) {
        this.utc = utc;
        this.doctorid = doctorid;
        this.specialityid = specialityid;
        this.date = date;
        this.starthour = starthour;
    }

    @Override
    public String toString() {
        return "DoctoHorariosRequest{" +
                "utc='" + utc + '\'' +
                ", doctorid=" + doctorid +
                ", specialityid='" + specialityid + '\'' +
                ", date='" + date + '\'' +
                ", starthour='" + starthour + '\'' +
                '}';
    }

    public String getUtc() {
        return utc;
    }

    public void setUtc(String utc) {
        this.utc = utc;
    }

    public Integer getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Integer doctorid) {
        this.doctorid = doctorid;
    }

    public String getSpecialityid() {
        return specialityid;
    }

    public void setSpecialityid(String specialityid) {
        this.specialityid = specialityid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Object getStarthour() {
        return starthour;
    }

    public void setStarthour(String starthour) {
        this.starthour = starthour;
    }
}

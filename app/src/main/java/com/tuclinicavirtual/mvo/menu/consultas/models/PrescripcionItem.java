package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrescripcionItem {
    @SerializedName("prescriptionid")
    @Expose
    private Integer prescriptionid;
    @SerializedName("consultationid")
    @Expose
    private Integer consultationid;
    @SerializedName("vademecumid")
    @Expose
    private Integer vademecumid;
    @SerializedName("vademecum")
    @Expose
    private String vademecum;
    @SerializedName("vademecum_presentation")
    @Expose
    private String vademecumPresentation;
    @SerializedName("vademecum_principle")
    @Expose
    private String vademecumPrinciple;
    @SerializedName("observation")
    @Expose
    private String observation;

    @Override
    public String toString() {
        return "PrescripcionItem{" +
                "prescriptionid=" + prescriptionid +
                ", consultationid=" + consultationid +
                ", vademecumid=" + vademecumid +
                ", vademecum='" + vademecum + '\'' +
                ", vademecumPresentation='" + vademecumPresentation + '\'' +
                ", vademecumPrinciple='" + vademecumPrinciple + '\'' +
                ", observation='" + observation + '\'' +
                '}';
    }

    public Integer getPrescriptionid() {
        return prescriptionid;
    }

    public void setPrescriptionid(Integer prescriptionid) {
        this.prescriptionid = prescriptionid;
    }

    public Integer getConsultationid() {
        return consultationid;
    }

    public void setConsultationid(Integer consultationid) {
        this.consultationid = consultationid;
    }

    public Integer getVademecumid() {
        return vademecumid;
    }

    public void setVademecumid(Integer vademecumid) {
        this.vademecumid = vademecumid;
    }

    public String getVademecum() {
        return vademecum;
    }

    public void setVademecum(String vademecum) {
        this.vademecum = vademecum;
    }

    public String getVademecumPresentation() {
        return vademecumPresentation;
    }

    public void setVademecumPresentation(String vademecumPresentation) {
        this.vademecumPresentation = vademecumPresentation;
    }

    public String getVademecumPrinciple() {
        return vademecumPrinciple;
    }

    public void setVademecumPrinciple(String vademecumPrinciple) {
        this.vademecumPrinciple = vademecumPrinciple;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
}

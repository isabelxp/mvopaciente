package com.tuclinicavirtual.mvo.menu.familiares.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeleteRequestFamilia {

    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("families")
    @Expose
    private List<Family> families = null;

    public DeleteRequestFamilia(Integer userid, List<Family> families) {
        this.userid = userid;
        this.families = families;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public List<Family> getFamilies() {
        return families;
    }

    public void setFamilies(List<Family> families) {
        this.families = families;
    }
}

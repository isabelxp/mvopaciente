package com.tuclinicavirtual.mvo.menu.solicitarConsulta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Doctor {

    @SerializedName("doctorid")
    @Expose
    private Integer doctorid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("identification_type")
    @Expose
    private String identificationType;
    @SerializedName("identification_number")
    @Expose
    private String identificationNumber;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("membership_code")
    @Expose
    private String membershipCode;
    @SerializedName("organism_code")
    @Expose
    private String organismCode;
    @SerializedName("firm")
    @Expose
    private String firm;
    @SerializedName("origin_countryid")
    @Expose
    private Integer originCountryid;
    @SerializedName("origin_countryname")
    @Expose
    private String originCountryname;
    @SerializedName("residence_countryid")
    @Expose
    private Integer residenceCountryid;
    @SerializedName("residence_countryname")
    @Expose
    private String residenceCountryname;
    @SerializedName("timezoneid")
    @Expose
    private Integer timezoneid;
    @SerializedName("timezonename")
    @Expose
    private String timezonename;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("bychat")
    @Expose
    private Integer bychat;
    @SerializedName("byemail")
    @Expose
    private Integer byemail;
    @SerializedName("byphone")
    @Expose
    private Integer byphone;
    @SerializedName("byvideo")
    @Expose
    private Integer byvideo;

    public Integer getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Integer doctorid) {
        this.doctorid = doctorid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getMembershipCode() {
        return membershipCode;
    }

    public void setMembershipCode(String membershipCode) {
        this.membershipCode = membershipCode;
    }

    public String getOrganismCode() {
        return organismCode;
    }

    public void setOrganismCode(String organismCode) {
        this.organismCode = organismCode;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public Integer getOriginCountryid() {
        return originCountryid;
    }

    public void setOriginCountryid(Integer originCountryid) {
        this.originCountryid = originCountryid;
    }

    public String getOriginCountryname() {
        return originCountryname;
    }

    public void setOriginCountryname(String originCountryname) {
        this.originCountryname = originCountryname;
    }

    public Integer getResidenceCountryid() {
        return residenceCountryid;
    }

    public void setResidenceCountryid(Integer residenceCountryid) {
        this.residenceCountryid = residenceCountryid;
    }

    public String getResidenceCountryname() {
        return residenceCountryname;
    }

    public void setResidenceCountryname(String residenceCountryname) {
        this.residenceCountryname = residenceCountryname;
    }

    public Integer getTimezoneid() {
        return timezoneid;
    }

    public void setTimezoneid(Integer timezoneid) {
        this.timezoneid = timezoneid;
    }

    public String getTimezonename() {
        return timezonename;
    }

    public void setTimezonename(String timezonename) {
        this.timezonename = timezonename;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getBychat() {
        return bychat;
    }

    public void setBychat(Integer bychat) {
        this.bychat = bychat;
    }

    public Integer getByemail() {
        return byemail;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "doctorid=" + doctorid +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", fullname='" + fullname + '\'' +
                ", sex='" + sex + '\'' +
                ", identificationType='" + identificationType + '\'' +
                ", identificationNumber='" + identificationNumber + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", membershipCode='" + membershipCode + '\'' +
                ", organismCode='" + organismCode + '\'' +
                ", firm='" + firm + '\'' +
                ", originCountryid=" + originCountryid +
                ", originCountryname='" + originCountryname + '\'' +
                ", residenceCountryid=" + residenceCountryid +
                ", residenceCountryname='" + residenceCountryname + '\'' +
                ", timezoneid=" + timezoneid +
                ", timezonename='" + timezonename + '\'' +
                ", userid=" + userid +
                ", email='" + email + '\'' +
                ", photo='" + photo + '\'' +
                ", status=" + status +
                ", bychat=" + bychat +
                ", byemail=" + byemail +
                ", byphone=" + byphone +
                ", byvideo=" + byvideo +
                '}';
    }

    public void setByemail(Integer byemail) {
        this.byemail = byemail;
    }

    public Integer getByphone() {
        return byphone;
    }

    public void setByphone(Integer byphone) {
        this.byphone = byphone;
    }

    public Integer getByvideo() {
        return byvideo;
    }

    public void setByvideo(Integer byvideo) {
        this.byvideo = byvideo;
    }
}

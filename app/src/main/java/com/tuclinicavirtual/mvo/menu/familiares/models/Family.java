package com.tuclinicavirtual.mvo.menu.familiares.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Family {
    @SerializedName("utc")
    @Expose
    private String utc;
    @SerializedName("patientid")
    @Expose
    private Integer patientid;

    public Family(String utc, Integer patientid) {
        this.utc = utc;
        this.patientid = patientid;
    }

    public String getUtc() {
        return utc;
    }

    public void setUtc(String utc) {
        this.utc = utc;
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }
}

package com.tuclinicavirtual.mvo.menu.solicitarConsulta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Availabilty {
    @SerializedName("doctorid")
    @Expose
    private Integer doctorid;
    @SerializedName("specialtyid")
    @Expose
    private Integer specialtyid;
    @SerializedName("utc")
    @Expose
    private String utc;
    @SerializedName("date_consultation")
    @Expose
    private String dateConsultation;
    @SerializedName("cast(date_consultation)")
    @Expose
    private String castDateConsultation;
    @SerializedName("start_hour")
    @Expose
    private String startHour;
    @SerializedName("finish_hour")
    @Expose
    private String finishHour;

    @Override
    public String toString() {
        return "Availabilty{" +
                "doctorid=" + doctorid +
                ", specialtyid=" + specialtyid +
                ", utc='" + utc + '\'' +
                ", dateConsultation='" + dateConsultation + '\'' +
                ", castDateConsultation='" + castDateConsultation + '\'' +
                ", startHour='" + startHour + '\'' +
                ", finishHour='" + finishHour + '\'' +
                '}';
    }

    public Availabilty(Integer doctorid, Integer specialtyid, String utc, String dateConsultation, String castDateConsultation, String startHour, String finishHour) {
        this.doctorid = doctorid;
        this.specialtyid = specialtyid;
        this.utc = utc;
        this.dateConsultation = dateConsultation;
        this.castDateConsultation = castDateConsultation;
        this.startHour = startHour;
        this.finishHour = finishHour;
    }

    public Integer getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Integer doctorid) {
        this.doctorid = doctorid;
    }

    public Integer getSpecialtyid() {
        return specialtyid;
    }

    public void setSpecialtyid(Integer specialtyid) {
        this.specialtyid = specialtyid;
    }

    public String getUtc() {
        return utc;
    }

    public void setUtc(String utc) {
        this.utc = utc;
    }

    public String getDateConsultation() {
        return dateConsultation;
    }

    public void setDateConsultation(String dateConsultation) {
        this.dateConsultation = dateConsultation;
    }

    public String getCastDateConsultation() {
        return castDateConsultation;
    }

    public void setCastDateConsultation(String castDateConsultation) {
        this.castDateConsultation = castDateConsultation;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getFinishHour() {
        return finishHour;
    }

    public void setFinishHour(String finishHour) {
        this.finishHour = finishHour;
    }
}

package com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.antecedentes.MisAntecedentesFragment;
import com.tuclinicavirtual.mvo.menu.antecedentes.adapter.AntecedentesAdapter;
import com.tuclinicavirtual.mvo.menu.examenes.adapters.ExamenesAdapter;
import com.tuclinicavirtual.mvo.menu.examenes.fragments.MisExamenesFragment;
import com.tuclinicavirtual.mvo.menu.examenes.interfaces.deleteExam;
import com.tuclinicavirtual.mvo.menu.examenes.models.Exam;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamModels;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequest;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequestDelete;
import com.tuclinicavirtual.mvo.menu.examenes.models.ModeloExamenes;
import com.tuclinicavirtual.mvo.menu.examenes.mvp.ExamContrato;
import com.tuclinicavirtual.mvo.menu.examenes.mvp.ExamPresentador;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.SolicitarConsultaRequest;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultaPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultasContrato;
import com.tuclinicavirtual.mvo.utils.Preferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.whalemare.sheetmenu.SheetMenu;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConsultaSolicitaFragment extends Fragment implements deleteExam,SolicitarConsultasContrato.View, ExamContrato.View{


    public ConsultaSolicitaFragment() {
        // Required empty public constructor
    }
    public static String BASE64 = "data:image/png;base64,";
    private  final static int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    final int GALLERY_REQUEST = 22131;

    @BindView(R.id.recycler)
    RecyclerView recycler;

    @BindView(R.id.motivo_consulta)
    EditText motivo_consulta;

    public static String motivoConsulta="nada";

    public static String getMotivoConsulta() {
        return motivoConsulta;
    }

    public static void setMotivoConsulta(String motivoConsulta) {
        ConsultaSolicitaFragment.motivoConsulta = motivoConsulta;
    }

    ExamenesAdapter adaptador;
    ArrayList<ModeloExamenes> listExamenes=new ArrayList<>();
    ExamPresentador presentador;
    Uri uri;
    String exam;
    int id;
    List<ExamModels> exameneslist=new ArrayList<>();
    ExamRequest examRequest;
    List<Exam> examList=new ArrayList<>();
    SolicitarConsultaPresentador presenter;
    String nameArchivo="";
    ProgressDialog progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_consulta_solicita, container, false);
        ButterKnife.bind(this, v);
        initUI();
        adaptador= new ExamenesAdapter(examList,getActivity(),ConsultaSolicitaFragment.this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(getActivity(), R.color.button_bg));
        }

        Log.d("ConsultaSolicita",""+AnteSolicitadFragment.getIdPacienteConsulta());

        motivo_consulta.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                setMotivoConsulta(motivo_consulta.getText().toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }

        });



        return v;
    }


    @OnClick(R.id.floatingActionButton)
    public void floatingActionButton() {
        SheetMenu.with(getActivity())
                .setTitle(R.string.text_examenes)
                .setMenu(R.menu.menu_sheet)
                .setAutoCancel(true)
                .setClick(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if(item.getItemId()==R.id.id_eco)
                        {
                            id=4;
                            exam="Eco";
                            Log.d("MenuSheet","id_eco");
                        }
                        if(item.getItemId()==R.id.id_examenes_laboratorio)
                        {
                            id=1;
                            exam="Examenes de laboratorio";
                            Log.d("MenuSheet","id_examenes_gramgrama");
                        }
                        if(item.getItemId()==R.id.id_examenes_gramgrama)
                        {
                            id=5;
                            exam="Gramgrama";
                            Log.d("MenuSheet","id_examenes_gramgrama");
                        }
                        if(item.getItemId()==R.id.id_informe_medico)
                        {
                            id=2;
                            exam="Informe Médico";
                            Log.d("MenuSheet","id_informe_medico");
                        }
                        if(item.getItemId()==R.id.id_placa_radiologica)
                        {
                            id=3;
                            exam="Placa radiologica";
                            Log.d("MenuSheet","id_examenes_gramgrama");
                        }
                        if(item.getItemId()==R.id.id_otros_exam)
                        {
                            id=6;
                            exam="Otros exam";
                            Log.d("MenuSheet","id_otros_exam");
                        }
                        getRecurso();
                        return false;
                    }
                }).show();
    }


    public void getRecurso()
    {
        if(Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                } else {
                    // No explanation needed, we can request the permission.
                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }

            } else {
                Intent photoIntent = new Intent(Intent.ACTION_PICK);
                photoIntent.setType("image/*");
                startActivityForResult(photoIntent, GALLERY_REQUEST);
            }

            //ANDROID SDK <= 22
        } else {
            //add Photo from gallery
            Intent photoIntent = new Intent(Intent.ACTION_PICK);
            photoIntent.setType("image/*");
            startActivityForResult(photoIntent, GALLERY_REQUEST);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE : {
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
                }
                else {
                    //  Toast.makeText(getActivity(), "Denegado"), Toast.LENGTH_SHORT).show();
                }
            }
            /* other permissions */
        }
    }

    public static String encodeImage(Bitmap bm) {
        Bitmap resize = scaleDown(bm, 500, true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        resize.compress(Bitmap.CompressFormat.PNG, 75, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK)
        {

            if( requestCode == GALLERY_REQUEST) {

                String filePath = null;
                Uri _uri = data.getData();

                Log.d("selectedImageUri",""+ _uri);
                if (_uri != null && "content".equals(_uri.getScheme())) {
                    Cursor cursor = getActivity().getContentResolver().query(_uri, new String[] { android.provider.MediaStore.Images.ImageColumns.DATA }, null, null, null);
                    cursor.moveToFirst();
                    filePath = cursor.getString(0);
                    cursor.close();
                    Log.d("selectedImageUri","33"+ filePath );
                } else {
                    filePath = _uri.getPath();
                    Log.d("selectedImageUri","66"+ filePath );
                }
                if(filePath!=null)
                {

                    File file = new File(filePath);
                    try {
                        FileInputStream fileInputStream = new FileInputStream(file);
                        Log.d("selectedImageUri",""+ fileInputStream);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    nameArchivo=file.getName();
                    RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
                    progress.show();
                    presentador.uploadExam(Preferences.getIDUser(),Preferences.getIDPatientid(),fbody,file.getName());
                }else
                {
                    Toast.makeText(getActivity(),"Error en archivo seleccionado!", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize, boolean filter) {
        float ratio = Math.min((float) maxImageSize / realImage.getWidth()
                , (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height, filter);
        return newBitmap;
    }

    public void initUI()
    {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setTitle("En proceso..");
        presenter=new SolicitarConsultaPresentador(this);
        presentador=new ExamPresentador(this);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(reci);
        recycler.setHasFixedSize(true);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String num) {
        Log.d("pasarSiguiente","Antecedente"+num);

        if(num.equals("4"))
        {
            presentador.getExam(AnteSolicitadFragment.getIdPacienteConsulta());
            Log.d("pasarSiguiente","Antecedente"+AnteSolicitadFragment.getIdPacienteConsulta());
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void getListEspecialidad(List<Especialidad> especialistas) {

    }

    @Override
    public void getListDoctor(List<Doctor> doctor) {

    }

    @Override
    public void getHorarioDoctor(List<Availabilty> horarioDoctor) {

    }

    @Override
    public void getSolicitarConsulta(DataSolicitudConsulta code) {
     Log.d("DataSolicitudConsulta","--->"+code);
    }

    @Override
    public void getPaymentsResp(DataPayment dataPayment) {

    }

    @Override
    public void getRespDoctorFail(String text) {

    }

    @Override
    public void getShoulderDoctor(List<Schedule> hours) {

    }

    @Override
    public void getError(String dato) {

    }

    @Override
    public void getExamenes(List<Exam> dato) {
        Log.d("getExamenes",""+dato);
        Log.d("getExamenes","------->");
        examList.clear();
        examList=dato;
        adaptador= new ExamenesAdapter(examList,getActivity(),ConsultaSolicitaFragment.this);
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

    @Override
    public void getExamRespFalse() {
        examList.clear();
        Log.d("getExamenes","------->FAIL");
        adaptador= new ExamenesAdapter(examList,getActivity(),ConsultaSolicitaFragment.this);
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

    @Override
    public void getDeleteResp(String dato) {

    }

    @Override
    public void setExam(String dato) {
        presentador.getExam(AnteSolicitadFragment.getIdPacienteConsulta());
    }

    @Override
    public void setUpload(String dato) {
        Log.d("-->","setUpload "+dato.toString());
        progress.dismiss();
        exameneslist.clear();
        exameneslist.add(new ExamModels(Preferences.getUtcPacienteCrearCons(),nameArchivo,dato.toString(),id,id));
        examRequest=new ExamRequest(AnteSolicitadFragment.getIdPacienteConsulta(),AnteSolicitadFragment.getIdUserPaciente(),exameneslist);
        presentador.setExam(examRequest);
    }


    @Override
    public void deleteExam() {
        presentador.getExam(AnteSolicitadFragment.getIdPacienteConsulta());
    }
}

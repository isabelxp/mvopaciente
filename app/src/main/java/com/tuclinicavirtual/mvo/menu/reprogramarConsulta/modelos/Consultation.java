package com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Consultation {

    @SerializedName("consultationid")
    @Expose
    private Integer consultationid;
    @SerializedName("code")
    @Expose
    private String code;

    @Override
    public String toString() {
        return "Consultation{" +
                "consultationid=" + consultationid +
                ", code='" + code + '\'' +
                '}';
    }

    public Integer getConsultationid() {
        return consultationid;
    }

    public void setConsultationid(Integer consultationid) {
        this.consultationid = consultationid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}

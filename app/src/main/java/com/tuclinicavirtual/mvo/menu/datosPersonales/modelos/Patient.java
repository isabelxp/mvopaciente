package com.tuclinicavirtual.mvo.menu.datosPersonales.modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Patient {

    @SerializedName("patientid")
    @Expose
    private Integer patientid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("identification_type")
    @Expose
    private String identificationType;
    @SerializedName("identification_number")
    @Expose
    private String identificationNumber;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("occupationid")
    @Expose
    private Integer occupationid;
    @SerializedName("occupation_name")
    @Expose
    private String occupationName;
    @SerializedName("countryid")
    @Expose
    private Integer countryid;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("res_countryid")
    @Expose
    private Integer resCountryid;
    @SerializedName("res_country_name")
    @Expose
    private String resCountryName;
    @SerializedName("timezoneid")
    @Expose
    private Integer timezoneid;
    @SerializedName("timezone_name")
    @Expose
    private String timezoneName;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("relationshipid")
    @Expose
    private Integer relationshipid;
    @SerializedName("relationship_name")
    @Expose
    private String relationshipName;

    @Override
    public String toString() {
        return "Patient{" +
                "patientid=" + patientid +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", sex='" + sex + '\'' +
                ", identificationType='" + identificationType + '\'' +
                ", identificationNumber='" + identificationNumber + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", occupationid=" + occupationid +
                ", occupationName='" + occupationName + '\'' +
                ", countryid=" + countryid +
                ", countryName='" + countryName + '\'' +
                ", resCountryid=" + resCountryid +
                ", resCountryName='" + resCountryName + '\'' +
                ", timezoneid=" + timezoneid +
                ", timezoneName='" + timezoneName + '\'' +
                ", userid=" + userid +
                ", email='" + email + '\'' +
                ", relationshipid=" + relationshipid +
                ", relationshipName='" + relationshipName + '\'' +
                '}';
    }

    public Patient(String name, String lastname, String sex, String identificationType, String identificationNumber, String birthdate, Integer occupationid,Integer countryid,Integer resCountryid,Integer timezoneid, Integer relationshipid) {
        this.name = name;
        this.lastname = lastname;
        this.sex = sex;
        this.identificationType = identificationType;
        this.identificationNumber = identificationNumber;
        this.birthdate = birthdate;
        this.occupationid = occupationid;
        this.countryid = countryid;
        this.resCountryid = resCountryid;
        this.timezoneid = timezoneid;
        this.relationshipid = relationshipid;
    }

    public Patient(Integer patientid, String name, String lastname, String sex, String identificationType, String identificationNumber, String birthdate, Integer occupationid, String occupationName, Integer countryid, String countryName, Integer resCountryid, String resCountryName, Integer timezoneid, String timezoneName, Integer userid, String email, Integer relationshipid, String relationshipName) {
        this.patientid = patientid;
        this.name = name;
        this.lastname = lastname;
        this.sex = sex;
        this.identificationType = identificationType;
        this.identificationNumber = identificationNumber;
        this.birthdate = birthdate;
        this.occupationid = occupationid;
        this.occupationName = occupationName;
        this.countryid = countryid;
        this.countryName = countryName;
        this.resCountryid = resCountryid;
        this.resCountryName = resCountryName;
        this.timezoneid = timezoneid;
        this.timezoneName = timezoneName;
        this.userid = userid;
        this.email = email;
        this.relationshipid = relationshipid;
        this.relationshipName = relationshipName;
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Integer getOccupationid() {
        return occupationid;
    }

    public void setOccupationid(Integer occupationid) {
        this.occupationid = occupationid;
    }

    public String getOccupationName() {
        return occupationName;
    }

    public void setOccupationName(String occupationName) {
        this.occupationName = occupationName;
    }

    public Integer getCountryid() {
        return countryid;
    }

    public void setCountryid(Integer countryid) {
        this.countryid = countryid;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Integer getResCountryid() {
        return resCountryid;
    }

    public void setResCountryid(Integer resCountryid) {
        this.resCountryid = resCountryid;
    }

    public String getResCountryName() {
        return resCountryName;
    }

    public void setResCountryName(String resCountryName) {
        this.resCountryName = resCountryName;
    }

    public Integer getTimezoneid() {
        return timezoneid;
    }

    public void setTimezoneid(Integer timezoneid) {
        this.timezoneid = timezoneid;
    }

    public String getTimezoneName() {
        return timezoneName;
    }

    public void setTimezoneName(String timezoneName) {
        this.timezoneName = timezoneName;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getRelationshipid() {
        return relationshipid;
    }

    public void setRelationshipid(Integer relationshipid) {
        this.relationshipid = relationshipid;
    }

    public String getRelationshipName() {
        return relationshipName;
    }

    public void setRelationshipName(String relationshipName) {
        this.relationshipName = relationshipName;
    }

}

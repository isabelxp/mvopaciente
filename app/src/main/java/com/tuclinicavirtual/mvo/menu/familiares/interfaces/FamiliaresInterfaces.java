package com.tuclinicavirtual.mvo.menu.familiares.interfaces;

import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPacientes;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionResponse;
import com.tuclinicavirtual.mvo.menu.familiares.models.DeleteRequestFamilia;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaRequest;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface FamiliaresInterfaces {

    @GET("patients/family/{id}")
    Call<DataPacientes> getFamiliares(@Path("id") int id,@Header("Authorization") String valor);

    @Headers("Content-Type:application/json")
    @POST("patients/family")
    Call<FamiliaResponse> setFamiliar(@Body FamiliaRequest familir,@Header("Authorization") String valor);

    @Headers("Content-Type:application/json")
    @HTTP(method = "DELETE", path = "patients/family", hasBody = true)
    Call<FamiliaResponse> deleteFamiliar(@Body DeleteRequestFamilia familir, @Header("Authorization") String valor);

}

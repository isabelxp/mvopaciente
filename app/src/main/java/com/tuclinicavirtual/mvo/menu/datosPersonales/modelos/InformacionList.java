package com.tuclinicavirtual.mvo.menu.datosPersonales.modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InformacionList {

    @SerializedName("listid")
    @Expose
    private Integer listid;
    @SerializedName("listname")
    @Expose
    private String listname;
    @SerializedName("valueid")
    @Expose
    private Integer valueid;
    @SerializedName("valuename")
    @Expose
    private String valuename;
    @SerializedName("parentid")
    @Expose
    private Integer parentid;

    public Integer getListid() {
        return listid;
    }

    public void setListid(Integer listid) {
        this.listid = listid;
    }

    public String getListname() {
        return listname;
    }

    public void setListname(String listname) {
        this.listname = listname;
    }

    public Integer getValueid() {
        return valueid;
    }

    public void setValueid(Integer valueid) {
        this.valueid = valueid;
    }

    public String getValuename() {
        return valuename;
    }

    public void setValuename(String valuename) {
        this.valuename = valuename;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(int parentid) {
        this.parentid = parentid;
    }

    @Override
    public String toString() {
        return "InformacionList{" +
                "listid=" + listid +
                ", listname='" + listname + '\'' +
                ", valueid=" + valueid +
                ", valuename='" + valuename + '\'' +
                ", parentid=" + parentid +
                '}';
    }
}

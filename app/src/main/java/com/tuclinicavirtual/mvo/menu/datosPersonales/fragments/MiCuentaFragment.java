package com.tuclinicavirtual.mvo.menu.datosPersonales.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionList;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.PasswordRequest;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesContrato;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesPresentador;
import com.tuclinicavirtual.mvo.utils.Preferences;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MiCuentaFragment extends Fragment implements DatosPersonalesContrato.View{


    public MiCuentaFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.image_uno)
    ImageView image_uno;
    @BindView(R.id.image_dos)
    ImageView image_dos;
    @BindView(R.id.image_tres)
    ImageView image_tres;
    @BindView(R.id.confirmar_contrasena_nuev)
    EditText confirmar_contrasena_nuev;
    @BindView(R.id.password_nueva)
    EditText password_nueva;
    ProgressDialog progress;
    DatosPersonalesPresentador presentador;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_mi_cuenta, container, false);
        ButterKnife.bind(this, v);
        progress = new ProgressDialog(getActivity());
        progress.setTitle(getActivity().getString(R.string.registro_cargando));
        presentador=new DatosPersonalesPresentador(this);
        email.setText(Preferences.getEmailUser());
        editImg();
        return v;
    }

    @OnClick(R.id.botton_guardar)
    public void botton_guardar() {
        if(camposValidar()) {
            if (password.getText().length() > 5) {
                if (isValidPassword(password.getText().toString())) {
                    if (password.getText().toString().equals(Preferences.getClaveLogin())) {
                        if (isValidPassword(password_nueva.getText().toString())) {
                            if (password_nueva.getText().toString().equals(confirmar_contrasena_nuev.getText().toString())) {
                                progress.show();
                                PasswordRequest request = new PasswordRequest(Preferences.getEmailUser(), confirmar_contrasena_nuev.getText().toString(),3);
                                presentador.setNewPassword(request);
                            } else {
                                dialog("No coinciden  contraseñas");
                                confirmar_contrasena_nuev.setError("No coinciden contraseñas");
                                password_nueva.setError("No coinciden  contraseñas");
                            }
                        } else {
                            password_nueva.setError("Contraseña no valida");
                        }
                    }
                } else {
                    password.setError("Contraseña no valida");
                }
            }
        }
    }

public boolean camposValidar()
{
    if(password.getText().length()==0)
    {
        password.setError("contraseña");
        return false;
    }
    if(password_nueva.getText().length()==0)
    {
        password_nueva.setError("Contraseña nueva");
        return false;
    }
    if(confirmar_contrasena_nuev.getText().length()==0)
    {
        confirmar_contrasena_nuev.setError("Confirmar nueva contraseña");
        return false;
    }
    return true;
}
    public void editImg()
    {
        password.addTextChangedListener(new TextWatcher() {

        public void afterTextChanged(Editable s) {

        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(password.length()>5)
            {
                if(isValidPassword(password.getText().toString()))
                {
                    if(password.getText().toString().equals(Preferences.getClaveLogin()))
                    {
                        image_uno.setVisibility(View.VISIBLE);
                    }else
                    {
                        image_uno.setVisibility(View.INVISIBLE);
                    }
                }else{
                    image_uno.setVisibility(View.INVISIBLE);
                }
            }else
                image_uno.setVisibility(View.INVISIBLE);
        }
    });
        password_nueva.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                confirmar_contrasena_nuev.setError(null);
                if(password_nueva.length()>5)
                {
                    if(isValidPassword(password_nueva.getText().toString()))
                    {
                            image_dos.setVisibility(View.VISIBLE);
                    }else
                    {
                        image_dos.setVisibility(View.INVISIBLE);
                    }
                    if (password_nueva.getText().toString().equals(confirmar_contrasena_nuev.getText().toString()))
                    {
                        image_tres.setVisibility(View.VISIBLE);
                    }else
                    {
                        image_tres.setVisibility(View.INVISIBLE);
                    }

                }else
                    image_dos.setVisibility(View.INVISIBLE);
            }
        });

        confirmar_contrasena_nuev.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                password_nueva.setError(null);
                if(confirmar_contrasena_nuev.length()>4)
                {
                    if(isValidPassword(confirmar_contrasena_nuev.getText().toString()))
                    {
                        if (password_nueva.getText().toString().equals(confirmar_contrasena_nuev.getText().toString()))
                        {
                            image_tres.setVisibility(View.VISIBLE);
                        }else
                        {
                            image_tres.setVisibility(View.INVISIBLE);
                        }
                    }else
                    {
                        image_tres.setVisibility(View.INVISIBLE);
                    }
                }else
                    image_tres.setVisibility(View.INVISIBLE);
            }
        });

    }



    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[-@.#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    @Override
    public void setDataPaises(List<InformacionList> dato) {

    }

    @Override
    public void setDataOcupaciones(List<InformacionList> dato) {

    }

    @Override
    public void setDataParentesco(List<InformacionList> dato) {

    }

    @Override
    public void setZonaHoraria(List<InformacionList> dato) {

    }

    @Override
    public void setRespDatosUsuario(String sms) {

    }

    @Override
    public void getDatosPacientes(Patient a) {

    }

    @Override
    public void setRespPasswordNew(String resp, String Token) {
        progress.cancel();
        if(resp.equals("ok"))
        {
            Preferences.setClaveLogin(password_nueva.getText().toString());
            Log.d("setRespPasswordNew222","----->"+Token);
            dialog("Su cambio de contraseña fue exitoso!");
            password.setText("");
            password_nueva.setError(null);
            password.setError(null);
            confirmar_contrasena_nuev.setError(null);
            confirmar_contrasena_nuev.setText("");
            password_nueva.setText("");

        }else
        {
            dialog("Error de conexion con el Servidor. Intente mas tarde");
        }

        Log.d("setRespPasswordNew","----->"+Token);
    }

    public void dialog(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage(resp);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

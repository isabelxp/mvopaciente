package com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens.SolicitarConsultaFragment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponsesEspecialidad;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListEspecialidadAdapter extends RecyclerView.Adapter<ListEspecialidadAdapter.ViewHolder> {
    List<Especialidad> listEspecialidad=new ArrayList<>();
    Context context;

    public static int idEspecialidad;

    public static int getIdEspecialidad() {
        return idEspecialidad;
    }

    public static void setIdEspecialidad(int idEspecialidad) {
        ListEspecialidadAdapter.idEspecialidad = idEspecialidad;
    }


    public static Especialidad especialidad;

    public static Especialidad getEspecialidad() {
        return especialidad;
    }

    public static void setEspecialidad(Especialidad especialidad) {
        ListEspecialidadAdapter.especialidad = especialidad;
    }

    public ListEspecialidadAdapter(List<Especialidad> listEspecialidad, Context context) {
        this.listEspecialidad = listEspecialidad;
        this.context=context;
    }

    int auxiliarId=-1;

    @Override
    public ListEspecialidadAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_especialidad, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ListEspecialidadAdapter.ViewHolder holder,final int position) {
     holder.text_especialidad.setText(listEspecialidad.get(position).getName());

     holder.layout_especialidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.button_bg));
                }
                Log.d("layout_especialidad",""+listEspecialidad.get(position).getSpecialitiesid());
                setEspecialidad(listEspecialidad.get(position));
                auxiliarId=position;
                notifyDataSetChanged();
                setIdEspecialidad(listEspecialidad.get(position).getSpecialitiesid());
                SolicitarConsultaFragment.setPasarSiguiente(true);
                SolicitarConsultaFragment.setEspecialidad(listEspecialidad.get(position).getName());
                SolicitarConsultaFragment.setNumEspecialidad(listEspecialidad.get(position).getSpecialitiesid());
            }
        });
        if(auxiliarId==position){
            holder.layout_especialidad.setBackgroundResource(R.color.blue_tab);
        }
        else
        {
            holder.layout_especialidad.setBackgroundResource(R.color.form_bg);
        }
    }

    @Override
    public int getItemCount() {
        return listEspecialidad.size();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_especialidad)
        TextView text_especialidad;
        @BindView(R.id.layout_especialidad)
        LinearLayout layout_especialidad;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bindData(ResponsesEspecialidad c) {

        }
    }
}

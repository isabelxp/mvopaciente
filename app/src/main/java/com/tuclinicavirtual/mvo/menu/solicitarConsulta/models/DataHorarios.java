package com.tuclinicavirtual.mvo.menu.solicitarConsulta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataHorarios {
    @SerializedName("availabilties")
    @Expose
    private List<Availabilty> availabilties = null;

    public List<Availabilty> getAvailabilties() {
        return availabilties;
    }

    public void setAvailabilties(List<Availabilty> availabilties) {
        this.availabilties = availabilties;
    }
}

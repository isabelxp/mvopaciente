package com.tuclinicavirtual.mvo.menu.examenes.models;

public class ModeloExamenes {
    String examen;
    String asunto;

    public ModeloExamenes(String examen, String asunto) {
        this.examen = examen;
        this.asunto = asunto;
    }

    public String getExamen() {
        return examen;
    }

    public void setExamen(String examen) {
        this.examen = examen;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }
}

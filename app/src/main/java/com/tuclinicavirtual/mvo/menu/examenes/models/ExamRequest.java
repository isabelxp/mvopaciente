package com.tuclinicavirtual.mvo.menu.examenes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExamRequest {

    @SerializedName("patientid")
    @Expose
    private Integer patientid;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("exams")
    @Expose
    private List<ExamModels> exams = null;

    public ExamRequest(Integer patientid, Integer userid, List<ExamModels> exams) {
        this.patientid = patientid;
        this.userid = userid;
        this.exams = exams;
    }

    @Override
    public String toString() {
        return "ExamRequest{" +
                "patientid=" + patientid +
                ", userid=" + userid +
                ", exams=" + exams +
                '}';
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public List<ExamModels> getExams() {
        return exams;
    }

    public void setExams(List<ExamModels> exams) {
        this.exams = exams;
    }
}

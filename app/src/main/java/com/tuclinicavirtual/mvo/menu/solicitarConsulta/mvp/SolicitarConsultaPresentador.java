package com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp;

import android.util.Log;

import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPacientes;
import com.tuclinicavirtual.mvo.menu.familiares.interfaces.FamiliaresInterfaces;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.interfaces.SolicitarConsultaInterfaces;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DoctoHorariosRequest;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.RequestPaymens;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponseDoctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponseHorarioDoctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponsePayments;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponsesEspecialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.SheduleResponse;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.SolicitarConsultaRequest;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.SolicitarConsultaResponse;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.utils.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SolicitarConsultaPresentador implements SolicitarConsultasContrato.Presenter{

    SolicitarConsultaInterfaces rest;
    public SolicitarConsultasContrato.View view;
    private String TAG;

    public SolicitarConsultaPresentador(SolicitarConsultasContrato.View view) {
        this.view = view;
        TAG = this.getClass().getSimpleName();

    }

    @Override
    public void getEspecialistas() {
        rest = RetrofitClient.getClient(Consts.BASE_URL).create(SolicitarConsultaInterfaces.class);
        rest.getEspecialidad("bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ResponsesEspecialidad>() {
            @Override
            public void onResponse(Call<ResponsesEspecialidad> call, Response<ResponsesEspecialidad> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getListEspecialidad(response.body().getData().getSpecialities());
                        Log.i(TAG, "--->Especialistas " + response.body().getData().toString());
                    }
                }else{
                    Log.i(TAG, "--->Error en Especialistas " + call.request().url());
                    Log.i(TAG, "--->Error en Especialistas " + response.code());
                }  }
            @Override
            public void onFailure(Call<ResponsesEspecialidad> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    @Override
    public void getDoctorList(int especialidadNum) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(SolicitarConsultaInterfaces.class);

        rest.getDoctores(especialidadNum,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ResponseDoctor>() {
            @Override
            public void onResponse(Call<ResponseDoctor> call, Response<ResponseDoctor> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getListDoctor(response.body().getData().getDoctors());
                        Log.i(TAG, "--->Doctores " + response.body().getData().getDoctors().toString());
                    }
                }else{

                    Log.i(TAG, "--->Error en Doctores " + call.request().url());
                    Log.i(TAG, "--->Error en Especialistas " + response.code());
                }  }
            @Override
            public void onFailure(Call<ResponseDoctor> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    @Override
    public void getShoulderDoctor(String utc) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(SolicitarConsultaInterfaces.class);

        rest.getShoulderDoctor(utc,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<SheduleResponse>() {
            @Override
            public void onResponse(Call<SheduleResponse> call, Response<SheduleResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getShoulderDoctor(response.body().getData().getSchedules());
                        Log.i(TAG, "--->getShoulderDoctor " + call.request().url());
                        Log.i(TAG, "--->getShoulderDoctor " + response.body().getData().toString());
                    }
                }else{

                    Log.i(TAG, "--->Error en getShoulderDoctor " + call.request().url());
                    Log.i(TAG, "--->Error en getShoulderDoctor " + response.code());
                }  }
            @Override
            public void onFailure(Call<SheduleResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }
    @Override
    public void setHorarioDoctor(DoctoHorariosRequest request) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(SolicitarConsultaInterfaces.class);
        Log.i(TAG, "--->Horrario en Doctores" + request.toString());
        rest.setDoctHorarios(request,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ResponseHorarioDoctor>() {
            @Override
            public void onResponse(Call<ResponseHorarioDoctor> call, Response<ResponseHorarioDoctor> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getHorarioDoctor(response.body().getData().getAvailabilties());
                        Log.i(TAG, "--->Doctores " + response.body().getMessage());
                        Log.i(TAG, "--->Doctores " + response.body().getStatus());
                    }
                }else{
                    view.getRespDoctorFail(String.valueOf(response.code()));
                    Log.i(TAG, "--->Error en Doctores " + call.request().url());
                    Log.i(TAG, "--->Error en Especialistas " + response.code());
                    Log.i(TAG, "--->Error en Especialistas " + response.message());
                }  }
            @Override
            public void onFailure(Call<ResponseHorarioDoctor> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }


    @Override
    public void setSolicitarConsulta(SolicitarConsultaRequest request) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(SolicitarConsultaInterfaces.class);
        Log.i(TAG, "--->setSolicitarConsulta " + request.toString());
        rest.setConsultaPaciente(request,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<SolicitarConsultaResponse>() {
            @Override
            public void onResponse(Call<SolicitarConsultaResponse> call, Response<SolicitarConsultaResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getSolicitarConsulta(response.body().getData());
                        Log.i(TAG, "--->setSolicitarConsulta " + response.body().getMessage());
                        Log.i(TAG, "--->setSolicitarConsulta " + response.body().getStatus());
                    }
                }else{
                    view.getError("error");
                    Log.i(TAG, "--->Error en setSolicitarConsulta " + call.request().url());
                    Log.i(TAG, "--->Error en setSolicitarConsulta " + response.code());
                    Log.i(TAG, "--->Error en setSolicitarConsulta" + response.message());
                }  }
            @Override
            public void onFailure(Call<SolicitarConsultaResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    @Override
    public void setPaymentsUser(RequestPaymens request) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(SolicitarConsultaInterfaces.class);
        Log.i(TAG, "--->setPaymentsUser " + request.toString());
        rest.setPaymentsUser(request,"bearer "+ Preferences.getTokenUser()).enqueue(new Callback<ResponsePayments>() {
            @Override
            public void onResponse(Call<ResponsePayments> call, Response<ResponsePayments> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getPaymentsResp(response.body().getData());
                        Log.i(TAG, "--->setPaymentsUser " + response.body().getMessage());
                        Log.i(TAG, "--->setPaymentsUser " + response.body().getStatus());
                    }
                }else{
                    Log.i(TAG, "--->Error en setSolicitarConsulta " + call.request().url());
                    Log.i(TAG, "--->Error en setSolicitarConsulta " + response.code());
                    Log.i(TAG, "--->Error en setSolicitarConsulta" + response.message());
                }  }
            @Override
            public void onFailure(Call<ResponsePayments> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }


}

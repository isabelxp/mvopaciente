package com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.antecedentes.adapter.AntecedentesAdapter;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.MisConsultasFragment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter.ListDoctoresAdapter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter.ListEspecialidadAdapter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.interfaces.GuardarAntecedentesInter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.SolicitarConsultaRequest;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultaPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultasContrato;
import com.tuclinicavirtual.mvo.utils.Preferences;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class SolicitarConsultaFragment extends Fragment implements SolicitarConsultasContrato.View{


    private SectionsPagerAdapter mSectionsPagerAdapter;
    public SolicitarConsultaFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.container)
    ViewPager mViewPager;

    SolicitarConsultaPresentador presenter;
    @BindView(R.id.cancelar)
    Button cancelar;
    int auxiliar=1000;

    public static Button aceptar;
    ProgressDialog progress;
    public static int numEspecialidad;
    public static boolean pasarSiguiente=false;
    public static boolean pasarSiguienteAuxi=false;
    public static String especialidad;
    public static boolean pago=false;
    int i=1;

    public static String getEspecialidad() {
        return especialidad;
    }

    public static void setEspecialidad(String especialidad) {
        SolicitarConsultaFragment.especialidad = especialidad;
    }

    public static boolean isPasarSiguiente() {
        return pasarSiguiente;
    }

    public static void setPasarSiguiente(boolean pasarSiguiente) {
        SolicitarConsultaFragment.pasarSiguiente = pasarSiguiente;
    }

    public static int getNumEspecialidad() {
        return numEspecialidad;
    }

    public static void setNumEspecialidad(int numEspecialidad) {
        SolicitarConsultaFragment.numEspecialidad = numEspecialidad;
    }

   // GuardarAntecedentesInter inter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_solicitar_consulta, container, false);
        ButterKnife.bind(this, v);

        aceptar=(Button)v.findViewById(R.id.aceptar);


        initUI();
        progress = new ProgressDialog(getContext());

        //inter=(GuardarAntecedentesInter)SolicitarConsultaFragment.this;
        return v;
    }


    @OnClick(R.id.cancelar)
    public void cancelar() {
        Log.d("pasarSiguiente","1"+i);
        if(i==2 || (i==3 && !pago) || i==5 )
        {
            i--;
            Log.d("pasarSiguiente","aqui"+i);
            if(i==1)
            {
                if(!pago)
                {
                    mViewPager.setCurrentItem(0);
                    SolicitarConsultaFragment.setPasarSiguiente(true);
                }
            }
            if(i==4)
            {
                mViewPager.setCurrentItem(3);
                aceptar.setText("siguiente");
                Log.d("pasarSiguiente","Holaaaaaaaa Mundooooo");
               // EventBus.getDefault().post(String.valueOf(3));
            }
            if(i==3)
            {
                if(!pago)
                {
                    mViewPager.setCurrentItem(3);
                }
               // EventBus.getDefault().post(String.valueOf(2));
            }
            if(i==2)
            {
                if(!pago)
                {
                    mViewPager.setCurrentItem(1);
                    EventBus.getDefault().post(String.valueOf(55));
                    pasarSiguienteAuxi=true;
                }
                //EventBus.getDefault().post(String.valueOf(1));
            }

            Log.d("pasarSiguiente","2"+i);
        }

        else
        {
            if(i==1) {
                if(!pago)
                {
                initFragments(new MisConsultasFragment());
                }
            }
         //   initFragments(new MisConsultasFragment());
        }
    }

    @OnClick(R.id.aceptar)
    public void aceptar() {
        boolean verdad=false;
        Log.d("pasarSiguiente","bollean"+pasarSiguiente);
        if (i==5)
        {
             Log.d("solicitaconsulta-",""+ConsultaSolicitaFragment.getMotivoConsulta().trim().replaceAll("\\s{2,}", " "));

                if(ConsultaSolicitaFragment.getMotivoConsulta().trim().replaceAll("\\s{2,}", " ").length()>=6)
                {
                    progress.setCancelable(false);
                    progress.setTitle("Estamos registrando su consulta...");
                    progress.show();
                    SolicitarConsultaRequest request;
                    request = new SolicitarConsultaRequest(Preferences.getUtcPacienteCrearCons(), AnteSolicitadFragment.getIdUserPaciente(), AnteSolicitadFragment.getIdPacienteConsulta(), ListDoctoresAdapter.getIdMedico(), ListEspecialidadAdapter.getIdEspecialidad(), ListDoctoresAdapter.getTipoConsulta(),PagoFragment.confirma, ConsultaSolicitaFragment.getMotivoConsulta().trim().replaceAll("\\s{2,}", " "), ListDoctoresAdapter.getFechaSelec(), ListDoctoresAdapter.getHoraSelec());
                    Log.d("quieroverrrr","bollean"+PagoFragment.confirma);
                    presenter.setSolicitarConsulta(request);
                    ConsultaSolicitaFragment.setMotivoConsulta("");
                }else
                {
                    dialog("Debe escribir un motivo para solicitar la consulta.");
                }

        }
        if(i==4)
        {
            EventBus.getDefault().post("100");
        }
        if(pasarSiguiente || pasarSiguienteAuxi)
        {

            if (i<=4)
            {
                if(i==1)
                {
                        if(auxiliar!=ListEspecialidadAdapter.getIdEspecialidad() && pasarSiguiente) {
                            EventBus.getDefault().post(String.valueOf(i));
                            verdad = true;

                        }else
//                        {
////                            if()
////                            {
////
////                            }
//                            SolicitarConsultaFragment.setPasarSiguiente(true);
//                        }

                    auxiliar=ListEspecialidadAdapter.getIdEspecialidad();
                }else

                Log.d("pasarSiguiente","--->"+String.valueOf(i));
                 if(!pasarSiguiente)
                 {
                     i--;
                     aceptar.setBackgroundTintList(ContextCompat.getColorStateList(getActivity(), R.color.form_shadow));
                 }else
                 {
                     EventBus.getDefault().post(String.valueOf(i));
                     mViewPager.setCurrentItem(i);
                 }

            }
            //colorIcon(i);
            i++;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                aceptar.setBackgroundTintList(ContextCompat.getColorStateList(getActivity(), R.color.form_shadow));
            }
            pasarSiguiente=false;
            pasarSiguienteAuxi=false;
        }else
        {
            //inter.editextError();
            if(i==4)
            {
                EventBus.getDefault().post("101");
             //   dialog("Debes indicar tu altura y peso para continuar!");
            }
            if(i<4)
            {
                dialog("Debes seleccionar una opción para continuar..");
            }

        }
        if (i==2)
        {
            EventBus.getDefault().post(String.valueOf(22));
            cancelar.setText("REGRESAR");
        }
        if (i==1)
        {
            cancelar.setText("CANCELAR");

        }
        if (i==4)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                aceptar.setBackgroundTintList(ContextCompat.getColorStateList(getActivity(), R.color.button_bg));
            }
        }
        if (i==5)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                aceptar.setBackgroundTintList(ContextCompat.getColorStateList(getActivity(), R.color.button_bg));
            }
            aceptar.setText("Guardar");
        }
        if (verdad && pasarSiguiente)
        {
            if(i==2)
            {
                if(auxiliar==ListEspecialidadAdapter.getIdEspecialidad()) {

                    if(ListDoctoresAdapter.getNombreDr()!=null)
                    {
                        if(ListDoctoresAdapter.getNombreDr().length()>4)
                        {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                aceptar.setBackgroundTintList(ContextCompat.getColorStateList(getActivity(), R.color.button_bg));
                            }
                            pasarSiguiente=true;
                        }

                    }
                }
            }

        }

    }

    public void dialog(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage(resp);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
              dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void dialog2(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Consulta guardada");
        builder.setMessage(resp);
        builder.setCancelable(false);

        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                initFragments(new MisConsultasFragment());
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }



    public void initFragments(Fragment fragment)
    {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }


    public void initUI()
    {
        presenter=new SolicitarConsultaPresentador(this);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager(),getActivity());
        mViewPager.setCurrentItem(2,true);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabsFromPagerAdapter(mSectionsPagerAdapter);
        tabLayout.getTabAt(0).setIcon(R.drawable.item_one);
        tabLayout.getTabAt(1).setIcon(R.drawable.item_two);
        tabLayout.getTabAt(2).setIcon(R.drawable.item_three);
        tabLayout.getTabAt(3).setIcon(R.drawable.item_four);
        tabLayout.getTabAt(4).setIcon(R.drawable.item_five);

        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }

    }
    public static String hourFormatter(String startTime){
        //String startTime = "2013-02-27 21:06:30";
        String formatedDate =null;

        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
        Date date = null;
        try {
            date = dateFormatter.parse(startTime);


            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
            String displayValue = timeFormatter.format(date);
            return displayValue;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formatedDate;
    }


    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    private static String deleteChar(String text, int charPos) {
        return new StringBuilder(text).deleteCharAt(charPos).toString();
    }
    @Override
    public void getListEspecialidad(List<Especialidad> especialistas) {

    }

    @Override
    public void getListDoctor(List<Doctor> doctor) {

    }

    @Override
    public void getHorarioDoctor(List<Availabilty> horarioDoctor) {

    }

    public void colorIcon(int num)
    {
        Log.d("colorIcon","--->"+num);
        if (num==0)
        {
            Log.d("colorIcon","0");
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_one);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_two_gris);
        }
        if (num==1)
        {
            Log.d("colorIcon","1");
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_one_gris);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_two);
        }
        if (num==2)
        {
            Log.d("colorIcon","2");
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_two_gris);
            tabLayout.getTabAt(2).setIcon(R.drawable.ic_three);
        }
        if (num==3)
        {
            Log.d("colorIcon","3");
            tabLayout.getTabAt(2).setIcon(R.drawable.ic_three_gris);
            tabLayout.getTabAt(3).setIcon(R.drawable.ic_four);
        }
        if (num==4)
        {
            Log.d("colorIcon","4");
            tabLayout.getTabAt(3).setIcon(R.drawable.ic_four_gris);
            tabLayout.getTabAt(4).setIcon(R.drawable.ic_five);
        }
    }

    @Override
    public void getSolicitarConsulta(DataSolicitudConsulta code) {
        if (ListDoctoresAdapter.getTipoConsulta()==1)
        {
            String title="Ud. tiene una nueva consulta escrita en la especialidad "+SolicitarConsultaFragment.getEspecialidad() +" con el Dr "+ ListDoctoresAdapter.getNombreDr() +". En 24 horas le será notificada la respuesta.";
            dialog2(title);
        }else
        {
            String modalidad=null;
            if (ListDoctoresAdapter.getTipoConsulta()==2)
            {
                modalidad="Chat";
            }
            if (ListDoctoresAdapter.getTipoConsulta()==3)
            {
                modalidad="Llamada";
            }
            if (ListDoctoresAdapter.getTipoConsulta()==4)
            {
                modalidad="VideoLlamada";
            }
            String title="Ud. tiene una nueva consulta programada en la modalidad de "+ modalidad +" en la especialidad "+SolicitarConsultaFragment.getEspecialidad() +" con el Dr "+ ListDoctoresAdapter.getNombreDr() +" para el dia "+parseDateToddMMyyyy(ListDoctoresAdapter.getFechaSelec()) +" a las "+ListDoctoresAdapter.getHoraSelec();
            dialog2(title);
        }

        progress.dismiss();
    }

    @Override
    public void getPaymentsResp(DataPayment dataPayment) {

    }

    @Override
    public void getRespDoctorFail(String text) {

    }

    @Override
    public void getShoulderDoctor(List<Schedule> hours) {

    }

    @Override
    public void getError(String dato) {
        progress.dismiss();
        dialog("Intente mas tarde!");
    }


    public static class SectionsPagerAdapter extends FragmentPagerAdapter {
        Context context;
        public SectionsPagerAdapter(FragmentManager fm,Context context) {
            super(fm);
            this.context=context;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case 0:
                    return new EspecialidadFragment();
                case 1:
                    return new MedicoFragment();
                case 2:
                    return new PagoFragment();
                case 3:
                    return new AnteSolicitadFragment();
                case 4:
                    return new ConsultaSolicitaFragment();
                default:
                    return null;

            }

        }

        @Override
        public int getCount() {

            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return context.getResources().getString(R.string.solicita_especialidad);
                case 1:
                    return context.getResources().getString(R.string.solicita_medico);
                case 2:
                    return context.getResources().getString(R.string.solicita_pago);
                case 3:
                    return context.getResources().getString(R.string.solicita_antecedentes);
                case 4:
                    return context.getResources().getString(R.string.solicita_consulta);
            }
            return null;
        }
    }

}

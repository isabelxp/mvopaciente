package com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp;

import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaRequest;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Consultation;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DoctoHorariosRequest;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.RequestPaymens;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponseHorarioDoctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.SolicitarConsultaRequest;

import java.util.List;

public class SolicitarConsultasContrato {

    public interface View{
        void getListEspecialidad(List<Especialidad> especialistas);
        void getListDoctor(List<Doctor> doctor);
        void getHorarioDoctor(List<Availabilty> horarioDoctor);
        void getSolicitarConsulta(DataSolicitudConsulta code);
        void getPaymentsResp(DataPayment dataPayment);
        void getRespDoctorFail(String text);
        void getShoulderDoctor(List<Schedule> hours);
        void getError(String dato);
    }

    public interface Presenter{
        void getEspecialistas();
        void getDoctorList(int especialidadNum);
        void setHorarioDoctor(DoctoHorariosRequest request);
        void setSolicitarConsulta(SolicitarConsultaRequest request);
        void setPaymentsUser(RequestPaymens request);
        void getShoulderDoctor(String text);
    }
}

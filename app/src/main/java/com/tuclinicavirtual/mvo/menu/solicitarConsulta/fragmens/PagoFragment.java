package com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter.ListDoctoresAdapter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter.ListEspecialidadAdapter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.RequestPaymens;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultaPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultasContrato;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.paypal.PaypalManager;
import com.tuclinicavirtual.mvo.utils.Preferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.tuclinicavirtual.mvo.menu.solicitarConsulta.paypal.PaypalManager.REQUEST_CODE_PAYPAL;

/**
 * A simple {@link Fragment} subclass.
 */
public class PagoFragment extends Fragment implements SolicitarConsultasContrato.View{


    public PagoFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.img_paypal)
    ImageView img_paypal;
    @BindView(R.id.text_paypal)
    TextView text_paypal;

    SolicitarConsultaPresentador presentador;

    public static int confirma;
    boolean pagoExitoso=false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_pago, container, false);
        ButterKnife.bind(this, v);
        presentador=new SolicitarConsultaPresentador(this);



        PaypalManager.getInstance().initPaypalService(getActivity());


//        dialog("Pago Exitoso");
//        SolicitarConsultaFragment.setPasarSiguiente(true);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(getActivity(), R.color.button_bg));
//
//        }

        return v;
    }

    @OnClick(R.id.img_paypal)
    public void img_paypal() {
        if(!pagoExitoso)
        {
            startActivityForResult(PaypalManager.getInstance().makePayment(getActivity(), tipo() +", "+ SolicitarConsultaFragment.getEspecialidad(), String.valueOf(ListEspecialidadAdapter.getEspecialidad().getConsultationCost()), new PaypalManager.PaypalPayListener() {
                @Override
                public void onPaypalPaySuccess(JSONObject paymentConfirm) {
                    pagoExitoso=true;
                    String nota="Su pago de US $ "+ListEspecialidadAdapter.getEspecialidad().getConsultationCost()+" se ha realizado exitosamente";
                    dialog(nota);
                    SolicitarConsultaFragment.pago=true;
                    SolicitarConsultaFragment.setPasarSiguiente(true);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(getActivity(), R.color.button_bg));
                    }
                    Log.d("DAWDA","--->onPaypalPaySuccess");
                }

                @Override
                public void onPaypalPayFail() {
                    dialog("Debe cancelar la consulta para continua");
                    Log.d("DAWDA","--->onPaypalPayFail");
                }
            }), REQUEST_CODE_PAYPAL);
        }else
        {
            dialog("Usted ya cancelo la consulta");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE_PAYPAL)
        {
            PaypalManager.getInstance().onActivityResult(resultCode, data);

            presentador.setPaymentsUser(new RequestPaymens("UTC -03:00", Preferences.getIDUser(), "321564", 30, 2, Preferences.getIDUser()));
//            Log.d("DAWDA","--->"+requestCode);
//            Log.d("DAWDA","--->"+data.getData());
//            Log.d("DAWDA","--->"+resultCode);

          //Log.d("DAWDA","--->"+data.getData().getAuthority());
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String num) {

        Log.d("pasarSiguiente","Pago Exitoso"+num);

        if(num.equals("2"))
        {
            if(tipo().equals("Escrita"))
            {
                text_paypal.setText(" La consulta solicitada es: modalidad "+ tipo() + ", " +
                        "\n Especialidad "+ SolicitarConsultaFragment.getEspecialidad() +", con el Dr. "+ListDoctoresAdapter.getNombreDr()+"" +" ("+ListDoctoresAdapter.getObjetoDoctorSelec().getTimezonename() +")"+
                        " \n Costo: "+ListEspecialidadAdapter.getEspecialidad().getConsultationCost()+"$.");
            }else
            {
                text_paypal.setText(" La consulta solicitada es: modalidad "+ tipo() + ", " +
                        "\n Especialidad "+ SolicitarConsultaFragment.getEspecialidad() +", con el Dr. "+ListDoctoresAdapter.getNombreDr()+"" +
                        " \n para la fecha "+parseDateToddMMyyyy(ListDoctoresAdapter.getFechaSelec())+" a la hora "+ListDoctoresAdapter.getHoraSelec()+" ("+ListDoctoresAdapter.getObjetoDoctorSelec().getTimezonename() +")"+
                        " \n Costo: "+ListEspecialidadAdapter.getEspecialidad().getConsultationCost()+"$.");
            }

        }

    };
    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    private static String deleteChar(String text, int charPos) {
        return new StringBuilder(text).deleteCharAt(charPos).toString();
    }
    public String tipo() {

        String tipo="";
        if(ListDoctoresAdapter.getTipoConsulta()==1)
        {
            tipo="Escrita";
        }
        if(ListDoctoresAdapter.getTipoConsulta()==2)
        {
            tipo="Chat";
        }
        if(ListDoctoresAdapter.getTipoConsulta()==3)
        {
            tipo="Llamada";
        }
        if(ListDoctoresAdapter.getTipoConsulta()==4)
        {
            tipo="VideoLlamada";
        }
        return tipo;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void dialog(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage(resp);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //presenter.getAntecedente(Preferences.getIDPatientid());
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public void getListEspecialidad(List<Especialidad> especialistas) { }

    @Override
    public void getListDoctor(List<Doctor> doctor) { }

    @Override
    public void getHorarioDoctor(List<Availabilty> horarioDoctor) { }

    @Override
    public void getSolicitarConsulta(DataSolicitudConsulta code) { }

    @Override
    public void getPaymentsResp(DataPayment dataPayment) {
        Log.d("getPaymentsResp","--->onPaypalPaySuccess"+dataPayment);
        confirma=dataPayment.getPaymentid();
    }

    @Override
    public void getRespDoctorFail(String text) {

    }

    @Override
    public void getShoulderDoctor(List<Schedule> hours) {

    }

    @Override
    public void getError(String dato) {

    }
}

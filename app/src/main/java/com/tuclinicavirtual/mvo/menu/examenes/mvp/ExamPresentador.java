package com.tuclinicavirtual.mvo.menu.examenes.mvp;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuclinicavirtual.mvo.menu.datosPersonales.interfaces.RestdatosPersonalesInterfaces;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionResponse;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesContrato;
import com.tuclinicavirtual.mvo.menu.examenes.interfaces.ExamInterfaces;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamDeleteResponse;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequest;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequestDelete;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamResponse;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRespuestaPost;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.models.RegistroResponse;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExamPresentador {

    ExamInterfaces rest;
    public ExamContrato.View view;
    private String TAG;

    public ExamPresentador(ExamContrato.View view) {
        this.view = view;
        TAG = this.getClass().getSimpleName();
    }
    public void getExam(int id) {
        Log.d("getExamenes",""+id);
        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ExamInterfaces.class);

        rest.getExam(id,"bearer "+Preferences.getTokenUser()).enqueue(new Callback<ExamResponse>() {
            @Override
            public void onResponse(Call<ExamResponse> call, Response<ExamResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i(TAG, "--->exam: " + response.body().getData().getExams().size());

                        view.getExamenes(response.body().getData().getExams());

                        Log.i(TAG, "--->exam: " + response.body().getMessage());
                        Log.i(TAG, "--->exam: " + response.body().getStatus());
                    }
                }else{
                    view.getExamRespFalse();
                    Log.i(TAG, "--->Error en exam: " + call.request().url());
                    Log.i(TAG, "--->Error en exam: " + response.code());
                    Log.i(TAG, "--->Error en exam: " + response.errorBody());
                    Log.i(TAG, "--->Error en exam: " + response.message());
                    Log.i(TAG, "--->Error en exam: " + call.request().toString());
                   // Log.i(TAG, "--->Error en exam: " + response.raw().);
                    //Log.i(TAG, "--->Error en exam: " + response.body().getMessage());
                    //Log.i(TAG, "--->Error en exam: " + response.body().getStatus());
                }  }
            @Override
            public void onFailure(Call<ExamResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void setExam(ExamRequest guardar) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ExamInterfaces.class);
        Log.d("---->","ExamRequest"+guardar.toString());
        rest.setExam(guardar,"bearer "+Preferences.getTokenUser()).enqueue(new Callback<ExamRespuestaPost>() {
            @Override
            public void onResponse(Call<ExamRespuestaPost> call, Response<ExamRespuestaPost> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setExam(response.body().getMessage());
                        Log.i(TAG, "--->set exam: " + response.body().getMessage());
                        Log.i(TAG, "--->set exam: " + response.body().getStatus());
                    }
                }else{
                    Gson gson = new GsonBuilder().create();
                    ExamRespuestaPost pojo = new ExamRespuestaPost();
                    try {
                        pojo = gson.fromJson(response.errorBody().string(), ExamRespuestaPost.class);
                        view.setExam(pojo.getMessage());
                    } catch (IOException e) { }

                    Log.i(TAG, "--->set Error en exam: " + call.request().url());
                    Log.i(TAG, "--->set Error en exam: " + response.code());

                }  }
            @Override
            public void onFailure(Call<ExamRespuestaPost> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void deleteExam(ExamRequestDelete guardar) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ExamInterfaces.class);
        Log.d("---->","ExamRequest"+guardar.toString());

        rest.deleteExam(guardar,"bearer "+Preferences.getTokenUser()).enqueue(new Callback<ExamDeleteResponse>() {
            @Override
            public void onResponse(Call<ExamDeleteResponse> call, Response<ExamDeleteResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                    view.getDeleteResp(response.body().getMessage());
                        Log.i(TAG, "--->set exam: " + response.body().getMessage());
                        Log.i(TAG, "--->set exam: " + response.body().getStatus());
                    }
                }else{
                    Gson gson = new GsonBuilder().create();
                    ExamDeleteResponse pojo = new ExamDeleteResponse();
                    try {
                        pojo = gson.fromJson(response.errorBody().string(), ExamDeleteResponse.class);
                        view.getDeleteResp(pojo.getMessage());
                    } catch (IOException e) { }

                    Log.i(TAG, "--->set Error en exam: " + call.request().url());
                    Log.i(TAG, "--->set Error en exam: " + response.code());

                }  }
            @Override
            public void onFailure(Call<ExamDeleteResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    public void uploadExam(int userid, int pacienteid, RequestBody requestFile, String nombreArchivo) {

        rest = RetrofitClient.getClient(Consts.BASE_URL).create(ExamInterfaces.class);


        MultipartBody.Part multipartBody =MultipartBody.Part.createFormData("file",nombreArchivo,requestFile);

        rest.uploadExam(userid,pacienteid,
                multipartBody,
                "bearer "+Preferences.getTokenUser()).enqueue(new Callback<ExamDeleteResponse>() {
            @Override
            public void onResponse(Call<ExamDeleteResponse> call, Response<ExamDeleteResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setUpload(response.body().getMessage());
                        Log.i(TAG, "--->upload exam: " + response.body().getMessage());
                        Log.i(TAG, "--->upload exam: " + response.body().getStatus());
                    }
                }else{
                    Gson gson = new GsonBuilder().create();
                    ExamDeleteResponse pojo = new ExamDeleteResponse();
                    try {
                        pojo = gson.fromJson(response.errorBody().string(), ExamDeleteResponse.class);
                        view.setUpload(pojo.getMessage());
                    } catch (IOException e) { }

                    Log.i(TAG, "--->upload Error en exam: " + call.request().url());
                    Log.i(TAG, "--->upload Error en exam: " + response.code());

                }  }
            @Override
            public void onFailure(Call<ExamDeleteResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }


}

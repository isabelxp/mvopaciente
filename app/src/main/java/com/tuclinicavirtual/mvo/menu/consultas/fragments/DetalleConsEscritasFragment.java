package com.tuclinicavirtual.mvo.menu.consultas.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasEscritas;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasProgramada;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.ListAdapter;
import com.tuclinicavirtual.mvo.menu.consultas.models.Consultation;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostic;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostico;
import com.tuclinicavirtual.mvo.menu.consultas.models.EstatusResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ExamLab;
import com.tuclinicavirtual.mvo.menu.consultas.models.Prescripcion;
import com.tuclinicavirtual.mvo.menu.consultas.models.PrescripcionItem;
import com.tuclinicavirtual.mvo.menu.consultas.models.ReporteRequest;
import com.tuclinicavirtual.mvo.menu.consultas.models.ScoreRequest;
import com.tuclinicavirtual.mvo.menu.consultas.mvp.ConsultasContrato;
import com.tuclinicavirtual.mvo.menu.consultas.mvp.ConsultasPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultaPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultasContrato;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetalleConsEscritasFragment extends Fragment implements ConsultasContrato.View, SolicitarConsultasContrato.View  {


    public DetalleConsEscritasFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.img_typo)
    ImageView img_typo;

    @BindView(R.id.text_consulta)
    TextView text_consulta;

    @BindView(R.id.text_nombre_dr)
    TextView text_nombre_dr;

    @BindView(R.id.text_especialidad)
    TextView text_especialidad;

    @BindView(R.id.button_reportar)
    Button button_reportar;

    @BindView(R.id.text_nombre)
    TextView text_nombre;

    @BindView(R.id.text_sexo)
    TextView text_sexo;

    @BindView(R.id.text_pago)
    TextView text_pago;

    @BindView(R.id.text_edad)
    TextView text_edad;

    @BindView(R.id.text_motivo)
    TextView text_motivo;

    @BindView(R.id.text_date)
    TextView text_date;

    @BindView(R.id.text_respuesta_especialista)
    TextView text_respuesta_especialista;

    @BindView(R.id.text_informe_medico)
    TextView text_informe_medico;

    @BindView(R.id.text_informe_medico2)
    TextView text_informe_medico2;

    @BindView(R.id.text_informe_medico3)
    TextView text_informe_medico3;

    @BindView(R.id.layout_respuesta_especialista)
    LinearLayout layout_respuesta_especialista;

    @BindView(R.id.layout_informe_medico)
    LinearLayout layout_informe_medico;

    @BindView(R.id.text_status)
    TextView text_status;

    @BindView(R.id.text_recipe_receta)
    TextView text_recipe_receta;

    @BindView(R.id.diagnostic_presuntivo)
    TextView diagnostic_presuntivo;

    @BindView(R.id.layout_observacion_del_diagnostico)
    LinearLayout layout_observacion_del_diagnostico;

    @BindView(R.id.text_indicaciones)
    TextView text_indicaciones;

    @BindView(R.id.text_status_consulta)
    TextView text_status_consulta;

    @BindView(R.id.listRecipe_receta)
    RecyclerView listRecipe_receta;

//    @BindView(R.id.listRecipe_receta2)
//    RecyclerView listRecipe_receta2;

    @BindView(R.id.img_rating)
    ImageView img_rating;

    @BindView(R.id.text_price)
    TextView text_price;

    @BindView(R.id.text_recipe)
    TextView text_recipe;

    @BindView(R.id.text_limite_resp)
    TextView text_limite_resp;

    @BindView(R.id.layout_text_status)
    LinearLayout layout_text_status;

    @BindView(R.id.layout_ordenes)
    LinearLayout layout_ordenes;

    @BindView(R.id.layout_recipes)
    LinearLayout layout_recipes;

    @BindView(R.id.listview)
    RecyclerView listview;

    @BindView(R.id.recycler_diagnostico)
    RecyclerView recycler_diagnostico;

    @BindView(R.id.recycler_indicaciones3)
    RecyclerView recycler_indicaciones3;

    @BindView(R.id.recycler_indicaciones2)
    RecyclerView recycler_indicaciones2;

    @BindView(R.id.recycler_indicaciones1)
    RecyclerView recycler_indicaciones1;



    @BindView(R.id.layout_recipe_indicaciones)
    LinearLayout layout_recipe_indicaciones;

    ListAdapter listAdapter;
    ListAdapter listDiagnostico;

    ConsultasPresentador presentador;
    SolicitarConsultaPresentador presenterSolicitarConsulta;

    List<String> arraySpinner=new ArrayList<>();

    ProgressDialog progress;
    boolean reportar=false;
    boolean rate=false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_detalle_cons_escritas, container, false);
        ButterKnife.bind(this, v);

        presentador=new ConsultasPresentador(this);
        presentador.getDiagnostico(AdapterConsultasEscritas.getConsultaEscrita().getConsultationid());
        presentador.getPrescripcion(AdapterConsultasEscritas.getConsultaEscrita().getConsultationid());
        presentador.getExamLab(AdapterConsultasEscritas.getConsultaEscrita().getConsultationid());
        presentador.getPrescripcionItem(AdapterConsultasEscritas.getConsultaEscrita().getConsultationid());
        presentador.getDiagnosticoItem(AdapterConsultasEscritas.getConsultaEscrita().getConsultationid());
        Log.d("idConsulta",""+AdapterConsultasEscritas.getConsultaEscrita().toString());
        AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito());
        initUI();
        if(AdapterConsultasEscritas.getConsultaEscrita().getDoctorAnswer()!=null)
        {
            text_respuesta_especialista.setText(AdapterConsultasEscritas.getConsultaEscrita().getDoctorAnswer().toString());
        }
        if(AdapterConsultasEscritas.getConsultaEscrita().getMedicalReport()!=null)
        {
            text_informe_medico2.setText(AdapterConsultasEscritas.getConsultaEscrita().getMedicalReport().toString());
        }

        HomeActivity.setDetalleConsulta(false);
        presenterSolicitarConsulta=new SolicitarConsultaPresentador(this);
        presenterSolicitarConsulta.getEspecialistas();
        Log.d("idConsulta",""+AdapterConsultasEscritas.getConsultaEscrita().toString());

        return v;
    }

    public void initUI()
    {
        progress = new ProgressDialog(getContext());
        progress.setTitle(getContext().getString(R.string.registro_cargando));
        progress.setCancelable(false);
        initImagenIcon();
        text_limite_resp.setText(parseDateToddMMyyyy2(AdapterConsultasEscritas.getConsultaEscrita().getRealFinishDate()));
        condicionesUso(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getStatusid());
        text_date.setText(parseDateToddMMyyyy(AdapterConsultasEscritas.getConsultaEscrita().getConsultationDate()));
        text_status_consulta.setText(AdapterConsultasEscritas.getConsultaEscrita().getStatusname());
        //text_hour.setText(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getConsultationHour());
        text_nombre.setText(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getPatientName());
        text_sexo.setText(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getSex());
        text_pago.setText(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getPaymentnumber());
       // text_edad.setText();

        text_motivo.setText(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getReason());
        text_nombre_dr.setText(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getDoctorName());
        text_consulta.setText("CONSULTA "+ AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getCode());
        text_especialidad.setText(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getSpecialityname());
        text_status.setText(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getStatusname());

        int edad = 0;
        try {
            DateFormat dateFormat = dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date fechaNacimiento = dateFormat.parse(AdapterConsultasEscritas.getConsultaEscrita().getBirthdate().replace("/", "-"));
            Calendar cal = Calendar.getInstance();
            Date fechaActual = cal.getTime();

            edad = getEdad(fechaNacimiento, fechaActual);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        text_edad.setText(""+edad);

        if(AdapterConsultasEscritas.getConsultaEscrita().getStatusid()==4)
        {
            if(AdapterConsultasEscritas.getConsultaEscrita().getIsRated()==0)
            {
                text_status.setText("Calificar");
            }else
            {
                img_rating.setImageResource(R.drawable.ic_star_black_24dp);
                text_status.setText(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getStatusname());
            }
        }


    }

    public int getEdad(Date fechaNacimiento, Date fechaActual) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int dIni = Integer.parseInt(formatter.format(fechaNacimiento));
        int dEnd = Integer.parseInt(formatter.format(fechaActual));
        int age = (dEnd - dIni) / 10000;
        return age;
    }

    public int validateDate(String date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date strDate = null;
        try {
            strDate = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (new Date().equals(strDate)) {
            Log.d("validatefech","-si--->"+strDate);
            return 1;
            //catalog_outdated = 1;
        }
        if (new Date().after(strDate)) {
            Log.d("validatefech","-si--->"+strDate);
            return 2;
            //catalog_outdated = 1;
        }else
        {
            Log.d("validatefech","-no--->");
            return 3;
        }

    }
    public void  initImagenIcon()
    {

        button_reportar.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_edit, 0, 0, 0);
        img_typo.setBackgroundResource(R.drawable.circule);

    }
    @Override
    public void getDiagnosticoList(List<Diagnostico> dato) {

        diagnostic_presuntivo.setText(dato.get(0).getDiagnostic());

    }


    public void condicionesUso(int num)
    {
        button_reportar.setBackgroundResource(R.color.button_red);
        if(num==3)
        {
            button_reportar.setVisibility(View.VISIBLE);
            button_reportar.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_edit, 0, 0, 0);
            button_reportar.setText("Reportar");
            button_reportar.setBackgroundResource(R.color.button_red);
            layout_respuesta_especialista.setVisibility(View.GONE);
            layout_informe_medico.setVisibility(View.GONE);
           // text_status.setTextColor(Color.parseColor("#FFFFFF"));
         //   layout_text_status.setBackgroundResource(R.drawable.status_no_respondio);
        }
        if(num==7)
        {
            button_reportar.setClickable(false);
            layout_respuesta_especialista.setVisibility(View.GONE);
            layout_informe_medico.setVisibility(View.GONE);
           // text_status.setTextColor(Color.parseColor("#FFFFFF"));
         //   layout_text_status.setBackgroundResource(R.drawable.efecto_semi_circulo);
        }
        if(num==8)
        {
            button_reportar.setClickable(false);
            layout_respuesta_especialista.setVisibility(View.GONE);
            layout_informe_medico.setVisibility(View.GONE);
          //  text_status.setTextColor(Color.parseColor("#FFFFFF"));
         //   layout_text_status.setBackgroundResource(R.drawable.efecto_semi_circulo);
        }

        if(num==4)
        {
            button_reportar.setClickable(false);
            layout_respuesta_especialista.setVisibility(View.VISIBLE);
            layout_informe_medico.setVisibility(View.VISIBLE);
            layout_ordenes.setVisibility(View.VISIBLE);
            layout_recipes.setVisibility(View.VISIBLE);
            layout_recipe_indicaciones.setVisibility(View.VISIBLE);
            layout_observacion_del_diagnostico.setVisibility(View.VISIBLE);

        }

    }
    @OnClick(R.id.button_reportar)
    public void solicitarConsulta() {
        if(validateDate(AdapterConsultasEscritas.getConsultaEscrita().getConsultationDate())==1)
        {
            dialog("No es posible reportar la consulta. Sólo se puede reportar una consulta pendiente por responder y luego de 24 horas de su registro.");

        }
        if(validateDate(AdapterConsultasEscritas.getConsultaEscrita().getConsultationDate())==2)
        {
            dialog("No es posible reportar la consulta. Sólo se puede reportar una consulta pendiente por responder y luego de 24 horas de su registro.");
                   }
        if(validateDate(AdapterConsultasEscritas.getConsultaEscrita().getConsultationDate())==3)
        {
            if(AdapterConsultasEscritas.getConsultaEscrita().getStatusid()==2)
            {
                if(!reportar)
                {
                    reporteConsulta();
                    reportar=true;
                }
            }
        }

    }
    @OnClick(R.id.img_rating)
    public void rating() {
        if(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getStatusid()==4)
        {
            if(AdapterConsultasEscritas.getConsultaEscrita().getIsRated()==0)
            {
                if(!rate)
                {
                    setRate(getContext());
                }
            }else
            {
                dialog("La consulta ya fue calificada.");
            }
        }
        else
        {
            dialog("No es posible calificar una consulta que no ha sido atendida por el médico.");
        }

    }

    public void setRate(Context context) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialogo_rating, null);
        final RatingBar rating=(RatingBar)view.findViewById(R.id.ratingBar);

        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                presentador.setRate(new ScoreRequest(Preferences.getUTC_usuario(),AdapterConsultasEscritas.getConsultaEscrita().getConsultationid(),Preferences.getIDUser(),rating.getNumStars()));
                dialog.cancel();
            }
        });

        builder.setView(view);
        builder.show();

    }


    @Override
    public void getPrescripcionList(List<Prescripcion> dato) {
        final ArrayList<String> list1 = new ArrayList<String>();
        for (int i=0;i<dato.size();i++)
        {
            list1.add(dato.get(i).getName()+""+"\n"+dato.get(i).getObservation());
        }

        listAdapter = new ListAdapter(list1);
        LinearLayoutManager reci1= new LinearLayoutManager(getContext());

        recycler_indicaciones1.setLayoutManager(reci1);
        recycler_indicaciones1.setHasFixedSize(true);
        recycler_indicaciones1.setAdapter(listAdapter);

        final ArrayList<String> list2 = new ArrayList<String>();
        for (int i=0;i<dato.size();i++)
        {
            list2.add(dato.get(i).getName()+""+"\n"+dato.get(i).getPresentation());
        }

        listAdapter = new ListAdapter(list2);
        LinearLayoutManager reci2 = new LinearLayoutManager(getContext());

        recycler_indicaciones2.setLayoutManager(reci2);
        recycler_indicaciones2.setHasFixedSize(true);
        recycler_indicaciones2.setAdapter(listAdapter);
    }

    @Override
    public void getPrescripcionItemList(List<PrescripcionItem> dato) {
       // text_recipe_receta.setText(dato.get(0).getVademecum()+": "+dato.get(0).getVademecumPresentation());
       // text_indicaciones.setText(dato.get(0).getVademecum()+": "+dato.get(0).getObservation());
        final ArrayList<String> list = new ArrayList<String>();String[] values;
        for (int i=0;i<dato.size();i++)
        {
            list.add(dato.get(i).getVademecum()+""+"\n"+dato.get(i).getVademecumPresentation());
        }

        listAdapter = new ListAdapter(list);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());

        listRecipe_receta.setLayoutManager(reci);
        listRecipe_receta.setHasFixedSize(true);
        listRecipe_receta.setAdapter(listAdapter);







    }

    @Override
    public void getExamLab(List<ExamLab> dato) {
        final ArrayList<String> list = new ArrayList<String>();String[] values;
        for (int i=0;i<dato.size();i++)
        {
            list.add(dato.get(i).getExamType()+""+"\n"+dato.get(i).getExamCategory()+": "+""+dato.get(i).getExam());
        }

        listAdapter = new ListAdapter(list);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());

        listview.setLayoutManager(reci);
        listview.setHasFixedSize(true);
        listview.setAdapter(listAdapter);

        Log.d(" getExamLab",""+dato);
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
    @Override
    public void getConsultation(List<Consultation> dato) {

    }

    @Override
    public void getReporte(String dato) {
         progress.cancel();
        if(dato.equals("ok"))
        {
            dialog("La consulta ha sido reportada exitosamente. Un administrador le contactara para reprogramarla.");
        }else
        {
            dialog("Error de conexion con el Servidor. Intente mas tarde");
        }
    }
    public void dialog(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage(resp);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    public void getScore(String sms) {
        rate=true;
        img_rating.setImageResource(R.drawable.ic_star_black_24dp);
        text_status.setText(AdapterConsultasEscritas.listConsultaEscritas.get(AdapterConsultasEscritas.getPosicionEscrito()).getStatusname());
    }

    @Override
    public void getDiagnosticoItem(List<Diagnostic> dato) {
        // text_respuesta_especialista.setText(dato.get(0).getDiagnostic());

        Log.d("getDiagnosticoList",""+dato);

        final ArrayList<String> list = new ArrayList<String>();String[] values;
        for (int i=0;i<dato.size();i++)
        {
            Log.d("getDiagnosticoList",""+dato.get(i).getDiseasetype());
            list.add(dato.get(i).getDiseasetype()+": "+dato.get(i).getDiseasename());
        }

        listDiagnostico = new ListAdapter(list);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());

        recycler_diagnostico.setLayoutManager(reci);
        recycler_diagnostico.setHasFixedSize(true);
        recycler_diagnostico.setAdapter(listDiagnostico);
    }

    @Override
    public void getStatusToken(EstatusResponse status) {

    }

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    public static String parseDateToddMMyyyy2(String time) {
        String inputPattern = "yyyy/MM/dd h:mm";
        String outputPattern = "dd/MM/yyyy h:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    private static String deleteChar(String text, int charPos) {
        return new StringBuilder(text).deleteCharAt(charPos).toString();
    }
    public void reporteConsulta()
    {
        arraySpinner.clear();
        arraySpinner.add("El medico no asistio");
        arraySpinner.add("El medico no ha respondido");
        arraySpinner.add("Inconvenientes técnicos");


        final AlertDialog.Builder[] builder = {new AlertDialog.Builder(getActivity())};

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.layout_dialogo_reportar, null );

        TextView text_tipo =(TextView)view.findViewById(R.id.text_tipo);


        Spinner fecha_consulta =(Spinner) view.findViewById(R.id.fecha_consulta);
        Spinner hora_consulta =(Spinner) view.findViewById(R.id.hora_consulta);

        final EditText edit_observaciones=(EditText) view.findViewById(R.id.edit_observaciones);
        Button aceptar =(Button) view.findViewById(R.id.btton_aceptar);
        final Button cancelar =(Button) view.findViewById(R.id.btton_cancelar);
        final int[] reporte = {1};

        text_tipo.setText("Escrita");


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, arraySpinner);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fecha_consulta.setAdapter(dataAdapter);
        fecha_consulta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                reporte[0] =pos;
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        builder[0].setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                progress.show();
                ReporteRequest request= new ReporteRequest(

                        AdapterConsultasEscritas.getConsultaEscrita().getConsultationid(),
                        Preferences.getUTC_usuario(),
                        Preferences.getIDUser(),
                        reporte[0],
                        edit_observaciones.getText().toString());

                presentador.setReporte(request);
            }
        });
        builder[0].setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder[0].setView(view);
        builder[0].show();

    }

    @Override
    public void getListEspecialidad(List<Especialidad> especialistas) {
        for (int i=0;i<especialistas.size();i++)
        {
            if(especialistas.get(i).getSpecialitiesid()==AdapterConsultasEscritas.getConsultaEscrita().getSpecialityid())
            {
                text_price.setText("$"+especialistas.get(i).getConsultationCost());
            }
        }
    }

    @Override
    public void getListDoctor(List<Doctor> doctor) {

    }

    @Override
    public void getHorarioDoctor(List<Availabilty> horarioDoctor) {

    }

    @Override
    public void getSolicitarConsulta(DataSolicitudConsulta code) {

    }

    @Override
    public void getPaymentsResp(DataPayment dataPayment) {

    }

    @Override
    public void getRespDoctorFail(String text) {

    }

    @Override
    public void getShoulderDoctor(List<Schedule> hours) {

    }

    @Override
    public void getError(String dato) {

    }
}

package com.tuclinicavirtual.mvo.menu.antecedentes.mvp;

import com.tuclinicavirtual.mvo.menu.antecedentes.models.AntecedenteRequest;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.Antecedentes2;

import java.util.List;

public class AntecedenteContrato {

    public interface View{
        void getListAntecedente(List<Antecedentes2> antecedente);
        void setAntecedenteResp(String resp);
    }

    public interface Presenter{
        void getAntecedente(int id);
        void setPutAntecedente(AntecedenteRequest request);
    }
}

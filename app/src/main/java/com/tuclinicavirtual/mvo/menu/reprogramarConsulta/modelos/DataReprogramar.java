package com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataReprogramar {

        private List<Consultation> consultations = null;

        public List<Consultation> getConsultations() {
            return consultations;
        }

        public void setConsultations(List<Consultation> consultations) {
            this.consultations = consultations;
        }
}

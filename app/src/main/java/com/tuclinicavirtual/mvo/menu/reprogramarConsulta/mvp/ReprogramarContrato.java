package com.tuclinicavirtual.mvo.menu.reprogramarConsulta.mvp;

import com.tuclinicavirtual.mvo.menu.antecedentes.models.AntecedenteRequest;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.Antecedentes2;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos.ReprogramarRequest;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.modelos.ReprogramarResponses;

import java.util.List;

public class ReprogramarContrato {

    public interface View{
        void getRespuestaPutReprogramr(ReprogramarResponses responses);
    }

    public interface Presenter{
        void setPutReprogramar(ReprogramarRequest guardar);
    }

}

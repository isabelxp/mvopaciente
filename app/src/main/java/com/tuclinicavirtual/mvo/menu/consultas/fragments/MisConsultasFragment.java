package com.tuclinicavirtual.mvo.menu.consultas.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionList;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesContrato;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesPresentador;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens.SolicitarConsultaFragment;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.tuclinicavirtual.mvo.R.color.button_red;
import static com.tuclinicavirtual.mvo.views.activitis.HomeActivity.setDetalleConsulta;

/**
 * A simple {@link Fragment} subclass.
 */
public class MisConsultasFragment extends Fragment implements DatosPersonalesContrato.View,FamiliaresContrato.View{

    SectionsPagerAdapter mSectionsPagerAdapter;

    public MisConsultasFragment() {

    }



    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.container)
    ViewPager mViewPager;

    DatosPersonalesPresentador presenter;
    List<String> listPaciente=new ArrayList<>();
    List<Integer> idPacientes=new ArrayList<>();
    List<Integer> idUser=new ArrayList<>();
    List<String> utcPaciente=new ArrayList<>();

    public static  LinearLayout layout_mis_consultas;
    public static TextView text_mis_consultas;


    public static boolean controlEspecialidad=false;
    public static boolean TipoDeConsulta=false;

    public static boolean isTipoDeConsulta() {
        return TipoDeConsulta;
    }

    public static void setTipoDeConsulta(boolean tipoDeConsulta) {
        TipoDeConsulta = tipoDeConsulta;
    }
    FamiliaresPresentador presentadorFamiliares;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_mis_consultas_, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ButterKnife.bind(this, v);
        Log.d("initUiHome","-----fragment------>");
        presentadorFamiliares=new FamiliaresPresentador(this);
        presenter = new DatosPersonalesPresentador(this);
        presenter.getPaciente(Preferences.getIDPatientid());

        text_mis_consultas=(TextView)v.findViewById(R.id.text_mis_consultas);
        layout_mis_consultas=(LinearLayout) v.findViewById(R.id.layout_mis_consultas);
        HomeActivity.setDetalleConsulta(true);

        presentadorFamiliares.getFamiliares(Preferences.getIDUser());


        initUI();
        return v;
    }

    @OnClick(R.id.btn_solicitar_consulta)
    public void solicitarConsulta() {
        SolicitarConsultaFragment.pago=false;
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, new SolicitarConsultaFragment());
        ft.addToBackStack(null);
        ft.commit();

    }

    public void initUI()
    {

        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mViewPager.setCurrentItem(2,true);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabsFromPagerAdapter(mSectionsPagerAdapter);

        if (isTipoDeConsulta())
        {
            mViewPager.setCurrentItem(0);
//            layout_mis_consultas.setBackgroundResource(R.color.white);
//            text_mis_consultas.setTextColor(Color.parseColor("#303F9F"));
        }else
        {
            mViewPager.setCurrentItem(1);
//            layout_mis_consultas.setBackgroundResource(R.color.colorPrimaryDark);
//            text_mis_consultas.setTextColor(Color.parseColor("#DDEEF4"));
        }

    }

    @Override
    public void setDataPaises(List<InformacionList> dato) {

    }

    @Override
    public void setDataOcupaciones(List<InformacionList> dato) {

    }

    @Override
    public void setDataParentesco(List<InformacionList> dato) {

    }

    @Override
    public void setZonaHoraria(List<InformacionList> dato) {

    }

    @Override
    public void setRespDatosUsuario(String sms) {

    }

    @Override
    public void getDatosPacientes(Patient a) {
        try {
            if(a!=null)
            {

                HomeActivity.nombre_pais.setText(""+a.getResCountryName());
                HomeActivity.nombre_paciente.setText(""+a.getName()+" "+a.getLastname());
                Preferences.setUTC_usuario(a.getTimezoneName());

                if(a.getTimezoneName()==null)
                {
                    Preferences.setUTC_usuario("UTC −04:00");
                }


                if(a.getSex().equals("Femenino"))
                {
                    HomeActivity.img_profile.setImageDrawable(getContext().getDrawable(R.mipmap.ic_icon_women));
                }
                if(a.getSex().equals("Masculino"))
                {
                    HomeActivity.img_profile.setImageDrawable(getContext().getDrawable(R.mipmap.ic_icon_man));
                }
                EventBus.getDefault().post("200");
            }
        } catch (Exception e){
            throw new RuntimeException(e);
        }

    }

    @Override
    public void setRespPasswordNew(String resp, String Token) {

    }

    @Override
    public void getListFamiliares(List<Patient> familiar) {

    }

    @Override
    public void setFamiliainfo(String rest) {

    }

    @Override
    public void setDeleteFamiliar(String rest) {

    }


    public static class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    layout_mis_consultas.setBackgroundResource(R.color.white);
                    text_mis_consultas.setTextColor(Color.parseColor("#303F9F"));
                    setTipoDeConsulta(false);
                    return new ConsultasEscritaFragment();
                case 1:
                    layout_mis_consultas.setBackgroundResource(R.color.colorPrimaryDark);
                    text_mis_consultas.setTextColor(Color.parseColor("#DDEEF4"));
                    setTipoDeConsulta(true);
                    return new ConsultasPragramadaFragment();
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Consultas escritas";
                case 1:
                    return "Consultas programadas";
            }
            return null;
        }
    }
}

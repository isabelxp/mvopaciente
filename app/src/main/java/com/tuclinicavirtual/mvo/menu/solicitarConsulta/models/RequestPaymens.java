package com.tuclinicavirtual.mvo.menu.solicitarConsulta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestPaymens {

    @SerializedName("utc")
    @Expose
    private String utc;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("confirmation_number")
    @Expose
    private String confirmationNumber;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("payment_typeid")
    @Expose
    private Integer paymentTypeid;
    @SerializedName("usertitular")
    @Expose
    private Integer usertitular;

    public RequestPaymens(String utc, Integer userid, String confirmationNumber, Integer amount, Integer paymentTypeid, Integer usertitular) {
        this.utc = utc;
        this.userid = userid;
        this.confirmationNumber = confirmationNumber;
        this.amount = amount;
        this.paymentTypeid = paymentTypeid;
        this.usertitular = usertitular;
    }

    @Override
    public String toString() {
        return "RequestPaymens{" +
                "utc='" + utc + '\'' +
                ", userid=" + userid +
                ", confirmationNumber='" + confirmationNumber + '\'' +
                ", amount=" + amount +
                ", paymentTypeid=" + paymentTypeid +
                ", usertitular=" + usertitular +
                '}';
    }

    public String getUtc() {
        return utc;
    }

    public void setUtc(String utc) {
        this.utc = utc;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getConfirmationNumber() {
        return confirmationNumber;
    }

    public void setConfirmationNumber(String confirmationNumber) {
        this.confirmationNumber = confirmationNumber;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPaymentTypeid() {
        return paymentTypeid;
    }

    public void setPaymentTypeid(Integer paymentTypeid) {
        this.paymentTypeid = paymentTypeid;
    }

    public Integer getUsertitular() {
        return usertitular;
    }

    public void setUsertitular(Integer usertitular) {
        this.usertitular = usertitular;
    }

}

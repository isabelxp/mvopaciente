package com.tuclinicavirtual.mvo.menu.examenes.mvp;


import com.tuclinicavirtual.mvo.menu.examenes.models.Exam;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequest;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequestDelete;

import java.util.List;

import okhttp3.RequestBody;

public class ExamContrato {
    public interface View{
        void getExamenes(List<Exam> dato);
        void getExamRespFalse();
        void getDeleteResp(String dato);
        void setExam(String dato);
        void setUpload(String dato);
    }

    public interface Presenter{
        void getExam(int patientid);
        void setExam(ExamRequest guardar);
        void deleteExam(ExamRequestDelete guardar);
        void uploadExam(int userid, int pacienteid, RequestBody requestFile);
    }
}

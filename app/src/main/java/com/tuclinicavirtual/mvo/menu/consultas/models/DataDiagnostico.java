package com.tuclinicavirtual.mvo.menu.consultas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataDiagnostico {
    @SerializedName("diagnostics")
    @Expose
    private List<Diagnostico> diagnostics = null;

    public List<Diagnostico> getDiagnostics() {
        return diagnostics;
    }

    public void setDiagnostics(List<Diagnostico> diagnostics) {
        this.diagnostics = diagnostics;
    }
}

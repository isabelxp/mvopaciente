package com.tuclinicavirtual.mvo.menu.examenes.interfaces;

import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.DataPacientes;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.GuardarUsuarioRequest;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamDeleteResponse;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequest;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRequestDelete;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamResponse;
import com.tuclinicavirtual.mvo.menu.examenes.models.ExamRespuestaPost;
import com.tuclinicavirtual.mvo.menu.familiares.models.DeleteRequestFamilia;
import com.tuclinicavirtual.mvo.menu.familiares.models.FamiliaResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ExamInterfaces {

    @GET("patients/exams/UTC-03:00/{id}")
    Call<ExamResponse> getExam(@Path("id") int id,@Header("Authorization") String valor);

    @Headers("Content-Type:application/json")
    @POST("patients/exams")
    Call<ExamRespuestaPost> setExam(@Body ExamRequest guardar, @Header("Authorization") String valor);

    @Headers("Content-Type:application/json")
    @HTTP(method = "DELETE", path = "patients/exams", hasBody = true)
    Call<ExamDeleteResponse> deleteExam(@Body ExamRequestDelete familir, @Header("Authorization") String valor);

    @Multipart
    @POST("upload")
    Call<ExamDeleteResponse> uploadExam(@Query("userid") int userid,
                                        @Query("patientid") int patientid,
                                        @Part MultipartBody.Part file,
                                        @Header("Authorization") String valor);
}

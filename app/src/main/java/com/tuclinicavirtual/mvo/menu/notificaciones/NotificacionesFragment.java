package com.tuclinicavirtual.mvo.menu.notificaciones;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasProgramada;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.ConsultasPragramadaFragment;
import com.tuclinicavirtual.mvo.menu.consultas.models.Consultation;
import com.tuclinicavirtual.mvo.menu.notificaciones.adapters.NotificacionesAdapter;
import com.tuclinicavirtual.mvo.menu.notificaciones.models.ModeloNotificaciones;
import com.tuclinicavirtual.mvo.menu.notificaciones.models.Notification;
import com.tuclinicavirtual.mvo.menu.notificaciones.mvp.NotificacionesContrato;
import com.tuclinicavirtual.mvo.menu.notificaciones.mvp.NotificacionesPresentador;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificacionesFragment extends Fragment implements NotificacionesContrato.View {


    public NotificacionesFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.search2)
    EditText search;
    NotificacionesAdapter adaptador;
    NotificacionesPresentador presentador;
    List<Notification> noti=new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_notificaciones, container, false);
        ButterKnife.bind(this, v);
        initUI();
        presentador.getNotificaciones(Preferences.getIDUser());
        HomeActivity.setDetalleConsulta(false);
        return v;
    }

    public void initUI()
    {
        presentador=new NotificacionesPresentador(this);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(reci);
        recycler.setHasFixedSize(true);
        addTextListener(noti);
    }


    @Override
    public void getListNotificaciones(List<Notification> noti) {
        adaptador= new NotificacionesAdapter(noti);
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
        addTextListener(noti);
    }

    public void addTextListener(final List<Notification> dato){

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();

                final List<Notification> datoAuxi = new ArrayList<>();

                for (int i = 0; i < dato.size(); i++) {

                    final String text = dato.get(i).getDescription().toLowerCase();
                    if (text.contains(query)) {
                        datoAuxi.add(dato.get(i));
                    }
                }

                adaptador= new NotificacionesAdapter(datoAuxi);
                recycler.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
            }
        });
    }
}

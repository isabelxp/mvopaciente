package com.tuclinicavirtual.mvo.menu.reprogramarConsulta.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.examenes.adapters.ExamenesAdapter;
import com.tuclinicavirtual.mvo.menu.examenes.models.Exam;
import com.tuclinicavirtual.mvo.menu.examenes.mvp.ExamContrato;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter.ListDoctoresAdapter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter.ListEspecialidadAdapter;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens.SolicitarConsultaFragment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DoctoHorariosRequest;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultaPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultasContrato;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class reprogramarConsultaAdapter extends RecyclerView.Adapter<reprogramarConsultaAdapter.ViewHolder>implements SolicitarConsultasContrato.View {

    SolicitarConsultaPresentador presentador;
    List<Doctor> listDoctor=new ArrayList<>();
    Context context;


    List<String> listFechaConsulta=new ArrayList<>();
    List<String> listFechaFormatoConsulta=new ArrayList<>();
    List<String> listFechaFormatoConsultaAuxi=new ArrayList<>();
    List<String> listHoraConsulta=new ArrayList<>();
    List<String> listfechadeConsultaAuxi=new ArrayList<>();

    boolean img_chat=false;
    boolean img_video=false;
    boolean img_email=false;
    boolean img_phone=false;

    public static Doctor objetoDoc;
    public static int especialidadSelec;
    public static String fechaSelec;
    public static String horaSelec;

    List<Availabilty> listAvailabilities=new ArrayList<>();

    int auxiliarId=-1;
    int auxiliarClick=-1;
    int itemClick=-1;

    boolean consultarHora=false;
    public ProgressDialog progress;
    ArrayAdapter<String> dataAdapter2;

    public reprogramarConsultaAdapter(List<Doctor> list, Context context) {
        this.listDoctor=list;
        this.context=context;
        presentador=new SolicitarConsultaPresentador(this);
        dataAdapter2 = null;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_medicos_solicitud, parent, false);
        progress = new ProgressDialog(context);
        progress.setTitle("Cargando horario...");
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.bindData(listDoctor.get(position),context);

        holder.text_hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DoctoHorariosRequest requestHorario = new DoctoHorariosRequest(listDoctor.get(position).getTimezonename(),listDoctor.get(position).getDoctorid(),Integer.toString(ListEspecialidadAdapter.getIdEspecialidad()),null,null);
                presentador.setHorarioDoctor(requestHorario);

            }
        });


        if(auxiliarClick==position)
        {
            holder.layout_doctor.setBackgroundResource(R.color.form_pressed);
        }else
        {
            holder.layout_doctor.setBackgroundResource(R.color.color_layout);
        }

        holder.id_frame_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                objetoDoc=listDoctor.get(position);
                especialidadSelec=1;
                auxiliarId=0;
                itemClick=position;
                holder.id_frame_email.setBackgroundResource(R.color.button_bg);
                img_email=true;
                notifyDataSetChanged();
            }
        });

        holder.id_frame_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progress.show();
                DoctoHorariosRequest requestHorario = new DoctoHorariosRequest(listDoctor.get(position).getTimezonename(),listDoctor.get(position).getDoctorid(),"4",null,null);
                presentador.setHorarioDoctor(requestHorario);
                objetoDoc=listDoctor.get(position);
                especialidadSelec=2;
                itemClick=position;
                auxiliarId=1;
                holder.id_frame_chat.setBackgroundResource(R.color.button_bg);
                img_chat=true;

                notifyDataSetChanged();

            }
        });

        holder.id_frame_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.show();
                DoctoHorariosRequest requestHorario = new DoctoHorariosRequest(listDoctor.get(position).getTimezonename(),listDoctor.get(position).getDoctorid(),"4",null,null);
                presentador.setHorarioDoctor(requestHorario);
                objetoDoc=listDoctor.get(position);
                especialidadSelec=3;
                itemClick=position;
                auxiliarId=2;
                holder.id_frame_phone.setBackgroundResource(R.color.button_bg);
                img_phone=true;
                notifyDataSetChanged();
            }
        });
        holder.id_frame_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.show();
                DoctoHorariosRequest requestHorario = new DoctoHorariosRequest(listDoctor.get(position).getTimezonename(),listDoctor.get(position).getDoctorid(),"4",null,null);
                presentador.setHorarioDoctor(requestHorario);
                objetoDoc=listDoctor.get(position);
                especialidadSelec=4;
                itemClick=position;
                auxiliarId=3;
                holder.id_frame_video.setBackgroundResource(R.color.button_bg);
                img_video=true;
                notifyDataSetChanged();
            }
        });

        holder.id_frame_email.setBackgroundResource(R.color.color_item_doc);
        holder.id_frame_chat.setBackgroundResource(R.color.color_item_doc);
        holder.id_frame_phone.setBackgroundResource(R.color.color_item_doc);
        holder.id_frame_video.setBackgroundResource(R.color.color_item_doc);

        holder.text_hora.setVisibility(View.INVISIBLE);


        if(itemClick==position)
        {

            holder.text_horario.setText(fechaSelec);

            holder.text_disponible.setText(horaSelec);

            if(auxiliarId==0)
            {
                holder.id_frame_email.setBackgroundResource(R.color.button_bg);
                holder.text_horario.setText("");
                holder.text_disponible.setText("");

            }
            if(auxiliarId==1)
            {
                holder.id_frame_chat.setBackgroundResource(R.color.button_bg);
            }
            if(auxiliarId==2)
            {
                holder.id_frame_phone.setBackgroundResource(R.color.button_bg);
            }
            if(auxiliarId==3)
            {
                holder.id_frame_video.setBackgroundResource(R.color.button_bg);
            }

        }else
        {
            holder.text_disponible.setText("");
            holder.text_horario.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return listDoctor.size();
    }

    @Override
    public void getListEspecialidad(List<Especialidad> especialistas) {

    }

    @Override
    public void getListDoctor(List<Doctor> doctor) {

    }

    @Override
    public void getHorarioDoctor(List<Availabilty> horarioDoctor) {
        progress.cancel();
        Log.d("getHorarioDoctor","--->1"+horarioDoctor.toString());
        if (!consultarHora)
        {
            Log.d("getHorarioDoctor","--->2");
            listFechaConsulta.clear();
            listFechaFormatoConsulta.clear();
            listAvailabilities=horarioDoctor;
            for (int i=0;i<horarioDoctor.size();i++)
            {
                listFechaConsulta.add(new String(horarioDoctor.get(i).getCastDateConsultation()));
                listFechaFormatoConsultaAuxi.add(new String(horarioDoctor.get(i).getDateConsultation()));
            }
            borrarElementos(listFechaConsulta);
            borrarElementosAuxi(listFechaFormatoConsultaAuxi);
            Log.d("getHorarioDoctor","--->22");
            dialogHorario();
        }
        if(consultarHora)
        {
            Log.d("getHorarioDoctor","--->3");
            listHoraConsulta.clear();
            for (int i=0;i<horarioDoctor.size();i++)
            {
                listHoraConsulta.add(horarioDoctor.get(i).getStartHour());
            }
            Log.d("horarioAdapter",""+horarioDoctor.toString());
            dataAdapter2.notifyDataSetChanged();
        }

    }

    @Override
    public void getSolicitarConsulta(DataSolicitudConsulta code) {

    }

    @Override
    public void getPaymentsResp(DataPayment dataPayment) {

    }

    @Override
    public void getRespDoctorFail(String text) {

    }

    @Override
    public void getShoulderDoctor(List<Schedule> hours) {

    }

    @Override
    public void getError(String dato) {

    }

    public void borrarElementos(List<String>  list)
    {
        for(int i=0;i<list.size();i++){
            for(int j=0;j<list.size()-1;j++){
                if(i!=j){
                    if(list.get(i).equals(list.get(j))){
                        // eliminamos su valor
                        list.set(i,"");
                    }
                }
            }
        }

        for (int k=0;k<=list.size()-1;k++){
            if(list.get(k)!=""){
                listfechadeConsultaAuxi.add(new String(list.get(k)));
                Log.d("borrarElementos",""+list.get(k));
            }
        }
    }

    public void borrarElementosAuxi(List<String>  list)
    {
        for(int i=0;i<list.size();i++){
            for(int j=0;j<list.size()-1;j++){
                if(i!=j){
                    if(list.get(i).equals(list.get(j))){
                        // eliminamos su valor
                        list.set(i,"");
                    }
                }
            }
        }

        for (int k=0;k<=list.size()-1;k++){
            if(list.get(k)!=""){
                listFechaFormatoConsulta.add(new String(list.get(k)));
                Log.d("borrarElementos",""+list.get(k));
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nombre_doc)
        TextView text_nombre_doc;

        @BindView(R.id.especialidad_doc)
        TextView text_especialidad_doc;

        @BindView(R.id.text_hora)
        TextView text_hora;

        @BindView(R.id.text_disponible)
        TextView text_disponible;

        @BindView(R.id.layout_doctor)
        LinearLayout layout_doctor;

        @BindView(R.id.id_frame_chat)
        FrameLayout id_frame_chat;

        @BindView(R.id.id_frame_email)
        FrameLayout id_frame_email;

        @BindView(R.id.id_frame_phone)
        FrameLayout id_frame_phone;

        @BindView(R.id.id_frame_video)
        FrameLayout id_frame_video;

        @BindView(R.id.img_chat)
        ImageView img_chat;

        @BindView(R.id.img_email)
        ImageView img_email;

        @BindView(R.id.img_phone)
        ImageView img_phone;

        @BindView(R.id.img_video)
        ImageView img_video;

        @BindView(R.id.text_horario)
        TextView text_horario;


        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
        public static ProgressDialog progress;
        Context context;

        public void bindData(Doctor c,Context context) {
            this.context=context;
            progress = new ProgressDialog(context);
            progress.setTitle("Cargando horario...");
            text_nombre_doc.setText(c.getFullname());
            text_especialidad_doc.setText(SolicitarConsultaFragment.getEspecialidad());
        }
    }

    public void dialog(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Horario");
        builder.setMessage(resp);

        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //presenter.getAntecedente(Preferences.getIDPatientid());
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void dialogHorario()
    {


        final List<String> hora = new ArrayList<>();

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dialogo_horario, null );

        TextView text_tipo_llamada =(TextView)view.findViewById(R.id.text_tipo_llamada);
        //text_tipo_llamada.setText(getTipoSeleccion());

        Spinner fecha_consulta =(Spinner) view.findViewById(R.id.fecha_consulta);

        Spinner hora_consulta =(Spinner) view.findViewById(R.id.hora_consulta);

        Button aceptar =(Button) view.findViewById(R.id.btton_aceptar);
        final Button cancelar =(Button) view.findViewById(R.id.btton_cancelar);


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, listfechadeConsultaAuxi);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fecha_consulta.setAdapter(dataAdapter);

        dataAdapter2 = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, listHoraConsulta);

        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        hora_consulta.setAdapter(dataAdapter2);

        hora_consulta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                 horaSelec = listHoraConsulta.get(pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        fecha_consulta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                progress.show();
                fechaSelec=listFechaFormatoConsulta.get(pos);
                consultarHora=true;
                dialogHora(listAvailabilities.get(pos).getUtc(), listAvailabilities.get(pos).getDoctorid(),listAvailabilities.get(pos).getSpecialtyid(), listFechaFormatoConsulta.get(pos));
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                consultarHora=false;
                notifyDataSetChanged();

            }
        });
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                consultarHora=false;
                fechaSelec="";
                dialog.cancel();
            }
        });

        builder.setView(view);
        builder.show();

    }


    public void dialogHora(String zonaH, int idDoc,int idEspecialidad, String fecha)
    {

        DoctoHorariosRequest requestHorario = new DoctoHorariosRequest(zonaH,idDoc,"4",fecha,null);
        presentador.setHorarioDoctor(requestHorario);

    }
}

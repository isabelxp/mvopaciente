package com.tuclinicavirtual.mvo.menu.antecedentes;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.antecedentes.adapter.AntecedentesAdapter;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.antecedents;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.AntecedenteRequest;
import com.tuclinicavirtual.mvo.menu.antecedentes.models.Antecedentes2;
import com.tuclinicavirtual.mvo.menu.antecedentes.mvp.AntecedenteContrato;
import com.tuclinicavirtual.mvo.menu.antecedentes.mvp.AntecedentePresentador;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresContrato;
import com.tuclinicavirtual.mvo.menu.familiares.mvp.FamiliaresPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.interfaces.GuardarAntecedentesInter;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MisAntecedentesFragment extends Fragment implements AntecedenteContrato.View,FamiliaresContrato.View{


    public MisAntecedentesFragment() {

    }
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.spinner_antecedente)
    Spinner spinner_antecedente;
    AntecedentesAdapter adaptador;
    List<Antecedentes2> listAntecedentes=new ArrayList<>();
    AntecedentePresentador presenter;
    AntecedenteRequest request;
    ProgressDialog progress;
    GuardarAntecedentesInter inter;
    FamiliaresPresentador presentadorFamiliares;
    int IDPatientid;
    int IDUsuarioid;

    List<String> listPaciente=new ArrayList<>();
    List<Integer> idPacientes=new ArrayList<>();
    List<Integer> idUser=new ArrayList<>();

    public static ArrayList<antecedents> listAntecedenteGuardar=new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_mis_antecedentes, container, false);
        ButterKnife.bind(this, v);
        initUI();
        presentadorFamiliares.getFamiliares(Preferences.getIDUser());
        progress.show();
        presenter.getAntecedente(Preferences.getIDPatientid());
        IDPatientid=Preferences.getIDPatientid();
        IDUsuarioid=Preferences.getIDUser();
        HomeActivity.setDetalleConsulta(false);
        return v;
    }
    @OnClick(R.id.id_guardar)
    public void botton_guardar() {
        progress.show();
        boolean valida=true;
        for (int i=0;i<AntecedentesAdapter.listAuxiliar.size();i++)
        {
            if(AntecedentesAdapter.listAuxiliar.get(i).getAntecedentid()==12)
            {

               if(AntecedentesAdapter.listAuxiliar.get(i).getObservation().equals(""))
               {
                   progress.cancel();
                   valida=false;

                   dialog("Usted debe indicar su estatura!");
                   Log.d("AntecedentesAdapter","Esta sin nada 1");
               }else
               {
                   int num=Integer.parseInt(AntecedentesAdapter.listAuxiliar.get(i).getObservation());
                   if(num<20||num>250)
                   {
                       progress.cancel();
                       valida=false;
                       dialog("Estatura (cm): Valor entero entre 20 y 250 cm.");

                   }
               }


            }
            if(AntecedentesAdapter.listAuxiliar.get(i).getAntecedentid()==13)
            {
                if(AntecedentesAdapter.listAuxiliar.get(i).getObservation().equals(""))
                {
                    progress.cancel();
                    valida=false;
                    dialog("Usted debe indicar su peso!");
                    Log.d("AntecedentesAdapter","Esta sin nada 2");
                }else
                {
                    {
                        int num=Integer.parseInt(AntecedentesAdapter.listAuxiliar.get(i).getObservation());
                        if(num<15||num>300)
                        {
                            progress.cancel();
                            valida=false;
                            dialog("Peso (kgs): Valor entero entre 15 y 300 kg.");

                        }
                    }
                }

            }

        }
        if(valida)
        {
            presenter.setPutAntecedente(new AntecedenteRequest(IDUsuarioid,IDPatientid,AntecedentesAdapter.listAuxiliar));
        }


    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            inter=(GuardarAntecedentesInter)context;
        } catch (ClassCastException e) {
            Log.d("Antecedentes",""+e.getLocalizedMessage());
            Log.d("Antecedentes",""+e.getMessage());
            Log.d("Antecedentes",""+e.getCause());
        }
    }

    public void initUISpinner(final List<String> familiar)
    {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, familiar);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_antecedente.setAdapter(dataAdapter);
        if(familiar!=null)
        {
            for(int i=0;i<familiar.size();i++)
            {
                if(familiar.get(i).contains("Titular"))
                {
                    spinner_antecedente.setSelection(i);
                }
            }

        }
        spinner_antecedente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                presenter.getAntecedente(idPacientes.get(pos));
                IDPatientid=idPacientes.get(pos);
                IDUsuarioid=idUser.get(pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void dialog(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(resp);
        builder.setCancelable(false);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                presenter.getAntecedente(IDPatientid);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    public void getListAntecedente(List<Antecedentes2> antecedente) {
        progress.cancel();
        Log.d("getListAntecedente","-->"+antecedente);
        adaptador= new AntecedentesAdapter(antecedente);
        recycler.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();

        Log.d("getListAntecedente","-->"+antecedente);
    }

    @Override
    public void setAntecedenteResp(String resp) {
        progress.cancel();

        //dialog(resp);
        if(resp.equals("ok"))
        {
            dialog("Sus antecendentes han sido actualizados");
            //presenter.getAntecedente(Preferences.getIDPatientid());
        }else
        {
            dialog(resp);
        }

        Log.d("setAntecedente","-->"+resp);
    }

    public void initUI()
    {
        progress = new ProgressDialog(getActivity());
        progress.setTitle(getActivity().getString(R.string.registro_cargando));
        progress.setCancelable(false);
        presenter=new AntecedentePresentador(this);
        presentadorFamiliares=new FamiliaresPresentador(this);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());
        reci.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(reci);
        recycler.setNestedScrollingEnabled(false);
        //recycler.setHasFixedSize(true);
    }

    @Override
    public void getListFamiliares(List<Patient> familiar) {
        for (int i=0;i<familiar.size();i++)
        {

            listPaciente.add(familiar.get(i).getName()+" "+familiar.get(i).getLastname()+" - "+familiar.get(i).getRelationshipName());
            idPacientes.add(familiar.get(i).getPatientid());
            idUser.add(familiar.get(i).getPatientid());

        }

        initUISpinner(listPaciente);

    }

    @Override
    public void setFamiliainfo(String rest) {

    }

    @Override
    public void setDeleteFamiliar(String rest) {

    }
}

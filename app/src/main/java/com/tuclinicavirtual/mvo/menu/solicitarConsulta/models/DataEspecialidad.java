package com.tuclinicavirtual.mvo.menu.solicitarConsulta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataEspecialidad {
    @SerializedName("specialities")
    @Expose
    private List<Especialidad> specialities = null;

    public List<Especialidad> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(List<Especialidad> specialities) {
        this.specialities = specialities;
    }
}

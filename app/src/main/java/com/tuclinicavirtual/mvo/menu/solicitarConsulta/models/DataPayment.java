package com.tuclinicavirtual.mvo.menu.solicitarConsulta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataPayment {
    @SerializedName("paymentid")
    @Expose
    private Integer paymentid;
    @SerializedName("confirmation_number")
    @Expose
    private String confirmationNumber;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("payment_typeid")
    @Expose
    private Integer paymentTypeid;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("patientid")
    @Expose
    private Integer patientid;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("date_updated")
    @Expose
    private String dateUpdated;

    @Override
    public String toString() {
        return "DataPayment{" +
                "paymentid=" + paymentid +
                ", confirmationNumber='" + confirmationNumber + '\'' +
                ", amount=" + amount +
                ", paymentTypeid=" + paymentTypeid +
                ", paymentType='" + paymentType + '\'' +
                ", patientid=" + patientid +
                ", username='" + username + '\'' +
                ", status=" + status +
                ", dateCreated='" + dateCreated + '\'' +
                ", dateUpdated='" + dateUpdated + '\'' +
                '}';
    }

    public Integer getPaymentid() {
        return paymentid;
    }

    public void setPaymentid(Integer paymentid) {
        this.paymentid = paymentid;
    }

    public String getConfirmationNumber() {
        return confirmationNumber;
    }

    public void setConfirmationNumber(String confirmationNumber) {
        this.confirmationNumber = confirmationNumber;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPaymentTypeid() {
        return paymentTypeid;
    }

    public void setPaymentTypeid(Integer paymentTypeid) {
        this.paymentTypeid = paymentTypeid;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
}

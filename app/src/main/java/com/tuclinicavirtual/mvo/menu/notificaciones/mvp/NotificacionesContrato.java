package com.tuclinicavirtual.mvo.menu.notificaciones.mvp;


import com.tuclinicavirtual.mvo.menu.notificaciones.models.Notification;

import java.util.List;

public class NotificacionesContrato {
    public interface View{
        void getListNotificaciones(List<Notification> noti);

    }

    public interface Presenter{
        void getNotificaciones(int id);
    }
}

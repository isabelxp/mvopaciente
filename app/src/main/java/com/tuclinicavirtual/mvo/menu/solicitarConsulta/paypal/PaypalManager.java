package com.tuclinicavirtual.mvo.menu.solicitarConsulta.paypal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONObject;

import java.math.BigDecimal;

public class PaypalManager {

    private PayPalConfiguration config;
    private static PaypalManager instance;
    private PaypalPayListener listener;
    public static final int REQUEST_CODE_PAYPAL = 333;
    // sandbox
    //live
    private PaypalManager() {
        config = new PayPalConfiguration().environment("sandbox") // TODO change Enviroment segun build variant
                .clientId("AXdN_gaubiNAhMMOUtsXk-E4mivmksHI8Paqq4SlBg4_yT9IhCieTrnJa2zfxPRe2UDPZrUTaMbYxTCS");
        config.acceptCreditCards(false);
    }


    public static PaypalManager getInstance() {
        if (instance == null)
            instance = new PaypalManager();
        return instance;
    }


    public void initPaypalService(Context context) {
        Intent intent = new Intent(context, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        context.startService(intent);
    }


    public Intent makePayment(Context context, String item, String price, PaypalPayListener listener) {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(price), "USD", item,
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(context, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        this.listener = listener;
        return intent;
    }


    public void onDestroy(Context context) {
        context.stopService(new Intent(context, PayPalService.class));
    }

    public void onActivityResult(int resultCode, Intent data) {
        Log.d("DAWDA","--->"+resultCode);

        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            Log.d("DAWDA","confirm--->"+confirm.toString());
            if (confirm != null) {
                try {
                    listener.onPaypalPaySuccess(confirm.toJSONObject());
                } catch (Exception e) {
                    listener.onPaypalPayFail();

                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {

            listener.onPaypalPayFail();
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {

            listener.onPaypalPayFail();
        }
    }

    public interface PaypalPayListener {

        void onPaypalPaySuccess(JSONObject paymentConfirm);

        void onPaypalPayFail();
    }
}

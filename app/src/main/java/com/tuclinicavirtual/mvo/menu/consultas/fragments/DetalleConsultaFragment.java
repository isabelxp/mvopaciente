package com.tuclinicavirtual.mvo.menu.consultas.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;


import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.call.model.Session;
import com.tuclinicavirtual.mvo.call.ui.ActionBar;
import com.tuclinicavirtual.mvo.call.ui.ChatActivity;
import com.tuclinicavirtual.mvo.call.ui.VideoActivity;

import com.tuclinicavirtual.mvo.call.ui.VoiceActivity;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasEscritas;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasProgramada;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.ListAdapter;
import com.tuclinicavirtual.mvo.menu.consultas.models.Consultation;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostic;
import com.tuclinicavirtual.mvo.menu.consultas.models.Diagnostico;
import com.tuclinicavirtual.mvo.menu.consultas.models.EstatusResponse;
import com.tuclinicavirtual.mvo.menu.consultas.models.ExamLab;
import com.tuclinicavirtual.mvo.menu.consultas.models.Prescripcion;
import com.tuclinicavirtual.mvo.menu.consultas.models.PrescripcionItem;
import com.tuclinicavirtual.mvo.menu.consultas.models.ReporteRequest;
import com.tuclinicavirtual.mvo.menu.consultas.models.ScoreRequest;
import com.tuclinicavirtual.mvo.menu.consultas.mvp.ConsultasContrato;
import com.tuclinicavirtual.mvo.menu.consultas.mvp.ConsultasPresentador;
import com.tuclinicavirtual.mvo.menu.reprogramarConsulta.activity.ReprogramarConsultaActivity;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultaPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultasContrato;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.utils.Utils;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetalleConsultaFragment extends Fragment  implements ConsultasContrato.View, SolicitarConsultasContrato.View{


    public DetalleConsultaFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.img_typo)
    ImageView img_typo;

    @BindView(R.id.text_consulta)
    TextView text_consulta;

    @BindView(R.id.text_nombre_dr)
    TextView text_nombre_dr;

    @BindView(R.id.text_especialidad)
    TextView text_especialidad;

    @BindView(R.id.button_reportar)
    Button button_reportar;

    @BindView(R.id.button_conectarme)
    Button button_conectarme;

    @BindView(R.id.text_nombre)
    TextView text_nombre;

    @BindView(R.id.text_limite_resp)
    TextView text_limite_resp;

    @BindView(R.id.text_sexo)
    TextView text_sexo;

    @BindView(R.id.text_pago)
    TextView text_pago;

    @BindView(R.id.text_edad)
    TextView text_edad;

    @BindView(R.id.text_motivo)
    TextView text_motivo;

    @BindView(R.id.text_respuesta_especialista)
    TextView text_respuesta_especialista;

    @BindView(R.id.text_informe_medico)
    TextView text_informe_medico;

    @BindView(R.id.text_informe_medico2)
    TextView text_informe_medico2;

    @BindView(R.id.text_informe_medico3)
    TextView text_informe_medico3;

    @BindView(R.id.layout_button)
    LinearLayout layout_button;

    @BindView(R.id.layout_respuesta_especialista)
    LinearLayout layout_respuesta_especialista;

    @BindView(R.id.layout_informe_medico)
    LinearLayout layout_informe_medico;

    @BindView(R.id.layout_ordenes)
    LinearLayout layout_ordenes;

    @BindView(R.id.layout_recipes)
    LinearLayout layout_recipes;

    @BindView(R.id.text_status)
    TextView text_status;

    @BindView(R.id.text_date)
    TextView text_date;

    @BindView(R.id.text_hour)
    TextView text_hour;

//    @BindView(R.id.textpago)
//    TextView textpago;

    @BindView(R.id.text_price)
    TextView text_price;

    @BindView(R.id.layout_text_status)
    LinearLayout layout_text_status;

    @BindView(R.id.img_rating)
    ImageView img_rating;

    @BindView(R.id.listview)
    RecyclerView listview;

    @BindView(R.id.text_recipe_receta)
    TextView text_recipe_receta;

    @BindView(R.id.text_indicaciones)
    TextView text_indicaciones;

    @BindView(R.id.text_status_consulta)
    TextView text_status_consulta;

    @BindView(R.id.text_recipe)
    TextView text_recipe;

    @BindView(R.id.diagnostic_presuntivo)
    TextView diagnostic_presuntivo;

    @BindView(R.id.layout_observacion_del_diagnostico)
    LinearLayout layout_observacion_del_diagnostico;

    @BindView(R.id.layout_diagnostico_presuntivo)
    LinearLayout layout_diagnostico_presuntivo;

    @BindView(R.id.listRecipe_receta)
    RecyclerView listRecipe_receta;

    @BindView(R.id.listRecipe_receta2)
    RecyclerView listRecipe_receta2;

    @BindView(R.id.recycler_indicaciones2)
    RecyclerView recycler_indicaciones2;



    ListAdapter listAdapter;

    List<String> arraySpinner=new ArrayList<>();

    @BindView(R.id.recycler_diagnostico)
    RecyclerView recycler_diagnostico;

    ListAdapter listDiagnostico;

    ConsultasPresentador presentador;
    ProgressDialog progress;
    SolicitarConsultaPresentador presenterSolicitarConsulta;
    boolean reportar=false;
    boolean rate=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_detalle_consulta, container, false);
        ButterKnife.bind(this, v);

        presentador=new ConsultasPresentador(this);

        AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22());
        initUI();
        HomeActivity.setDetalleConsulta(false);
        initLoyout();
        presenterSolicitarConsulta=new SolicitarConsultaPresentador(this);
        presenterSolicitarConsulta.getEspecialistas();
        if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getStatusid()==7)
        {
            presentador.getTokenOpentok(Preferences.getUTC_usuario(),AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationid());
        }
        presentador.getTokenOpentok(Preferences.getUTC_usuario(),AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationid());


        if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getStatusid()==8&&AdapterConsultasProgramada.getDetalleConsultaProgramada().getStatusid()==9)
        {

        }


        Log.d("getConsultationid","---->"+AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationid());
        return v;
    }
    @OnClick(R.id.img_rating)
    public void rating() {
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==8||AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==9)
        {
            if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getIsRated()==0)
            {
                if(!rate)
                {
                    setRate(getContext());
                }
            }else
            {
                dialog("La consulta ya fue calificada.");
            }
        }
        else
        {
            dialog("No es posible calificar una consulta que no ha sido atendida por el médico.");
        }

    }
    public void initFragments(Context context)
    {
        FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, new MisConsultasFragment());
        ft.addToBackStack(null);
        ft.commit();
    }

    public void setRate(Context context) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialogo_rating, null);
        final RatingBar rating=(RatingBar)view.findViewById(R.id.ratingBar);

        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                presentador.setRate(new ScoreRequest(Preferences.getUTC_usuario(),AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getConsultationid(),Preferences.getIDUser(),rating.getNumStars()));
                dialog.cancel();
            }
        });

        builder.setView(view);
        builder.show();

    }

    @Override
    public void onPause() {
        super.onPause();
        initFragments(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        initFragments(getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            initFragments(getContext());
        } catch (Resources.NotFoundException e){
          //  throw new IOException(e.toString());
        }

    }

    public void initLoyout()
    {
//        button_reportar.setClickable(false);
        button_conectarme.setBackgroundResource(R.color.form_pressed);
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==7)
        {
            button_conectarme.setBackgroundResource(R.color.success_bg);
           // text_status.setTextColor(Color.parseColor("#DDEEF4"));
           // layout_text_status.setBackgroundResource(R.drawable.respondio_semi_circulo);
           // setRate(getContext());
        }

        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==9)
        {
            //text_status.setTextColor(Color.parseColor("#DDEEF4"));
            //layout_text_status.setBackgroundResource(R.drawable.status_finalizada_con_diagnostico);
            layout_observacion_del_diagnostico.setVisibility(View.VISIBLE);
            layout_diagnostico_presuntivo.setVisibility(View.VISIBLE);
            presentador.getDiagnosticoItem(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationid());
            presentador.getDiagnostico(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationid());
            presentador.getPrescripcion(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationid());
            presentador.getExamLab(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationid());
            presentador.getPrescripcionItem(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationid());
        }
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==3)
        {
            button_reportar.setClickable(true);
          //  text_status.setTextColor(Color.parseColor("#f95f62"));
          //  layout_text_status.setBackgroundResource(R.color.white);
        }
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==5)
        {
           button_reportar.setText("Completar");
          //  text_status.setTextColor(Color.parseColor("#DDEEF4"));
          //  layout_text_status.setBackgroundResource(R.drawable.status_incompleta);
        }
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==8)
        {
            //text_status.setTextColor(Color.parseColor("#DDEEF4"));
           // layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);

        }
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==6)
        {
           // text_status.setTextColor(Color.parseColor("#DDEEF4"));
         //   layout_text_status.setBackgroundResource(R.drawable.status_por_realizar);
        }
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==4)
        {
           // text_status.setTextColor(Color.parseColor("#DDEEF4"));
           // layout_text_status.setBackgroundResource(R.drawable.respondio_semi_circulo);
        }
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==11)
        {
           // text_status.setTextColor(Color.parseColor("#DDEEF4"));
           // layout_text_status.setBackgroundResource(R.drawable.status_reprogramada);
        }
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==10)
        {
            button_reportar.setClickable(true);
          //  text_status.setTextColor(Color.parseColor("#DDEEF4"));
          //  layout_text_status.setBackgroundResource(R.drawable.status_por_reprogramar);
            button_conectarme.setBackgroundResource(R.color.button_red);
            button_reportar.setText("Reprogramar");

        }
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==12)
        {
           // text_status.setTextColor(Color.parseColor("#DDEEF4"));
           // layout_text_status.setBackgroundResource(R.drawable.status_reprogramada);
        }

    }


    public int validateDate(String date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date strDate = null;
        try {
            strDate = sdf.parse(date);
            Log.d("validatefech","-DATE-1>"+strDate);
            Log.d("validatefech","-DATE-2>"+new Date());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (new Date().equals(strDate)) {
            Log.d("validatefech","-si--1->"+strDate);
            return 1;
            //catalog_outdated = 1;
        }
        if (new Date().after(strDate)) {
            Log.d("validatefech","-si--2->"+strDate);
            return 2;
            //catalog_outdated = 1;
        }
        else
        {
            Log.d("validatefech","-no--3->");
            return 3;
        }

    }

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    private static String deleteChar(String text, int charPos) {
        return new StringBuilder(text).deleteCharAt(charPos).toString();
    }
    @OnClick(R.id.button_conectarme)
    public void conectarme() {


        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==7)
        {
            if(Preferences.getToken_opentok()!="" && Preferences.getId_sesion()!="")
            {
                if (AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getTypeid()==2)
                {
                    ActionBar.chat=true;
                    startActivity(ChatActivity.intent(getContext(),Preferences.getNombre() , new Session(Preferences.getId_sesion(),Preferences.getToken_opentok())));
                }
                if (AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getTypeid()==3)
                {
                    ActionBar.chat=false;
                    startActivity(VoiceActivity.intent(getContext(),Preferences.getNombre() , new Session(Preferences.getId_sesion(),Preferences.getToken_opentok())));
                }
                if (AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getTypeid()==4)
                {
                    ActionBar.chat=false;

                    Log.d("OneSignalExample", "video"+AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getTypeid());
                    startActivity(VideoActivity.intent(getContext(),Preferences.getNombre() , new Session(Preferences.getId_sesion(),Preferences.getToken_opentok())));
                }
            }else
            {
                dialog("Ups ha ocurrido un error!");
            }

        }else
        {
            dialog("El Médico no se encuentra conectado.");
        }
        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==10)
        {
            startActivity(new Intent(getContext(), ReprogramarConsultaActivity.class));
        }

    }




    @OnClick(R.id.button_reportar)
    public void solicitarConsulta() {
//        if(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid()==5)
//        {
//
//        }else
//        {
//            if(validateDate(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationDate())==1)
//            {
//                if(!reportar)
//                {
//                    reporteConsulta();
//                    reportar=true;
//                }
//
//            }
        if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getStatusid()==6)
        {
            if(validateDate(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationDate())==1) {
                if (!reportar) {
                    reporteConsulta();
                    reportar = true;
                }
            }else
            if(validateDate(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationDate())==2)
            {
                if (!reportar) {
                    reporteConsulta();

                }

            }if(validateDate(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationDate())==3)
            {
            dialog("No es posible reportar la consulta. Sólo se puede reportar una consulta no atendida por el médico el día y la hora programada o que presente inconvenientes técnicos durante su ejecución.");
            }

        }else
        {
            dialog("No es posible reportar la consulta. Sólo se puede reportar una consulta no atendida por el médico el día y la hora programada o que presente inconvenientes técnicos durante su ejecución.");
        }
// else
//        {
//            if(validateDate(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationDate())==2)
//            {
//                dialog("No es posible reportar la consulta. Sólo se puede reportar una consulta no atendida por el médico el día y la hora programada o que presente inconvenientes técnicos durante su ejecución.");
//
//            }
//            if(validateDate(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationDate())==3)
//            {
//                dialog("No es posible reportar la consulta. Sólo se puede reportar una consulta no atendida por el médico el día y la hora programada o que presente inconvenientes técnicos durante su ejecución.");
//            }
//        }
    }
    public static String hourFormatter(String startTime){
        //String startTime = "2013-02-27 21:06:30";
        String formatedDate =null;

        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
        Date date = null;
        try {
            date = dateFormatter.parse(startTime);


            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
            String displayValue = timeFormatter.format(date);
            return displayValue;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formatedDate;
    }
    public void initUI()
    {
        initImagenIcon();
        progress = new ProgressDialog(getContext());
        progress.setTitle(getContext().getString(R.string.registro_cargando));
        progress.setCancelable(false);
        condicionesUso(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusid());
        text_nombre.setText(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getPatientName());
        text_date.setText(parseDateToddMMyyyy(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getConsultationDate()));
        text_hour.setText(hourFormatter(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationHour()));
        text_sexo.setText(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getSex());
        //textpago.setText(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).get);
        text_pago.setText(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getPaymentnumber());
        text_motivo.setText(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getReason());
        text_nombre_dr.setText(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getDoctorName());
        text_consulta.setText("CONSULTA "+ AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getCode());
        text_especialidad.setText(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getSpecialityname());
        text_status.setText(AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getStatusname());
        text_status_consulta.setText(AdapterConsultasProgramada.getDetalleConsultaProgramada().getStatusname());
        if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getDoctorAnswer()!=null)
        {
            text_respuesta_especialista.setText(AdapterConsultasProgramada.getDetalleConsultaProgramada().getDoctorAnswer().toString());
        }
        //text_limite_resp.setText(parseDateToddMMyyyy2(AdapterConsultasProgramada.getDetalleConsultaProgramada().getRealFinishDate()));
        if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getDoctorAnswer()!=null)
        {
            text_respuesta_especialista.setText(AdapterConsultasProgramada.getDetalleConsultaProgramada().getDoctorAnswer().toString());
        }
        if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getMedicalReport()!=null)
        {
            text_informe_medico2.setText(AdapterConsultasProgramada.getDetalleConsultaProgramada().getMedicalReport().toString());
        }

        int edad = 0;
        try {
            DateFormat dateFormat = dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date fechaNacimiento = dateFormat.parse(AdapterConsultasProgramada.getDetalleConsultaProgramada().getBirthdate().replace("/", "-"));
            Calendar cal = Calendar.getInstance();
            Date fechaActual = cal.getTime();

            edad = getEdad(fechaNacimiento, fechaActual);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        text_edad.setText(""+edad);


        if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getStatusid()==8&&AdapterConsultasProgramada.getDetalleConsultaProgramada().getStatusid()==9)
        {
            if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getIsRated()==0)
            {
                text_status.setText("Calificar");
                Utils.setRate(getContext());
            }else
            {
                img_rating.setImageResource(R.drawable.ic_star_black_24dp);
                text_status.setText(AdapterConsultasProgramada.getDetalleConsultaProgramada().getStatusname());
            }
        }
        if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getIsRated()!=0)
        {
            img_rating.setImageResource(R.drawable.ic_star_black_24dp);
        }


    }


    public static String parseDateToddMMyyyy2(String time) {
        String inputPattern = "yyyy/MM/dd h:mm";
        String outputPattern = "dd/MM/yyyy h:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    public int getEdad(Date fechaNacimiento, Date fechaActual) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int dIni = Integer.parseInt(formatter.format(fechaNacimiento));
        int dEnd = Integer.parseInt(formatter.format(fechaActual));
        int age = (dEnd - dIni) / 10000;
        return age;
    }

    public void condicionesUso(int num)
    {
            if(num==3)
            {
                button_reportar.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_edit, 0, 0, 0);
                button_reportar.setText("Reportar");
                button_reportar.setBackgroundResource(R.color.button_red);
                button_conectarme.setBackgroundResource(R.color.form_pressed);
                button_conectarme.setClickable(false);
                layout_respuesta_especialista.setVisibility(View.GONE);
                layout_informe_medico.setVisibility(View.GONE);
               // text_status.setTextColor(Color.parseColor("#FFFFFF"));
               // layout_text_status.setBackgroundResource(R.color.icon_bg);
            }
        if(num==7)
        {
            button_reportar.setClickable(false);
            layout_respuesta_especialista.setVisibility(View.GONE);
            layout_informe_medico.setVisibility(View.GONE);
            //text_status.setTextColor(Color.parseColor("#FFFFFF"));
            //layout_text_status.setBackgroundResource(R.drawable.efecto_semi_circulo);
        }

        if(num==8)
        {
            button_reportar.setClickable(false);
            layout_respuesta_especialista.setVisibility(View.GONE);
            layout_informe_medico.setVisibility(View.GONE);
          //  text_status.setTextColor(Color.parseColor("#FFFFFF"));
          //  layout_text_status.setBackgroundResource(R.drawable.efecto_semi_circulo);
        }

        if(num==9 || num==4)
        {
            //button_reportar.setBackgroundResource(R.color.color_item_doc);
            layout_respuesta_especialista.setVisibility(View.VISIBLE);
            layout_informe_medico.setVisibility(View.VISIBLE);
            layout_ordenes.setVisibility(View.VISIBLE);
            layout_recipes.setVisibility(View.VISIBLE);
            button_reportar.setClickable(false);
            text_status.setTextColor(Color.parseColor("#FFFFFF"));
           // layout_text_status.setBackgroundResource(R.drawable.respondio_semi_circulo);
        }

    }

    public void  initImagenIcon()
    {

        button_reportar.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_edit, 0, 0, 0);
        img_typo.setBackgroundResource(R.drawable.circule);

        if (AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getTypeid()==2)
        {
            //holder.img_consulta.setBackgroundColor(R.drawable.circulo);
            img_typo.setImageResource(R.drawable.ic_chat_bubble_black_24dp);
            button_conectarme.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_chat_bubble_black_24dp, 0, 0, 0);

        }
        if (AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getTypeid()==3)
        {
            button_conectarme.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_local_phone_black_24dp, 0, 0, 0);
            img_typo.setImageResource(R.drawable.ic_local_phone_black_24dp);
        }
        if (AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getTypeid()==4)
        {
            button_conectarme.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_videocam_white_24dp, 0, 0, 0);
            img_typo.setImageResource(R.drawable.ic_videocam_white_24dp);
        }

    }

    public void reporteConsulta()
    {
        arraySpinner.clear();
       // arraySpinner.add("El medico no ha respondido");
        arraySpinner.add("Seleccione");
        arraySpinner.add("El medico no asistio");
        arraySpinner.add("El medico no ha respondido");
        arraySpinner.add("Inconvenientes técnicos");


        final AlertDialog.Builder[] builder = {new AlertDialog.Builder(getActivity())};

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.layout_dialogo_reportar, null );

        TextView text_tipo =(TextView)view.findViewById(R.id.text_tipo);


        Spinner fecha_consulta =(Spinner) view.findViewById(R.id.fecha_consulta);
        Spinner hora_consulta =(Spinner) view.findViewById(R.id.hora_consulta);

        final EditText edit_observaciones=(EditText) view.findViewById(R.id.edit_observaciones);
        Button aceptar =(Button) view.findViewById(R.id.btton_aceptar);
        final Button cancelar =(Button) view.findViewById(R.id.btton_cancelar);
        final int[] reporte = {1};
        if (AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getTypeid()==2)
        {
            text_tipo.setText("Chat");
        }
        if (AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getTypeid()==3)
        {
            text_tipo.setText("Llamada");
        }
        if (AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getTypeid()==4)
        {
            text_tipo.setText("Video Llamada");
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, arraySpinner);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fecha_consulta.setAdapter(dataAdapter);
        fecha_consulta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            reporte[0] =pos;
        }
        public void onNothingSelected(AdapterView<?> parent) {
        }
    });


        builder[0].setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if(reporte[0]!=0)
                {
                    progress.show();
                    ReporteRequest request= new ReporteRequest(
                            AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getConsultationid(),
                            Preferences.getUTC_usuario(),
                            Preferences.getIDUser(),
                            reporte[0],
                            edit_observaciones.getText().toString());

                    presentador.setReporte(request);
                }else
                {
                    dialog("Seleccione una opción para reportar su consulta!");
                }

            }
        });
        builder[0].setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder[0].setView(view);
        builder[0].show();

    }

    @Override
    public void getDiagnosticoList(List<Diagnostico> dato) {
        //text_respuesta_especialista.setText(dato.get(0).getDiseasename());
        diagnostic_presuntivo.setText(dato.get(0).getDiagnostic());
        Log.d("getDiagnosticoList",""+dato);
    }

    @Override
    public void getConsultation(List<Consultation> dato) {

    }

    @Override
    public void getPrescripcionList(List<Prescripcion> dato) {
        Log.d("getPrescripcionList",""+dato.toString());


        final ArrayList<String> list = new ArrayList<String>();String[] values;
        for (int i=0;i<dato.size();i++)
        {
            list.add(dato.get(i).getName()+""+"\n"+dato.get(i).getPresentation());
        }

        listAdapter = new ListAdapter(list);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());

        listRecipe_receta2.setLayoutManager(reci);
        listRecipe_receta2.setHasFixedSize(true);
        listRecipe_receta2.setAdapter(listAdapter);

    }

    @Override
    public void getPrescripcionItemList(List<PrescripcionItem> dato) {


        final ArrayList<String> list = new ArrayList<String>();String[] values;
        for (int i=0;i<dato.size();i++)
        {
            list.add(dato.get(i).getVademecum()+""+"\n"+dato.get(i).getVademecumPresentation());
        }

        listAdapter = new ListAdapter(list);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());

        listRecipe_receta.setLayoutManager(reci);
        listRecipe_receta.setHasFixedSize(true);
        listRecipe_receta.setAdapter(listAdapter);

        final ArrayList<String> list2 = new ArrayList<String>();
        for (int i=0;i<dato.size();i++)
        {
            list2.add(dato.get(i).getVademecum()+""+"\n"+dato.get(i).getObservation());
        }

        listAdapter = new ListAdapter(list2);
        LinearLayoutManager reci2 = new LinearLayoutManager(getContext());

        recycler_indicaciones2.setLayoutManager(reci2);
        recycler_indicaciones2.setHasFixedSize(true);
        recycler_indicaciones2.setAdapter(listAdapter);






    }

    @Override
    public void getExamLab(List<ExamLab> dato) {
        final ArrayList<String> list = new ArrayList<String>();String[] values;
        for (int i=0;i<dato.size();i++)
        {
            list.add(dato.get(i).getExamType()+""+"\n"+dato.get(i).getExamCategory()+": "+""+dato.get(i).getExam());
        }

        listAdapter = new ListAdapter(list);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());

        listview.setLayoutManager(reci);
        listview.setHasFixedSize(true);
        listview.setAdapter(listAdapter);
        Log.d("getExamLab",""+dato);
    }
    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
    @Override
    public void getReporte(String dato) {
        progress.cancel();
        if(dato.equals("ok"))
        {
            reportar = true;
//
//                dialog("¡Envío exitoso!\n" +
//                        "                Su reporte se ha enviado satisfactoriamente.");
            dialog("La consulta ha sido reportada exitosamente. Un administrador le contactara para reprogramarla.");
        }else
        {
            reportar=false;
            dialog("Error de conexion con el Servidor. Intente mas tarde");
        }


     Log.d("getReporte",""+dato);
    }

    @Override
    public void getScore(String sms) {
        img_rating.setImageResource(R.drawable.ic_star_black_24dp);
        rate=true;
        Log.d("getScore",""+sms);
    }

    @Override
    public void getDiagnosticoItem(List<Diagnostic> dato) {
        Log.d("getDiagnosticoList",""+dato);

        final ArrayList<String> list = new ArrayList<String>();String[] values;
        for (int i=0;i<dato.size();i++)
        {
            Log.d("getDiagnosticoList",""+dato.get(i).getDiseasetype());
            list.add(dato.get(i).getDiseasetype()+": "+dato.get(i).getDiseasename());
        }

        listDiagnostico = new ListAdapter(list);
        LinearLayoutManager reci = new LinearLayoutManager(getContext());

        recycler_diagnostico.setLayoutManager(reci);
        recycler_diagnostico.setHasFixedSize(true);
        recycler_diagnostico.setAdapter(listDiagnostico);
    }

    @Override
    public void getStatusToken(EstatusResponse status) {
        Preferences.setId_sesion(status.getSession());
        Preferences.setToken_opentok(status.getMessage());
        Log.d("getStatusToken","---->"+status.getSession());
        Log.d("getStatusToken","---->"+status.getMessage());
    }

    public void dialog(String resp)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage(resp);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public void getListEspecialidad(List<Especialidad> especialistas) {

        for (int i=0;i<especialistas.size();i++)
        {
            if(especialistas.get(i).getSpecialitiesid()==AdapterConsultasProgramada.listConsultaAdapter.get(AdapterConsultasProgramada.getNumeroPosicion22()).getSpecialityid())
            {
                text_price.setText("$"+especialistas.get(i).getConsultationCost());
            }
        }


    }

    @Override
    public void getListDoctor(List<Doctor> doctor) { }

    @Override
    public void getHorarioDoctor(List<Availabilty> horarioDoctor) { }

    @Override
    public void getSolicitarConsulta(DataSolicitudConsulta code) { }

    @Override
    public void getPaymentsResp(DataPayment dataPayment) { }

    @Override
    public void getRespDoctorFail(String text) { }

    @Override
    public void getShoulderDoctor(List<Schedule> hoursString) {

    }

    @Override
    public void getError(String dato) {

    }
}

package com.tuclinicavirtual.mvo.menu.solicitarConsulta.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens.MedicoFragment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.fragmens.SolicitarConsultaFragment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Availabilty;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataPayment;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DataSolicitudConsulta;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.DoctoHorariosRequest;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Doctor;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Especialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.ResponsesEspecialidad;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.models.Schedule;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultaPresentador;
import com.tuclinicavirtual.mvo.menu.solicitarConsulta.mvp.SolicitarConsultasContrato;
import com.tuclinicavirtual.mvo.utils.Preferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListDoctoresAdapter extends RecyclerView.Adapter<ListDoctoresAdapter.ViewHolder> implements SolicitarConsultasContrato.View{


    List<Doctor> listDoctor=new ArrayList<>();

    Context context;

    SolicitarConsultaPresentador presentador;

    boolean img_chat=false;
    boolean img_video=false;
    boolean img_email=false;
    boolean img_phone=false;

    boolean consultarHora=false;

    ArrayAdapter<String> dataAdapter2;

    public static int tipoConsulta=-1;

    public static String horaSelec;
    public static String fechaSelec;
    public static String nombreDr;
    public static Doctor objetoDoctorSelec;


    public static int position2;

    public static Doctor getObjetoDoctorSelec() {
        return objetoDoctorSelec;
    }

    public static void setObjetoDoctorSelec(Doctor objetoDoctorSelec) {
        ListDoctoresAdapter.objetoDoctorSelec = objetoDoctorSelec;
    }

    public static String getNombreDr() {
        return nombreDr;
    }

    public static void setNombreDr(String nombreDr) {
        ListDoctoresAdapter.nombreDr = nombreDr;
    }

    public static String getHoraSelec() {
        return horaSelec;
    }

    public static void setHoraSelec(String horaSelec) {
        ListDoctoresAdapter.horaSelec = horaSelec;
    }

    public static String getFechaSelec() {
        return fechaSelec;
    }

    public static void setFechaSelec(String fechaSelec) {
        ListDoctoresAdapter.fechaSelec = fechaSelec;
    }

    public static int getTipoConsulta() {
        return tipoConsulta;
    }

    public static void setTipoConsulta(int tipoConsulta) {
        ListDoctoresAdapter.tipoConsulta = tipoConsulta;
    }

    public static AlertDialog dialogoConfirmaAsignacion;


    public static int idMedico;

    public static int getIdMedico() {
        return idMedico;
    }

    public static void setIdMedico(int idMedico) {
        ListDoctoresAdapter.idMedico = idMedico;
    }

    int auxiliarId=-1;
    int auxiliarClick=-1;

    int itemClick=-1;
    int itemClick2=-1;

    List<String> listFechaConsulta=new ArrayList<>();
    List<String> listFechaFormatoConsulta=new ArrayList<>();
    List<String> listFechaFormatoConsultaAuxi=new ArrayList<>();
    List<String> listHoraConsulta=new ArrayList<>();
    List<String> listfechadeConsultaAuxi=new ArrayList<>();
    List<Availabilty> listAvailabilities=new ArrayList<>();

    List<Schedule> listShedule=new ArrayList<>();


    DoctoHorariosRequest requestHorario;


    public ProgressDialog progress;

    public static String tipoSeleccion;

    public static String getTipoSeleccion() {
        return tipoSeleccion;
    }

    public static void setTipoSeleccion(String tipoSeleccion) {
        ListDoctoresAdapter.tipoSeleccion = tipoSeleccion;
    }

    public ListDoctoresAdapter(List<Doctor> list, Context context) {
        this.listDoctor=list;
        this.context=context;
        dataAdapter2 = null;
        presentador=new SolicitarConsultaPresentador(this);
        Log.d("ListDoctoresAdapter","--->1");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_medicos_solicitud, parent, false);


        Log.d("ListDoctoresAdapter","--->2");
        progress = new ProgressDialog(context);
        progress.setTitle("En proceso..");
        progress.setCancelable(true);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {
    holder.bindData(listDoctor.get(position),context);


        holder.text_hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.show();
                DoctoHorariosRequest requestHorario = new DoctoHorariosRequest(listDoctor.get(position).getTimezonename(),listDoctor.get(position).getDoctorid(),Integer.toString(ListEspecialidadAdapter.getIdEspecialidad()),null,null);
                presentador.setHorarioDoctor(requestHorario);

            }
        });

        holder.layout_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.show();
                Log.d("dialogHorarioDisponible","UTC"+listDoctor.get(position).getTimezonename());
                presentador.getShoulderDoctor(listDoctor.get(position).getTimezonename());
                setIdMedico(listDoctor.get(position).getDoctorid());
                setObjetoDoctorSelec(listDoctor.get(position));

            }
        });

    if(auxiliarClick==position)
    {
        holder.layout_doctor.setBackgroundResource(R.color.form_pressed);
    }else
    {
        holder.layout_doctor.setBackgroundResource(R.color.color_layout);
    }

        holder.id_frame_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("DoctoresInformacion","------>"+listDoctor.get(position).toString());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.button_bg));
                }
                Time today = new Time(Time.getCurrentTimezone());
                today.setToNow();

                String date=today.year+"/"+(today.month+1)+"/"+today.monthDay;
                setNombreDr(listDoctor.get(position).getName()+" "+listDoctor.get(position).getLastname());
                setFechaSelec(date);
                setHoraSelec("08:00:00");
                setObjetoDoctorSelec(listDoctor.get(position));
                setIdMedico(listDoctor.get(position).getDoctorid());
                SolicitarConsultaFragment.setPasarSiguiente(true);
                position2=position;
                setTipoSeleccion("Escrita");
                auxiliarId=0;
                setTipoConsulta(1);
                itemClick=position;

                        holder.id_frame_email.setBackgroundResource(R.color.button_bg);
                        img_email=true;

                notifyDataSetChanged();
            }
        });

        holder.id_frame_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.show();

                listFechaConsulta.clear();
                listFechaFormatoConsulta.clear();
                listfechadeConsultaAuxi.clear();
                listFechaFormatoConsultaAuxi.clear();
                listAvailabilities.clear();
                position2=position;
                Log.d("DoctoresInformacion","------>"+listDoctor.get(position).toString());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.form_shadow));
                }
                //ViewHolder.progress.show();
                requestHorario = new DoctoHorariosRequest(Preferences.getUTC_usuario(),listDoctor.get(position).getDoctorid(),Integer.toString(ListEspecialidadAdapter.getIdEspecialidad()),null,null);
                presentador.setHorarioDoctor(requestHorario);
                setObjetoDoctorSelec(listDoctor.get(position));
                setIdMedico(listDoctor.get(position).getDoctorid());

                setTipoSeleccion("Chat");

                itemClick=position;

                auxiliarId=1;
                setTipoConsulta(2);


                setNombreDr(listDoctor.get(position).getName()+" "+listDoctor.get(position).getLastname());
                    holder.id_frame_chat.setBackgroundResource(R.color.button_bg);
                    img_chat=true;
                SolicitarConsultaFragment.setPasarSiguiente(false);
                notifyDataSetChanged();

            }
        });

        holder.id_frame_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.show();

                listFechaConsulta.clear();
                listFechaFormatoConsulta.clear();
                listFechaFormatoConsultaAuxi.clear();
                listAvailabilities.clear();
                listfechadeConsultaAuxi.clear();
                position2=position;

                Log.d("DoctoresInformacion","------>"+listDoctor.get(position).toString());
                requestHorario = new DoctoHorariosRequest(Preferences.getUTC_usuario(),listDoctor.get(position).getDoctorid(),Integer.toString(ListEspecialidadAdapter.getIdEspecialidad()),null,null);
                presentador.setHorarioDoctor(requestHorario);
                setIdMedico(listDoctor.get(position).getDoctorid());

                setNombreDr(listDoctor.get(position).getName()+" "+listDoctor.get(position).getLastname());
                itemClick=position;
                setTipoSeleccion("Llamada");
                auxiliarId=2;
                setObjetoDoctorSelec(listDoctor.get(position));
                setTipoConsulta(3);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.form_shadow));
                }

                    holder.id_frame_phone.setBackgroundResource(R.color.button_bg);
                    img_phone=true;
                SolicitarConsultaFragment.setPasarSiguiente(false);
                notifyDataSetChanged();
            }
        });
        holder.id_frame_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.show();

                listFechaConsulta.clear();
                listAvailabilities.clear();
                listFechaFormatoConsulta.clear();
                listFechaFormatoConsultaAuxi.clear();
                listfechadeConsultaAuxi.clear();
                position2=position;

                Log.d("DoctoresInformacion","------>"+listDoctor.get(position).toString());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.form_shadow));
                }
                requestHorario = new DoctoHorariosRequest(Preferences.getUTC_usuario(),listDoctor.get(position).getDoctorid(),Integer.toString(ListEspecialidadAdapter.getIdEspecialidad()),null,null);
                presentador.setHorarioDoctor(requestHorario);
                setIdMedico(listDoctor.get(position).getDoctorid());
              //  SolicitarConsultaFragment.setPasarSiguiente(true);
                setNombreDr(listDoctor.get(position).getName()+" "+listDoctor.get(position).getLastname());
                setObjetoDoctorSelec(listDoctor.get(position));
               itemClick=position;
                setTipoSeleccion("Video llamada");
                auxiliarId=3;
                setTipoConsulta(4);

                    holder.id_frame_video.setBackgroundResource(R.color.button_bg);
                    img_video=true;
                SolicitarConsultaFragment.setPasarSiguiente(false);
                notifyDataSetChanged();

            }
        });

        holder.id_frame_email.setBackgroundResource(R.color.form_pressed);
        holder.id_frame_chat.setBackgroundResource(R.color.form_pressed);
        holder.id_frame_phone.setBackgroundResource(R.color.form_pressed);
        holder.id_frame_video.setBackgroundResource(R.color.form_pressed);

        holder.text_hora.setVisibility(View.INVISIBLE);
        //holder.text_disponible.setVisibility(View.INVISIBLE);
        if(itemClick2==position && auxiliarId!=0)
        {

            if (getFechaSelec()!=null)
            {
                holder.text_horario.setText(parseDateToddMMyyyy(getFechaSelec()));
                holder.text_disponible.setText(getHoraSelec());
            }


        }else
        {
            holder.text_disponible.setText("");
            holder.text_horario.setText("");
        }
        if(itemClick==position)
        {
            Log.d("fecha_consulta","--->VER"+getFechaSelec());

            if(auxiliarId==0)
            {
                holder.id_frame_email.setBackgroundResource(R.color.button_bg);
                holder.text_horario.setText("");
                holder.text_disponible.setText("");

            }
            if(auxiliarId==1)
            {
                holder.id_frame_chat.setBackgroundResource(R.color.button_bg);

            }
            if(auxiliarId==2)
            {
                holder.id_frame_phone.setBackgroundResource(R.color.button_bg);

            }
            if(auxiliarId==3)
            {
                holder.id_frame_video.setBackgroundResource(R.color.button_bg);

            }

        }


    }

    @Override
    public int getItemCount() {
        return listDoctor.size();
    }

    @Override
    public void getListEspecialidad(List<Especialidad> especialistas) { }

    @Override
    public void getListDoctor(List<Doctor> doctor) {

    }

    @Override
    public void getHorarioDoctor(List<Availabilty> horarioDoctor) {



        if (!consultarHora)
        {   listFechaConsulta.clear();
            listFechaFormatoConsulta.clear();
            listFechaFormatoConsultaAuxi.clear();

            listAvailabilities=horarioDoctor;
            for (int i=0;i<horarioDoctor.size();i++)
            {
                listFechaConsulta.add(new String(horarioDoctor.get(i).getCastDateConsultation()));
                listFechaFormatoConsultaAuxi.add(new String(horarioDoctor.get(i).getDateConsultation()));
            }
            borrarElementos(listFechaConsulta);
            borrarElementosAuxi(listFechaFormatoConsultaAuxi);
            dialogHorario();
        }
        if(consultarHora)
        {
            listHoraConsulta.clear();
            for (int i=0;i<horarioDoctor.size();i++)
            {
                listHoraConsulta.add(hourFormatter(horarioDoctor.get(i).getStartHour()));
            }
            Log.d("horarioAdapter",""+horarioDoctor.toString());
            dataAdapter2.notifyDataSetChanged();
        }
        progress.cancel();

    }
    public static String hourFormatter(String startTime){
        //String startTime = "2013-02-27 21:06:30";
        String formatedDate =null;

        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
        Date date = null;
        try {
            date = dateFormatter.parse(startTime);


            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
            String displayValue = timeFormatter.format(date);
            return displayValue;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formatedDate;
    }


    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    private static String deleteChar(String text, int charPos) {
        return new StringBuilder(text).deleteCharAt(charPos).toString();
    }

    @Override
    public void getSolicitarConsulta(DataSolicitudConsulta code) {

    }

    @Override
    public void getPaymentsResp(DataPayment dataPayment) {

    }

    @Override
    public void getRespDoctorFail(String text) {
        progress.cancel();
        listHoraConsulta.clear();
        listHoraConsulta.clear();
        consultarHora=false;
        SolicitarConsultaFragment.setPasarSiguiente(false);
        setFechaSelec("");
        dialog("Sin disponibilidad");
    }

    @Override
    public void getShoulderDoctor(List<Schedule> hours) {
        listShedule.clear();
        listShedule=hours;

        dialogHorarioDisponible(getIdMedico());
       progress.cancel();
    }

    @Override
    public void getError(String dato) {

    }


    public void borrarElementos(List<String>  list)
    {
        for(int i=0;i<list.size();i++){
            for(int j=0;j<list.size()-1;j++){
                if(i!=j){
                    if(list.get(i).equals(list.get(j))){
                        // eliminamos su valor
                        list.set(i,"");
                    }
                }
            }
        }

        for (int k=0;k<=list.size()-1;k++){
            if(list.get(k)!=""){
                listfechadeConsultaAuxi.add(new String(list.get(k)));
                Log.d("borrarElementos",""+list.get(k));
            }
        }
    }

    public void borrarElementosAuxi(List<String>  list)
    {
        for(int i=0;i<list.size();i++){
            for(int j=0;j<list.size()-1;j++){
                if(i!=j){
                    if(list.get(i).equals(list.get(j))){
                        // eliminamos su valor
                        list.set(i,"");
                    }
                }
            }
        }

        for (int k=0;k<=list.size()-1;k++){
            if(list.get(k)!=""){
                listFechaFormatoConsulta.add(new String(list.get(k)));
                Log.d("borrarElementos",""+list.get(k));
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nombre_doc)
        TextView text_nombre_doc;

        @BindView(R.id.especialidad_doc)
        TextView text_especialidad_doc;

        @BindView(R.id.text_hora)
        TextView text_hora;

        @BindView(R.id.text_disponible)
        TextView text_disponible;

        @BindView(R.id.layout_doctor)
        LinearLayout layout_doctor;

        @BindView(R.id.id_frame_chat)
        FrameLayout id_frame_chat;

        @BindView(R.id.id_frame_email)
        FrameLayout id_frame_email;

        @BindView(R.id.id_frame_phone)
        FrameLayout id_frame_phone;

        @BindView(R.id.id_frame_video)
        FrameLayout id_frame_video;

        @BindView(R.id.img_chat)
        ImageView img_chat;

        @BindView(R.id.img_email)
        ImageView img_email;

        @BindView(R.id.img_phone)
        ImageView img_phone;

        @BindView(R.id.img_video)
        ImageView img_video;

        @BindView(R.id.text_horario)
        TextView text_horario;

        Context context;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }

        public void bindData(Doctor c,Context context) {
             this.context=context;

             text_nombre_doc.setText(c.getFullname());
             text_especialidad_doc.setText(SolicitarConsultaFragment.getEspecialidad());
        }
    }

    public void dialog(String resp)
    {
        progress.cancel();

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(resp);

        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void dialogHorario()
    {

        progress.cancel();

        final List<String> hora = new ArrayList<>();

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dialogo_horario, null );

        TextView text_tipo_llamada =(TextView)view.findViewById(R.id.text_tipo_llamada);
        text_tipo_llamada.setText(getTipoSeleccion());

        Spinner fecha_consulta =(Spinner) view.findViewById(R.id.fecha_consulta);

        Spinner hora_consulta =(Spinner) view.findViewById(R.id.hora_consulta);

        Button aceptar =(Button) view.findViewById(R.id.btton_aceptar);
        final Button cancelar =(Button) view.findViewById(R.id.btton_cancelar);


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, listfechadeConsultaAuxi);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fecha_consulta.setAdapter(dataAdapter);

        dataAdapter2 = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, listHoraConsulta);

        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        hora_consulta.setAdapter(dataAdapter2);

        hora_consulta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
              setHoraSelec(listHoraConsulta.get(pos));
        }
        public void onNothingSelected(AdapterView<?> parent) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.form_shadow));
//            }
        }
    });

        fecha_consulta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Log.d("fecha_consulta","--->"+listAvailabilities.get(pos).getDateConsultation());
                setFechaSelec(listFechaFormatoConsulta.get(pos));
                consultarHora=true;

                dialogHora(Preferences.getUTC_usuario(), listAvailabilities.get(pos).getDoctorid(),listAvailabilities.get(pos).getSpecialtyid(), listFechaFormatoConsulta.get(pos));
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        //hora_consulta.setAdapter(dataAdapter2);

        Log.d("dialog",""+getTipoSeleccion());


        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                consultarHora=false;
                progress.cancel();

                SolicitarConsultaFragment.setPasarSiguiente(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    SolicitarConsultaFragment.aceptar.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.button_bg));
                }
                itemClick2=position2;
                notifyDataSetChanged();
                // sign in the user ...
            }
        });
                builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        progress.cancel();

                        consultarHora=false;
                        SolicitarConsultaFragment.setPasarSiguiente(false);
                        setFechaSelec("");
                        dialog.cancel();
                    }
                });

//        builder.setView(inflater.inflate(R.layout.dialogo_horario, null))
//
//                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//
//                    }
//                })
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//
//                    }
//                });
        builder.setView(view);
        builder.show();

    }
    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }




    public void dialogHorarioDisponible(int doctorid)
    {
        //listShedule.clear();
        progress.cancel();
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dialog_schedule_of_consultation, null );
        final ListView listview = (ListView) view.findViewById(R.id.listview);
        final ArrayList<String> list = new ArrayList<String>();
        if(listShedule!=null)
        {
            for (int i = 0; i < listShedule.size(); ++i) {
                if(doctorid==listShedule.get(i).getDoctorid() && ListEspecialidadAdapter.getEspecialidad().getSpecialitiesid()==listShedule.get(i).getSpecialtyid())
                {
                    list.add(listShedule.get(i).getDayname()+"\n"+hourFormatter(listShedule.get(i).getStartHour())+" - "+hourFormatter(listShedule.get(i).getFinishHour()));
                    Log.d("dialogHorarioDisponible",""+listShedule.get(i).getDayname()+" "+hourFormatter(listShedule.get(i).getStartHour())+" - "+hourFormatter(listShedule.get(i).getFinishHour()));
                    Log.d("dialogHorarioDisponible",""+listShedule.get(i).getDoctorid());
                    Log.d("dialogHorarioDisponible",""+doctorid);
                }

            }

        }
        final StableArrayAdapter adapter = new StableArrayAdapter(context,
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);

        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
//        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//
//                dialog.cancel();
//            }
//        });


        builder.setView(view);
        builder.show();

    }




    public void dialogHora(String zonaH, int idDoc,int idEspecialidad, String fecha)
    {
        //ViewHolder.progress.show();
        DoctoHorariosRequest requestHorario = new DoctoHorariosRequest(zonaH,idDoc,Integer.toString(ListEspecialidadAdapter.getIdEspecialidad()),fecha,null);
        presentador.setHorarioDoctor(requestHorario);
        progress.cancel();
    }


}

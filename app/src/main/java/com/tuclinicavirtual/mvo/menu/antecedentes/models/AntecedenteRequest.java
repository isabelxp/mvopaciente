package com.tuclinicavirtual.mvo.menu.antecedentes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AntecedenteRequest {
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("patientid")
    @Expose
    private Integer patientid;
    @SerializedName("antecedents")
    @Expose
    private List<com.tuclinicavirtual.mvo.menu.antecedentes.models.antecedents> antecedents = null;

    public AntecedenteRequest(Integer userid, Integer patientid, List<com.tuclinicavirtual.mvo.menu.antecedentes.models.antecedents> antecedents) {
        this.userid = userid;
        this.patientid = patientid;
        this.antecedents = antecedents;
    }

    public Integer getUserid() {
        return userid;
    }

    @Override
    public String toString() {
        return "AntecedenteRequest{" +
                "userid=" + userid +
                ", patientid=" + patientid +
                ", antecedents=" + antecedents +
                '}';
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    public List<com.tuclinicavirtual.mvo.menu.antecedentes.models.antecedents> getAntecedents() {
        return antecedents;
    }

    public void setAntecedents(List<com.tuclinicavirtual.mvo.menu.antecedentes.models.antecedents> antecedents) {
        this.antecedents = antecedents;
    }

}

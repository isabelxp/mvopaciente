package com.tuclinicavirtual.mvo.menu.antecedentes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Antecedentes2 {

    @SerializedName("recordid")
    @Expose
    private Integer recordid;
    @SerializedName("patientid")
    @Expose
    private Integer patientid;
    @SerializedName("antecedentid")
    @Expose
    private Integer antecedentid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("answer")
    @Expose
    private Integer answer;
    @SerializedName("observation")
    @Expose
    private String observation;

    @Override
    public String toString() {
        return "Antecedentes2{" +
                "recordid=" + recordid +
                ", patientid=" + patientid +
                ", antecedentid=" + antecedentid +
                ", name='" + name + '\'' +
                ", answer=" + answer +
                ", observation=" + observation +
                '}';
    }

    public Integer getRecordid() {
        return recordid;
    }

    public void setRecordid(Integer recordid) {
        this.recordid = recordid;
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    public Integer getAntecedentid() {
        return antecedentid;
    }

    public void setAntecedentid(Integer antecedentid) {
        this.antecedentid = antecedentid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAnswer() {
        return answer;
    }

    public void setAnswer(Integer answer) {
        this.answer = answer;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
}

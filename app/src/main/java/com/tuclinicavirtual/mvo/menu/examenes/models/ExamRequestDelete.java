package com.tuclinicavirtual.mvo.menu.examenes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExamRequestDelete {

    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("exams")
    @Expose
    private List<ExamDelete> exams = null;

    public ExamRequestDelete(Integer userid, List<ExamDelete> exams) {
        this.userid = userid;
        this.exams = exams;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public List<ExamDelete> getExams() {
        return exams;
    }

    public void setExams(List<ExamDelete> exams) {
        this.exams = exams;
    }

}

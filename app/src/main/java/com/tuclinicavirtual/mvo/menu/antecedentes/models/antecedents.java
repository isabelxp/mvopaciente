package com.tuclinicavirtual.mvo.menu.antecedentes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class antecedents {

    @SerializedName("recordid")
    @Expose
    private Integer recordid;
    @SerializedName("antecedentid")
    @Expose
    private Integer antecedentid;
    @SerializedName("answer")
    @Expose
    private Integer answer;
    @SerializedName("observation")
    @Expose
    private String observation;

    public antecedents(Integer recordid, Integer antecedentid, Integer answer, String observation) {
        this.recordid = recordid;
        this.antecedentid = antecedentid;
        this.answer = answer;
        this.observation = observation;
    }

    public void setRecordid(Integer recordid) {
        this.recordid = recordid;
    }

    public Integer getRecordid() {
        return recordid;
    }

    public Integer getAntecedentid() {
        return antecedentid;
    }

    public void setAntecedentid(Integer antecedentid) {
        this.antecedentid = antecedentid;
    }

    @Override
    public String toString() {
        return "antecedents{" +
                "recordid=" + recordid +
                ", antecedentid=" + antecedentid +
                ", answer=" + answer +
                ", observation='" + observation + '\'' +
                '}';
    }

    public Integer getAnswer() {
        return answer;
    }

    public void setAnswer(Integer answer) {
        this.answer = answer;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
}

package com.tuclinicavirtual.mvo.menu.notificaciones.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.menu.consultas.models.ModeloConsultasEscritas;
import com.tuclinicavirtual.mvo.menu.notificaciones.models.ModeloNotificaciones;
import com.tuclinicavirtual.mvo.menu.notificaciones.models.Notification;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificacionesAdapter extends RecyclerView.Adapter<NotificacionesAdapter.ViewHolder> {

    List<Notification> listNotificaciones;

    public NotificacionesAdapter(List listNotificaciones) {
        this.listNotificaciones=listNotificaciones;
    }

    @Override
    public NotificacionesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notificaciones, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NotificacionesAdapter.ViewHolder holder, int position) {
     holder.noti_mensaje.setText(listNotificaciones.get(position).getDescription());
     holder.noti_title.setText(listNotificaciones.get(position).getNotificationType());
    }

    @Override
    public int getItemCount() {
        return listNotificaciones.size();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.noti_title)
        TextView noti_title;
        @BindView(R.id.noti_mensaje)
        TextView noti_mensaje;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }


    }
}

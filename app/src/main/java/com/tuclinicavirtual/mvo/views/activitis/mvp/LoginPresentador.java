package com.tuclinicavirtual.mvo.views.activitis.mvp;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.views.activitis.interfaces.RestLogin;
import com.tuclinicavirtual.mvo.views.activitis.models.LoginRequest;
import com.tuclinicavirtual.mvo.views.activitis.models.LoginResponse;
import com.tuclinicavirtual.mvo.views.activitis.models.RegistroResponse;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresentador implements LoginContrato.Presenter{

    RestLogin restLogin;
    public LoginContrato.View view;
    private String TAG;

    public LoginPresentador(LoginContrato.View view){
        this.view=view;
        TAG=this.getClass().getSimpleName();
    }


    public void setLogin(String email, String password,String idonesignal,int rolid) {

        restLogin = RetrofitClient.getClient(Consts.BASE_URL).create(RestLogin.class);
        LoginRequest Registerequest = new LoginRequest(email,password,idonesignal,3);

        Gson gson2 = new GsonBuilder().serializeNulls().create();
        String json = gson2.toJson(Registerequest);

        Log.i(TAG,"---> Json Object to Login"+json);

        restLogin.getDataLogin(Registerequest).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setData(response.body().getData(),response.body().getStatus());
                        Log.i(TAG, "--->Token User: " + response.body().getData().toString());
                        Log.i(TAG, "--->Token User: " + response.body().getData().toString());
                    }
                }else{

                    Gson gson = new GsonBuilder().create();
                    RegistroResponse pojo = new RegistroResponse();
                    try {
                        pojo = gson.fromJson(response.errorBody().string(), RegistroResponse.class);
                        view.setData(pojo.getMessage());
                    } catch (IOException e) { }

                    Log.i(TAG, "--->Error en consulta registro: " + response.code());
                }  }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });

    }

}

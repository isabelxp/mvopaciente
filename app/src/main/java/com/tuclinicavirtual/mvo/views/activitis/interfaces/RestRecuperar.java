package com.tuclinicavirtual.mvo.views.activitis.interfaces;

import com.tuclinicavirtual.mvo.views.activitis.models.DataRecuperar;
import com.tuclinicavirtual.mvo.views.activitis.models.LoginRequest;
import com.tuclinicavirtual.mvo.views.activitis.models.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RestRecuperar {
    @Headers("Content-Type:application/json")
    @GET("users/password/{id}/3")
    Call<DataRecuperar> getRecuperar(@Path("id") String id);
}

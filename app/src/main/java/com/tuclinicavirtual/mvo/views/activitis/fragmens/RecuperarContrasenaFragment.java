package com.tuclinicavirtual.mvo.views.activitis.fragmens;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.views.activitis.mvp.RecuperarContrasenaContrato;
import com.tuclinicavirtual.mvo.views.activitis.mvp.RecuperarContrasenaPresentador;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecuperarContrasenaFragment extends Fragment implements RecuperarContrasenaContrato.View{


    public RecuperarContrasenaFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.email)
    EditText email;
    RecuperarContrasenaPresentador presentador;
    ProgressDialog progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_recuperar_contrasena, container, false);
        ButterKnife.bind(this, v);
        presentador=new RecuperarContrasenaPresentador(this);
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Cargando...");




        email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
                if(actionId == EditorInfo.IME_ACTION_DONE){

                    if(validarEmail(email.getText().toString()))
                    {
                        keybo();
                        progress.show();
                        presentador.getRecuperar(email.getText().toString());
                    }else
                    {
                        email.setError("Formato de correo inválido");
                        keybo();
                    }
                    return true;
                }
                return false;
            }
        });






        return v;
    }


    public void keybo()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);

        if(imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }




    @OnClick(R.id.email_sign_in_button)
    public void recuperar() {

        if(validarEmail(email.getText().toString()))
        {
            progress.show();
            presentador.getRecuperar(email.getText().toString());
        }else
        {
            email.setError("Formato de correo inválido");
        }
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    @Override
    public void setData(String sms) {

        if(sms.equals("ok"))
        {
            dialogo("Se ha enviado email con las instrucciones para recuperar la contraseña.",true);
        }else
        {
            dialogo(sms,false);
        }
        progress.cancel();
    }

    public void initFragments(Fragment fragment)
    {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }


    private void dialogo(String dialogo, final boolean resp)
    {

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
      //  alertDialog.setTitle("Notificación");
        alertDialog.setCancelable(false);
        alertDialog.setMessage(dialogo);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(resp)
                        {
                            initFragments(new LoginFragment());
                        }else
                        {
                            dialog.dismiss();
                        }
                    }
                });
        alertDialog.show();
    }

}

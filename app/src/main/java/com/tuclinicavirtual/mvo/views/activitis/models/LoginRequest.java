package com.tuclinicavirtual.mvo.views.activitis.models;

public class LoginRequest {

    String username;
    String password;
    String playerid;
    int roleid;

    public LoginRequest(String username, String password, String playerid,int roleid) {
        this.username = username;
        this.password = password;
        this.playerid = playerid;
        this.roleid = roleid;
    }
}

package com.tuclinicavirtual.mvo.views.activitis.mvp;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.views.activitis.interfaces.RestLogin;
import com.tuclinicavirtual.mvo.views.activitis.interfaces.RestRegistro;
import com.tuclinicavirtual.mvo.views.activitis.models.LoginRequest;
import com.tuclinicavirtual.mvo.views.activitis.models.LoginResponse;
import com.tuclinicavirtual.mvo.views.activitis.models.RegistroRequest;
import com.tuclinicavirtual.mvo.views.activitis.models.RegistroResponse;

import java.io.IOException;
import java.text.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroPresentador implements RegistroContrato.Presenter{

    RestRegistro restRegistro;
    public RegistroContrato.View view;
    private String TAG;

    public RegistroPresentador(RegistroContrato.View view){
        this.view=view;
        TAG=this.getClass().getSimpleName();
    }


    public void setRegistro(String email, String password) {

        restRegistro = RetrofitClient.getClient(Consts.BASE_URL).create(RestRegistro.class);
        final RegistroRequest registerequest = new RegistroRequest(email,password,"n");

        Gson gson2 = new GsonBuilder().serializeNulls().create();
        String json = gson2.toJson(registerequest);

        Log.i(TAG,"---> Json Object to Login"+json);

        restRegistro.setRegistro(registerequest).enqueue(new Callback<RegistroResponse>() {
            @Override
            public void onResponse(Call<RegistroResponse> call, Response<RegistroResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setData(response.body().getMessage());
                        Log.i(TAG, "--->Token User: " + response.body().getMessage());
                    }
                }else{
                    //view.setData(response.message());
                    Log.i(TAG, "--->Error en consulta registro: " + response.code());

                        Gson gson = new GsonBuilder().create();
                        RegistroResponse pojo = new RegistroResponse();
                        try {
                            pojo = gson.fromJson(response.errorBody().string(), RegistroResponse.class);
                            view.setData(pojo.getMessage());

                        } catch (IOException e) { }

                }  }
            @Override
            public void onFailure(Call<RegistroResponse> call, Throwable t) {
                view.setData(t.getMessage());
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });

    }



}

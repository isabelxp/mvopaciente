package com.tuclinicavirtual.mvo.views.activitis.fragmens;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;
import com.tuclinicavirtual.mvo.views.activitis.models.Data;
import com.tuclinicavirtual.mvo.views.activitis.mvp.LoginContrato;
import com.tuclinicavirtual.mvo.views.activitis.mvp.LoginPresentador;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.whalemare.sheetmenu.SheetMenu;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements LoginContrato.View{


    public LoginFragment() {
        // Required empty public constructor
    }


    LoginPresentador loginPresentador;
    ProgressDialog progress;

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.checkbox_login)
    AppCompatCheckBox checkbox_login;
    @BindView(R.id.image_error)
    ImageView image_error;
    @BindView(R.id.image_done)
    ImageView image_done;
    @BindView(R.id.image_done_pass)
    ImageView image_done_pass;
    @BindView(R.id.image_error_email)
    ImageView image_error_email;

    String idplayer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, v);

       // toolbar.setTitle("");
        progress = new ProgressDialog(getActivity());
        progress.setTitle("En proceso...");
        progress.setCancelable(false);
        loginPresentador = new LoginPresentador(this);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("hola", "Refreshed token: " + refreshedToken);


        email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
                if(actionId == EditorInfo.IME_ACTION_DONE){

                    if(validarEmail(email.getText().toString()))
                    {
                        progress.show();
                        if (validarLogin())
                        {
                            loginPresentador.setLogin(email.getText().toString(),password.getText().toString(),idplayer,3);
                        }else
                            progress.cancel();
                        keybo();

                    }else
                    {
                        email.setError("Formato no valido de correo");
                        keybo();
                    }
                    return true;
                }
                return false;
            }
        });

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
                if(actionId == EditorInfo.IME_ACTION_DONE){

                    if(validarEmail(email.getText().toString()))
                    {
                        progress.show();
                        if (validarLogin())
                        {

//                            OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
//                            idplayer=status.getSubscriptionStatus().getUserId();
                            Log.d("debug", "registrationId:" + idplayer);
                            loginPresentador.setLogin(email.getText().toString(),password.getText().toString(),idplayer,3);
                        }else
                            progress.cancel();
                        keybo();

                    }else
                    {
                        email.setError("Formato no valido de correo");
                        keybo();
                    }
                    return true;
                }
                return false;
            }
        });





        email.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(validarEmail(email.getText().toString()))
                {
                    image_error_email.setVisibility(View.GONE);
                    image_done.setVisibility(View.VISIBLE);
                }else
                {
                    image_error_email.setVisibility(View.VISIBLE);
                    image_done.setVisibility(View.GONE);
                }
                if(email.getText().length()==0)
                {
                    image_error_email.setVisibility(View.GONE);
                    image_done.setVisibility(View.GONE);
                }
            }
        });
        password.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(isValidPassword(password.getText().toString()))
                {
                    image_done_pass.setVisibility(View.VISIBLE);
                    image_error.setVisibility(View.GONE);

                }else
                {
                    image_error.setVisibility(View.VISIBLE);
                    image_done_pass.setVisibility(View.GONE);
                }
                if(password.getText().length()==0)
                {
                    image_error.setVisibility(View.GONE);
                    image_done_pass.setVisibility(View.GONE);
                }
            }
        });


        return v;
    }



    public void keybo()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);

        if(imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    @OnClick(R.id.email_sign_in_button)
    public void singIn() {
        progress.show();
        if (validarLogin())
        {
            loginPresentador.setLogin(email.getText().toString(),password.getText().toString(),FirebaseInstanceId.getInstance().getToken(),3);
        }else
        if (password.getText().toString().isEmpty() && email.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(),"Debe completar información", Toast.LENGTH_LONG).show();
        }
        if(email.getText().toString().isEmpty())
        {
            email.setError("Este campo es requerido");
        }
        if(password.getText().toString().isEmpty())
        {
           password.setError("Este campo es requerido");
        }
            //progress.cancel();
    }
    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[.@#$%^&+=!-])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    @OnClick(R.id.checkbox_login)
    public void checkbox() {
        if (checkbox_login.isChecked())
        {
            Log.d("checkbox","---> true");
            Preferences.setCheckRecordar(true);
        }else
        {
            Log.d("checkbox","---> false");
            Preferences.setCheckRecordar(false);
        }
    }


    @OnClick(R.id.text_registro)
    public void registro() {
       initFragments(new RegistroFragment());
    }

    @OnClick(R.id.text_recuperarContrasana)
    public void recordarContrasena() {
        initFragments(new RecuperarContrasenaFragment());
    }

    public void initFragments(Fragment fragment)
    {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void setData(Data data, String codigoResp) {
        progress.cancel();
        Log.d("setData","--->"+codigoResp);
        if (codigoResp.equals("ok"))
        {
            Preferences.setIDUser(Integer.parseInt(data.getUserid().toString()));
            Preferences.setTokenUser(data.getToken());
            Preferences.setFotoUsuario(data.getPhoto());
            Preferences.setEmailUser(data.getUsername());
            Preferences.setIDPatientid(data.getPatientid());
            Preferences.setNombre(data.getName()+" "+data.getLastname());

            startActivity(new Intent(getActivity(), HomeActivity.class));

            Preferences.setTokenUser(data.getToken());
            if(Preferences.getCheckRecordar())
            {
                Preferences.setUusuarioLogin(email.getText().toString());
                Preferences.setClaveLogin(password.getText().toString());
            }
        }
        //Toast.makeText(getActivity(),codigoResp, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void setData(String codigoResp) {
        progress.cancel();

        Toast.makeText(getActivity(),codigoResp, Toast.LENGTH_LONG).show();
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private boolean validarLogin()
    {
       if (!password.getText().toString().isEmpty() && !email.getText().toString().isEmpty())
       {

           if(!validarEmail(email.getText().toString()))
           {
               progress.cancel();
               email.setError("Formato no válido de correo");
               return false;
           }
           if(password.length()<2)
           {
               progress.cancel();
               password.setError("Contraseña > 4");
               return false;
           }
           if(!isValidPassword(password.getText().toString()))
           {
               progress.cancel();
               password.setError("Formato de contraseña inválido!");
               return false;
           }

           return true;
       }

            return false;
    }


}

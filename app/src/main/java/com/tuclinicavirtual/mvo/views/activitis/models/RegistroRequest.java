package com.tuclinicavirtual.mvo.views.activitis.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegistroRequest {
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("newsletter")
    @Expose
    private String newsletter;

    public String getUsername() {
        return username;
    }

    public RegistroRequest(String username, String password, String newsletter) {
        this.username = username;
        this.password = password;
        this.newsletter = newsletter;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(String newsletter) {
        this.newsletter = newsletter;
    }

}

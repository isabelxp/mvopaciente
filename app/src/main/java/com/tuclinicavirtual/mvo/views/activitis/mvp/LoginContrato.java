package com.tuclinicavirtual.mvo.views.activitis.mvp;

import com.tuclinicavirtual.mvo.views.activitis.models.Data;

public class LoginContrato {
    public interface View{
        void setData(Data data,String sms);
        void setData(String sms);
    }

    public interface Presenter{
        void setLogin(String email, String contrasena,String idonesignal, int rolid);
    }
}

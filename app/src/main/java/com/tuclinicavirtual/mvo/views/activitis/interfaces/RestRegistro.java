package com.tuclinicavirtual.mvo.views.activitis.interfaces;

import com.tuclinicavirtual.mvo.views.activitis.models.RegistroRequest;
import com.tuclinicavirtual.mvo.views.activitis.models.RegistroResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RestRegistro {
    @Headers("Content-Type:application/json")
    @POST("/users")
    Call<RegistroResponse> setRegistro(@Body RegistroRequest registroRequest);
}

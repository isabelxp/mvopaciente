package com.tuclinicavirtual.mvo.views.activitis.mvp;



public class RegistroContrato {

    public interface View{
        void setData(String sms);
    }

    public interface Presenter{
        void setRegistro(String email, String contrasena);
    }
}

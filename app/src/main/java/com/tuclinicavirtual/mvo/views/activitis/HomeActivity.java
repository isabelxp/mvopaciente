package com.tuclinicavirtual.mvo.views.activitis;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.information.AyudaEnLineaActivity;
import com.tuclinicavirtual.mvo.information.CondicionesDeUsoActivity;
import com.tuclinicavirtual.mvo.information.HelpOnLineFragment;
import com.tuclinicavirtual.mvo.menu.antecedentes.MisAntecedentesFragment;
import com.tuclinicavirtual.mvo.menu.datosPersonales.DatosPersonalesFragment;
import com.tuclinicavirtual.mvo.menu.datosPersonales.fragments.MisDatosPersonalesFragment;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.InformacionList;
import com.tuclinicavirtual.mvo.menu.datosPersonales.modelos.Patient;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesContrato;
import com.tuclinicavirtual.mvo.menu.datosPersonales.mvp.DatosPersonalesPresentador;
import com.tuclinicavirtual.mvo.menu.examenes.fragments.MisExamenesFragment;
import com.tuclinicavirtual.mvo.menu.familiares.fragments.MisFamiliaresFragment;
import com.tuclinicavirtual.mvo.menu.consultas.fragments.MisConsultasFragment;
import com.tuclinicavirtual.mvo.menu.notificaciones.NotificacionesFragment;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,DatosPersonalesContrato.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;



    boolean homeAct=false;
    boolean SolicitudConsulta=false;
    public static boolean DetalleConsulta=false;

    public static TextView nombre_paciente;
    public static TextView nombre_pais;
    public static CircleImageView img_profile;

    DatosPersonalesPresentador presenter;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation_list);
        ButterKnife.bind(this);
        presenter = new DatosPersonalesPresentador(this);
        initUI();
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            String token= extras.getString("token");
            String session = extras.getString("session");
            Log.d("holaSplash","---->"+token);
            Log.d("holaSplash","---->"+session);
        }


//            if (extras.containsKey("perfil")) {
//
//            }
            //   extras();
        }





    @OnClick(R.id.iv_notificaciones)
    public void notification() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, new NotificacionesFragment());
        ft.addToBackStack(null);
        ft.commit();
    }

    public static boolean isDetalleConsulta() {
        return DetalleConsulta;
    }

    public static void setDetalleConsulta(boolean detalleConsulta) {
        DetalleConsulta = detalleConsulta;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onBackPressed() {
       if(isDetalleConsulta())
       {

       }else
       {
           super.onBackPressed();
       }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = null;
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_consultas:
                fragment = new MisConsultasFragment();
                break;
            case R.id.nav_examenes:
                fragment = new MisExamenesFragment();
                break;
            case R.id.nav_antecedentes:
                fragment = new MisAntecedentesFragment();
                break;
            case R.id.nav_familiares:
                fragment = new MisFamiliaresFragment();
                break;
            case R.id.nav_cerrar_sesion:
                Preferences.setCheckRecordar(false);
                Preferences.cleanPreferences();
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                HomeActivity.super.finish();
                break;
            case R.id.nav_help:
                fragment = new HelpOnLineFragment();
                break;
              }
        if (fragment != null) {
            initFragments(fragment);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void mostrar_dialog_salir() {


        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(getString(R.string.tv_desea));
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        Preferences.setCheckRecordar(false);
                        Preferences.cleanPreferences();
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        HomeActivity.super.finish();
                    }
                });

        builder1.setNegativeButton(
                getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public void initFragments(Fragment fragment)
    {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();

       // presenter.getPaciente(Preferences.getIDPatientid());
        Log.d("Preferences","--->"+Preferences.getNombre());
        Log.d("Preferences","--->"+Preferences.getPais());
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d("Preferences","--->"+Preferences.getNombre());
        Log.d("Preferences","--->"+Preferences.getPais());
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);

    }



    public void initUI()
    {
        Log.d("initUiHome","----------->");
        initFragments(new MisConsultasFragment());
//
//        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_notifications));
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_notifications));
        toolbar.setTitle("");
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ft.replace(R.id.contenedor, new NotificacionesFragment());
//                ft.addToBackStack(null);
//                ft.commit();
//            }
//        });

        setSupportActionBar(toolbar);
        presenter.getPaciente(Preferences.getIDPatientid());
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        View headerview = navigationView.getHeaderView(0);

        RelativeLayout profilename = (RelativeLayout) headerview.findViewById(R.id.id_menu_datos);
        nombre_paciente = (TextView) headerview.findViewById(R.id.nombre_paciente);
        nombre_pais = (TextView) headerview.findViewById(R.id.nombre_pais);
        img_profile = (CircleImageView) headerview.findViewById(R.id.imageViewFoto2);

        if(Preferences.getNombre().toString()!=null)
        {
            String name=Preferences.getNombre();
            String nameAuxi=name.replace("'","");
//            nombre_paciente.setText(""+nameAuxi);
//            nombre_pais.setText(Preferences.getPais());
        }if (Preferences.getNombre().equals("no posee"))
        {
            Log.d("Preferences","---> 22"+Preferences.getPais());
            initFragments(new DatosPersonalesFragment());
        }
        else
        {
            presenter.getPaciente(Preferences.getIDPatientid());
        }

        profilename.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
                initFragments(new MisDatosPersonalesFragment());
            }
        });


    }

    @Override
    public void setDataPaises(List<InformacionList> dato) {

    }

    @Override
    public void setDataOcupaciones(List<InformacionList> dato) {

    }

    @Override
    public void setDataParentesco(List<InformacionList> dato) {

    }

    @Override
    public void setZonaHoraria(List<InformacionList> dato) {

    }

    @Override
    public void setRespDatosUsuario(String sms) {

    }

    @Override
    public void getDatosPacientes(Patient a) {
        Log.d("getDatosPacientes","---> 66"+a.toString());
        if (a.getLastname().equals(""))
        {
            Log.d("Preferences","---> 22"+Preferences.getPais());
            initFragments(new DatosPersonalesFragment());
        }else
        {
            if(a!=null)
            {
                String name=Preferences.getNombre();
                String nameAuxi=name.replace("'","");
                nombre_paciente.setText(a.getName()+" "+a.getLastname());
                nombre_pais.setText(a.getResCountryName());

            }

        }

    }

    @Override
    public void setRespPasswordNew(String resp, String Token) {

    }
}

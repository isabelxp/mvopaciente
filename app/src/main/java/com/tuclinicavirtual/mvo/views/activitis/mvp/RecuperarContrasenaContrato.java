package com.tuclinicavirtual.mvo.views.activitis.mvp;

import com.tuclinicavirtual.mvo.views.activitis.models.Data;

public class RecuperarContrasenaContrato {
    public interface View{
        void setData(String sms);
    }

    public interface Presenter{
        void setRecuperarContrasena(String email);
    }
}

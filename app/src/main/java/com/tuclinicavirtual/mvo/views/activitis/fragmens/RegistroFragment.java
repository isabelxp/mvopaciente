package com.tuclinicavirtual.mvo.views.activitis.fragmens;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.information.CondicionesDeUsoActivity;
import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.information.TerminosYCondicionesActivity;
import com.tuclinicavirtual.mvo.information.TermsAndConditionsFragment;
import com.tuclinicavirtual.mvo.information.TermsOfUseFragment;
import com.tuclinicavirtual.mvo.utils.Utils;
import com.tuclinicavirtual.mvo.views.activitis.mvp.RegistroContrato;
import com.tuclinicavirtual.mvo.views.activitis.mvp.RegistroPresentador;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RegistroFragment extends Fragment implements RegistroContrato.View{


    public RegistroFragment() { }

    RegistroPresentador registroPresentador;

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.password_repetir)
    EditText password_repetir;
    @BindView(R.id.registro_button)
    Button registro_button;
    ProgressDialog progress;
    @BindView(R.id.checkbox_uno)
    AppCompatCheckBox checkbox_uno;
    @BindView(R.id.checkbox_dos)
    AppCompatCheckBox checkbox_dos;
    @BindView(R.id.image_uno)
    ImageView image_uno;
    @BindView(R.id.image_dos)
    ImageView image_dos;
    @BindView(R.id.image_tres)
    ImageView image_tres;
    @BindView(R.id.text_terminos_condiciones)
    TextView text_terminos_condiciones;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_registro, container, false);
        ButterKnife.bind(this, v);

        registroPresentador=new RegistroPresentador(this);
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setTitle(getActivity().getString(R.string.registro_cargando));

        validadarImg();

        return v;
    }
    public void initFragments(Fragment fragment)
    {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }
    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[-@.#$%^&+=!-])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }
    @OnClick(R.id.text_terminos_condiciones)
    public void text_terminos_condiciones() {
        initFragments(new TermsAndConditionsFragment());
    }

    @OnClick(R.id.text_condiciones)
    public void text_condiciones() {
        initFragments(new TermsOfUseFragment());
    }



    @OnClick(R.id.registro_button)
    public void registro() {
        progress.show();
        if (validarRegistro())
        {
            registroPresentador.setRegistro(email.getText().toString(),password.getText().toString());
        }
    }

    @Override
    public void setData(String sms) {
        progress.cancel();
        if (sms.equals("ok"))
        {
            dialogo(getActivity().getString(R.string.registro_exitoso),true);

        }else
        {
            dialogo(sms,false);
        }

    }


    private void dialogo(String dialogo,final boolean cierto)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage(dialogo)
                .setTitle(getActivity().getString(R.string.registro_notificaciones));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(cierto)
                {
                    initFragment();
                }
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public void initFragment()
    {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor, new LoginFragment());
        ft.addToBackStack(null);
        ft.commit();
    }

    private void init()
    {
        email.setText("");
        password.setText("");
        password_repetir.setText("");
        checkbox_uno.setChecked(false);
        checkbox_dos.setChecked(false);
    }

    private void validadarImg()
    {


        email.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(validarEmail(email.getText().toString()))
                {
                    image_uno.setVisibility(View.VISIBLE);
                }else
                    image_uno.setVisibility(View.INVISIBLE);
            }
        });
        password.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(password.length()>5)
                {
                    if(isValidPassword(password.getText().toString()))
                    {
                        image_dos.setVisibility(View.VISIBLE);
                    }
                }else
                    image_dos.setVisibility(View.INVISIBLE);
            }
        });
        password_repetir.addTextChangedListener(new TextWatcher() {


            public void afterTextChanged(Editable s) {

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String cadena1=password.getText().toString();
                String cadena2=password_repetir.getText().toString();

                if(cadena1.equals(cadena2) && cadena1.length()>5)
                {
                    if(isValidPassword(password_repetir.getText().toString()))
                    {
                        image_tres.setVisibility(View.VISIBLE);
                    }

                }else
                    image_tres.setVisibility(View.INVISIBLE);
            }
        });
    }



    private boolean validarRegistro()
    {
        String cadena1=password.getText().toString();
        String cadena2=password_repetir.getText().toString();

        if(!validarEmail(email.getText().toString()))
        {
            progress.cancel();
            email.setError(getActivity().getString(R.string.registro_formato_no_valido));
            return false;
        }
        if(password.length()<4)
        {
            progress.cancel();
            password.setError(getActivity().getString(R.string.registro_contrasena_mayor));
            return false;
        }
        if(!cadena1.equals(cadena2))
        {
            progress.cancel();
            password_repetir.setError(getActivity().getString(R.string.registro_no_coinciden));
            return false;
        }
        if(!isValidPassword(cadena1))
        {
            progress.cancel();
            password_repetir.setError(getActivity().getString(R.string.registro_no_caracter_especial));
            return false;
        }
        if(!checkbox_uno.isChecked())
        {
            progress.cancel();
            dialogo(getActivity().getString(R.string.registro_terminos_condiciones),false);
            return false;
        }
        return true;
    }
}

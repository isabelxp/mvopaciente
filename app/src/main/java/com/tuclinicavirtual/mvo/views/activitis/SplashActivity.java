package com.tuclinicavirtual.mvo.views.activitis;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.call.ui.ChatActivity;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.views.activitis.models.Data;
import com.tuclinicavirtual.mvo.views.activitis.mvp.LoginContrato;
import com.tuclinicavirtual.mvo.views.activitis.mvp.LoginPresentador;

public class SplashActivity extends AppCompatActivity implements LoginContrato.View{
    LoginPresentador loginPresentador;
    String idplayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        loginPresentador = new LoginPresentador(this);
        Intent intent = getIntent();

        if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            String token= extras.getString("token");
            String session = extras.getString("session");
            Preferences.setToken_opentok(token);
            Preferences.setId_sesion(session);
        }

        final int interval = 2000; // 2 Second
        Handler handler = new Handler();
        Runnable runnable = new Runnable(){
            public void run() {
                if(Preferences.getCheckRecordar())
                {

                    sesionActiva();


                }if(Preferences.getCheckRecordar()==false)
                {
                    Log.d("getCheckRecordar","----");
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
        handler.postAtTime(runnable, System.currentTimeMillis()+interval);
        handler.postDelayed(runnable, interval);


    }

    @Override
    public void setData(Data data, String sms) {
        Preferences.setIDUser(Integer.parseInt(data.getUserid().toString()));
        Preferences.setTokenUser(data.getToken());
        Preferences.setEmailUser(data.getUsername());
        Preferences.setNombre(data.getName());
        Preferences.setFotoUsuario(data.getPhoto());
        Preferences.setIDPatientid(data.getPatientid());

        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        finish();
    }

    @Override
    public void setData(String sms) {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }
    private void sesionActiva()
    {
        if(Preferences.getCheckRecordar())
        {
//            OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
//                @Override
//                public void idsAvailable(String userId, String registrationId) {
//                    Log.d("debug", "User:" + userId);
//                    if (registrationId != null)
//                        idplayer=userId;
//                    Log.d("debug", "registrationId:" + registrationId);
//
//                }
//            });

            if(Preferences.getUsuarioLogin()!=""&&Preferences.getClaveLogin()!="")
            {
                loginPresentador.setLogin(Preferences.getUsuarioLogin(),Preferences.getClaveLogin(),FirebaseInstanceId.getInstance().getToken(),3);
            }else
            {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }

    }
}

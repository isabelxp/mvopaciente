package com.tuclinicavirtual.mvo.views;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;


import com.tuclinicavirtual.mvo.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HelpOnlineAdapter extends RecyclerView.Adapter<HelpOnlineAdapter.ViewHolder> {

    private List<String> title;
    private List<String> descrip;
    private List<Boolean> isShow;

    public HelpOnlineAdapter(List<String> title, List<String> descrip, List<Boolean> isShow) {
        this.title = title;
        this.descrip = descrip;
        this.isShow = isShow;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_onlinehelp, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        if (this.title.get(position).equals("AYUDA EN LINEA")) {
            viewHolder.iv_anim.setVisibility(View.GONE);
        }
        if (isShow.get(position)) {
            viewHolder.tv_descrip.setVisibility(View.VISIBLE);
            viewHolder.iv_anim.setImageResource(R.drawable.ic_remove_circle_outline_black_24dp);
        } else {
            viewHolder.tv_descrip.setVisibility(View.GONE);
            viewHolder.iv_anim.setImageResource(R.drawable.ic_control_point_black_24dp);
        }
        viewHolder.tv_title.setText(this.title.get(position));
        viewHolder.tv_descrip.setText(this.descrip.get(position));
        viewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isShow.get(position)) {
                    isShow.set(position, false);
                    viewHolder.iv_anim.setImageResource(R.drawable.ic_control_point_black_24dp);
                    viewHolder.tv_descrip.setVisibility(View.GONE);
//                    expandOrCollapse(viewHolder.tv_descrip, "gone");
                } else {
                    for (int i = 0; i < isShow.size(); i++) {
                        isShow.set(position, false);
                    }
                    isShow.set(position, true);
                    viewHolder.iv_anim.setImageResource(R.drawable.ic_remove_circle_outline_black_24dp);
                    viewHolder.tv_descrip.setVisibility(View.VISIBLE);
//                    expandOrCollapse(viewHolder.tv_descrip, "visible");
                }
            }
        });
    }

    public void expandOrCollapse(final View v, String exp_or_colpse) {
        TranslateAnimation anim = null;
        if (exp_or_colpse.equals("visible")) {
            Log.e("anim", "visible");
            anim = new TranslateAnimation(0.0f, 0.0f, -v.getHeight(), 0.0f);
            v.setVisibility(View.VISIBLE);
        } else {
            Log.e("anim", "gone");
            anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, -v.getHeight());
            Animation.AnimationListener collapselistener = new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    v.setVisibility(View.GONE);
                }
            };
            anim.setAnimationListener(collapselistener);
        }
        anim.setDuration(300);
        anim.setInterpolator(new AccelerateInterpolator(0.5f));
        v.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return this.descrip.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item)
        ConstraintLayout item;

        @BindView(R.id.iv_anim)
        ImageView iv_anim;

        @BindView(R.id.tv_title)
        TextView tv_title;
        @BindView(R.id.tv_descrip)
        TextView tv_descrip;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }
}
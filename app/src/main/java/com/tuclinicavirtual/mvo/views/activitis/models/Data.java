package com.tuclinicavirtual.mvo.views.activitis.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("patientid")
    @Expose
    private int patientid;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("identifictype")
    @Expose
    private String identifictype;
    @SerializedName("identificnumber")
    @Expose
    private String identificnumber;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("rolename")
    @Expose
    private String rolename;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("token")
    @Expose
    private String token;

    public Data(String userid, String username, String identifictype, String identificnumber, String name, String lastname, String sex, String rolename, String photo, String token) {
        this.userid = userid;
        this.username = username;
        this.identifictype = identifictype;
        this.identificnumber = identificnumber;
        this.name = name;
        this.lastname = lastname;
        this.sex = sex;
        this.rolename = rolename;
        this.photo = photo;
        this.token = token;
    }

    @Override
    public String toString() {
        return "DataPaciente{" +
                "userid='" + userid + '\'' +
                "patientid='" + patientid + '\'' +
                ", username='" + username + '\'' +
                ", identifictype='" + identifictype + '\'' +
                ", identificnumber='" + identificnumber + '\'' +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", sex='" + sex + '\'' +
                ", rolename='" + rolename + '\'' +
                ", photo='" + photo + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    public int getPatientid() {
        return patientid;
    }

    public void setPatientid(int patientid) {
        this.patientid = patientid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIdentifictype() {
        return identifictype;
    }

    public void setIdentifictype(String identifictype) {
        this.identifictype = identifictype;
    }

    public String getIdentificnumber() {
        return identificnumber;
    }

    public void setIdentificnumber(String identificnumber) {
        this.identificnumber = identificnumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

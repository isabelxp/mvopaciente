package com.tuclinicavirtual.mvo.views.activitis.interfaces;

import com.tuclinicavirtual.mvo.views.activitis.models.LoginRequest;
import com.tuclinicavirtual.mvo.views.activitis.models.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RestLogin {
    @Headers("Content-Type:application/json")
    @POST("users/login")
    Call<LoginResponse> getDataLogin(@Body LoginRequest loginRequest);
}

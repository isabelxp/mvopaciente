package com.tuclinicavirtual.mvo.views.activitis.mvp;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;
import com.tuclinicavirtual.mvo.utils.Consts;
import com.tuclinicavirtual.mvo.views.activitis.interfaces.RestLogin;
import com.tuclinicavirtual.mvo.views.activitis.interfaces.RestRecuperar;
import com.tuclinicavirtual.mvo.views.activitis.models.DataRecuperar;
import com.tuclinicavirtual.mvo.views.activitis.models.LoginRequest;
import com.tuclinicavirtual.mvo.views.activitis.models.LoginResponse;
import com.tuclinicavirtual.mvo.views.activitis.models.RegistroResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecuperarContrasenaPresentador {

    RestRecuperar restRecuperar;
    public RecuperarContrasenaContrato.View view;
    private String TAG;

    public RecuperarContrasenaPresentador(RecuperarContrasenaContrato.View view){
        this.view=view;
        TAG=this.getClass().getSimpleName();
    }


    public void getRecuperar(String email) {

        restRecuperar = RetrofitClient.getClient(Consts.BASE_URL).create(RestRecuperar.class);

        Log.d(TAG,"---> recuperar"+email);

        restRecuperar.getRecuperar(email).enqueue(new Callback<DataRecuperar>() {
            @Override
            public void onResponse(Call<DataRecuperar> call, Response<DataRecuperar> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.setData(response.body().getMessage());
                    }
                }else{

                    Gson gson = new GsonBuilder().create();
                    DataRecuperar pojo = new DataRecuperar();
                    try {
                        pojo = gson.fromJson(response.errorBody().string(), DataRecuperar.class);
                        view.setData(pojo.getMessage());
                    } catch (IOException e) { }

                    Log.d(TAG, "--->Error en recuperar: " + response.code());
                }  }
            @Override
            public void onFailure(Call<DataRecuperar> call, Throwable t) {
                Log.d(TAG,"--->failed :( "+t.getMessage());
            }
        });

    }
}

package com.tuclinicavirtual.mvo.views.activitis.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tuclinicavirtual.mvo.retrofit.RetrofitClient;

import java.io.IOException;
import java.text.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class RegistroResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @Override
    public String toString() {
        return "RegistroResponse{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public static RegistroResponse parseError(Response<?> response) {
        Converter<ResponseBody, RegistroResponse> converter =
                RetrofitClient.retrofit()
                        .responseBodyConverter(RegistroResponse.class, (java.lang.annotation.Annotation[]) new Annotation[0]);

        RegistroResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new RegistroResponse();
        }

        return error;
    }

}

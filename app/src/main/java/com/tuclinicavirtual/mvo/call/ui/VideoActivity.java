package com.tuclinicavirtual.mvo.call.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.call.config.OpenTokConfig;
import com.tuclinicavirtual.mvo.call.model.Session;
import com.tuclinicavirtual.mvo.call.oplogic.BaseOpenTok;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasProgramada;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.utils.Utils;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.ButterKnife;


public class VideoActivity extends BaseCallActivity implements BaseOpenTok.VideoCallListener {


    //Fragments and containers

    private RelativeLayout remoteStream;
    private RelativeLayout localStream;
    private TextView tv_name,tv_modalidad,tv_especialidad;
    private ImageView avatar,iv_imagen;


    public VideoActivity() {
        super(TypeCall.VIDEOCALL);
    }


    public static Intent intent(Context context, String chatName, Session session) {
        Intent intent = new Intent(context, VideoActivity.class);
        intent.putExtra("CHATNAME", chatName);
        intent.putExtra("session", session);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        getSupportActionBar().hide();
        ButterKnife.bind(this);

        tv_modalidad = (TextView) findViewById(R.id.tv_modalidad);
        tv_especialidad = (TextView) findViewById(R.id.tv_especialidad);
        tv_modalidad.setText("CONSULTA VIDEOLLAMADA");
        tv_especialidad.setText("Dr. "+AdapterConsultasProgramada.getDetalleConsultaProgramada().getSpecialityname());
        avatar = new ImageView(this);
        iv_imagen = (ImageView) findViewById(R.id.iv_imagen);
        iv_imagen.setImageResource(R.drawable.ic_videocam_white_24dp);


        Session session = getIntent().getParcelableExtra("session");
        baseOpenTok = new BaseOpenTok();
        baseOpenTok.init(this, this, new Session(session.getSessionId(), session.getToken()));

        mTextChatContainer = (FrameLayout) findViewById(R.id.textchat_fragment_container);
        localStream = (RelativeLayout) findViewById(R.id.rl_local_stream);
        remoteStream = (RelativeLayout) findViewById(R.id.rl_remote_stream);
        tv_name= (TextView) findViewById(R.id.tv_name);

        tv_name.setText(""+ AdapterConsultasProgramada.getDetalleConsultaProgramada().getDoctorName());

        actionBar = (new ActionBar());
        actionBar.onAttach(this);
        actionBar.init(findViewById(R.id.actionBar), baseOpenTok, true);

        initCronometro();
    }
    @Override
    public void onBackPressed() {
        mostrar_dialog_salir();
        //super.onBackPressed();
    }

    public void mostrar_dialog_salir() {


        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(getString(R.string.tv_desea));
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                        startActivity(new Intent(VideoActivity.this, HomeActivity.class));

                    }
                });

        builder1.setNegativeButton(
                getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onRemoteVideoChanged(boolean videoActive) {
        Log.i("tag", "--->////////onRemoteVideoChanged  " + videoActive);
        if (!videoActive) {
            remoteStream.removeView(avatar);
            avatar.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.avatar));
            avatar.setScaleType(ImageView.ScaleType.FIT_XY);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            avatar.setLayoutParams(params);

            remoteStream.addView(avatar, params);
        } else {
            remoteStream.removeView(avatar);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        actionBar.onDetach();
    }

    @Override
    public void addRemoteStream(View remoteStream) {
        this.remoteStream.addView(remoteStream);
        onRemoteVideoChanged(true);
    }

    @Override
    public void addLocalStream(View localStream) {
        this.localStream.addView(localStream);
    }

    @Override
    public void removeRemoteStream() {
        this.remoteStream.removeAllViews();
        onRemoteVideoChanged(false);
    }

    @Override
    public void removeLocalStream() {
        this.localStream.removeAllViews();
    }

    @Override
    public void qualityAlert() {

    }

}
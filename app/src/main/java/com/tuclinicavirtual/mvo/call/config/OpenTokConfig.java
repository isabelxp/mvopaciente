package com.tuclinicavirtual.mvo.call.config;

import com.tokbox.android.annotations.BuildConfig;

public class OpenTokConfig {

    // *** Fill the following variables using your own Project info from the OpenTok dashboard  ***
    // ***                      https://dashboard.tokbox.com/projects                           ***
    public static final String SESSION_ID = "1_MX40NjExOTM0Mn5-MTUyODIyOTUwMzE2OH5nQ2ZlWmFia09ENVF6MThiUk45VGExdWd-fg";
    // Replace with a generated token (from the dashboard or using an OpenTok server SDK)
    public static final String TOKEN = "T1==cGFydG5lcl9pZD00NjExOTM0MiZzaWc9NTAwYTIyZTJmMjAxMmM5OTQzZjdlOTcxNTA4NDA5ODgwMDI1ZTljMDpzZXNzaW9uX2lkPTFfTVg0ME5qRXhPVE0wTW41LU1UVXlPREl5T1RVd016RTJPSDVuUTJabFdtRmlhMDlFTlZGNk1UaGlVazQ1VkdFeGRXZC1mZyZjcmVhdGVfdGltZT0xNTI4MzgyOTQwJm5vbmNlPTAuMTEwNTc4NDMyMjc1OTI3NTYmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTUzMDk3NDkyNyZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==";
    // Replace with your OpenTok API key
    public static final String API_KEY = "45934192";


    // For internal use only. Please do not modify or remove this code.
    public static final String LOGGING_BASE_URL = "https://hlg.tokbox.com/prod/logging/ClientEvent";
    public static final String LOG_CLIENT_VERSION = BuildConfig.acceleratorVersion;
    public static final String LOG_COMPONENTID = "textChatAccPack";
    public static final String LOG_ACTION_INITIALIZE = "Init";
    public static final String LOG_ACTION_OPEN = "OpenTC";
    public static final String LOG_ACTION_CLOSE = "CloseTC";
    public static final String LOG_ACTION_SEND_MESSAGE = "SendMessage";
    public static final String LOG_ACTION_RECEIVE_MESSAGE = "ReceiveMessage";
    public static final String LOG_ACTION_SET_MAX_LENGTH = "SetMaxLength";

    public static final String LOG_VARIATION_ATTEMPT = "Attempt";
    public static final String LOG_VARIATION_ERROR = "Failure";
    public static final String LOG_VARIATION_SUCCESS = "Success";
}
package com.tuclinicavirtual.mvo.call.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.call.config.OpenTokConfig;
import com.tuclinicavirtual.mvo.call.model.Session;
import com.tuclinicavirtual.mvo.call.oplogic.BaseOpenTok;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasProgramada;
import com.tuclinicavirtual.mvo.views.activitis.HomeActivity;

import butterknife.ButterKnife;


public class VoiceActivity  extends BaseCallActivity implements BaseOpenTok.VoiceCallListener{

    public VoiceActivity() {
        super(TypeCall.VOICECALL);
    }

    public static Intent intent(Context context, String chatName, Session session) {
        Intent intent = new Intent(context, VoiceActivity.class);
        intent.putExtra("CHATNAME", chatName);
        intent.putExtra("session", session);
        return intent;
    }

    private TextView tv_name;
    private TextView tv_especialidad;
    private TextView tv_modalidad;
    private ImageView iv_imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice);
        getSupportActionBar().hide();
        ButterKnife.bind(this);

        Session session = getIntent().getParcelableExtra("session");
        baseOpenTok = new BaseOpenTok();
        if(session.getSessionId()!=null && session.getToken()!=null)
        {
            baseOpenTok.init(this, this, new Session(session.getSessionId(), session.getToken()));

        }
        tv_especialidad = (TextView) findViewById(R.id.tv_especialidad);
        tv_modalidad = (TextView) findViewById(R.id.tv_modalidad);
        tv_modalidad.setText("CONSULTA LLAMADA");
        tv_name= (TextView) findViewById(R.id.tv_name);
        iv_imagen = (ImageView) findViewById(R.id.iv_imagen);
        iv_imagen.setImageResource(R.drawable.ic_local_phone_black_24dp);
        tv_name.setText("Dr. "+AdapterConsultasProgramada.getDetalleConsultaProgramada().getDoctorName());
        tv_especialidad.setText(""+AdapterConsultasProgramada.getDetalleConsultaProgramada().getSpecialityname());
        mTextChatContainer = (FrameLayout) findViewById(R.id.textchat_fragment_container);
        actionBar = (new ActionBar());
        actionBar.onAttach(this);
        actionBar.init(findViewById(R.id.actionBar), baseOpenTok, false);
        initCronometro();
    }

    @Override
    public void onBackPressed() {
             if(!ActionBar.chat)
            {
              mostrar_dialog_salir();
            }else
            {
                super.onBackPressed();
                ActionBar.chat=false;
            }
    }

    public void mostrar_dialog_salir() {


        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(getString(R.string.tv_desea));
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                        startActivity(new Intent(VoiceActivity.this, HomeActivity.class));

                    }
                });

        builder1.setNegativeButton(
                getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}

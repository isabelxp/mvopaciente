package com.tuclinicavirtual.mvo.call.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.call.config.OpenTokConfig;
import com.tuclinicavirtual.mvo.call.oplogic.BaseOpenTok;
import com.tuclinicavirtual.mvo.call.otokchat.ChatMessage;
import com.tuclinicavirtual.mvo.call.otokchat.TextChatFragment;
import com.tuclinicavirtual.mvo.menu.consultas.adapters.AdapterConsultasProgramada;
import com.tuclinicavirtual.mvo.utils.Preferences;
import com.tuclinicavirtual.mvo.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;


/**
 * Created by Pedro Gomez on 08/06/2018.
 */

public class BaseCallActivity extends AppCompatActivity implements BaseOpenTok.CallStateListener, TextChatFragment.TextChatListener, ActionBar.PreviewControlCallbacks {


    protected BaseOpenTok baseOpenTok;
    CountDownTimer countDownTimer;

    private final String[] permissions = {Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int permsRequestCode = 200;
    //Permissions
    private boolean mAudioPermission = false;
    private boolean mVideoPermission = false;
    private boolean mWriteExternalStoragePermission = false;
    private boolean mReadExternalStoragePermission = false;
    protected FrameLayout mTextChatContainer;
    protected ActionBar actionBar;
    Long restante;
   // TextView text_chronometerView;
    protected TextChatFragment mTextChatFragment;
    protected TypeCall type;

    protected enum TypeCall {

        VIDEOCALL, VOICECALL, CHATCALL;
    }
    @BindView(R.id.text_chronometerView)
    TextView tvChronometer;

    public BaseCallActivity(TypeCall type) {
        this.type = type;
    }

    public BaseCallActivity() {
    }
    MaterialDialog materialDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (ContextCompat.checkSelfPermission(this, permissions[1]) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, permsRequestCode);
            }
        } else {
            mVideoPermission = true;
            mAudioPermission = true;
            mWriteExternalStoragePermission = true;
            mReadExternalStoragePermission = true;
        }
        materialDialog = new MaterialDialog.Builder(this)
                .content("Finalizando Cita...")
                .progress(true, 0).build();
    }

    @Override
    protected void onDestroy() {
        if (countDownTimer != null)
            countDownTimer.cancel();
        baseOpenTok.disconnect();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (baseOpenTok != null)
            baseOpenTok.onPause();
    }
    private void hideProgress() {
        materialDialog.dismiss();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (baseOpenTok != null)
            baseOpenTok.onResume();
    }



    @Override
    public void onRequestPermissionsResult(final int permsRequestCode, final String[] permissions,
                                           int[] grantResults) {
        switch (permsRequestCode) {
            case 200:
                mVideoPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                mAudioPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                mReadExternalStoragePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                mWriteExternalStoragePermission = grantResults[3] == PackageManager.PERMISSION_GRANTED;

                if (!mVideoPermission || !mAudioPermission || !mReadExternalStoragePermission || !mWriteExternalStoragePermission) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(BaseCallActivity.this);
                    builder.setTitle(getResources().getString(R.string.permissions_denied_title));
                    builder.setMessage(getResources().getString(R.string.alert_permissions_denied));
                    builder.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("RE-TRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(permissions, permsRequestCode);
                            }
                        }
                    });
                    builder.show();
                }
                break;
        }
    }

    public void warningNetworkQuality() {

    }
    private void countDownStart() {
        Log.d("countDownStart","----->0_"+AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationDate());
        Log.d("countDownStart","----->0_"+AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationHour());
        Log.d("countDownStart","----->0_"+Preferences.getUTC_usuario());

        if (countDownTimer == null) {

            Log.d("countDownStart","----->"+Preferences.getUTC_usuario());
            Date currentTime = Calendar.getInstance(TimeZone.getTimeZone(Preferences.getUTC_usuario())).getTime();
            Date dateConsultation = null;
            String s = parseDateToddMMyyyy(AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationDate()) + " " + AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationHour();
            Log.d("countDownStart","----->"+AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationDate());
            Log.d("countDownStart","----->"+AdapterConsultasProgramada.getDetalleConsultaProgramada().getConsultationHour());
            Log.d("countDownStart","----->"+s);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            try {
                dateConsultation = simpleDateFormat.parse(s);

            } catch (ParseException ex) {
                System.out.println("Exception " + ex);
            }

            if(AdapterConsultasProgramada.getDetalleConsultaProgramada().getSpecialityid()==26||AdapterConsultasProgramada.getDetalleConsultaProgramada().getSpecialityid()==25)
            {
                 restante = getDateDifference(currentTime, dateConsultation) + 3000000;
            }else
            {
                 restante = getDateDifference(currentTime, dateConsultation) + 1800000;
            }

                if (restante > 1 ) {
                    countDownTimer = new CountDownTimer(restante, 1000) {

                        public void onTick(long millisUntilFinished) {
                            if (!BaseCallActivity.this.isFinishing()) {

                                    long restoHora = millisUntilFinished % 3600000;
                                    long minutos = restoHora / 60000;
                                    long restoMinutos = restoHora % 60000;
                                    long segundos = restoMinutos / 1000;
                                    tvChronometer.setText(minutos + ":" + segundos);

                            }
                            //here you can have your logic to set text to edittext
                        }

                        public void onFinish() {
                            if (!BaseCallActivity.this.isFinishing()) {
                                 showProgress();
                                 finish();
                                // presenter.finalizarLlamada(AppMvo.cons.getConsultationid());
                            }
                            //  mTextField.setText("done!");
                        }

                    }.start();

            } else {
                if (!BaseCallActivity.this.isFinishing()) {
                    hideProgress();
                }

                    Toast.makeText(this, "Tiempo Consulta Finalizado", Toast.LENGTH_SHORT).show();
                   // Utils.setRate(this);
                    finish();
                }

        } else {
            countDownTimer.start();
        }
    }
    private void showProgress() {
        materialDialog.show();
    }
    private static String deleteChar(String text, int charPos) {
        return new StringBuilder(text).deleteCharAt(charPos).toString();

    }
    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (str != null) {
            if ('0' == str.charAt(3)) {
                str = deleteChar(str, 3);
            }
            if ('0' == str.charAt(0)) {
                str = deleteChar(str, 0);
            }
        }
        return str;
    }

    public Long getDateDifference(Date fechaInicial, Date fechaFinal) {
        return fechaFinal.getTime() - fechaInicial.getTime();
    }

    protected void initTextChatFragment() {
        String chatName;
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getStringExtra("CHATNAME") != null)
            chatName = getIntent().getStringExtra("CHATNAME");
        else
            chatName = "sin nombre";

        mTextChatFragment = TextChatFragment.newInstance(baseOpenTok.getWrapper().getSession(), OpenTokConfig.API_KEY);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.textchat_fragment_container, mTextChatFragment).commit();
        getSupportFragmentManager().executePendingTransactions();
        try {
            mTextChatFragment.setSenderAlias(chatName);
            mTextChatFragment.setMaxTextLength(140);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mTextChatFragment.setListener(this);

    }

    @Override
    public void onNewSentMessage(ChatMessage message) {

    }

    @Override
    public void onNewReceivedMessage(ChatMessage message) {

    }

    @Override
    public void onTextChatError(String error) {

    }

    @Override
    public void onClosed() {
        mTextChatContainer.setVisibility(View.GONE);
    }

    @Override
    public void onRestarted() {

    }

    @Override
    public void onBackPressed() {
        if (mTextChatContainer.getVisibility() == View.VISIBLE && type != TypeCall.CHATCALL) {
            mTextChatContainer.setVisibility(View.GONE);
            baseOpenTok.onResume();
        } else
            super.onBackPressed();
    }

    @Override
    public void onConnect() {
        Log.i("tag", "--->////////onConnect");
        initTextChatFragment();
    }

    @Override
    public void onConnected() {
        Log.i("tag", "--->////////onConnected");
        if (type != TypeCall.CHATCALL)
            actionBar.setEnabled(true);
        mTextChatFragment.init();
    }

    @Override
    public void onReconnecting() {
        Log.i("tag", "--->////////onReconnecting");

    }

    @Override
    public void onReconnected() {
        Log.i("tag", "--->////////onReconnected");
    }

    @Override
    public void onDisconnected() {
        Log.i("tag", "--->////////onDisconnected");
    }

    @Override
    public void onRemoteJoined() {
        Log.i("tag", "--->////////onRemoteJoined");
    }

    @Override
    public void onRemoteLeft() {
        Log.i("tag", "--->////////onRemoteLeft");
    }


    @Override
    public void onError() {
        Log.i("tag", "--->////////onError");
    }


    @Override
    public void onDisableLocalAudio(boolean audio) {
        baseOpenTok.enableLocalAudio(audio);
    }

    @Override
    public void onDisableLocalVideo(boolean video) {
        baseOpenTok.enableLocalVideo(video);
    }

    @Override
    public void onTextChat() {
        mTextChatContainer.setVisibility(View.VISIBLE);
        baseOpenTok.onPause();
    }
    public void initCronometro()
    {
        countDownStart();
    }
}

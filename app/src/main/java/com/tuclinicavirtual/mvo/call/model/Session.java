package com.tuclinicavirtual.mvo.call.model;


import android.os.Parcel;
import android.os.Parcelable;

public class Session  implements Parcelable {

        public String sessionId;
        public String token;

        public Session(String sessionId, String token) {
            this.sessionId = sessionId;
            this.token = token;
        }

        protected Session(Parcel in) {
            sessionId = in.readString();
            token = in.readString();
        }

        public static final Creator<Session> CREATOR = new Creator<Session>() {
            @Override
            public Session createFromParcel(Parcel in) {
                return new Session(in);
            }

            @Override
            public Session[] newArray(int size) {
                return new Session[size];
            }
        };

        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(sessionId);
            parcel.writeString(token);
        }
    }



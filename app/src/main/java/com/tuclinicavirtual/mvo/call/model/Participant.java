package com.tuclinicavirtual.mvo.call.model;

import com.tokbox.android.otsdkwrapper.utils.StreamStatus;

public class Participant {
    public enum Type {
        LOCAL,
        REMOTE
    }

    public Type mType;
    public String id = null;
    public StreamStatus mStatus;

    public Participant(Type type, StreamStatus status) {
        this.mType = type;
        this.mStatus = status;
    }

    public Participant(Type type, StreamStatus status, String id) {
        this.mType = type;
        this.mStatus = status;

        this.id = id;
    }


    public StreamStatus getStatus() {
        return mStatus;
    }

    public String getId() {
        return id;
    }

    public Type getType() {
        return mType;
    }

}

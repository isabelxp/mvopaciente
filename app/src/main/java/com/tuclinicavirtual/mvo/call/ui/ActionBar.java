package com.tuclinicavirtual.mvo.call.ui;

import android.app.Activity;
import android.content.Context;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;


import com.tokbox.android.otsdkwrapper.utils.MediaType;
import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.call.oplogic.BaseOpenTok;


public class ActionBar {

    private static final String LOGTAG = BaseOpenTok.class.getName();

    private View rootView;
    public static boolean chat=false;
    private ImageButton mAudioBtn;
    private ImageButton mVideoBtn;
    private ImageButton mTextChatBtn;
    private ImageButton mUnreadMessages;

    VectorDrawableCompat drawableBckBtn;
    BaseOpenTok otWrapper;

    private PreviewControlCallbacks mControlCallbacks = previewCallbacks;

    public interface PreviewControlCallbacks {

        public void onDisableLocalAudio(boolean audio);

        public void onDisableLocalVideo(boolean video);

        public void onTextChat();

    }

    private static PreviewControlCallbacks previewCallbacks = new PreviewControlCallbacks() {
        @Override
        public void onDisableLocalAudio(boolean audio) {

        }

        @Override
        public void onDisableLocalVideo(boolean video) {
        }


        @Override
        public void onTextChat() {
        }
    };

    private View.OnClickListener mBtnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.localAudio:
                    updateLocalAudio();
                    break;

                case R.id.localVideo:
                    updateLocalVideo();
                    break;

                case R.id.textchat:
                    Log.i(LOGTAG, "Floting button");
                    ActionBar.chat=true;
                    updateTextChat();
                    break;
            }
        }
    };

    public void onAttach(Context context) {
        Log.i(LOGTAG, "OnAttach ActionBarFragment");
        this.mControlCallbacks = (PreviewControlCallbacks) context;
    }


    public void onAttach(Activity activity) {
        this.mControlCallbacks = (PreviewControlCallbacks) activity;

    }

    public void onDetach() {

        mControlCallbacks = previewCallbacks;
    }

    public void init(View view, BaseOpenTok otWrapper, boolean isVideo) {
        rootView = view;
        this.otWrapper = otWrapper;
        mAudioBtn = (ImageButton) rootView.findViewById(R.id.localAudio);
        mVideoBtn = (ImageButton) rootView.findViewById(R.id.localVideo);
        mTextChatBtn = (ImageButton) rootView.findViewById(R.id.textchat);
        mUnreadMessages = (ImageButton) rootView.findViewById(R.id.unread_messages);

        if (!isVideo) {
            mVideoBtn.setVisibility(View.GONE);
        }
        drawableBckBtn = VectorDrawableCompat.create(view.getResources(), R.drawable.bckg_icon, null);

        mAudioBtn.setImageResource(otWrapper.getWrapper().isLocalMediaEnabled(MediaType.AUDIO)
                ? R.drawable.mic_icon
                : R.drawable.muted_mic_icon);
        mAudioBtn.setBackground(drawableBckBtn);

        mVideoBtn.setImageResource(otWrapper.getWrapper().isLocalMediaEnabled(MediaType.VIDEO)
                ? R.drawable.video_icon
                : R.drawable.no_video_icon);
        mVideoBtn.setBackground(drawableBckBtn);

        setEnabled(otWrapper.isCallInProgress());

    }


    public void updateLocalAudio() {
        if (!otWrapper.getWrapper().isLocalMediaEnabled(MediaType.AUDIO)) {
            mControlCallbacks.onDisableLocalAudio(true);
            mAudioBtn.setImageResource(R.drawable.mic_icon);
        } else {
            mControlCallbacks.onDisableLocalAudio(false);
            mAudioBtn.setImageResource(R.drawable.muted_mic_icon);
        }
    }

    public void updateLocalVideo() {
        if (!otWrapper.getWrapper().isLocalMediaEnabled(MediaType.VIDEO)) {
            mControlCallbacks.onDisableLocalVideo(true);
            mVideoBtn.setImageResource(R.drawable.video_icon);
        } else {
            mControlCallbacks.onDisableLocalVideo(false);
            mVideoBtn.setImageResource(R.drawable.no_video_icon);
        }
    }

    private void updateTextChat() {
        mControlCallbacks.onTextChat();
    }

    public void unreadMessages(final boolean unread) {
       /* rootView.getContext().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (unread) {
                    mUnreadMessages.setVisibility(View.VISIBLE);
                } else {
                    mUnreadMessages.setVisibility(View.GONE);
                }
            }
        });*/
    }

    public void setEnabled(boolean enabled) {
        if (mVideoBtn != null && mAudioBtn != null) {
            if (enabled) {
                mAudioBtn.setOnClickListener(mBtnClickListener);
                mVideoBtn.setOnClickListener(mBtnClickListener);
                mTextChatBtn.setOnClickListener(mBtnClickListener);
            } else {
                mAudioBtn.setOnClickListener(null);
                mVideoBtn.setOnClickListener(null);
                mAudioBtn.setImageResource(R.drawable.mic_icon);
                mVideoBtn.setImageResource(R.drawable.video_icon);
                mTextChatBtn.setOnClickListener(null);
            }
        }
    }

}
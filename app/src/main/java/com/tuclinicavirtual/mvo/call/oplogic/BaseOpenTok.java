package com.tuclinicavirtual.mvo.call.oplogic;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.opentok.android.OpentokError;
import com.tokbox.android.otsdkwrapper.listeners.AdvancedListener;
import com.tokbox.android.otsdkwrapper.listeners.BasicListener;
import com.tokbox.android.otsdkwrapper.listeners.ListenerException;
import com.tokbox.android.otsdkwrapper.listeners.PausableAdvancedListener;
import com.tokbox.android.otsdkwrapper.listeners.PausableBasicListener;
import com.tokbox.android.otsdkwrapper.utils.MediaType;
import com.tokbox.android.otsdkwrapper.utils.OTConfig;
import com.tokbox.android.otsdkwrapper.utils.PreviewConfig;
import com.tokbox.android.otsdkwrapper.wrapper.OTWrapper;

import com.tuclinicavirtual.mvo.call.config.OpenTokConfig;
import com.tuclinicavirtual.mvo.call.model.Participant;
import com.tuclinicavirtual.mvo.call.model.Session;
import com.tuclinicavirtual.mvo.call.ui.BaseCallActivity;
import com.tuclinicavirtual.mvo.utils.Utils;

public class BaseOpenTok {

    private final String LOG_TAG = BaseOpenTok.class.getSimpleName();

    //OpenTok calls
    private OTWrapper mWrapper;

    //Participants Grid management
    private Participant remoteParticipant;
    private Participant localParticipant;

    // interfaces
    VideoCallListener videoCallListener;
    VoiceCallListener voiceCallListener;
    ChatCallListener chatCallListener;

    //status
    private boolean isConnected = false;
    private boolean isCallInProgress = false;
    private boolean isReadyToCall = true;

    Context context;
    //  iniciar solo video
    public void init(Context context, VoiceCallListener voiceCallListener, Session session) {
        this.voiceCallListener = voiceCallListener;
        this.context=context;
        initOpenTok(context, session);
    }

    //  iniciar solo voz
    public void init(Context context, VideoCallListener videoCallListener, Session session) {
        this.videoCallListener = videoCallListener;
        this.context=context;
        initOpenTok(context, session);

    }

    //  iniciar solo chat
    public void init(Context context, ChatCallListener chatCallListener, Session session) {
        this.chatCallListener = chatCallListener;
        this.context=context;
        initOpenTok(context, session);

    }

    private void initOpenTok(Context context, Session session) {
        //init the sdk wrapper
        this.context=context;
        OTConfig config = new OTConfig.OTConfigBuilder(session.getSessionId(),session.getToken(),OpenTokConfig.API_KEY).name("accelerator-sample-app").subscribeAutomatically(true).subscribeToSelf(false).build();
        if(config!=null)
        {
            mWrapper = new OTWrapper(context, config);

            //set listener to receive the communication events, and add UI to these events
            mWrapper.addBasicListener(mBasicListener);
            mWrapper.addAdvancedListener(mAdvancedListener);
        }


        //use a custom video renderer for the annotations. It will be applied to the remote. It will be applied before to start subscribing

        if (voiceCallListener != null) {
            enableLocalVideo(false);
            enableLocalAudio(true);
            mWrapper.stopPreview();
        } else if (videoCallListener != null) {
            enableLocalVideo(true);
            enableLocalAudio(true);
        } else {
            enableLocalVideo(false);
            enableLocalAudio(false);
            mWrapper.stopPreview();
        }
        //connect
        if (mWrapper != null) {
            mWrapper.connect();
            if (getCallStateListener() != null)
                getCallStateListener().onConnect();
        }

    }


    public void onPause() {
        if (mWrapper != null) {
            mWrapper.pause();
        }
    }

    public void onResume() {
        if (mWrapper != null) {
            mWrapper.resume(true);
        }
    }

    public void disconnect() {
        mWrapper.disconnect();
    }

    public OTWrapper getWrapper() {
        return mWrapper;
    }

    public boolean isCallInProgress() {
        return isCallInProgress;
    }


    public void enableLocalVideo(boolean video) {
        Log.i(LOG_TAG, "--->Disable/Enable local video");
        if (mWrapper != null) {
            mWrapper.enableLocalMedia(MediaType.VIDEO, video);
        }
    }

    public void enableLocalAudio(boolean audio) {
        Log.i(LOG_TAG, "--->Disable/Enable local audio_icon");
        if (mWrapper != null) {
            mWrapper.enableLocalMedia(MediaType.AUDIO, audio);
        }
    }

    public void onCall() {
        Log.i(LOG_TAG, "--->OnCall");
        if (mWrapper != null && isConnected) {
            if (!isCallInProgress) {
                isCallInProgress = true;

                mWrapper.startPublishingMedia(new PreviewConfig.PreviewConfigBuilder().
                        name("Tokboxer").build(), false);
            } else {
                mWrapper.stopPublishingMedia(false);
                mWrapper.disconnect();
                isCallInProgress = false;
                cleanViewsAndControls();
            }
        } else {
            if (mWrapper != null) {
                isReadyToCall = true;
                mWrapper.connect();
                if (getCallStateListener() != null)
                    getCallStateListener().onConnect();
            }
        }
    }

    private CallStateListener getCallStateListener() {
        if (voiceCallListener != null)
            return voiceCallListener;
        else if (videoCallListener != null)
            return videoCallListener;
        else if (chatCallListener != null)
            return chatCallListener;
        return null;
    }


    //Basic Listener from OTWrapper
    private BasicListener mBasicListener =
            new PausableBasicListener(new BasicListener<OTWrapper>() {

                @Override
                public void onConnected(OTWrapper otWrapper, int participantsCount, String connId, String data) throws ListenerException {
                    Log.i(LOG_TAG, "--->Connected to the session. Number of participants: " + participantsCount);
                    if (mWrapper.getOwnConnId().equals(connId)) {
                        isConnected = true;
                        if (getCallStateListener() != null)
                            getCallStateListener().onConnected();

                        //TextchatFragment requires a session. In the current accelerator, the session is connected in the app and then,
                        // the accelerator is initialized.
                      /*  if (mTextChatFragment != null) {
                            mTextChatFragment.init();
                        }*/
                    }

                    if (isReadyToCall && chatCallListener == null) {
                        isReadyToCall = false;
                        onCall();

                    }
                }

                @Override
                public void onDisconnected(OTWrapper otWrapper, int participantsCount, String connId, String data) throws ListenerException {
                    Log.i(LOG_TAG, "--->Connection dropped: " + connId);
                  //  Utils.setRate(context);
                    if (connId.equals(mWrapper.getOwnConnId())) {
                        Log.i(LOG_TAG, "Disconnected to the session");
                        cleanViewsAndControls();
                        isConnected = false;
                        if (getCallStateListener() != null)
                            getCallStateListener().onDisconnected();
                    }
                }

                @Override
                public void onPreviewViewReady(OTWrapper otWrapper, View localView) throws ListenerException {
                    Log.i(LOG_TAG, "--->Local preview view is ready");
                    //audio/video call view
                    Participant participant = new Participant(Participant.Type.LOCAL, mWrapper.getLocalStreamStatus());
                    addNewParticipant(Participant.Type.LOCAL, participant);
                }

                @Override
                public void onPreviewViewDestroyed(OTWrapper otWrapper, View localView) throws ListenerException {
                    Log.i(LOG_TAG, "--->Local preview view is destroyed");
                    removeParticipant(Participant.Type.LOCAL);
                }

                @Override
                public void onRemoteViewReady(OTWrapper otWrapper, View remoteView, String remoteId, String data) throws ListenerException {
                    Log.i(LOG_TAG, "--->Participant remote view is ready");
                    Participant newParticipant = new Participant(Participant.Type.REMOTE, mWrapper.getRemoteStreamStatus(remoteId), remoteId);
                    addNewParticipant(Participant.Type.REMOTE, newParticipant);

                }

                @Override
                public void onRemoteViewDestroyed(OTWrapper otWrapper, View remoteView, String remoteId) throws ListenerException {
                    Log.i(LOG_TAG, "--->Remote view is destroyed");

                    removeParticipant(Participant.Type.REMOTE);

                }

                @Override
                public void onStartedPublishingMedia(OTWrapper otWrapper, boolean screensharing) throws ListenerException {
                    Log.i(LOG_TAG, "--->Local started streaming video.");

                }

                @Override
                public void onStoppedPublishingMedia(OTWrapper otWrapper, boolean isScreensharing) throws ListenerException {
                    Log.i(LOG_TAG, "--->Local stopped streaming video.");
                    isCallInProgress = false;
                }

                @Override
                public void onRemoteJoined(OTWrapper otWrapper, String remoteId) throws ListenerException {
                    if (getCallStateListener() != null)
                        getCallStateListener().onRemoteJoined();
                    Log.i(LOG_TAG, "--->A new remote joined.");
                }

                @Override
                public void onRemoteLeft(OTWrapper otWrapper, String remoteId) throws ListenerException {
                    if (getCallStateListener() != null)
                        getCallStateListener().onRemoteLeft();
                      // Utils.setRate(context);
                    Log.i(LOG_TAG, "--->A new remote left.");
                }

                @Override
                public void onRemoteVideoChanged(OTWrapper otWrapper, String remoteId, String reason, boolean videoActive, boolean subscribed) throws ListenerException {
                    Log.i(LOG_TAG, "--->Remote video changed");
                    if (isCallInProgress) {
                        if (reason.equals("quality")) {
                            if (videoCallListener != null) {
                                videoCallListener.qualityAlert();
                            }
                        } else if (reason.equals("publishVideo")) {
                            if (videoCallListener != null) {
                                videoCallListener.onRemoteVideoChanged(videoActive);
                            }
                        }
                    }
                }

                @Override
                public void onError(OTWrapper otWrapper, OpentokError error) throws ListenerException {
                    Log.i(LOG_TAG, "--->Error " + error.getErrorCode() + "-" + error.getMessage());
                    mWrapper.disconnect(); //end communication
                    cleanViewsAndControls(); //restart views
                }
            });

    //Advanced Listener from OTWrapper
    private AdvancedListener mAdvancedListener =
            new PausableAdvancedListener(new AdvancedListener<OTWrapper>() {

                @Override
                public void onCameraChanged(OTWrapper otWrapper) throws ListenerException {
                    Log.i(LOG_TAG, "--->The camera changed");
                }

                @Override
                public void onReconnecting(OTWrapper otWrapper) throws ListenerException {
                    if (getCallStateListener() != null)
                        getCallStateListener().onReconnecting();
                    Log.i(LOG_TAG, "--->The session is reconnecting.");

                }

                @Override
                public void onReconnected(OTWrapper otWrapper) throws ListenerException {
                    if (getCallStateListener() != null)
                        getCallStateListener().onReconnected();
                    Log.i(LOG_TAG, "--->The session reconnected.");

                }

                @Override
                public void onVideoQualityWarning(OTWrapper otWrapper, String remoteId) throws ListenerException {
                    Log.i(LOG_TAG, "--->The quality has degraded");

                }

                @Override
                public void onVideoQualityWarningLifted(OTWrapper otWrapper, String remoteId) throws ListenerException {
                    Log.i(LOG_TAG, "--->The quality has improved");
                }

                @Override
                public void onError(OTWrapper otWrapper, OpentokError error) throws ListenerException {
                    Log.i(LOG_TAG, "--->Error " + error.getErrorCode() + "-" + error.getMessage());
                    mWrapper.disconnect(); //end communication
                    cleanViewsAndControls(); //restart views
                    if (getCallStateListener() != null)
                        getCallStateListener().onError();
                }
            });


    private void cleanViewsAndControls() {
        remoteParticipant = null;
        localParticipant = null;
        isCallInProgress = false;
        isReadyToCall = false;
    }

    public BaseOpenTok() {
    }

    private void addNewParticipant(Participant.Type remote, Participant newParticipant) {
        if (remote.equals(Participant.Type.REMOTE)) {
            remoteParticipant = newParticipant;
            if (videoCallListener != null) {
                videoCallListener.addRemoteStream(newParticipant.getStatus().getView());
            }

        } else {
            localParticipant = newParticipant;
            if (videoCallListener != null) {
                videoCallListener.addLocalStream(newParticipant.getStatus().getView());
            }
        }
    }

    private void removeParticipant(Participant.Type type) {

        if (type.equals(Participant.Type.REMOTE)) {
            if (videoCallListener != null) {
                videoCallListener.removeRemoteStream();
            }
            remoteParticipant = null;
        } else {
            //local participant
            if (videoCallListener != null) {
                videoCallListener.removeLocalStream();
            }
            localParticipant = null;
        }
    }

    public interface CallStateListener {

        void onConnect();

        void onConnected();

        void onReconnecting();

        void onReconnected();

        void onDisconnected();

        void onRemoteJoined();

        void onRemoteLeft();

        void onError();
    }

    public interface VideoCallListener extends CallStateListener {
        void addRemoteStream(View remoteStream);

        void addLocalStream(View localStream);

        void removeRemoteStream();

        void removeLocalStream();

        void qualityAlert();

        void onRemoteVideoChanged(boolean videoActive);
    }

    public interface VoiceCallListener extends CallStateListener {

    }

    public interface ChatCallListener extends CallStateListener {

    }
}

package com.tuclinicavirtual.mvo.firebase;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tuclinicavirtual.mvo.R;
import com.tuclinicavirtual.mvo.utils.Preferences;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService{

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("hola", "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Preferences.setId_sesion(remoteMessage.getData().get("session"));
            Preferences.setToken_opentok(remoteMessage.getData().get("token"));
            EventBus.getDefault().post("noti");
            Log.d("onMessageReceived", "Message data payload: " + remoteMessage.getData().get("session"));
            Log.d("onMessageReceived", "Message data payload: " + remoteMessage.getData().get("token"));
            Log.d("onMessageReceived", "Message data payload: " + remoteMessage.getData());
        }
        String titulo = remoteMessage.getNotification().getTitle();
        String texto = remoteMessage.getNotification().getBody();
        showNotification(titulo,texto);
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("onMessageReceived", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }


        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Preferences.setId_sesion(remoteMessage.getData().get("session"));
            Preferences.setToken_opentok(remoteMessage.getData().get("token"));

//           Preferences.setcToken_opentok(data.optString("consultationid"));
            Log.d("onMessageReceived", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }

    private void showNotification(String title, String text) {

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher_mvo)
                        .setColor(getResources().getColor(R.color.accent))
                        .setContentTitle(title)
                        .setContentText(text);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
